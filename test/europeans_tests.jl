# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "EuropeanOption unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for U in (Stock, EuropeanOption, Indices{Stock}, Futures)
      @test EuropeanOption{U} <: Options{U} <: Options
    end
    for f in (
              :strike, :expiry, :option, :underlying, :inception,
              :prices, :times, :params
             )
      @test hasfield(EuropeanOption, f)
    end
    for R in (Float64, Float32, Float16, Int128, Int64, Int32, Rational)
      for Q in (Float64, Float32, DateTime, Date, Rational)
        @test hasmethod(EuropeanOption, Tuple{R,Q})
        @test hasmethod(payoff, Tuple{EuropeanOption,R,S})
      end
    end
    @test hasmethod(payoff,Tuple{EuropeanOption})
    @test hasmethod(show, Tuple{IO,MIME"text/plain",EuropeanOption})
  end
  
  @testset "Operability" begin
    strike  = 125.50
    expiryr = 0.25
    expiryd = Date(2024, 3, 31)
    incepto = Date(2024, 1, 1)
    underlg = Stock("SOEL")
    dummyop = EuropeanOption(0.0, expiryd)
    optionp = EuropeanOption(
                             strike, expiryd, option=Put,
                             underlying=underlg, inception=incepto
              )
    optionc = EuropeanOption(
                             strike, expiryd, option=Call,
                             underlying=underlg, inception=incepto
              )
    optionr = EuropeanOption(strike,expiryr,option=Put,underlying=underlg)
    
    @test isa(dummyop, EuropeanOption)
    @test isa(dummyop, EuropeanOption{DummyObs})
    @test isequal(getfield(optionp, :option), Put)
    @test isequal(getfield(optionc, :option), Call)
    @test isequal(getfield(optionr, :option), Call)
    @test isequal(getfield(dummyop, :option), Call)
    for op in (optionp, optionr, optionc)
      @test isa(op, EuropeanOption)
      @test isa(op, Options{Stock})
      @test isa(getfield(op, :strike), Float64)
      @test isequal(getfield(op, :strike), strike)
      @test isequal(getfield(op, :expiry), expiryd)
      @test isequal(getfield(op, :inception), incepto)
      @test isempty(op, :prices)
      @test isempty(op, :times)
      @test isempty(op)
      for (t,p) in zip(
                       (125.50, 100.87, 97.69, 101.35),
                       Tuple(Date(2024,2,j) for j in 1:4)
                   )
        @test 0 <= payoff(op, p, t) <= abs(p - op.strike)
        @test ifelse(
                     isequal(op.option,Call),
                     payoff(op,p,t) <= p,
                     payoff(op,p,t) <= op.strike
              )
        @test iszero(payoff(op, p, t))
      end
    end
    @test isa(underlg, Stock)
    uprices = [123.89,120.12,124.49,125.50,126.82]
    utimes  = collect(Date(2024,3,j) for j in 27:31)
    setprices!(underlg, uprices, utimes)
    for op in (optionp, optionr, optionc)
      if isequal(op.option, Call)
        @test isless(0, payoff(op))
        @test isequal(payoff(op), uprices[end] - op.strike)
      else
        @test iszero(payoff(op))
      end
      for t in utimes
        @test isequal(S(underlg)(t), S(op.underlying)(t)) 
      end
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError EuropeanOption(1,1,inception=today())
    @test_throws ArgumentError EuropeanOption(0.0,0.5,inception=1.0)
  end
end