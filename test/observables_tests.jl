# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "" begin
  @testset "Integrity and Consistency" begin
    @test isabstracttype(Observables)
    for U in (
              Stock, InterestRate, EuropeanOption, AmericanOption,
              BermudanOption, BinaryOption, AsianOption, Basket,
              ETF, Floor, Cap, Swap, Bond, FRA, Commodity, Forward,
              Futures, EconoIndicator, Rates, Marketables
             )
      @test U<:Observables
    end
    @test DummyObs <: Observables
    for T in (Date, DateTime, Rational, Float64, Int64)
      for R in (Float64,Float32,Float16,Rational,Int64,Int128,Int32)
        @test hasmethod(setprices!, Tuple{Observables,Vector{R},Vector{T}})
        @test hasmethod(resetprices!, Tuple{Observables,Vector{R},Vector{T}})
        @test hasmethod(setparams!, Tuple{Observables,String,R})
        @test hasmethod(setparams!, Tuple{Observables,String,Vector{R}})
        @test hasmethod(setparams!, Tuple{Observables,String,Set{R}})
      end
      @test hasmethod(settimes!, Tuple{Observables,Vector{T}})
      @test hasmethod(resettimes!, Tuple{Observables,Vector{T}})
    end
    @test hasmethod(settimes!, Tuple{Observables})
    @test hasmethod(resettimes!, Tuple{Observables})
    @test hasmethod(setprices!, Tuple{Observables})
    @test hasmethod(isempty, Tuple{Observables})
    @test hasmethod(S, Tuple{Observables})
  end
  
  @testset "Operability" begin
    xyz = InterestRate(Tenor("3m"))
    
    @test isa(xyz, Observables)
    @test isempty(xyz)
    settimes!(xyz, [1])
    @test ~isempty(xyz)
    resettimes!(xyz)
    @test isempty(xyz)
    setprices!(xyz, [0.0500,0.0505], [Date(2024,1,31),Date(2024,2,29)])
    @test isequal(S(xyz)(Date(2024,1,31)), 0.0500)
    @test isequal(S(xyz)(Date(2024,2,29)), 0.0505)
    v = [0.0808, "good", "morning", (1,"monad"), []]
    k = ["A","B","C","D","E"]
    for (tag, value) in zip(k, v)
      setparams!(xyz, tag, value)
      @test hasparams(xyz, tag)
      @test isequal(getparams(xyz, tag), value)
    end
  end
  
  @testset "Erroring" begin
    obs = InterestRate(Tenor("3m"))
    @test_throws ArgumentError S(obs)
  end
end