# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "BesselProcess unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test BesselProcess <: StochasticProcesses
    for f in (:kinda, :times, :brownian, :randz, :paths)
      @test hasfield(BesselProcess, f)
    end
    for A in (Int32, Int64, Int128)
      for B in (Int32, Int64, Int128)
        for C in (Int64, Int128, Float32, Float64, Rational)
          @test hasmethod(BesselProcess, Tuple{A,B,C})
        end
      end
    end
    @test hasmethod(show, Tuple{IO,MIME"text/plain",BesselProcess})
    for foo in (eltype, size, simulate, simulate!, resimulate, simulate!)
      @test hasmethod(foo, Tuple{BesselProcess})
    end
  end
  
  @testset "Operability" begin
    bps = Dict{Int64, BesselProcess}()
    for n in (10,100,1000)
      bps[n] = BesselProcess(/(n,10), n, 1.0, nsteps=n)
      @test isa(bps[n], BesselProcess)
      @test isa(eltype(bps[n]), Real)
      @test isequal(size(bps[n]), size(bps[n].paths))
      @test isequal(length(bps[n]), length(bps[n].times))
      @test iszero(bps[n].paths)
      simulate(bps[n])
      @test iszero(bps[n])
      simulate!(bps[n])
      @test iszero(bps[n].paths)
      @test isequal(size(bps[n]), (1+n,n)) 
    end
  end
  
  @testset "Erroring" begin
    @test_throws AssertionError BesselProcess(3,3,[-0.01,0.0,0.01,0.02])
  end
end