# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "BarrierOption unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test BarrierOption <: Options <: Derivatives <: Contracts
    @test hasmethod(getparams, Tuple{BarrierOption,String})
    @test hasmethod(setparams!, Tuple{BarrierOption,String,Any})
    for U in (Stock, Commodity, Swap, Cap, Floor, InterestRate, Indices)
      @test BarrierOption{U} <: Options
      @test BarrierOption{U} <: Options{U}
      @test BarrierOption{U} <: BarrierOption
    end
    for j in (
              :strike, :expiry, :barrier, :barriertype, :option,
              :underlying, :inception, :barrier2, :barrier2type,
              :exestyle, :prices, :times, :params
             )
      for U in (Stock, Commodity, Swap, Cap, Floor, IntestRate, Indices)
        @test hasfield(BarrierOption{U}, j)
      end
      @test hasfield(BarrierOption, j)
    end
    for R in (Float64, Float32, Float16, Rational, Int128, Int64, Int32)
      for Q in (Date, Float64, Float32, Rational, Int64, DateTime)
        @test hasmethod(BarrierOption, Tuple{R,Q,R})
      end
    end
  end
  
  @testset "Operability" begin
    strike  = Float64(60)
    barrier1= Float64(45)
    barrier2= Float64(50)
    expiry  = 0.75
    incept  = 0.25 
    undl    = Stock("PLTR")
    upricz1 = [57.3,51.2,53.7,45.1,39.1,47.3,54.7,58.3,60.1,62.3,59.9]
    upricz2 = [70.1,65.5,70.3,67.5,60.0,57.9,59.3,51.7,47.2,44.7,50.0]
    utimez  = [0.26,0.31,0.37,0.55,0.57,0.60,0.67,0.69,0.70,0.71,0.74]
    
    @test isa(incept, TimePoint)
    @test isa(expiry, TimePoint)
    @test isa(undl, Stock)
    @test isempty(getfield(undl, :prices))
    @test isempty(getfield(undl, :times))
    
    amdic = BarrierOption(
                          strike, expiry, barrier1,
                          barriertype=DownIn, inception=incept,
                          exestyle=AmericanStyle, underlying=undl
            ) # upricz1
    amdop = BarrierOption(
                          strike, expiry, barrier1, option=Put,
                          barriertype=DownOut, inception=incept,
                          exestyle=AmericanStyle, underlying=undl
            ) # upricz2
    euuic = BarrierOption(
                          strike, expiry, strike, barriertype=UpIn,
                          inception=incept, exestyle=EuropeanStyle,
                          underlying=undl
            ) # upricz1
    euuop = BarrierOption(
                          strike, expiry, 2barrier1, barriertype=UpOut,
                          inception=incept, exestyle=EuropeanStyle,
                          underlying=undl, option=Put
            ) # upricz2
    
    for op in (amdic, amdop, euuic, euuop)
      @test isa(op, BarrierOption{Stock})
      @test isa(op, BarrierOption)
      @test isa(getfield(op, :underlying), Stock)
      @test isequal(getfield(op, :underlying), undl)
      @test isequal(getfield(op, :strike), strike)
      @test in(getfield(op, :barrier), (barrier1, barrier2, 2barrier1))
    end
    for op in (amdic, euuic)
      @test isequal(getfield(op, :option), Call)
    end
    for op in(amdop, euuop)
      @test isequal(getfield(op, :option), Put)
    end
    # amdic
    @test isequal(getfield(amdic, :barriertype), DownIn)
    @test isequal(getfield(amdic, :option), Call)
    
    setprices!(undl, upricz1, utimez)
    
    for (t, v) in zip(utimez, upricz1)
      @test isequal(S(undl)(t), v)
      if t >= 0.57
        @test isequal(payoff(amdic, S(undl)(t), t), max(S(undl)(t) - strike, 0))
      else
        @test iszero(payoff(amdic, S(undl)(t), t))
      end
    end
    
    # amdop
    @test isequal(getfield(amdop, :barriertype), DownOut)
    @test isequal(getfield(amdop, :option), Put)
    
    setprices!(undl, upricz2, utimez)
    
    for (t, v) in zip(utimez, upricz2)
      @test isequal(S(undl)(t), v)
      if t >= 0.71
        @test iszero(payoff(amdop, S(undl)(t), t))
      else
        @test isequal(payoff(amdop, S(undl)(t), t), max(strike - S(undl)(t), 0))
      end
    end
    
    # euuic
    @test isequal(getfield(euuic, :barriertype), UpIn)
    @test isequal(getfield(euuic, :option), Call)
    @test isequal(getfield(euuic, :exestyle), EuropeanStyle)
    
    setprices!(undl, upricz1, utimez)
    
    for (t, v) in zip(utimez, upricz1)
      @test isequal(S(undl)(t), v)
      @test iszero(payoff(euuic, S(undl)(t), t))
    end
    for s in (60.10,62.00,60.05,100.00)
      @test ~iszero(payoff(euuic, s, getfield(euuic, :expiry)))
    end
    for s in (60.0, 59.99, 49.50, 45.85)
      @test iszero(payoff(euuic, s, getfield(euuic, :expiry)))
    end
    
    # euuop
    @test isequal(getfield(euuop, :barriertype), UpOut)
    @test isequal(getfield(euuop, :option), Put)
    @test isequal(getfield(euuop, :exestyle), EuropeanStyle)
    
    setprices!(undl, upricz2, utimez)
    
    for (t, v) in zip(utimez, upricz2)
      @test isequal(S(undl)(t), v)
      @test iszero(payoff(euuop, S(undl)(t), t))
    end
    for s in (60.10,62.00,60.05,100.0)
      @test iszero(payoff(euuop, s, getfield(euuop, :expiry)))
    end
    for s in (60.0, 59.99, 49.50, 45.85)
      @test isequal(payoff(euuop, s, euuop.expiry), max(strike - s, 0))
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError BarrierOption(1,1,10,inception=Date(2024,1,1))
  end
end