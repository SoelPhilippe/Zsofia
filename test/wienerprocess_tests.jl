# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "WienerProcess unit testing (Zsofia)" begin
   @testset "Integrity and Consistency" begin
     @test BrownianMotion <: StochasticProcesses <: AbstractModel
     @test WienerProcess <: StochasticProcesses <: AbstractModel
     for f in (:kinda, :times, :randz, :paths, :dymes)
       @test hasfield(BrownianMotion, f)
       @test hasfield(WienerProcess, f)
     end
     for R in (Int64, Int128, Float64, Float32, Rational)
       @test hasmethod(BrownianMotion, Tuple{Int64,Vector{R}})
       @test hasmethod(WienerProcess, Tuple{Int64,R})
     end
     @test hasmethod(show, Tuple{IO,::MIME"text/plain"}, BrownianMotion)
     @test hasmethod(show, Tuple{IO,::MIME"text/plain"}, WienerProcess)
     @test hasmethod(eltype, Tuple{BrownianMotion})
     @test hasmethod(eltype, Tuple{WienerProcess})
     @test hasemthod(size, Tuple{BrownianMotion})
     for A in (Int32, Int64, Int128)
       @test hasemthod(size, Tuple{BrownianMotion,A})
     end
     @test hasemthod(length, Tuple{BrownianMotion})
     @test hasemthod(simulate!, Tuple{BrownianMotion})
     @test hasemthod(simulate!, Tuple{WienerProcess})
     @test hasmethod(resimulate!, Tuple{BrownianMotion})
     @test hasmethod(resimulate!, Tuple{WienerProcess})
   end
   
   @testset "Operability" begin
     wps = Dict{Int64,WienerProcess}()
     for n in (10, 100, 1000)
       wps[n] = WienerProcess(n,0.1n,nsteps=10n,dimension=2n)
     end
     for n in keys(wps)
       @test isa(wps[n], WienerProcess)
       @test isa(wps[n], BrownianMotion)
       @test isa(wps[n], StochasticProcesses)
       @test isequal(wps[n].kinda, (10n+1, n, 2n))
       @test iszero(wps[n].times[begin])
       @test iszero(wps[n].randz)
       @test iszero(wps[n].paths)
       simulate!(wps[n])
       @test ~iszero(wps[n].randz)
       @test ~iszero(wps[n].paths)
       @test size(wps[n].paths) == (10n+1,n,1)
     end
   end
   
   @testset "Erroring" begin
     @test_throws ArgumentError WienerProcess(0,1.0)
     @test_throws ArgumentError WienerProcess(-1,1.0)
   end
 end