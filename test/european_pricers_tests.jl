# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "EuropeanOption pricers unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for m in (BlackScholes, Black76, MonteCarlo)
      @test isa(m, AbstractModel)
      for U in (Stock, Futures, Swaps, Rates, Commodities)
        for D in (Day, Month, Minute)
          if ~isa(m,MonteCarlo)
            @test hasmethod(evaluate,Tuple{MonteCarlo{m},EuropeanOption{U},D})
          end
        end
        @test hasmethod(evaluate,Tuple{m,EuropeanOption{U}})
        @test hasmethod(evaluate!,Tuple{m,EuropeanOption{U}})
      end
    end
    for M in (BlackScholes, Black76)
      for foo in (delta, gamma, vega, epsilon, rho, omega)
        @test isa(foo, Function)
        for U in (Stock,Futures,InterestRate,Swap,Commodity)
          @test hasmethod(foo,Tuple{M,EuropeanOption{U}})
        end
      end
    end
  end
  
  @testset "Operability" begin
    s0    = 79.89
    pltr  = Stock("PLTR")
    ccy   = Currency("USD")
    rfr   = InterestRate(0.041037, currency=ccy)
    bs    = BlackScholes(rfr)
    blk76 = Black76(rfr)
    mc76  = MonteCarlo(blk76)
    mcbs  = MonteCarlo(bs)
    dcc   = Thirty360()
    start = Date(2025,1,3)
    expiry= Date(2025,1,31)
    strike= 60.0
    σ     = 0.6327
    
    setparams!(pltr, "calendar", UnitedStatesEx(), force=true)
    setparams!(pltr, "sigma", σ, force=true)
    insertprices!(pltr, s0, start)
    
    @testset "BlackScholes" begin
      euput = EuropeanOption(
                             strike, expiry, option=Put,
                             underlying=pltr, inception=start
              )
      value = evaluate(bs, euput, sigma=getparams(pltr,"sigma"), dcc=dcc)
      @test isapprox(value, 0.2566547270622772, atol=1e-6)
      greeks = commongreeks(bs,euput,sigma=getparams(pltr,"sigma"),dcc=dcc)
      @test isapprox(greeks[:Δ],-0.0419144,atol=1e-6)
      @test isapprox(greeks[:Γ],0.00634929,atol=1e-6)
      @test isapprox(greeks[:V],1.99418,atol=1e-5)
      @test isapprox(greeks[:Ρ],-0.280404,atol=1e-6)
      @test isapprox(greeks[:Θ],-7.96308,atol=1e-5)
      @test isapprox(greeks[:Λ],-13.0469,atol=1e-4)
    end
    
    @testset "Black76" begin
      euput = EuropeanOption(
                             strike, expiry, option=Put,
                             underlying=pltr, inception=start
              )
      value = evaluate(blk76, euput, sigma=getparams(pltr,"sigma"), dcc=dcc)
      @test isapprox(value, 0.2566547270622772, atol=1e-6)
      greeks = commongreeks(bs,euput,sigma=getparams(pltr,"sigma"),dcc=dcc)
      @test isapprox(greeks[:Δ],-0.0419144,atol=1e-6)
      @test isapprox(greeks[:Γ],0.00634929,atol=1e-6)
      @test isapprox(greeks[:V],1.99418,atol=1e-5)
      @test isapprox(greeks[:Ρ],-0.280404,atol=1e-6)
      @test isapprox(greeks[:Θ],-7.96308,atol=1e-5)
      @test isapprox(greeks[:Λ],-13.0469,atol=1e-4)
    end
  end
  
  @testset "Erroring" begin
    nothing
  end
end