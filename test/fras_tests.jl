# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "FRA unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test FRA <: Derivatives{InterestRate} <: Derivatives <: Marketables
    for f in (
              :strike, :fixing, :maturity, :underlying,
              :notional, :inception, :ispayer, :dcc,
              :currency, :prices, :times, :params
             )
      @test hasfield(FRA, f)
    end
    for R in (Float64, Rational, Int64, Int128, Float32)
      for Q in (Date, DateTime, Float64, Int64, Rational)
        for U in (InterestRate,)
          @test hasmethod(FRA, Tuple{R,Q,Q,U})
        end
        @test hasmethod(payoff, Tuple{FRA,R,Q})
      end
    end
    @test hasmethod(payoff, Tuple{FRA})
    @test hasmethod(show, Tuple{IO,MIME"text/plain",FRA})
  end
  
  @testset "Operability" begin
    currency   = Currency("usd")
    interest_r = InterestRate(Tenor("6m"), currency=currency, isfixed=false)
    strike     = 0.03500
    fixing     = Date(2024, 10, 2)
    maturity   = Date(2025, 4, 2)
    inception  = Date(2024, 9, 4)
    ispayer    = true
    daycount   = Act360()
    notional   = 10_000_000
    uprices    = [    0.03587,         0.03489,         0.03699    ]
    utimes     = [Date(2024,9,15), Date(2024,10,2), Date(2024,2,12)]
    
    fra = FRA(
              strike, fixing, maturity, interest_r,
              ispayer=ispayer, notional=notional, inception=inception,
              dcc=daycount, currency=currency
          )
    
    @test isa(interest_r, InterestRate)
    @test isequal(getfield(fra, :strike), strike)
    @test isequal(getfield(fra, :fixing), fixing)
    @test isequal(getfield(fra, :maturity), maturity)
    @test isequal(getfield(fra, :underlying), interest_r)
    @test isequal(getfield(fra, :notional), notional)
    @test isequal(getfield(fra, :inception), inception)
    @test isequal(getfield(fra, :ispayer), ispayer)
    @test isequal(getfield(fra, :dcc), daycount)
    @test isequal(getfield(fra, :currency), currency)
    @test isempty(fra)
    @test isempty(fra, :prices)
    @test isempty(fra, :times)
    @test isempty(interest_r)
    @test isempty(intrest_r, :times)
    @test isempty(interest_r, :prices)
    
    setprices!(intrest_r, uprices, utimes)
    
    for (t, p) in zip(uprices, utimes)
      @test isequal(S(interest_r)(t), p)
      @test isequal(S(fra.underlying)(t), p)
    end
    @test isless(payoff(fra), 0)
    @test iszero(payoff(fra, uprices[1], utimes[1]))
    for t in getfield(interest_r, :times)
      @test payoff(fra, S(interest_r)(t), t) <= 0
    end
    @test isapprox(
                   payoff(fra, uprices[1], utimes[1]),
                   0,
                   atol=1e-6
          )
    @test isapprox(
                   payoff(fra, uprices[2], utimes[2]),
                   (uprices[2] - fra.strike) * (1/2) * notional,
                   atol=1e-6
          )
    @test isapprox(
                   payoff(fra),
                   payoff(fra, uprices[2], utimes[2]),
                   atol=1e-6
          )
  end
  
  @testset "Erroring" begin
    @test_throws MethodError FRA(0.0,0.0,Date(2024,1,1),InterestRate(0.05))
    @test_throws ArgumentError FRA(0,0.5,0.7,InterestRate(0),inception=1.0)
  end
end