# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "AmericanOption unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for U in (Stock, Commodity, Swap, Cap, Floor, NonMarketables, Indices)
      @test AmericanOption{U} <: Options
    end
    @test AmericanOption <: Contracts <: Observables
    @test hasmethod(getparams, Tuple{AmericanOption,String})
    @test hasmethod(setparams!, Tuple{AmericanOption,String,Any})
    for U in (Stock, Commodity, Swap, Cap, Floor, InterestRate, Indices)
      @test AmericanOption{U} <: Options
      @test AmericanOption{U} <: AmericanOption
    end
    for f in (
              :strike, :expiry, :option, :underlying, :inception,
              :prices, :times, :params
             )
      for U in (Stock, Commodity, Futures, Swap, EuropeanOption)
        @test hasfield(AmericanOption{U}, f)
      end
    end
    for R in (Float64, Float32, Float16, Rational, Int128, Int64, Int32)
      for Q in (Date, Float64, Float32, Rational, Int64, DateTime)
        @test hasmethod(AmericanOption, Tuple{R,Q})
        for U in (Stock, Commodity, Swap, Cap, Floor, NonMarketables, Indices)
          @test hasmethod(AmericanOption, Tuple{R,Q})
        end
      end
    end
  end
  
  @testset "Operability" begin
    strike = 125.50
    expiry = Date(2024, 10, 23)
    incept = Date(2024, 10, 10)
    stock1 = Stock("SOEL")
    opricz = [0.05, 0.07, 0.03]
    otimez = [Date(2024, 10, 14), Date(2024, 10, 17), Date(2024, 10, 21)]
    dummyo = AmericanOption(strike, expiry)
    op  = AmericanOption(strike, expiry, underlying=stock1, inception=incept)
    op_ = AmericanOption(
                         strike, expiry, option=Put, underlying=stock1,
                         inception=incept
          )
    for R in (Float64, Float32, Int64, Rational, Int128, Real)
      for U in (Stock, Commodity, Swap, Cap, Floor, NonMarketables, Indices)
        for D in (Date, DateTime, Float64, Rational, Int64)
          @test hasmethod(payoff, Tuple{AmericanOption{U},R,D})
        end
      end
    end
    @test isa(dummyo, AmericanOption)
    @test ~isa(dummyo, AmericanOption{Stock})
    @test isa(dummyo, AmericanOption{Zsofia.DummyObs})
    @test isa(op, AmericanOption)
    @test isa(op, AmericanOption{Stock})
    @test isa(op_, AmericanOption)
    @test isa(op_, AmericanOption{Stock})
    for o in (op, op_, dummyo)
      @test isempty(getfield(o, :prices))
      @test isempty(getfield(o, :times))
    end
    @test isequal(getfield(dummyo, :expiry), expiry)
    @test isequal(getfield(dummyo, :inception), firstdayofyear(expiry))
    for o in (op, op_)
      @test isequal(getfield(o, :expiry), expiry)
      @test isequal(getfield(o, :inception), incept)
      @test isequal(getfield(o, :strike), strike)
      @test isa(getfield(o, :underlying), Stock)
      @test isequal(getfield(o, :underlying), stock1)
      for k in ("ticker", "μ", "σ", "carryrate")
        @test haskey(getfield(o, :params), k)
      end
    end
    @test isequal(getfield(op, :option), Call)
    @test isequal(getfield(op_, :option), Put)
    @test isequal(getfield(dummyo, :strike), strike)
    
    for o in (op, op_, dummyo)
      setprices!(o, opricz, otimez)
      @test ~isempty(getfield(o, :prices))
      @test ~isempty(getfield(o, :times))
      for (t, p) in zip(otimez, opricz)
        @test isapprox(S(o)(t), p, atol=1e-7)
      end
      @test isapprox(S(o)(), opricz[end], atol=1e-7)
      resetprices!(o)
      @test isempty(getfield(o, :prices))
      @test isempty(getfield(o, :times))
    end
    
    setprices!(
               stock1,
               [100+1.25*j for j in 1:25],
               [Date(2024,10,j) for j in 1:25]
    )
    
    for o in (op, op_)
      @test first(getfield(stock1, :times)) < getfield(o, :inception)
      @test last(getfield(stock1, :times)) > getfield(o, :expiry)
      for t in (Date(2024, 10, j) for j in 1:20)
        @test iszero(payoff(op, S(stock1)(t), t))
        if t >= Date(2024,10,10)
          @test ~iszero(payoff(op_, S(stock1)(t), t))
        else
          @test iszero(payoff(op_, S(stock1)(t), t))
        end
      end
    end
  end
  
  @testset "Erroring" begin
    # inception and expiry should have same type!
    @test_throws TypeError AmericanOption(1.0,1.0,inception=Date(2024,11,8))
    # in payoff, remember the method Price comes before Time
    @test ~hasmethod(payoff, Tuple{AmericanOption, TimePoint, Float64})
  end
end