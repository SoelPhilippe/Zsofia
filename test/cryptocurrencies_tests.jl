# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "CryptoCurrency unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test CryptoCurrency <: Commodities <: Marketables <: Observables
    for f in (:quoteccy, :prices, :times, :params)
      @test hasfield(CryptoCurrency, f)
    end
    for T in (Currency, CryptoCurrency)
      @test hasmethod(CryptoCurrency, Tuple{AbstractString,T})
    end
    @test hasmethod(show, Tuple{IO,MIME"text/plain",CryptoCurrency})
  end
  
  @testset "Operability" begin
    qccy1 = Currency("USD")
    qccy2 = Currency("EUR")
    qccy3 = CryptoCurrency()
    cryp1 = CryptoCurrency("BTC", qcc1)
    cryp2 = CryptoCurrency("BTC", qcc2)
    cryp3 = CryptoCurrency("BTC", qcc3)
    prycz = [102_582, 106_785]
    tymcz = [Date(2024,12,12),Date(2024,12,20)]
    
    @test isa(qccy1, Currency)
    @test isa(qccy2, Currency)
    @test isa(qccy3, CryptoCurrency)

    for c in (cryp1, cryp2, cryp3)
      @test isempty(getfield(c, :prices))
      @test isempty(getfield(c, :times))
      @test iszero(getparams(c, "μ"))
      @test iszero(getparams(c, "σ"))
      @test isempty(getfield(c, :prices))
      @test isempty(getfield(c, :times))
      @test hasparams(c,"ticker")
      @test hasparams(c,"μ")
      @test hasparams(c,"σ")
    end
    
    setprices!(cryp1,prices,times)
    
    @test isempty(cryp1)
    for (t,p) in zip(tymcz,prycz)
      @test isapprox(S(cryp1)(t),p)
    end
    @test ~isempty(cryp1)
    resetprices!(cryp1)
  end
  
  @testset "Erroring" begin
    @test_throws MethodError CryptoCurrency(today())
  end
end