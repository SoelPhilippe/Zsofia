# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Compoundings unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test isabstracttype(CompoundFrequencies)
    for T in (Annually,SemiAnnually,Quarterly,Monthly,Weekly,Daily,Linearly)
      @test isa(T,CompoundFrequencies)
      @test hasmethod(T,Tuple{})
      @test isa(T,Function)
      @test Base.issingletontype(T)
      @test hasmethod(show,Tuple{IO,MIME"text/plain",T})
      for D in (ActAct,Thirty360,ThirtyE360,OneOne,Act365,Act360)
        @test hasmethod(toyearfraction,Tuple{D,T})
      end
      @test hasmethod(Zsofia._nwaar,Tuple{T})
    end
    @test isa(CompoundFrequency,CompoundFrequencies)
    @test hasmethod(Tuple{IO,MIME"text/plain",CompoundFrequency})
    @test isa(Zsofia._nwaar, Function)
  end
  
  @testset "Operability" begin
    for D in (ActAct,Thirty360,ThirtyE360,OneOne,Act365,Act360)
      @test isone(Zsofia._nwaar(Daily(),OneOne(),D()))
    end
    @test begin
      isequal(
              Zsofia._nwaar(Daily(),Thirty360(),true),
              Zsofia._nwaar(Daily(),Thirty360(),false)
      ) && isequal(Zsofia._nwaar(Daily(),Thirty360(),true),360)
    end
    @test begin
      isequal(
              Zsofia._nwaar(Daily(),ThirtyE360(),true),
              Zsofia._nwaar(Daily(),ThirtyE360(),false)
      ) && isequal(Zsofia._nwaar(Daily(),ThirtyE360(),true),360)
    end
    @test isequal(Zsofia._nwaar(Daily(),ActAct(),false),365)
    @test isequal(Zsofia._nwaar(Daily(),ActAct(),false),366)
    @test isequal(Zsofia._nwaar(Daily(),ActAct()),365)
    @test isequal(Zsofia._nwaar(Daily()),365)
    @test isequal(Zsofia._nwaar(Annually()),1)
    @test isequal(Zsofia._nwaar(SemiAnnually()),2)
    @test isequal(Zsofia._nwaar(Quarterly()),4)
    @test isequal(Zsofia._nwaar(Monthly()),12)
    @test isequal(Zsofia._nwaar(Weekly()),52)
    # ::Linearly returns linearly and the number of time it compounds is inf
    @test iseequal(Zsofia._nwaar(Linearly()),Linearly())
    # toyearfraction tests
    for dcc in (ActAct(),ThirtyE360(),Thirty360,Act365,Act360)
      for cmpf in (Annually, SemiAnnually, Quarterly,Monthly,Weekly,Daily)
        @test isequal(toyearfraction(dcc,cmpf),inv(Zsofia._nwaar(cmpf,dcc)))
      end
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError CompoundFrequency(Linearly())
  end
end