# This script is part of Zsofia. Soel Philippe © 2024

using Test, Zsofia

begin
  deps = (
          "americans_tests.jl",
          "asians_tests.jl",
          "barriers_tests.jl",
          "baskets_tests.jl",
          "bermudans_tests.jl",
          "besselprocess_tests.jl",
          "binaries_pricers_tests.jl",
          "binaries_tests.jl",
          "bonds_tests.jl",
          #"calendars_tests.jl",
          "cryptocurrencies_tests.jl",
          "currency_tests.jl",
          "daycountconventions_tests.jl",
          "dirac_tests.jl",
          "econoindicators_tests.jl",
          "etfs_tests.jl",
          "european_pricers_tests.jl",
          "europeans_tests.jl",
          "forwards_tests.jl",
          "fras_tests.jl",
          "futures_tests.jl",
          "instruments_types_tests.jl",
          "interestrates_tests.jl",
          "models_types_tests.jl",
          "montecarlo_tests.jl",
          "observables_tests.jl",
          "olennainen_tests.jl",
          "parisian_options_tests.jl",
          "stocks_tests.jl",
          "swaps_tests.jl",
          "tenors_tests.jl",
          "wienerprocess_tests.jl",
          "yieldcurves_tests.jl"
         )

  for jl_file in deps
      include(jl_file)
  end
end