# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Models Types unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test isabstracttype(AbstractModel)
    for j in (
              StochasticProcesses, EquityModels, RatesModels,
              VolatilityModels
             )
      @test j<:AbstractModel
    end
    @test ~(EquityModels <: StochasticProcesses)
    @test ~(RatesModels <: VolatilityModels)
  end
  
  @testset "Operability" begin
  
  end
  
  @testset "Erroring" begin
  
  end
end