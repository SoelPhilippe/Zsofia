# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "ParisianOption unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for U in (Stock, Commodity, Swap, Cap, Floor, NonMarketables, Indices)
      @test ParisianOption{U} <: Options
      @test ParisianOption{U} <: Options{U}
      for f in (
                :strike, :expiry, :barrier, :barriertype, :triggerclock,
                :option, :underlying, :inception, :barrier2, :barrier2type,
                :exestyle
               )
        @test hasfield(ParisianOption{U},f)
      end
      @test hasmethod(show, Tuple{IO,MIME"text/plain",ParisianOption{U})
      for R in (Real,Int128,Int64,Rational,Float64)
        @test hasmethod(payoff, Tuple{
                                      ParisianOption{U},R,TimePoint,
                                      AbstractVector{Real},
                                      AbstractVector{TimePoint},
                                      Real
                                }
              )
      end
    end
    for Q in (Int128,Int64,Int32,Float64,Float32,Rational)
      for W in (Date,DateTime,Real,Float64,Int128,Int32,Rational)
        for R in (Real,Rational,Int128,Int64,Int64,Float64)
          @test hasmethod(ParisianOption,Tuple{Q,W,R,W})
        end
      end
    end
  end
  
  @testset "Operability" begin
    strike = 15.50, 25.75
    expiry = Date(2025,3,21), 0.25
    incept = Date(2024,12,21), 0.00
    underl = Stock("ABC"), Stock("DEF")
    
  end
  
  @testset "Erroring" begin
    nothing
  end
end