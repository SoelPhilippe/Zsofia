# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "BermudanOption unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test BermudanOption <: Options <: Derivatives <: Contracts <: Observables
    @test hasmethod(getparams, Tuple{BermudanOption,String})
    @test hasmethod(setparams!, Tuple{BermudanOption,String,Any})
    @test hasmethod(settimes!, Tuple{BermudanOption,Vector{TimePoint}})
    @test hasmethod(resettimes!, Tuple{BermudanOption,Vector{TimePoint})
    for R in (Float64, Float32, Int128, Int64, Rational)
      for Q in (Date, DateTime, Float64, Float32)
        @test hasmethod(setprices!, Tuple{BermudanOption,Vector{R},Vector{Q}})
      end
    end
    for j in (
              :strike, :expiry, :exetimes, :option, :underlying,
              :inception, :prices, :times, :params
             )
      for U in (Stock, Swap, Futures, Forward, Options, Indices)
        @test hasfield(BermudanOption{U}, j)
      end
      @test hasfield(BermudanOption, j)
    end
    for R in (Float64, Float32, Int128, Int64, Rational)
      for Q in (Date, Float64, Float32, Rational, DateTime)
        @test hasmethod(BermudanOption, Tuple{R,Q,Vector{R}})
        @test hasmethod(BermudanOption, Tuple{R,Q,R})
        @test hasmethod(payoff, Tuple{BermudanOption,R,Q})
      end
    end
    @test hasmethod(Base.show, Tuple{IO,MIME,BermudanOption})
  end
  
  @testset "Operability" begin
    strike = 3//5
    expiry = 6//6
    incept = 0//6
    exetmz = [k//6 for k in (0,2,4,6)]
    aal    = Stock("AAL")
    upricz = [k//5 for k in (2,3,4,3)]
    utimez = [k//6 for k in (0,2,4,6)]
    
    dummyo = BermudanOption(strike, expiry, exetmz)
    bermuc = BermudanOption(
                            strike, expiry, exetmz,
                            option=Call, underlying=aal, inception=incept
             )
    bermup = BermudanOption(
                            strike, expiry, exetmz,
                            option=Put, underlying=aal, inception=incept
             )
    
    for op in (dummyo, bermuc, bermup)
      @test isequal(getfield(op, :strike), strike)
      @test isequal(getfield(op, :exetimes), exetmz)
      if op != dummyo
        @test isa(getfield(op, :underlying), Stock)
      end
      @test isequal(getfield(op, :underlying), aal)
      @test isempty(getfield(op, :prices))
      @test isempty(getfield(op, :times))
      for k in ("ticker", "μ", "σ", "carryrate")
        @test haskey(getfield(op, :params), k)
      end
      
      setprices!(op, upricz, utimez)
      
      @test ~isempty(getfield(op, :prices))
      @test ~isempty(getfield(op, :times))
      
      resetprices!(op)
      
      @test ~isempty(getfield(op, :prices))
      @test ~isempty(getfield(op, :times))
    end
    
    setprices!(aal, upricz, utimez)
    
    @test ~isempty(getfield(aal, :prices))
    @test ~isempty(getfield(aal, :times))
    
    for t in exetmz
      @test isequal(payoff(bermuc, S(aal)(t), t), max(S(aal)(t) - strike, 0))
      @test isequal(payoff(bermup, S(aal)(t), t), max(strike - S(aal)(t), 0))
    end
    
    for t in (2//1,3//2,4//3)
      for s in (7//8,9//41,41//9)
        @test iszero(payoff(bermuc, s, t))
        @test iszero(payoff(bermup, s, t))
      end
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError BermudanOption(1,0.5,[Date(2024,11,12),Date(2024,12,11)])
    @test_throws MethodError BermudanOption(1,Date(2024,11,12), [0.1,0.5])
  end
end