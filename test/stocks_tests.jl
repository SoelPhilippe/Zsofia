# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Stock unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test Stock <: Equity
    @test Equity <: Securities
    @test Securities <: Contracts
    @test Contracts <: Marketables
    @test Marketables <: Observables
    
    @test hasfield(Stock, :prices)
    @test hasfield(Stock, :times)
    @test hasfield(Stock, :params)
    
    @test hasmethod(Stock, Tuple{String})
    for R in (Float64, Float32, Float16, Rational, Int128, Int64, Int32)
      @test hasmethod(Stock, Tuple{String, Vector{R})
      @test hasmethod(Stock, Tuple{String, Vector{TimePoint}})
      @test hasmethod(Stock, Tuple{String, Vector{R}, Vector{TimePoint}})
      for S in (Float64, Float32, Float16, Rational, Int128, Int64, Int32, Date)
        @test hasmethod(Stock, Tuple{String, Vector{R}, Vector{S}})
      end
    end
    @test hasmethod(Base.show, Tuple{IO,MIME,Stock})
    @test hasmethod(S, Tuple{Stock})
    @test hasmethod(settimes!, Tuple{Stock, Vector{TimePoint}})
    @test hasmethod(resettimes!, Tuple{Stock, Vector{TimePoint}})
    @test hasmethod(resettimes, Tuple{Stock})
    @test hasmethod(setprices!, Tuple{Stock, Vector{Real}, Vector{TimePoint}})
    @test hasmethod(setprices!, Tuple{Stock})
    @test hasmethod(setparams!, Tuple{Stock,String,Any})
    @test hasmethod(getparams, Tuple{Stock,String})
    for R in (Float64, Float32, Float16, Rational, Int128, Int64, Int32, Real)
      @test hasmethod(insertprice!, Tuple{Stock, R, TimePoint})
    end
    @test hasmethod(isempty, Tuple{Stock})
    @test hasmethod(isempty, Tuple{Stock,Symbol})
  end
  
  @testset "Operability" begin
    pltr = Stock("PLTR")
    prcz = [15.0, 30.0, 45.0]
    tmzr = [0.25, 0.50, 0.75]
    tmzd = [Date(2024,11,19), Date(2025,2,19), Date(2025, 5, 19)]
    μ, σ = 0.037, 0.539
    
    @test isa(pltr, Stock)
    @test isa(prcz, Vector{Real})
    @test isa(tmzr, Vector{TimePoint})
    @test isa(tmzd, Vector{TimePoint})
    @test isequal(getparams["ticker"], "PLTR")
    
    setprices!(pltr, prcz, tmzr)
    
    @test isequal(pltr.prices, prcz)
    @test isequal(pltr.times, tmzr)
    for (p, t) in zip(prcz, tmzr)
      @test isequal(S(pltr)(t), p)
    end
    @test isequal(getparams(pltr, "mu"), zero(Float64))
    @test isequal(getparams(pltr, "sigma"), zero(Float64))
    
    setparams!(pltr, "μ", μ)
    setparams!(pltr, "σ", σ)
    
    @test isequal(getparams(pltr, "σ"), σ)
    @test isequal(getparams(pltr, "μ"), μ)
    
    resetprices!(pltr)
    
    setprices!(pltr, prcz, tmzd)
    
    @test isequal(pltr.prices, prcz)
    @test isequal(pltr.times, tmzd)
    for (p, t) in zip(prcz, tmzd)
      @test isequal(S(pltr)(t), p)
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError settimes!(pltr, [1.0, 7im])
    @test_throws MethodError settimes!(pltr, [Date(1111,11,11), 1.0])
  end
end