# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Basket unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test Basket <: Indices <: NonMarketables <: Observables
    for U in (Stock, Cap, Floor, Options, Indices{Stock}, Equity, Commodities)
      for N in (Int64, Int128, Int64, Float64, Float32)
        @test Basket{U,N} <: Indices{U}
        @test Basket{U,N} <: Indices
        for f in (:grupp, :prices, :times, :params)
          @test hasfield(Basket{U,N}, f)
        end
      end
    end
    for U in (Stock, Indices{Stock}, Equity, Debt, Commodities, InterestRate)
      for N in (Int64, Int128, Int64, Float32, Float64)
        @test hasmethod(Basket{U,N}, Tuple{Vector{U},Vector{N}})
        @test hasmethod(Basket{N}, Tuple{Vector{U},Vector{N}})
        @test hasmethod(Basket{U}, Tuple{Vector{U}, Vector{N}})
        @test hasmethod(Basket, Tuple{Dict{U,N}})
        @test hasmethod(Basket, Tuple{Vector{U}})
        @test hasmethod(show, Tuple{IO,MIME,Basket{U,N}})
        @test hasmethod(show, Tuple{IO,MIME"text/plain",Basket{U,N}})
        @test hasmethod(show, Tuple{IO,MIME"text/plain",Portfolio{U}})
        @test hasmethod(length, Tuple{Basket{U,N}})
        @test hasmethod(size, Tuple{Basket{U,N}})
        @test hasmethod(isempty, Tuple{Basket{U,N}})
        @test hasmethod(in, Tuple{U,Basket{U,N}})
        @test hasmethod(push!, Tuple{Basket{U,N},U,N})
        @test hasmethod(put!, Tuple{Basket{U,N},Vector{U},Vector{N}})
        @test Basket{U,Int64} <: Portfolio
        @test Basket{U,Int64} <: Portfolio{U}
      end
    end
  end
  
  @testset "Operability" begin
    portf = Basket{Stock, Int64}()
    aapl  = Stock("AAPL")
    pltr  = Stock("PLTR")
    msft  = Stock("MSFT")
    
    @test ~isa(Basket([Stock(),Stock()],[0.1,0.2]), Portfolio)
    @test isa(Basket([Stock(),Stock()],[1,2]), Portfolio)
    @test isa(aapl, Stock)
    @test isa(pltr, Stock)
    @test isa(portf, Portfolio)
    @test isa(portf, Basket)
    @test isa(portf, Portfolio)
    @test isempty(portf)
    push!(portf, aapl)
    @test ~isempty(portf)
    @test isone(length(portf))
    @test isone(size(portf))
    @test isone(count(portf))
    push!(portf, pltr)
    @test >(size(portf), 1)
    @test ==(size(portf), 2)
    @test ==(count(portf), 2)
    for s in (aapl, pltr)
      @test in(s, portf)
    end
    insertprice!(aapl, 225.25, 0.0)
    insertprice!(pltr, 17.85, 0.0)
    insertprice!(msft, 471.79, 0.0)
    push!(portf, aapl, 1250)
    push!(portf, pltr, 1789)
    for c in get(portf, :components)
      @test isa(c, Observables)
      @test isa(c, Stock)
    end
    @test isequal(get(portf, :value), 225.25*1250 + 1789*17.85)
    @test isequal(count(>=(1200), portf), 2)
    @test isequal(count(>=(1200), portf, :units), 2)
    @test isequal(count(portf), 2)
    @test isequal(Set(get(portf, :components)), Set([aapl, pltr]))
    empty!(portf)
    @test isempty(portf)
    @test iszero(count(portf))
    @test iszero(size(portf))
    @test iszero(length(portf))
    put!(portf, msft, -120)
    @test isone(size(portf))
    put!(portf, aapl, 258)
    @test ==(size(portf),2)
    put!(portf, pltr, 80)
    @test all(in(portf), [msft,aapl,pltr])
    deleteat!(portf, 3)
    @test ~in(pltr, portf)
    put!(portf, pltr)
    @test in(pltr, portf)
    deleteat!(portf, 2)
    @test ~in(aapl, portf)
    put!(portf, aapl)
    keepat!(portf, 2)
    @test ~in(msft, portf)
    @test ~in(aapl, portf)
    @test in(pltr, portf)
    put!(portf, msft, -120)
    put!(portf, pltr, 80)
    put!(portf, aapl, 258)
    @test isequal(get(portf, :value),-120*471.79 + 80*17.85 + 258*225.25)
    insertprice!(aapl, 227.89, 1.0)
    insertprice!(msft, 449.87, 1.0)
    insertprice!(pltr, 19.47, 1.0)
    @test isapprox(
                   get(portf, :value), 
                   -120*449.87 + 258*227.89 + 80*19.47,
                   atol=1e-5
          )
    a=popfirst!(portf)[1]
    @test isequal(a, pltr)
    @test in(aapl, portf)
    pop!(portf)
    @test in(msft, portf)
    @test isequal(get(portf, :value), -120*S(msft)())
  end
  
  @testset "Erroring" begin
    aapl, pltr, msft = Stock("AAPL"), Stock("PLTR"), Stock("MSFT")
    insertprice!(aapl, 225.25, 0.0)
    insertprice!(pltr, 17.85, 0.0)
    portf = Basket([aapl, pltr], [1250,1789])
    
    @test_throws ArgumentError get(portf, :component)
    @test_throws ArgumentError get(portf, :values)
    @test_throws ArgumentError get(portf, :monads)
    @test_throws DomainError get(portf, msft)
    @test_throws UndefVarError get(portf, aapl)
  end
end