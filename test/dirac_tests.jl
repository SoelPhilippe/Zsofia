# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Dirac unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test isa(dirac, Function)
    for A in (Any, Vector, Float64, Real, Complex, Rational, Date)
      @test hasmethod(dirac, Tuple{A,Set})
      @test hasmethod(dirac, Tuple{A,Vector{A}})
    end
    for R in (Float64, Int64, Rational, Complex, Date, DateTime, String, Char)
      for S in (Float64, Rational, Complex, String)
        @test hasmethod(dirac, Tuple{R,Vector{S}})
      end
      if isa(S, Real)
        @test hasmethod(dirac, Tuple{R,S,S})
        @test hasmethod(dirac, Tuple{R,R,S})
      end
    end
  end
  
  @testset "Operability" begin
    set_int = Set{Integer}([-1,8,-3,741,7_777_777])
    set_str = Set{String}(["good", "percocet", "money", "finance", "a"])
    set_any = Set([today(), now(), "quant", 777, 5//9])
    vec_flt = Vector{Float64}([0.0, 3.14, 3.1415, log(2), exp(π)])
    
    @test isa(set_int, Set{Int64}) && isa(set_int, Set{Integer})
    @test isa(set_str, Set{String})
    @test isa(set_flt, Set{Float64})
    for s in (set_int, set_str, set_any)
      r = rand(s)
      @test isa(s, Set)
      @test isequal(length(s), 5)
      @test in(r, s) && Bool(dirac(r,s))
    end
    
    foo(x) = dirac(x, set_int) + dirac(x, set_str) + dirac(x, set_any)
    
    for elmt in union(set_int, set_str, set_any)
      @test 0 <= foo(elmt) <= 3
      @test 0 <= foo(elmt) <= 2
      @test 0 <= foo(elmt) <= 1
      for i in 1:length(union(set_int, set_str, set_any))
        @test foo(elmt) >= (1 - /(1,i)) 
      end
    end
    for elmt in (1 => "1", (π, π^π), (π,))
      @test iszero(foo(elmt))
    end
    for s in (set_int, set_str, set_any, vec_flt)
      for elmt in s
        @test isone(dirac(elmt, s))
      end
    end
  end
  
  @testset "Erroring" begin
    @test MethodError dirac(today(), 1, 2)
    @test MethodError dirac(1, today(), π)
  end
end