# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Binary Options pricers unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for m in (BlackScholes, MonteCarlo)
      @test ise(m, AbstractModel)
      for U in (Stock, Futures, Forward, Swaption, FRA, Cap, Floor)
        for D in (Day, Month, Minute)
          if ~isa(m,MonteCarlo)
            @test hasmethod(evaluate,Tuple{MonteCarlo{m},BinaryOption{U},D})
          end
        end
        @test hasmethod(evaluate,Tuple{m,BinaryOption{U}})
        @test hasmethod(evaluate!,Tuple{m,BinaryOption{U}})
      end
    end
    for M in (BlackScholes,)
      for foo in (delta,gamma,vega,epsilon,rho,omega)
        @test isa(foo,Function)
        for U in (Stock,Futures,InterestRate,Cap,Floor,Forward,Commodity)
          @test hasmethod(foo,Tuple{M,BinaryOption})
        end
      end
    end
  end

  @testset "Operability" begin
    s0    = 79.89
    pltr  = Stock("PLTR")
    ccy   = Currency("USD")
    rfr   = InterestRate(0.041037, currency=ccy)
    bs    = BlackScholes(rfr)
    mcbs  = MonteCarlo(bs)
    dcc   = Thirty360()
    start = Date(2025,1,3)
    expiry= Date(2025,1,31)
    strike= 60.0
    σ     = 0.6327

    setparams!(pltr, "calendar", UnitedStatesEx(), force=true)
    setparams!(pltr, "sigma", σ, force=true)
    insertprices!(pltr, s0, start)

    @testset "BlackScholes" begin
      bp = BinaryOption(strike,expiry,option=Put,underlying=pltr,inception=start)
      bc = BinaryOption(strike,expiry,option=Call,underlying=pltr,inception=start)
      vs = evaluate(bs,bp,dcc=dcc), evaluate(bs,bc,dcc=dcc)
      df = discount(rfr, start, expiry)
      @test isapprox(sum(vs), df, tol=1e-9)
      gr = commongreeks(bs,bp,dcc=dcc)
    end

    @testset "Black76" begin
      nothing
    end
  end

  @testset "Erroring" begin
    nothing
  end
end