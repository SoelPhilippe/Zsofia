# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "olennainen unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for P in (Float64,Float32,Float16,Rational,Int64,Int128,Int32)
      for Q in (Float64,Float32,Float16,Rational,Int64,Int128,Int32)
        @test hasmethod(pnl, Tuple{P,Q})
        @test hasmethod(pnl, Tuple{Vector{P},Vector{Q}})
        @test hasmethod(pnl, Tuple{P,Vector{Q}})
        @test hasmethod(pnl, Tuple{Vector{P}})
        @test hasmethod(indcz, Tuple{P,Q})
        @test hasmethod(indcz, Tuple{Vector{P},Vector{Q}})
        @test hasmethod(gap, Tuple{Vector{P},Vector{Q}})
        for R in (Float64,Float32,Float16,Rational,Int64,Int128,Int32)
          @test hasmethod(gauge, Tuple{P,Q,R})
          @test hasmethod(gauge, Tuple{Vector{P},Vector{Q},Vector{R}})
        end
        @test hasmethod(Zsofia._r∞, Tuple{P,Q})
      end
      @test hasmethod(ticksize, Tuple{Vector{P}})
      @test hasmethod(luvut, Tuple{P})
      @test hasmethod(cummax, Tuple{P})
      @test hasmethod(cummax, Tuple{AbstractVector{P}})
      @test hasmethod(cummin, Tuple{P})
      @test hasmethod(cummin, Tuple{AbstractVector{P}})
      @test hasmethod(muladd, Tuple{P})
    end
    @test hasmethod(typpyst, Tuple{String})
  end
  
  @testset "Operability" begin
    for (p,q,r) in zip((1.0,1.0,1.0), (2.0,1.5,0.5), (100.0,50.0,-50.0))
      @test isapprox(pnl(p,q), r, atol=1e-6)
    end
    @test isapprox(pnl([1.0],[2.0]), [100.0], atol=1e-6)
    @test isapprox(pnl(1.0, [2.0,1.5,0.5]), [100.0,50.0,-50.0], atol=1e-6)
    @test isapprox(pnl([1.0,1.5,3.0]), [50.0,100.0], atol=1e-6)
    @test iszero(indcz(1,0))
    @test isequal(indcz([1],[0]), [0])
    @test isapprox(gap([1.0,1.0], [2.0,1.5]), [0.0,50.0], atol=1e-6)
    @test isequal(ticksize([6001.25,6006.75,6005.25,6000.00,6000.75]), 1//4)
    @test isequal(luvut(12585), "12.56K")
  end
  
  @testset "Erroring" begin
    @test_throws DimensionMismatch gap([1.0,7.5], [1.02,3.52,7.85])
  end
end