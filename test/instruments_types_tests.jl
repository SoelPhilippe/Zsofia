# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Instruments[types] unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for j in (
              Marketables, NonMarketables, Contracts, Commodities,
              Indices, Rates, Securities, Options, Swaps, InterestRate
             )
      @test j <: Observables
    end
    @test Base.:(<:)(Marketables, Observables)
    @test Base.:(<:)(NonMarketables, Observables)
    @test Base.:(<:)(Securities, Contracts)
    @test Base.:(<:)(Contracts, Observables)
    @test Base.:(<:)(InterestRate, Rates)
    @test Base.:(<:)(Debt, Observables)
    @test Base.:(<:)(Equity, Observables)
    @test Base.:(<:)(Options{InterestRate}, Options)
    @test Base.:(<:)(Swaps{InterestRate}, Swaps)
    @test Base.:(<:)(Swaps{Contracts}, Swaps)
    @test Base.:(<:)(Swaps{InterestRate}, Derivatives)
    @test Base.:(<:)(Options{InterestRate}, Derivatives)
    @test Base.:(<:)(Options{Derivatives}, Options)
    @test Base.:(<:)(Options{Derivatives}, Options)
    @test Base.:(<:)(Options{Rates}, Options)
    @test Base.:(<:)(Options{Securities}, Options)
    @test Base.:(<:)(Options{Swaps}, Options)
    @test !Base.:(<:)(Options{InterestRate}, Options{Equity})
    @test !Base.:(<:)(Options{Debt}, Options{Securities})
    @test !Base.:(<:)(Swaps, Derivatives)
    @test !Base.:(<:)(Options, Derivatives)
  end
  
  @testset "Operability" begin
    nothing
  end
  
  @testset "Erroring" begin
    @test_throws TypeError Derivatives{Any} <: Derivatives
    @test_throws TypeError Options{Any} <: Options
  end
end