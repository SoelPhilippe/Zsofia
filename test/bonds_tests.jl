# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Bonds unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test Bond <: Debt <: Securities <: Marketables <: Observables
    for f in (
              :maturity, :coupon, :notional, :inception,
              :currency, :dcc, :prices, :times, :params
             )
      @test hasfield(Bond, f)
    end
    for Q in (Int64,Int32,Float32,Float64,Rational,Date,DateTime)
      @test hasmethod(Bond, Tuple{Q,InterestRate})
      for R in (Int64,Float16,Float64,Rational,Int128)
        for P in (Week,Day,Int64,Float64,Month,Quarter)
          @test hasmethod(Bond, Tuple{Q,R,P})
        end
      end
    end
    @test show(show,Tuple{IO,MIME"text/plain",Bond})
    for T in (Date,DateTime,Float64,Float16,Float32,Rational)
      @test hasmethod(payoff, Tuple{Bond,T})
    end
  end

  @testset "Operability" begin
    maturity = Date(2027,7,2)
    inceptio = Date(2025,7,2)
    notional = 1_000_000
    currency = Currency("USD")
    daycount = Thirty360()
    interest = InterestRate(Tenor("6m"), 0.05, currency=currency)
    pmts_dts = [Date(2027,7,2)+Month(6k) for k in 1:4]
    bonda = begin
      Bond(
           maturity, coupon, inception=inceptio, notional=notional,
           currency=currency, dcc=daycount
      )
    end
    bondb = begin
      Bond(
           maturity, 0.05, Month(6), inception=inceptio,
           notional=notional, currency=currency, dcc=daycount,
      )
    end
    bondc = begin
      Bond(
           maturity, 0.05, 2, inception=inceptio, notional=notional,
           currency=currency, dcc=daycount
      )
    end
    for b in (bonda, bondb, bondc)
      @test isa(b, Bond)
      @test isa(getfield(b,:coupon), InterestRate)
      @test isa(getfield(b,:currency), Currency)
      @test isa(getfield(b,:inception), Date)
      @test isa(getfield(b,:dcc),DayCountConventions)
      @test isequal(getfield(b,:maturity),maturity)
      @test getfield(b,:coupon).isfixed
      @test isequal(getfield(b,:notional),notional)
      @test isequal(getfield(b,:inception),inceptio)
      @test isequal(getfield(b,:currency),currency)
      @test isequal(getfield(b,:dcc),daycount)
      @test isequal(getfield(b,:prices),Real[])
      @test isequal(getfield(b,:times),TimePoint[])
      @test hasparams(b,"coupon_dates")
      for t in pmts_dts
        @test isless(0,payoff(b,t))
        @test isequal(25_000+isequal(t,maturity)*b.notional,payoff(b,t))
      end
    end
  end

  @testset "Erroring" begin
    @test_throws MethodError Bond(Date(2024,1,1),0.05,Month(6),inception=1)
    @test_throws ArgumentError Bond(0.5,0.05,2,inception=0.55)
  end
end