# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Futures unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test Futures <: Marketables <: Observables
    for U in (Futures, InterestRate, Stock, Commodity, FRA, Cap)
      @test Futures{U} <: Futures
      @test Futures{U} <: Derivatives{U} <: Derivatives
    end
    for f in (
              :strike, :expiry, :underlying, :isshort, :inception,
              :currency, :prices, :times, :params
             )
      @test hasfield(Futures, f)
    end
    for P in (Float64, Float32, Rational, Int128, Int64)
      for Q in (Date, DateTime, Float64, Float32, Rational)
        for U in (Stock, Cap, Futures, InterestRate, Forward)
          @test hasmethod(Futures, Tuple{P,Q,U})
          @test hasmethod(payoff, Tuple{Futures{U},P,Q})
        end
      end
    end
    @test hasmethod(payoff, Tuple{Futures})
  end
  
  @testset "Operability" begin
    strike  = 6050.75
    expiry  = Date(2025,3,21)
    ticker  = "ES"
    underlg = Stock("S&P500")
    ccy     = Currency("usd")
    uprices = [6003.12,6025.36,6047.85,6002.52]
    utimes  = [Date(2024,12,27),Date(2025,1,17),Date(2025,2,25),Date(2025,3,3)]
    
    fut = Futures(
                  strike, expiry, underlg,
                  inception=Date(2024,12,20), isshort=true, currency=ccy
          )
    
    @test isa(fut, Futures)
    @test isa(fut, Futures{Stock})
    @test isempty(fut, :prices)
    @test isempty(fut, :times)
    @test isequal(getfield(fut, :underlying), underlg)
    @test isequal(getfield(fut, :ticker), ticker)
    @test isequal(getfield(fut, :currency), ccy)
    @test getfield(fut, :isshort)
    @test getfield(fut, :expiry), expiry)
    
    setprices!(underlg, uprices, utimes)
    
    for (p, t) in zip(uprices, utimes)
      @test isequal(S(t), p)
      @test isapprox(payoff(fut, S(t), t), fut.strike - p, atol=1e-6)
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError Futures(0.0,0.0,DumyObs(),inception=today())
  end
end