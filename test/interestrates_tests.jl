# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "InterestRate unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test InterestRate <: Rates <: NonMarketables
    for f in (:tenor, :isfixed, :currency, :prices, :times, :params)
      @test hasfield(InterestRate, f)
    end
    @test hasmethod(InterestRate, Tuple{Tenor,Currency,Bool})
    for R in (Float64, Int128, Int64, Rational)
      @test hasmethod(InterestRate, Tuple{Tenor,R})
      @test hasmethod(InterestRate, Tuple{R})
    end
    @test hasmethod(show, Tuple{IO,MIME"text/plain",InterestRate)
    @test hasmethod(S,Tuple{InterestRate})
  end
  
  @testset "Operability" begin
    tenor_1y = Tenor("1y")
    tenor_6m = Tenor("6m")
    tenor_3m = Tenor("3m")
    tenor_1m = Tenor("1m")
    currency = Currency("usd")
    interest = Dict{Tenor,InterestRate}()
    
    for t in (tenor_1m,tenor_6m,tenor_3m,tenor_1m)
      @test isa(t, Tenor)
      interest[t] = InterestRate(t, currency=currency, isfixed=false)
      @test isa(interest[t], InterestRate)
    end
    # those rates shouldn't be having "_rate" params
    for t in keys(interest)
      @test ~hasparams(interst[t], "_rate")
    end
    r = randn(10)
    for p in r
      for ten in keys(interest)
        @test isempty(interest[ten])
        insertprice!(interest[ten],p,today())
        @test ~isempty(interest[ten])
        @test isequal(S(interest[ten])(today()), p)
      end
    end
    for ten in (tenor_1y,tenor_6m,tenor_3m,tenor_1m)
      @test isequal(getfield(interest[ten],:tenor), ten)
    end
  end
  
  @testset "Erroring" begin
    ir = InterestRate(Tenor("3m"))
    @test_throws KeyError getparams(ir, "_rate")
  end
end