# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Swap unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test ~isabstracttype(Swap)
    @test isabstracttype(Swaps)
    @test Swap <: Swaps <: Derivatives <: Marketables <: Observables
    @test Swap <: Swaps{InterestRate}
    for f in (
              :swaprate, :expiry, :underlying, :inception,
              :fixingsflt, :fixingsfix, :ispayer, :currencyflt,
              :currencyfix, :dccflt, :dccfix, :prices, :times,
              :params
             )
      @test hasfield(Swap, f)
    end
    for R in (Float64, Int128, Int64, Rational, Float32)
      for S in (Date, DateTime, Float64, Rational, Int64)
        @test hasmethod(Swap, Tuple{R,S,InterestRate,S})
        @test hasmethod(Swap, Tuple{R,Tenor,InterestRate,S})
        @test hasmethod(payoff, Tuple{Swap,R,S})
      end
    end
    @test hasmethod(payoff, Tuple{Swap})
    @test hasmethod(show, Tuple{IO,MIME"text/plain",Swap)
  end
  
  @testset "Operability" begin
    swaprate = 0.03750
    tenor    = Tenor("2y")
    expiry   = Date(2026,6,3)
    inception= Date(2026,3,3)
    notional = 1_000_000
    currency = Currency("USD")
    daycount = Act360()
    fxgsfix  = [
                Date(2024,9,3), Date(2025,3,3),
                Date(2025,9,3), Date(2026,3,3)
               ]
    fxgsflt  = [
                Date(2024,6,3), Date(2024,9,3),
                Date(2024,12,3), Date(2025,3,3),
                Date(2025,6,3), Date(2025,9,3),
                Date(2025,12,3), Date(2026,3,3)
               ]
    ir = InterestRate(Tenor("3m"), currency=currency, isfixed=false)
    setparms!(ir, "ticker", "SOFR3M")
    uprices = [
               0.03750, 0.03790, 0.03810, 0.03740, 0.03450,
               0.03980, 0.03470, 0.03730, 0.03770
              ]
    utimes  = vcat(inception, fxgsflt)
    
    @test isa(ir, InterestRate)
    @test ~getfield(ir, :isfixed)
    @test isempty(ir)
    @test isempty(ir, :prices)
    @test isempty(ir, :times)
    
    swap1 = Swap(
                 swaprate, expiry, ir, inception,
                 fixingsflt=fxgsflt, fixingsfix=fxgsfix,
                 ispayer=true, notional=notional,
                 currencyflt=currency, currencyfix=currency,
                 dccflt=daycount, dccfix=daycount
            )
    swap2 = Swap(
                 swaprate, tenor, ir, inception,
                 fixingsflt=fxgsflt, fixingsfix=fxgsfix,
                 ispayer=true, notional=notional,
                 currencyflt=currency, currencyfix=currency,
                 dccflt=daycount, dccfix=daycount
            )
    for s in (swap1, swap2)
      @test isequal(getfield(s,:swaprate), swaprate)
      @test isequal(getfield(s,:expiry), expiry)
      @test isequal(getfield(s,:underlying), ir)
      @test isequal(getfield(s,:inception), inception)
      @test isequal(getfield(s,:fixingsflt), fxgsflt)
      @test isequal(getfield(s,:fixingsfix), fxgsfix)
      @test getfield(s,:ispayer)
      @test isequal(getfield(s,:notional), notional)
      @test isequal(getfield(s,:currencyflt), currency)
      @test isequal(getfield(s,:currencyfix), currency)
      @test isequal(getfield(s,:dccflt), daycount)
      @test isequal(getfield(s,:dccfix), daycount)
      @test isempty(getfield(s, :prices))
      @test isempty(getfield(s, :times))
    end
    
    setprices!(ir, uprices, utimes)
    
    for swap in (swap1, swap2)
      for t in in getfield(ir, :times)
        if t in swap.fixingsflt
          @test isless(swaprate, payoff(swap, S(ir)(t), t, leg=:fix))
        else
          @test isequal(payoff(swap, S(ir)(t), t, leg=:fix), swaprate)
        end
      end
    end
    
    poffs = Dict{TimePoint,Float64}()
    for t in swap.fixingsflt
      poff[t] = notional * (S(ir)(t) - swaprate) * 0.25
    end
    
    for swap in (swap1, swap2)
      for t in swap.fixingsfix
        @test isapprox(payoff(swap,S(ir)(t),t,leg=:flt),poffs[t],atol=1e-6)
      end
    end
  end
  
  @testset "Erroring" begin
    ir = InterestRate(Tenor("1m"))
    @test_throws ArgumentError Swap(0,0.5,ir,fixingsflt=[0.0,0.55])
  end
end