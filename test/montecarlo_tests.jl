# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "MonteCarlo unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test MonteCarlo <: AbstractModel
    for M in (BlackScholes, CIR, HullWhite1F, BlackKarasinski)
      @test MonteCarlo{M} <: AbstractModel
      for U in (Int64, Int128, Int32)
        @test hasmethod(MonteCarlo, Tuple{M,U})
      end
    end
    @test hasmethod(show, Tuple{IO,MIME"text/plain",MonteCarlo)
  end
  
  @testset "Operability" begin
    mc = MonteCarlo(BlackScholes(0.050))
    @test isa(mc, MonteCarlo{BlackScholes})
  end
  
  @testset "Erroring" begin
    @test_throws MethodError MonteCarlo(120, 12.3)
    @test_throws MethodError MonteCarlo("BlackScholes", 1)
    @test_throws ArgumentError MonteCarlo(DummyModel(),-1)
  end
end