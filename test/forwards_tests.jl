# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Forward unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test Forward <: Marketables <: Observables
    for U in (Forward, InterestRate, Stock, Commodity, FRA, Cap)
      @test Forward{U} <: Forward
      @test Forward{U} <: Derivatives{U} <: Derivatives
    end
    for f in (
              :strike, :expiry, :underlying, :isshort, :inception,
              :currency, :prices, :times, :params
             )
      @test hasfield(Forward, f)
    end
    for P in (Float64, Float32, Rational, Int128, Int64)
      for Q in (Date, DateTime, Float64, Float32, Rational)
        for U in (Stock, Cap, Forward, InterestRate, Forward)
          @test hasmethod(Forward, Tuple{P,Q,U})
          @test hasmethod(payoff, Tuple{Forward{U},P,Q})
        end
      end
    end
    @test hasmethod(payoff, Tuple{Forward})
  end
  
  @testset "Operability" begin
    strike  = 6050.75
    expiry  = Date(2025,3,21)
    ticker  = "ES"
    underlg = Stock("S&P500")
    ccy     = Currency("usd")
    uprices = [6003.12,6025.36,6047.85,6002.52]
    utimes  = [Date(2024,12,27),Date(2025,1,17),Date(2025,2,25),Date(2025,3,3)]
    
    fwd = Forward(
                  strike, expiry, underlg,
                  inception=Date(2024,12,20), isshort=true, currency=ccy
          )
    
    @test isa(fwd, Forward)
    @test isa(fwd, Forward{Stock})
    @test isempty(fwd, :prices)
    @test isempty(fwd, :times)
    @test isequal(getfield(fwd, :underlying), underlg)
    @test isequal(getfield(fwd, :ticker), ticker)
    @test isequal(getfield(fwd, :currency), ccy)
    @test getfield(fwd, :isshort)
    @test getfield(fwd, :expiry), expiry)
    
    setprices!(underlg, uprices, utimes)
    
    for (p, t) in zip(uprices, utimes)
      @test isequal(S(t), p)
      @test iszero(payoff(fwd, S(t), t))
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError Forward(0.0,0.0,DumyObs(),inception=today())
  end
end