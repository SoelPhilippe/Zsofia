# This script is part of Zsofia. Soel Philippe © 2025

@test verbose=true "DayCountConventions unit testing (Zsofia)" begin
  @test "Integrity and Consistency" begin
    @test DayCountConventions <: Conventions
    for dcc in (OneOne,Thirty360,ThirtyE360,ActAct,Act360,Act365)
      @test isa(dcc,DayCountConventions)
      @test isa(dcc,Conventions)
      @test hasmethod(dcc,Tuple{})
      @test Base.issingletontype(dcc)
      @test hasmethod(show,Tuple{IO,MIME"text/plain",dcc})
    end
  end

  @test "Operability" begin
    dt10, dt20 = Date(2024,2,1), Date(2024,3,1)
    dt11, dt21 = Date(2025,2,1), Date(2025,3,1)
    @testset "OneOne" begin
      @test isone(yearfraction(OneOne,dt10,dt20))
      @test isone(yearfraction(OneOne,dt11,dt21))
      @test isone(yearfraction(OneOne,dt10,dt11))
      @test isone(yearfraction(OneOne,dt20,dt21))
    end
    @testset "Thirty360" begin
      @test isapprox(yearfraction(Thirty360,dt10,dt20),1/12)
      @test isapprox(yearfraction(Thirty360,dt11,dt21),1/12)
      @test isone(yearfraction(Thirty360,dt10,dt11))
      @test isone(yearfraction(Thirty360,dt20,dt21))
    end
    @testset "ThirtyE360" begin
      @test isapprox(yearfraction(ThirtyE360,dt10,dt20),1/12)
      @test isapprox(yearfraction(ThirtyE360,dt11,dt21),1/12)
      @test isone(yearfraction(ThirtyE360,dt10,dt11))
      @test isone(yearfraction(ThirtyE360,dt20,dt21))
    end
    @testset "ActAct" begin
      @test isapprox(yearfraction(ActAct,dt10,dt20),29/366)
      @test isapprox(yearfraction(ActAct,dt11,dt21),28/366)
      @test isone(yearfraction(ActAct,dt10,dt11))
      @test isone(yearfraction(ActAct,dt20,dt21))
    end
    @test "Act360" begin
      @test isapprox(yearfraction(Act360,dt10,dt20),29/360)
      @test isapprox(yearfraction(Act360,dt11,dt21),28/360)
      @test isapprox(yearfraction(Act360,dt10,dt11),366/360)
      @test isone(yearfraction(Act360,dt20,dt21))
    end
    @testset "Act365" begin
      @test isapprox(yearfraction(Act365,dt10,dt20),29/365)
      @test isapprox(yearfraction(Act365,dt11,dt21),28/365)
      @test isapprox(yearfraction(Act365,dt10,dt11),366/365)
      @test isone(yearfraction(Act365,dt20,dt21))
    end
  end

  @test "Erroring" begin
    nothing
  end
end