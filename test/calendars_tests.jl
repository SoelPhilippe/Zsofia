# This script is part of Zsofia. Soel Philippe © 2024

@testset verbose=true "Calendars unit testing (Zsofia)" begin
  d = parsefile("holidays_tests_zsofia.toml")
  for cal in keys(d)
    @testset "[$cal] calendar" begin
      xpr = Meta.parse("$cal()")
      for (k, v) in keys(d[cal])
        if isa(v, String)
          @test isholiday(eval(xpr), Date(k))
        else
          @test ~isholiday(eval(xpr), Date(k))
        end
      end
    end
  end
end