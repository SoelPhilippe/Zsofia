# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "ETF unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for j in (:basket, :prices, :times, :params)
      @test hasfield(ETF, j)
    end
    @test ETF{<:Marketables} <: Observables
    @test ETF{<:Marketables} <: Marketables
    @test ETF{<:Marketables} <: Equity
    for U in (Marketables, Options, Securities, Debt)
        @test ETF{U} <: Equity
        @test ETF{U} <: Observables
    end
    for U in (Marketables, Securities, Contracts, Options, Swaps, Futures)
      @test hasmethod(ETF, Tuple{String,Basket{U,Integer}})
    end
    for U in (Contracts, Options, Derivatives, Stock, ETF, Futures)
      @test ~(ETF{U} <: Portfolio)
    end
  end
  
  @testset "Operability" begin
    bask = Basket{Stock,Int64}()
    aapl = Stock("AAPL")
    pltr = Stock("PLTR")
    msft = Stock("MSFT")
    etf  = ETF("myetf",bask)

    @test isa(ETF("io"),ETF)
    @test isa(ETF(Basket([Stock(),Stock()],[1,2])), ETF)
    @test hasmethod(show, Tuple{IO,MIME"text/plain",ETF})
    for j in (length, size, isempty)
      @test hasmethod(j, Tuple{ETF})
    end
    for L in (Swap, Options, Futures, Stock, Forward, Commodities)
      for l in (push!, put!)
        @test hasmethod(l, Tuple{ETF{L}, L, Integer})
        @test hasmethod(l, Tuple{ETF{L}, Vector{L}, Vector{Integer}})
        @test hasmethod(l, Tuple{ETF{L}, Vector{L}, Integer})
      end
    end
    @test isa(aapl, Stock)
    @test isa(pltr, Stock)
    @test isa(bask, Portfolio)
    @test isa(bask, Basket)
    @test isempty(etf)
    push!(bask, aapl)
    @test ~isempty(etf)
    @test isone(length(etf))
    @test isone(size(etf))
    @test isone(count(etf))
    push!(etf, pltr)
    @test >(size(etf), 1)
    @test ==(size(etf), 2)
    @test ==(count(etf), 2)
    for s in (aapl, pltr)
      @test in(s, etf)
    end
    insertprice!(aapl, 225.25, 0.0)
    insertprice!(pltr, 17.85, 0.0)
    insertprice!(msft, 471.79, 0.0)
    push!(etf, aapl, 1250)
    push!(etf, pltr, 1789)
    for c in get(etf, :components)
      @test isa(c, Observables)
      @test isa(c, Marketables)
      @test isa(c, Stock)
    end
    @test isequal(get(etf, :value), 225.25*1250 + 1789*17.85)
    @test isequal(count(>=(1200), etf), 2)
    @test isequal(count(>=(1200), etf, :units), 2)
    @test isequal(count(etf), 2)
    @test isequal(Set(get(etf, :components)), Set([aapl, pltr]))
    empty!(etf)
    @test isempty(etf)
    @test iszero(count(etf))
    @test iszero(size(etf))
    @test iszero(length(etf))
    put!(etf, msft, -120)
    @test isone(size(etf))
    put!(etf, aapl, 258)
    @test ==(size(etf),2)
    put!(etf, pltr, 80)
    @test all(in(etf), [msft,aapl,pltr])
    deleteat!(etf, 3)
    @test ~in(pltr, etf)
    put!(etf, pltr)
    @test in(pltr, etf)
    deleteat!(etf, 2)
    @test ~in(aapl, etf)
    put!(etf, aapl)
    keepat!(etf, 2)
    @test ~in(msft, etf)
    @test ~in(aapl, etf)
    @test in(pltr, etf)
    put!(etf, msft, -120)
    put!(etf, pltr, 80)
    put!(etf, aapl, 258)
    @test isequal(get(etf, :nav),-120*471.79 + 80*17.85 + 258*225.25)
    insertprice!(aapl, 227.89, 1.0)
    insertprice!(msft, 449.87, 1.0)
    insertprice!(pltr, 19.47, 1.0)
    @test isapprox(
                   get(etf, :nav), 
                   -120*449.87 + 258*227.89 + 80*19.47,
                   atol=1e-5
          )
    a=popfirst!(etf)[1]
    @test isequal(a, pltr)
    @test in(aapl, etf)
    pop!(etf)
    @test in(msft, etf)
    @test isequal(get(etf, :value), -120*S(msft)())
  end
  
  @testset "Erroring" begin
    aapl, pltr, msft = Stock("AAPL"), Stock("PLTR"), Stock("MSFT")
    insertprice!(aapl, 225.25, 0.0)
    insertprice!(pltr, 17.85, 0.0)
    etf = ETF(Basket([aapl, pltr], [1250,1789]))
    @test_throws ArgumentError get(etf, :component)
    @test_throws ArgumentError get(etf, :values)
    @test_throws ArgumentError get(etf, :monads)
    @test isa(get(etf, aapl), Tuple{Stock, Int64})
    @test isa(get(etf, pltr), Tuple{Stock, Int64})
    @test size(etf) == 2
    @test_throws DomainError get(etf, msft)
    @test_throws UndefVarError get(etf, aal)
  end
end