# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Currency unit testing" begin
  @testset "Integrity and Consistency" begin
    @test Currency <: Conventions
    @test ~isabstracttype(Currency)
    @test isabstracttype(Conventions)
    for T in (String,AbstractString)
      @test hasmethod(Currency,Tuple{T})
    end
    @test hasmethod(Currency,Tuple{})
    @test hasmethod(show, Tuple{IO,MIME"text/plain",Currency})
  end

  @testset "Operability" begin
    c1,c2,c3 = Currency("USD"),Currency("EUR"),Currency("HUF")
    for ccy in (c1,c2,c3)
      @test isa(ccy, Conventions)
      @test isa(ccy, Currency)
      @test isless(Currency("AAA", ccy))
    end
    @test isa(Currency("jpy"), Currency)
    @test isa(Currency("jPy"), Currency)
    @test isequal(Currency("jpy"), Currency("jPy"))
  end

  @testset "Erroring" begin
    @test_throws MethodError Currency(nothing)
    @test_throws MethodError Currency("")
    @test_throws ArgumentError Currency("AAPL")
  end
end