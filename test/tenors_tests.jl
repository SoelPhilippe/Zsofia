# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Tenor unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    for U in (Day, Month, Quarter, Year)
      @test Tenor{U} <: Tenor
      @test hasmethod(Tenor, Tuple{U})
    end
    @test hasemethod(show, Tuple{IO,MIME"text/plain",Tenor})
  end
  
  @testset "Operability" begin
    d0 = Date(3333, 3, 3)
    d1 = collect(d0+Month(i) for i in range(1,8))
    mm = [Tenor("3m") Tenor("6m"); Tenor("21m") Tenor("24m")]
    for i in 1:size(d1,1)
      @test isequal(d1[i], d0+Tenor("$(i)m"))
    end
    basic = Tenor("2Y")
    @test (t=Tenor("3m"); t.period == Month(3))
    @test (t=Tenor("3M"); t.period == Month(3))
    @test (t=Tenor("0m"); t.period == Month(0))
    @test (t=Tenor(Month(1)); t.period == Month(1))
    @test (t=Tenor("2y"); t == basic)
    @test (t=Tenor("2Y"); t == basic)
    @test isequal(Tenor("3m") + Tenor("4m"), Tenor("7m"))
    @test isequal(Tenor("1y") + Tenor("2y"), Tenor("3y"))
    @test isequal(Tenor("1w") + Tenor("50w"), Tenor("51w"))
    @test isequal(Tenor("1m") - Tenor("7m"), Tenor(Month(-6)))
    @test isequal(Tenor(Month(-1)), -Tenor("1m"))
    @test isequal(Tenor(d0 - Date(3333, 3, 1)), Tenor("2d"))
    @test isequal(/(Tenor("3m"), 3), Tenor("1m"))
    @test isequal(*(Tenor("2y"), 2), Tenor("4y"))
    @test isequal(*(Tenor("3m"), [1 2; 7 8]), mm)
    @test isless(Tenor("52w"), Tenor("53w"))
    @test isequal(Tenor("1w"), Tenor("7d"))
    @test isless(Tenor("1d"), Tenor("2d"))
    @test (Tenor("4w") < Tenor("29d") < Tenor("30d"))
    @test ~isequal(Tenor("52w"), Tenor("1y"))
  end
  
  @testset "Erroring" begin
    @test_throws ArgumentError Tenor("-3m")
    @test_throws ArgumentError Tenor("1monad")
    @test_throws ArgumentError Tenor("1month")
    @test_throws ArgumentError Tenor("1 m")
    @test_throws MethodError Tenor(1)
    @test_throws MethodError Tenor(-3)
    @test_throws InexactError Tenor("3m")/2
    @test_throws MethodError Tenor("3m") * Tenor("1m")
  end
end
