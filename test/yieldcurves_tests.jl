# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "YieldCurve unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test YieldCurve <: AbstractCurves
    for f in (:grippie,:params)
      @test hasfield(YieldCurve, f)
    end
    @test hasmethod(YieldCurve,Tuple{AbstractVector{InterestRate}})
    @test hasmethod(YieldCurve,Tuple{Vector{InterestRate}})
    @test hasmethod(YieldCurve,Tuple{Tuple{Vararg{InterestRate}}})
    @test hasmethod(show,Tuple{IO,MIME"text/plain",YieldCurve})
    @test hasmethod(getcurve,Tuple{YieldCurve})
    @test hasmethod(getyield,Tuple{YieldCurve,Tenor})
    for f in (first, length, isempty, iterate, only, collect, eltype)
      @test hasmethod(f,Tuple{YieldCurve})
    end
  end
  
  @testset "Operability" begin
    irs  = Dict{AbstractString,InterestRate}()
    irs_ = Dict{AbstractString,InterestRate}()
    ccy = Currency("USD")
    for i in (1,2,3,4)
      irs[string("ir",i)] = InterestRate(Tenor(string(i,"m")), currency=ccy)
    end
    vrs = collect(values(irs))
    tee = [0.00,0.25,0.50,0.75]
    pee = [
           [4.3525,4.2777,4.1997,4.1625];;
           [4.3102,4.2907,4.2809,4.2717];;
           [4.4417,4.4314,4.4328,4.4129];;
           [4.8795,4.7581,4.8419,4.7419];;
          ]
    for i in 1:4
      setprices!(irs[string("ir",i)], pee[:,i], tee)
    end
    yc = YieldCurve(vrs, title="yc-test")
    
    @test isa(vrs,AbstractVector) && isa(vrs,Vector)
    @test isa(vrs,Vector{InterestRate})
    for i in 1:4
      crv = getcurve(yc,tee[i])
      @test isa(crv, Dict)
      @test isa(crv, Dict{Tenor,Real})
      setparams!(yc, "setting", 1, force=true)
      @test hasparams(yc, "setting")
      @test isone(getprams(yc,"setting"))
      for t in keys(crv)
        @test isapprox(crv[t], S(irs[string("ir",i)])(tee[i]))
      end
    end
  end
  
  @testset "Erroring" begin
    yc = YieldCurve()
    @test ~hasparams(yc,"a")
    setparams!(yc, "a", 1)
    @test_throws ArgumentError setparams!(yc, "1", 1)
  end
end