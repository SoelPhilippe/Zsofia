# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "AsianOption unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test AsianOption <: Contracts
    for f in (
              :strike, :expiry, :option, :underlying, :inception,
              :striketype, :avgtype, :prices, :times, :params
             )
      for U in (Stock, Swap, Futures, Floor, Cap, InterestRate)
        @test hasfield(AsianOption{U}, f)
      end
    end
    for Q in (Date, Float64, Float32, Rational, Int64, DateTime)
      for R in (Float64, Float32, Float16, Rational, Int128, Int64, Int32)
        @test hasmethod(AsianOption, Tuple{R,Q})
      end
      @test hasmethod(AsianOption, Tuple{Q})
    end
    @test hasmethod(show, Tuple{IO, MIME"text/plain", AsianOption})
    for T in (AsianOptionType, AverageType)
      @test T <: Enum
      @test T <: Enum{Int32}
    end
    for R in (Real, Float32, Float64, Rational, Int8, Int32, Int64)
      for T in (Date, DateTime, Float64, Rational, Float32, Float16)
        @test hasmethod(payoff, Tuple{AsianOption,R,T,Vector{R},Vector{T}})
      end
    end
    for R in (Real, Float32, Float64, Rational, Int32, Int64)
      @test hasmethod(
                      averagelevel,
                      Tuple{Vector{R},AverageType,AbstractWeights}
            )
    end
  end
  
  @testset "Operability" begin
    strike = 5950.00
    expiry = Date(2025,3,21)
    spx    = Basket()
    incept = Date(2024,12,23)
    ccy    = Currency("USD")
    underl = Futures(5950, expiry, spx, inception=incept, currency=ccy)
    ops    = Dict{String,AsianOption}()
    uprices= [5925.75, 6001.25, 5960.50]
    udates = [Date(2025,1,7), Date(2025,2,7), Date(2025,3,7)]
    avgtyp = (ArithmeticAverage, GeometricAverage, HarmonicAverage)
    setparams!(spx, "ticker", "S&P 500 Index", force=true)
    for i in 1:3
      ops["op" * string(i)] = begin
        AsianOption(
                    strike, expiry, option=Call, underlying=underl,
                    inception=incept, currency=ccy, avgtype=avgtyp[i]
        )
      end
    end
    @test isa(spx, Indices)
    @test isa(underl, Futures)
    @test isempty(underl)
    setprices!(underl, uprices, udates)
    @test ~isempty(underl)
    for i in 1:3
      @test ops["op"*string(i)] isa AsianOption
      @test getfield(ops["op"*string(i)], :striketype) == FixedStrike
    end
    for i in 1:3
      @test getfield(ops["op"*string(i)], :inception) isa Date
      @test getfield(ops["op"*string(i)], :expiry) isa Date
      @test getfield(ops["op"*string(i)], :option) isa OptionColor
      @test getfield(ops["op"*string(i)], :underlying) isa Futures
      @test getfield(ops["op"*string(i)], :underlying) == underl
      @test getfield(ops["op"*string(i)], :striketype) isa AsianOptionType
      @test getfield(ops["op"*string(i)], :prices) isa Vector
      @test getfield(ops["op"*string(i)], :times) isa TimePoint
      @test getfield(ops["op"*string(i)], :params) isa Dict{AbstractString,Any}
    end
    a = Tuple(averagelevel(uprices,ops["op"*string(i)].avgtype) for i in 1:3)
    @test a[3] <= a[2] <= a[1]
    for i in 1:3
      for t in underl.times
        @test begin
          payoff(ops["op"*string(i)], S(underl)(t), t, uprices, udates) ≈ 0
        end
        @test payoff(ops["op"*string(i)], 6001.75, t, uprices, udates) > 0
      end
    end
  end
  
  @testset "Erroring" begin
    strike = 5950.00
    expiry = Date(2025,3,21)
    spx    = Basket()
    incept = Date(2024,12,23)
    ccy    = Currency("USD")
    underl = Futures(5950, expiry, spx, inception=incept, currency=ccy)
    optio  = begin
      AsianOption(
                  strike, expiry, option=Call, underlying=underl,
                  inception=incept, currency=ccy
      )
    end
    @test_throws ArgumentError AsianOption(1, inception=0.5)
    @test_throws DimensionMismatch payoff(optio,3,0.1,[1],[0.15,0.16])
  end
end