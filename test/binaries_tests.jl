# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "BinaryOption unit testing (Zsofia)" begin
  @testset "Intergrity and Consistency" begin
    @test BinaryOption <: Options <: Derivatives <: Marketables <: Observables
    @test hasmethod(getparams, Tuple{BinaryOption,String})
    @test hasmethod(getparams, Tuple{BinaryOption,String,Any})
    for U in (Stock, Rates, Equity, Futures, InterestRate, Commodity)
      @test BinaryOption{U} <: Options
      @test BinaryOption{U} <: Options{U}
      @test BinaryOption{U} <: BinaryOption
    end
    for f in (
              :strike, :expiry, :option, :underlying,
              :inception, :prices, :times, :params
             )
      @test hasfield(BinaryOption, f)
    end
    for R in (Float64,Float32,Float16,Rational,Int128,Int64,Int32)
      for Q in (Date,Float64,Float32,Rational,Int64,DateTime)
        @test hasmethod(BinaryOption, Tuple{R,Q})
        for U in (Stock, Commodity, Equity, Derivatives, InterestRate)
          @test hasmethod(BinaryOption{U}, Tuple{R,Q})
        end
      end
    end
    @test hasmethod(S, Tuple{BinaryOption})
    @test hasmethod(show, Tuple{IO,MIME"text/plain",BinaryOption})
  end
  
  @testset "Operability" begin
    strike = 4.444
    expiry = Date(2024,12,6)
    incept = Date(2024,11,29)
    underl = InterestRate(Tenor("3m"), Currency("USD"), isfixed=false)
    ulevel = [4.655, 4.687, 4.349, 4.281, 4.399, 4.445]
    opricz = [0.851, 0.869, 0.223, 0.117, 0.577, 0.999]
    otimez = vcat(incept, [Date(2024,12,i) for i in 2:6])
    dummyo = BinaryOption(strike, expiry)
    bcall  = BinaryOption(strike, expiry, underlying=underl, inception=incept)
    bput   = BinaryOption(
                          strike, expiry, underlying=underl,
                          inception=incept, option=Put
             )
    
    for R in (Float64, Float32, Int64, Int32, Rational, Int128, Real)
      for U in (Stock, Commodity, Swap, InterestRate, NonMarketables, Indices)
        for T in (Date, DateTime, Float64, Rational, Int64)
          @hasmethod payoff(payoff, Tuple{BinaryOption{U},R,T})
        end
      end
    end
    @test isa(underl, InterestRate)
    for o in (dummyo, bcall, bput)
      @test isa(o, Options)
      for p in (:strike, :expiry, :option, :underlying, :prices, :times)
        @test hasproperty(o, p)
      end
      @test isequal(getfield(o, :strike), strike)
      @test isequal(getfield(o, :expiry), expiry)
      @test in(getfield(o, :option), (Call,Put))
      @test isempty(getfield(o, :prices))
      @test isempty(getfield(o, :times))
    end
    @test isempty(getfield(underl, :prices))
    @test isempty(getfield(underl, :times))
    for o in (dummyo, bcall)
      @test isequal(getfield(o, :option), Call)
    end
    @test isa(getfield(dummyo, :underlying), DummyObs)
    
    setprices!(underl, ulevel, otimez)
    setprices!(bcall, opricz, otimez)
    
    @test ~isempty(getfield(underl, :prices))
    @test ~isempty(getfield(underl, :times))
    @test ~isempty(getfield(bcall, :prices))
    @test ~isempty(getfield(bcall, :times))
    for (t, po, pu) in zip(otimez, opricz, ulevel)
      @test isequal(S(underl)(t), pu)
      @test isequal(S(bcall)(t), po)
    end
    for (t,s) in zip(otimez, ulevel)
      @test iszero(payoff(bcall, t, s) * payoff(bput, t, s)) 
    end
    for t in otimez
      for s in ulevel[1:(end-1)]
        @test iszero(payoff(bcall, s, t))
        @test iszero(payoff(bput, s, t))
      end
    end
    @test isone(payoff(bcall, ulevel[end], otimez[end]))
    @test iszero(payoff(bput, ulevel[end], otimez[end]))
  end
  
  @testset "Erroring" begin
    @test_throws MethodError BinaryOption(1.0,0.0,inception=Date(2024,12,6))
  end
end