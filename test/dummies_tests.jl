# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "Dummies unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test Zsofia.DummyObs isa Observables
    @test ~isabstracttype(Zsofia.DummyObs)
    for foo in (Zsofia.dummyticker, Zsofia.dummyinception)
      @test isa(foo,Function)
      @test hasmethod(foo,Tuple{})
    end
    for foo in (
                payoff, hasparams, settimes!, resettimes!, setprices!,
                resetprices!, setparams!, getparams, insetprices!
               )
      @test begin
        hasmethod(foo,Tuple{Zsofia.DummyObs}) ||
        hasmethod(foo,Tuple{Zsofia.DummyObs,Vector{Real},Vector{Real}}) ||
        hasmethod(foo,Tuple{Zsofia.DummyObs,AbstractString,Any}) ||
        hasmethod(foo,Tuple{Zsofia.DummyObs,AbstractString}) ||
        hasmethod(foo,Tuple{Zsofia.DummyObs,Real,Date})
      end
    end
    @test hasmethod(S,Tuple{Zsofia.DummyObs})
    @test hasmethod(show,Tuple{IO,MIME"text/plain",DummyObs})
  end
  
  @testset "Operability" begin
    @test isa(Zsofia.dummyticker(), String)
    @test iszero(payoff(Zsofia.DummyObs()))
    for A in (Float64,Float32,Float16,Rational,Int32,Int64)
      @test iszero(Zsofia.dummyinception(one(A)))
    end
    chrs = string.(rand(Char,10))
    for chr in chrs
      @test ~hasparams(Zsofia.DummyObs(),chr)
    end
    @test isnothing(setprices!(Zsofia.DummyObs(),[1.0],[1]))
    @test isnothing(resetprices!(Zsofia.DummyObs(),[1.0],[1]))
    @test isnothing(setparams!(Zsofia.DummyObs(),"A","B"))
    @test isnothing(getparams(Zsofia.DummyObs(),"A","B"))
    @test isa(inserprices!(Zsofia.DummyObs(),1.0,now()),Tuple{DateTime,Float64})
  end
  
  @testset "Erroring" begin
    @test_throws ErrorException getfield(Zsofia.DummyObs(),:a)
  end
end