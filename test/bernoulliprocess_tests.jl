# This script is part of Zsofia. Soel Philippe © 2025

 @testset verbose=true "BernoulliProcess unit testing (Zsofia)" begin
   @testset "Integrity and Consistency" begin
     @test BernoulliProcess <: StochasticProcesses <: AbstractModel
     for f in (:kinda,:times,:randz,:paths,:p)
       @test hasfield(BernoulliProcess, f)
     end
     for A in (Int128,Int64,Int32)
       for B in (Int64,Int32,Float64,Float32)
         @test hasmethod(BernoulliProcess,Tuple{A,AbstractVector{B}})
       end
     end
     @test hasmethod(show,Tuple{IO,MIME"text/plain",BernoulliProcess})
     @test hasmethod(simulate,Tuple{BernoulliProcess})
     @test hasmethod(resimulate,Tuple{BernoulliProcess})
     @test hasmethod(StatsBase.fit,Tuple{BernoulliProcess,AbstractVector{Bool}})
   end
 
   @testset "Operability" begin
     bnp = Dict{Integer,BernoulliProcess}()
     for n in (10,100,1000)
       bnp[n] = BernoulliProcess(n,p=0.25,dimension=2n)
     end
     for n in keys(bnp)
       @test isa(n,Int64)
       @test isa(bnp[n],StochasticProcesses)
       @test isa(bnp[n],BernoulliProcess)
       @test isequal(bnp[n].kinda, (n+1,n,2n))
       @test iszero(bnp[n].times[begin])
       @test iszero(bnp[n].paths)
       simulate!(bnp[n])
       @test ~iszero(bnp[n].randz) # bad statistical idea
       @test ~iszero(bnp[n].paths) # bad statistical idea
       @test isequal(size(bnp[n].paths), (1+n,n,2n))
     end
   end
 
   @testset "Erroring" begin
     @test_throws ArgumentError BernoulliProcess(-1,[0.3])
     @test_throws MethodError BernoulliProcess(10,0.1)
   end
 end