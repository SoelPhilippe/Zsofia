# This script is part of Zsofia. Soel Philippe © 2025

@testset verbose=true "EconoIndicator unit testing (Zsofia)" begin
  @testset "Integrity and Consistency" begin
    @test EconoIndicator <: Indices{Rates} <: Indices <: NonMarketables
    for f in (:prices, :times, :params)
      @test hasfield(EconoIndicator, f)
    end
    @test hasmethod(EconoIndicator, Tuple{String})
    @test hasmethod(show, Tuple{IO,MIME"text/plain",EconoIndicator})
  end
  
  @testset "Operability" begin
    ecn = EconoIndicator("SOFR")
    @test isa(ecn, EconoIndicator)
    for k in ("ticker", "μ", "σ", "carryrate")
      @test haskey(ecn.params, k)
      if k!="ticker"
        setparams!(ecn, k, one(Float64))
      end
      @test isone(getparams(ecn, k))
    end
  end
  
  @testset "Erroring" begin
    @test_throws MethodError EconoIndicator(1)
  end
end