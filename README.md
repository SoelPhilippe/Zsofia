# Zsofia.jl

![license-badge](https://img.shields.io/badge/license-MIT-blue)

A [Julia Programming Language](www.julialang.org) package for applications
in *quantitative finance*; number of tools daily used by quants and traders
are herein implemented. The API provides a facility to manage
and monitor portfolios of (tailor-made) financial derivatives
and their xVAs.

## Get started

### Installation

This package is not yet publicly registered so that for `using` it in the
REPL it is required to start the package manager by typing `]` and
use the `add` command followed by the repo's URL:
```julia
add https://codeberg.org/SoelPhilippe/Zsofia.jl
```

> **Still in development, may experience severe bugs...**

**Zsofia** (re)exports
[`Dates`](https://github.com/JuliaLang/julia/tree/master/stdlib/Dates) and
[`StatsBase`](https://github.com/JuliaStats/StatsBase.jl) which are
`Julia`'s standard library packages as well as the very useful
[`DataFrames`](https://github.com/JuliaData/DataFrames.jl)
by Pr. Bogùl Kaminski.  

Import the package by `using Zsofia` to get started in the REPL...

### Evaluate Options and Greeks

Let's evaluate a european put option incepted Jan 3, 2025
struck at 60.00 having as underlying a stock experiencing a
63.27\% volatility into a 4.1037\% interest rate realm.

```jl
begin
  # instantiate a stock with its ticker
  pltr = Stock("PLTR");
  
  strike, expiry = 60.00, Date(2025,1,31);

  # one can specify the currency and the day count convention (non mandatory)
  ccy, dcc = Currency("USD"), Thirty360();
  
  # specify the model (dynamic of the underlying risk factor)
  bs = BlackScholes(0.041037, currency=ccy);
  
  # specify the inception date of the contract
  inception = Date(2025, 1, 3);

  # that's all, now instantiate a european put on PALANTIR struck at 60.00
  op = EuropeanOption(
                      strike, expiry, option=Put,
                      underlying=pltr, inception=inception
       );
end
```
The facility to evaluate financial instrument is `evaluate`. In order to
`evaluate` the just defined option, the only missing informations (obv
under the risk-neutral measure) are the current price of `pltr` and the
volatility parameter.
```jl
begin
  # set the observed price of PALANTIR at `79.89` at `inception`.
  insertprices!(pltr, 79.89, inception)
  
  # set the volatility level
  setparams!(pltr, "sigma", 0.6327, force=true);
end
```
...ready to evaluate price and greeks!
```
julia> evaluate(bs, op, dcc=dcc)
0.2566547270622772

julia> commongreeks(bs, op, dcc=dcc)
┌────────────┬────────────┬─────────┬───────────┬──────────┬──────────┐
│          Δ │          Γ │       V │         Ρ │        Θ │        Λ │
│    Float64 │    Float64 │ Float64 │   Float64 │  Float64 │  Float64 │
├────────────┼────────────┼─────────┼───────────┼──────────┼──────────┤
│ -0.0419144 │ 0.00634929 │ 1.99418 │ -0.280404 │ -7.96308 │ -13.0469 │
└────────────┴────────────┴─────────┴───────────┴──────────┴──────────┘
```

### Fetch Stocks

With [**Zsofia**](https://codeberg.org/SoelPhilippe/Zsofia.jl), one can
fetch stocks price action... For that purpose, some contexts encapsulated
into an abstract type `ZsofiaDBContext` have been defined
such as **Hjordis** for intraday price action, **Brynhild** for
day-to-day price action etc... As an example, let's fetch
`BNP` (BNP Parisbas SA, traded on Euronext Paris-FRANCE) and
`PLTR` (Palantir Technologies, Inc., traded on the Nasdaq). 

```jl
begin
  dfs = Dict{Stock, DataFrame}()
         
  bnp, pltr = Stock("BNP"), Stock("PLTR")
         
  setparams!(bnp, "calendar", FranceEx())
  setparams!(pltr, "calendar", UnitedStatesEx())
         
  for stock in (bnp, pltr)
    dfs[stock] = fetch(stock,BRYNHILD,force=true);
  end
end
```
glimpse the first rows of `pltr`...
```
julia> first(dfs[pltr], 5)
5×15 DataFrame
 Row │ Date        Tckr    Price  Open   High   Low   Close  Shares     Co     ⋯
     │ Date        String  Real   Real   Real   Real  Real   Integer    Float6 ⋯
─────┼──────────────────────────────────────────────────────────────────────────
   1 │ 2020-09-30  PLTR     9.5   10.0   11.41  9.11   9.5   338584400  -5.0   ⋯
   2 │ 2020-10-01  PLTR     9.46   9.69  10.1   9.23   9.46  124297600  -2.373
   3 │ 2020-10-02  PLTR     9.2    9.06   9.28  8.94   9.2    55018300   1.545
   4 │ 2020-10-05  PLTR     9.03   9.43   9.49  8.92   9.03   36316900  -4.241
   5 │ 2020-10-06  PLTR     9.9    9.04  10.18  8.9    9.9    90864000   9.513 ⋯
                                                               7 columns omitted
```
