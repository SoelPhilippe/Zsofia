begin
  using Markdown
  using Zsofia
end

begin
  const BenchmarkFyles::Dict{String,Tuple{Vararg{String}}} = begin
    Dict{String,Tuple{Vararg{String}}}(
      "Instruments" => (
                        "interestrates","americans","asians",
                        "barriers","baskets","bermudans","binaries",
                        "bonds", "caps", "choosers", "commodities",
                        "cryptocurrencies", "econoindicators",
                        "etfs","europeans","floors","forwards",
                        "fras","fxrates","parisians","stocks",
                        "swaps","quotes"
                       ),
      "Processes" => (
                      "bernoulli","poisson","wiener","bessel",
                      "cauchy","ornsteinuhlenbeck"
                     )
    )
  end
end

# processing unit
begin
  for p in (
            "Instruments", "Processes",
           )
    for f in Benchmarkfyles[p]
      include(joinpath(@__DIR__, f, "_benchmarks.jl"))
    end
  end
end
