begin
  #------------------------------#
  import CSV: write as write_csv
  using Optim
  #------------------------------#
  foo(x::Vector{R}) where R<:Real = (1.0 - x[1])^2 + 100.0 * (x[2] - x[1]^2)^2
  optimizers = (
                NelderMead, SimulatedAnnealing, ParticleSwarm,
                ConjugateGradient, GradientDescent, BFGS, LBFGS,
                NGMRES, OACCEL, NewtonTrustRegion, Newton
               )
  sols = Dict()
  for opt in optimizers
    obj = optimize(foo, zeros(2), opt())
    get!(
         sols,
         replace(string(opt), "Optim." => ""),
         (
          getfield(obj, :minimum),
          getfield(obj, :minimizer)
         )
    )
  end
  df = DataFrame([(k, sols[k]) for k in keys(sols)], [:Optimize, :Result])
  write_csv("optimizers_test.csv", df)
end