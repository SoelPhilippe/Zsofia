# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      TermStructure <: Curves
  
  The term structure is intended to represent a yield curve at a very
  specific `TimePoint`.
  
  # Fields
  - `grippie::Dict{Tenor,Real}`: a collection of `Tenor => Real` relations.
  - `params::Dict{AbstractString,Real}`: casual parameters container.
  
  # Constructors
      TermStructure(yc::YieldCurve, [t::TimePoint]; [yf::Bool, dcc])
  
  ## Arguments
  - `yc::YieldCurve`: underlying yield curve.
  - `t::TimePoint`: slice the yield curve at `t`.
  - `yf::Bool=false`: transform tenor to year fractions when `true`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When `YieldCurve{Period}` type provided as argument, the `truthness` of
  `yf::Bool` is relevant in such a way that tenors are converted into
  year fraction according to the day count convention `dcc`.
  """
  struct TermStructure <: Curves
    grippie::Dict{U,S} where {U<:Union{Tenor,<:Real},S<:Real}
    params::Dict{AbstractString,Any}
	  
	  function TermStructure(
	                         yc::YieldCurve,
	                         t::TimePoint;
	                         yf::Bool=false,
	                         dcc::DayCountConventions=ActAct()
	           )
	    new(
	        getcurve(yc,t,yf=yf,dcc=dcc),
	        Dict{AbstractString,Any}("title" => DummyTicker())
	    )
	  end
	  
	  function TermStructure(
	                         yc::YieldCurve;
	                         yf::Bool=false,
	                         dcc::DayCountConventions=ActAct()
	           )
	    new(
	        getcurve(yc,yf=yf,dcc=dcc),
	        Dict{AbstractString,Any}("title" => DummyTicker())
	    )
	  end
	  
	  function TermStructure(
	                         dict::Dict{U,S}
	           ) where {U<:Union{Tenor,<:Real},S<:Real}
	    new(dict,Dict{AbstractString,Any}("title" => DummyTicker()))
	  end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ts::TermStructure)
    print(io, "TermStructure[",ts.grippie.count,"](",ts.params["title"],")")
  end

  """
      setparams!(ts::TermStructure, tag::AbstractString, value; [force])
      setparams!(ts::TermStructure, dict::Dict{AbstractString,Any}; [force])
  
  Set `value` as a parameter (uniquely) identified by `tag` into `ts`'
  parameters container.
  
  # Arguments
  - `ts::TermStructure`: the underlying observable to set parameter of.
  - `tag::AbstractString`: the parameter tag.
  - `value`: parameter value to be stored under the identifier `tag`.
  - `dict::Dict{AbstractString,Any}`: a dictionary of parameters to set.
  - `force::Bool=false`: kwarg, if `true` existing value at `tag` is replaced.
  
  # Notes
  The parameter's value `value` identified by `tag` can be of any type.
  The function returns the pair `tag => value`.
  Some `tag`s are treated particulary: "mu", "sigma", "μ", "σ" as they are
  widespread in the financial realm.
  """
  function setparams!(
                      ts::TermStructure,
                      tag::AbstractString,
                      value;
                      force::Bool=false
           )
    if haskey(getfield(ts, :params), tag) && ~force
      throw(ArgumentError("$(tag) exists! Use `force=true` to modify."))
    end
    if lowercase(tag) == "mu"
      ~isa(value, Real) ? throw(ArgumentError("$(tag) should be a number")) : 0
      ts.params["μ"] = value
    elseif (lowercase(tag) == "sigma") || (tag == "σ")
      ~isa(value, Real) ? throw(ArgumentError("$(tag) should be a number")) : 0
      <(0)(value) ? throw(ArgumentError("$(tag) should be non-negative")) : 0
      ts.params["σ"] = value
    else
      ts.params[tag] = value
    end
    tag => value
  end
  
  function setparams!(
                      ts::TermStructure,
                      dict::Dict{AbstractString,Any};
                      force::Bool=false
           )
    collect(setparams!(ts, k, v, force=force) for (k,v) in dict)
  end
  
  """
      hasparams(ts::TermStructure, tag::AbstractString)
      
  Return `true` is `ts` parameters containers has such a key as `tag`.
  """
  function hasparams(ts::TermStructure, tag::AbstractString)
    if lowercase(tag) == "mu"
      haskey(ts.params, "μ") || haskey(ts.params, tag)
    elseif lowercase(tag) == "sigma"
      haskey(ts.params, "σ") || haskey(ts.params, tag)
    else
      haskey(ts.params, tag)
    end
  end
  
  """
      getparams(ts::TermStructure, tag::AbstractString)
      getparams(ts::TermStructure, tag::AbstractString, default)
  
  Return the value of the parameter having `tag` as identifier.
  """
  function getparams(ts::TermStructure, tag::AbstractString)
    if lowercase(tag) == "mu"
      ts.params["μ"]
    elseif lowercase(tag) == "sigma"
      ts.params["σ"]
    else
      ts.params[tag]
    end
  end
  
  function getparams(ts::TermStructure, tag::AbstractString, default::Any)
    hasparams(ts,tag) ? getparams(ts,tag) : default
  end
  
  """
      getparams!(ts::TermStructure, tag::AbstractString, default::Any)
  
  Return the value stored into `ts`' parameters container for the given
  identifier `tag`, or if no mapping for the identifier is present,
  store `default` at that identifier and return `default`.
  
  # Arguments
  - `ts::TermStructure`: underlying `TermStructure` to process.
  - `tag::AbstractString`: parameter's identifier.
  - `default::Any`: default value to store if no value is present at `tag`.
  """
  function getparams!(ts::TermStructure, tag::AbstractString, default)
    if hasparams(ts, tag)
      getparams(ts, tag)
    else
      setparams!(ts, tag, default, force=true)
    end
    default
  end
end