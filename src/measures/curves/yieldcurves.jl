# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _dummy_clamped_lin_intrp(t, xs, ys; dcc=DayCountConventions=ActAct())
  
  # Internal Facility
  Return a dumb linear interpolated value of `t` according to data points
  `xs` and `ys`. This has been built for yield curve processings, therefore
  is very oriented to a specific purpose.
  """
  function _dummy_clamped_lin_intrp(
                                    t::A,
                                    xs::AbstractVector,
                                    ys::AbstractVector;
                                    dcc::DayCountConventions=ActAct()
           ) where A<:Union{Tenor,<:Real}
    u = clamp(toyearfraction(dcc,t), first(xs),last(xs))
    i = searchsortedfirst(xs,u)
    δ = isone(i) ? 0 : /(u - xs[i-1], xs[i] - xs[i-1])
    isone(i) ? ys[i] : (1-δ) * getindex(ys,i-1) + δ * getindex(ys,i)
  end
  
  """
    YieldCurve <: AbstractCurves
  
  The yield curve, simply an ordered collection of `InterestRate`s.
  
  # Fields
  - `grip::Vector{InterestRate}`: the nodes of the yield curve.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  See also [`InterestRate`](@ref).
  
  # Constructors
      YieldCurve(ir::Vector{InterestRate}; [title::AbstractString])
      YieldCure(ir::InterestRate...; [title::AbstractString])
  
  ## Arguments
  - `ir`: either a vector, tuple, or a sequence of `InterestRate`s to splat
  - `title::AbstractString`: yield curve's title/theme name.
  
  # Notes
  We chose to represent a `YieldCurve` as a vector of `InterestRate`s objects,
  from it, one can retrieve the *term structure*, or *shape*, or *curve* of
  the `YieldCurve` object we are looking at, at a specific point in time.  
  What we mean by *curve* is actually a 1-dimensional object showing a
  structure when moving along its freedom axis.
  
  # Examples
  ```julia
  julia> yc = YieldCurve(title="€STR-Swaps");
  
  julia> isa(yc, YieldCurve)
  true
  ```
  """
  struct YieldCurve <: AbstractCurves
    grip::AbstractVector{InterestRate}
    params::Dict{AbstractString,Any}
    
    function YieldCurve(
                        ir::AbstractVector{U};
                        title::AbstractString=dummyticker()
             ) where U<:InterestRate
      if isempty(ir)
        new(ir,Dict{AbstractString,Any}("title" => title))
      else
        _inds = sortperm(collect(i.tenor for i in ir))
        new(ir[_inds],Dict{AbstractString,Any}("title" => title))
      end
    end
    
    function YieldCurve(
                        ir::InterestRate...;
                        title::AbstractString=dummyticker()
             )
      if isempty(ir)
        YieldCurve(InterestRate[],title=title)
      else
        YieldCurve(collect(ir),title=title)
      end
    end
  end
 

  """
      hasparams(yc::YieldCurve, tag::AbstractString)
  
  Return `true` if `tag` is present in `yc`'s parameters container, `false
  otherwise.
  
  See also [`YieldCurve`](@ref).
  
  # Arguments
  - `yc::YieldCurve`: the yield curve to check `tag` is parameter of.
  - `tag::AbstractString`: key to check.
  """
  function hasparams(yc::YieldCurve, tag::AbstractString)
    haskey(getfield(yc,:params),tag)
  end
  
  """
      setparams!(yc::YieldCurve,tag::AbstractString,value; force::Bool=false)
      setparams!(yc::YieldCurve,dict::Dict{String,Any}; force::Bool=false)
  
  Set the parameter tagged `tag` to hold the value `value`. If a parameter
  parameter tagged with `tag` is already present, throw an `ArgumentError`
  when `force=false`.
  """
  function setparams!(
                      yc::YieldCurve,
                      tag::AbstractString,
                      value::Any;
                      force::Bool=false
           )
    if hasparams(yc, tag) && ~force
      throw(ArgumentError("$(tag) exists! Use `force=true` to modify."))
    end
    yc.params[tag] = value
    tag => value
  end
  
  function setparams!(
                      yc::YieldCurve,
                      dict::Dict{AbstractString,Any};
                      force::Bool=false
           )
    collect(setparams!(yc, k, v, force=force) for (k,v) in dict)
  end
  
  
  """
      getparams(yc::YieldCurve, tag::AbstractString, default::Any)
  
  Return the value binded to `tag` from `yc`'s parameters container or
  `default` if the later can't be found.
  """
  function getparams(yc::YieldCurve, tag::AbstractString, default::Any)
    get(getfield(yc,:params), tag, default)
  end 
  
  function getparams(yc::YieldCurve, tag::AbstractString)
    getfield(yc,:params)[tag]
  end
  
  function Base.show(io::IO, ::MIME"text/plain", yc::YieldCurve)
    print(io, "YieldCurve[",getparams(yc,"title"),"]")
  end
  
  """
      getcurve(yc::YieldCurve, [t::TimePoint]; dcc=ActAct(), yf::Bool=false)
  
  Return the a `Tuple{Vector{Tenor},Vector{Real}}` or a 
  `Tuple{Vector{Real},Vector{Real}}` representing the yield curve termstructure
  at time `t`.
  
  # Arguments
  - `yc::YieldCurve`: yield curve to slice.
  - `t::TimePoint`: timepoint slice.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `yf::Bool=false`: kwarg, if `true` return tenors in year fractions.
  
  # Notes
  The return value is a 2-slots `Tuple` having in the first slot the `Tenor`s
  (or terms/maturities) and the associated rate in the yield curve present
  at `t`. If `t` is not specified, return the last observation; note that in
  this case, the facility does not check whenever the encapsulated
  `InterestRate`s have been observed at the same terminal point.
  
  # Examples
  ```julia
  julia> nothing
  ```
  """
  function getcurve(
                    yc::YieldCurve,
                    t::TimePoint;
                    dcc::DayCountConventions=ActAct(),
                    yf::Bool=false
           )
    if yf
      collect(toyearfraction(dcc,getfield(r,:tenor)) for r in yc),
      collect(S(r)(t) for r in yc)
    else
      collect(getfield(r,:tenor) for r in yc),
      collect(S(r)(t) for r in yc)
    end
  end
  
  function getcurve(
                    yc::YieldCurve;
                    dcc::DayCountConventions=ActAct(),
                    yf::Bool=false
           )
    if yf
      collect(toyearfraction(dcc,getfield(r,:tenor)) for r in yc),
      collect(S(r)() for r in yc)
    else
      collect(getfield(r,:tenor) for r in yc),
      collect(S(r)() for r in yc)
    end
  end
  
  """
       gettenors(yc::YieldCurve; dcc=ActAct(), yf::Bool=false)
  
  Return yield curve's tenors.
  
  ## Arguments
  - `yc::YieldCurve`: yield curve to process.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `yf::Bool=false`: if `true`, return ternors as year fractions.
  
  # Examples
  ```julia
  julia> ir1, ir2 = InterestRate(Tenor("1m")), InterestRate(Tenor("2m"));
  
  julia> yc = YieldCurve(ir1, ir2);
  
  julia> gettenors(yc, dcc=Thirty360())
  2-element Vector{Tenor{Month}}:
   Tenor[1 month]
   Tenor[2 months]
  
  julia> gettenors(yc, dcc=Thirty360(), yf=true)
  2-element Vector{Float64}:
   0.08333333333333333
   0.16666666666666666
  ```
  """
  function gettenors(
                     yc::YieldCurve;
                     dcc::DayCountConventions=ActAct(),
                     yf::Bool=false
           )
    if yf
      collect(toyearfraction(dcc,getfield(r,:tenor)) for r in yc)
    else
      collect(getfield(r,:tenor) for r in yc)
    end
  end
  
  Base.lastindex(yc::YieldCurve) = lastindex(getfield(yc,:grip))
  
  Base.firstindex(yc::YieldCurve) = firstindex(getfield(yc,:grip))
  
  Base.getindex(yc::YieldCurve,inds) = getindex(getfield(yc,:grip),inds)
  
  Base.first(yc::YieldCurve) = first(getfield(yc,:grip))
  
  Base.last(yc::YieldCurve) = last(getfield(yc,:grip))
  
  Base.isempty(yc::YieldCurve) = isempty(getfield(yc,:grip))
  
  Base.length(yc::YieldCurve) = length(getfield(yc,:grip))
  
  Base.iterate(yc::YieldCurve) = iterate(getfield(yc,:grip))
  
  Base.iterate(yc::YieldCurve, state) = iterate(getfield(yc,:grip),state)
  
  Base.only(yc::YieldCurve) = only(getfield(yc,:grip))
  
  Base.maximum(yc::YieldCurve) = getfield(last(yc),:tenor)
  
  Base.minimum(yc::YieldCurve) = getfield(first(yc),:tenor)
  
  function Base.extrema(yc::YieldCurve; init=(0,0))
    isempty(yc) ? init : (minimum(yc),maximum(yc))
  end
  
  function Base.merge(yc::YieldCurve, others::YieldCurve...)
    irs = union(getfield(yc,:grip), getfield(others,:grip)...)
    _it = sortperm([toyearfraction(r.tenor) for r in irs])
    _pr = merge(getfield(yc,:params), getfield.(others,:params)...)
    _yc = YieldCurve(irs[_it])
    setparams!(_yc, _pr, force=true)
    return _yc
  end
  
  """
      _search_first(yc::YieldCurve,t,T;dcc=ActAct())
      _search_first(yc::YieldCurve,ten::Tenor)
  
  # Internal Facility
  Return the index of the first tenor in `yc` which maturity/term
  designates a period that is not less than `T-t`. If all tenors
  in `yc` are ordered to be less than `T-t`, return `lastindex(yc)+1`.
  
  See also [`searchsortedfirst`](@ref).
  
  # Arguments
  - `yc::YieldCurve`: the yield curve to process from.
  - `t::TimePoint`: the yield curve is observed at `t`.
  - `T::TimePoint`: the timepoint to discount from.
  - `dcc::DayCountConventions`: kwarg, the day count convention.

  # Notes
  When the method `_search_first(yc::YieldCurve, ten::Tenor)` is used,
  the index of the first tenor in `yc` which is not less than `ten`. If
  all tenors are ordered before `ten`, return `lastindex(yc)+1`.
  """
  function _search_first(
                         yc::YieldCurve,
                         t::A,
                         T::B;
                         dcc::DayCountConventions=ActAct()
           ) where {A<:TimePoint,B<:TimePoint}
    _tns = getfield.(getfield(yc,:grip), :tenor)
    _tnp = map(v -> toyearfraction(dcc,v), _tns)
    searchsortedfirst(_tnp,yearfraction(dcc,t,T))
  end

  function _search_first(
                         yc::YieldCurve,
                         ten::Tenor;
                         dcc::DayCountConventions=ActAct()
           )
    searchsortedfirst(getfield.(getfield(yc,:grip),:tenor), ten)
  end

  """
      _search_last(yc::YieldCurve, t, T; dcc=ActAct())
      _search_last(yc::YieldCurve, ten::Tenor)
  
  # Internal Facility
  Return the index of the last tenor in `yc` which maturity/term
  designates a period that is not greater than `T-t`. If all tenors
  in `yc` are ordered to be greater than `T-t`, return `firstindex(yc)-1`.
  
  See also [`searchsortedlast`](@ref).
  
  # Arguments
  - `yc::YieldCurve`: the yield curve to process from.
  - `t::TimePoint`: the yield curve is observed at `t`.
  - `T::TimePoint`: the timepoint to discount from.
  - `dcc::DayCountConventions`: kwarg, the day count convention.

  # Notes
  When the method `_search_last(yc::YieldCurve, ten::Tenor)` is used,
  the index of the last tenor in `yc` which is not greater than `ten`. If
  all tenors are ordered after `ten`, return `firstindex(yc)-1`.
  """
  function _search_last(
                        yc::YieldCurve,
                        t::A,
                        T::B;
                        dcc::DayCountConventions=ActAct()
           ) where {A<:TimePoint,B<:TimePoint}
    _tns = getfield.(getfield(yc,:grip), :tenor)
    _tnp = map(v -> toyearfraction(dcc,v), _tns)
    searchsortedlast(_tnp,yearfraction(dcc,t,T))
  end

  function _search_last(
                        yc::YieldCurve,
                        ten::Tenor;
                        dcc::DayCountConventions=ActAct()
           )
    searchsortedlast(getfield.(getfield(yc,:grip),:tenor),ten)
  end
end