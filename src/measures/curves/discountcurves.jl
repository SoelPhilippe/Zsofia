# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _convert_rates(rate_old::Real, cmpf_old, cmpf_new)
  
  # Internal Facility
  Return the `cmpf_new`-times-per-year compounding rate level equivalent
  to the `cmpf_old`-times-per-year compounding rate level `rate_old`.
  
  ## Arguments
  - `rate_old::Real`: old rate level.
  - `cmpf_old::Union{Real,CompoundFrequencies`: old compounding frequency.
  - `cmpf_new::Union{Real,CompoundFrequencies`: new compounding frequency.
  """
  function _convert_rates(
                          rate_old::A,
                          cmpf_old::B,
                          cmpf_new::C
           ) where {A<:Real,B<:Real,C<:Real}
    if iszero(rate_old) || iszero(cmpf_old) || iszero(cmpf_new)
      0
    elseif isinf(cmpf_old) && isinf(cmpf_new)
      rate_old
    elseif isinf(cmpf_old) && ~isinf(cmpf_new)
      *(-(exp(/(rate_old,cmpf_old)),1), cmpf_new)
    elseif ~isinf(cmpf_old) && isinf(cmpf_new)
      _r∞(rate_old,cmpf_old)
    else
      *(cmpf_new,-(^(1+/(rate_old,cmpf_old),/(cmpf_old,cmpf_new)),1))
    end
  end
  
  function _convert_rates(
                         rate_old::R,
                         cmpf_old::Linearly,
                         cmpf_new::Linearly
           ) where R<:Real
    rate_old
  end
  
  function _convert_rates(
                         rate_old::R,
                         cmpf_old::Linearly,
                         cmpf_new::A
           ) where {R<:Real,A<:Real}
    *(cmpf_new,^(1+rate_old,inv(cmpf_new)))
  end
  
  function _convert_rates(
                          rate_old::R,
                          cmpf_old::A,
                          cmpf_new::Linearly
           ) where {R<:Real,A<:Real}
    ^(1+/(rate_old,cmpf_old),cmpf_old)-1
  end
  
  function _convert_rates(
                          rate_old::R,
                          cmpf_old::Continuous,
                          cmpf_new::Linearly
           ) where {R<:Real}
    exp(rate_old) - 1
  end

  function _convert_rates(
                          rate_old::R,
                          cmpf_old::Linearly,
                          cmpf_new::Continuous
           ) where {R<:Real}
    log(+(1,rate_old))
  end
  
  function _convert_rates(
                          rate_old::R,
                          cmpf_old::Continuous,
                          cmpf_new::Continuous
           ) where R<:Real
    rate_old
  end
  
  function _convert_rates(
                          rate_old::R,
                          cmpf_old::Continuous,
                          cmpf_new::A
           ) where {R<:Real,A<:Real}
    *(
      cmpf_new,
      exp(/(rate_old,cmpf_new)) - 1
    )
  end
  
  function _convert_rates(
                          rate_old::R,
                          cmpf_old::A,
                          cmpf_new::Continuous
           ) where {R<:Real,A<:Real}
    *(
      cmpf_old,
      log(1+/(rate_old,cmpf_old))
    )
  end
  
  
  """
      convertrate(oldrate::Real, oldcmpf, newcmpf)
  
  Return the `newcmpf`-times-per-year compounding (interest-)rate level
  equivalent to the given `oldcmpf`-times-per-year compounding interest
  rate level `oldrate`.
  
  ## Arguments
  - `oldrate::Real`: interest rate level we want to convert.
  - `oldcmpf::CompoundFrequencies`: old rate compounding frequency.
  - `newcmpf::CompoundFrequencies`: new rate compounding frequency.
  
  # Notes
  The function is intended to be used with *physical (numerical)* rate
  levels, `InterestRate`s type objects. See `Base.convert` for such
  a method which is intended to construct a new `InterestRate` object
  with its level adjusted to reflect a new compounding frequency
  (determined by its new `tenor`).
  
  When relevant, some kwargs are available to precize *compounding
  frequencies*. That's the case for example when the `Daily`
  commpounding frequency is used.
  
  # Examples
  ```julia
  julia> convertrate(0.05, Continuous(), Linearly())
  0.05127109637602412
  
  julia> convertrate(0.05, Quarterly(), Annually())
  0.05094533691406222
  
  julia> convertrate(0.05, 4, 1)
  0.05094533691406222
  ```
  """
  function convertrate(
                       oldrate::A,
                       oldcmpf::B,
                       newcmpf::C
           ) where {A<:Real,B<:Real,C<:Real}
    _convert_rates(oldrate, oldcmpf, newcmpf)
  end
  
  function convertrate(
                       oldrate::A,
                       oldcmpf::B,
                       newcmpf::C
           ) where {A<:Real,B<:CompoundFrequencies,C<:Real}
    _convert_rates(oldrate, _nwaar(oldcmpf), newcmpf)
  end

  function convertrate(
                       oldrate::A,
                       oldcmpf::B,
                       newcmpf::C
           ) where {
                    A<:Real,
                    B>:Daily,
                    C>:Daily
                   }
    _convert_rates(oldrate, _nwaar(oldcmpf), _nwaar(newcmpf))
  end
  
  function convertrate(
                        oldrate::A,
                        oldcmpf::B,
                        newcmpf::C
           ) where {A<:Real,B<:Real,C<:CompoundFrequencies}
    _convert_rates(oldrate, oldcmpf, _nwaar(newcmpf))
  end

  function convertrate(
                       oldrate::A,
                       oldcmpf::B,
                       newcmpf::C;
                       dcc::DayCountConventions=ActAct(),
                       isleap::Bool=false
           ) where {A<:Real,B<:Daily,C<:Union{<:Real,CompoundFrequencies}}
    _convert_rates(
                   oldrate,
                   _nwaar(oldcmpf,dcc,isleap),
                   _nwaar(newcmpf)
    )
  end
  
  function convertrate(
                       oldrate::A,
                       oldcmpf::B,
                       newcmpf::C;
                       dcc::DayCountConventions=ActAct(),
                       isleap::Bool=false
           ) where {A<:Real,B<:Union{Real,CompoundFrequencies},C<:Daily}
    _convert_rates(
                   oldrate,
                   _nwaar(oldcmpf),
                   _nwaar(newcmpf, dcc, isleap)
    )
  end
  
  function convertrate(
                       oldrate::A,
                       oldcmpf::B,
                       newcmpf::C;
                       dccold::DayCountConventions=ActAct(),
                       isleapold::Bool=false,
                       dccnew::DayCountConventions=ActAct(),
                       isleapnew::Bool=false
           ) where {A<:Real,B<:Daily,C<:Daily}
    _convert_rates(
                   oldrate,
                   _nwaar(oldcmpf, dccold, isleapold),
                   _nwaar(newcmpf, dccnew, isleapnew)
    )
  end
  
  """
      _forward_rate(ir1::Real, τ1::Real, ir2::Real, τ2::Real)
  
  # Internal Facility
  Infer the forward rate induced by continuously compounded rates `ir1` and
  `ir2` applied to tenors `τ1` and `τ2` respectively.
  """
  function _forward_rate(
                         ir1::A,
                         τ1::B,
                         ir2::C,
                         τ2::D
           ) where {A<:Real,B<:Real,C<:Real,D<:Real}
    /(-(ir2*τ2, ir1*τ1), τ2-τ1)
  end
  
  """
      forwardrate(ir1, cmpf1, t1, ir2, cmpf2, t2;cmpf=Continuous(),dcc=ActAct())
  
  Return the implied forward rate given the state of interest rates `(ir1, ir2)`.
  The compounding frequency of the return value being specified by the `cmpf`
  kwarg.
  
  # Arguments
  - `ir1::Real`: first interest rate level.
  - `cmpf1::Union{Real,CompoundFrequencies}`: `ir1`'s compounding frequency.
  - `ir2::Real`: second interest rate level.
  - `cmpf2::Union{Real,CompoundFrequencies}`: `ir2`'s compounding frequency.
  - `t1::Union{Real,Tenor}`: term 1.
  - `t2::Union{Real,Tenor}`: term 2.
  - `cmpf::Union{Real,CompoundFrequencies}=Continous()`: forward compound freq.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  Return the implied forward rate to be applied for the time/period-window
  equivalent to `t2-t2` running from a time span `t1`.
  """
  function forwardrate(
                       ir1::A,
                       cmpf1::Union{<:Real,CompoundFrequencies},
                       t1::Tenor,
                       ir2::B,
                       cmpf2::Union{<:Real,CompoundFrequencies},
                       t2::Tenor;
                       cmpf::Union{CompoundFrequencies,<:Real}=Continuous(),
                       dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real}
    r = _forward_rate(
                      convertrate(ir1,cmpf1,Continuous()),
                      toyearfraction(dcc,t1),
                      convertrate(ir2,cmpf2,Continuous()),
                      toyearfraction(dcc,t2)
        )
    convertrate(r,Continuous(),cmpf)
  end
  
  function forwardrate(
                       ir1::A,
                       cmpf1::Union{<:Real,CompoundFrequencies},
                       t1::C,
                       ir2::B,
                       cmpf2::Union{<:Real,CompoundFrequencies},
                       t2::D;
                       cmpf::Union{CompoundFrequencies,<:Real}=Continuous(),
                       dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,D<:Real}
    r = _forward_rate(
                      convertrate(ir1,cmpf1,Continuous()), t1,
                      convertrate(ir2,cmpf2,Continuous()), t2
        )
    convertrate(r,Continuous(),cmpf)
  end
  
  
  
  """
      _discount(ir::Union{Real,InterestRate}, cmpf, t, T; dcc)
  
  # Internal Facility
  Return the discount factor at `t` from `T`, corresponding to the interest
  rate level `ir` with a compounding frequency `cmpd` per year, where the
  coverage period between `T` and `t` is computed assuming the day count
  convention `dcc`.
  
  # Arguments
  - `ir::Union{Real,InterestRate}`: interest rate level.
  - `cmpf::Union{Real,CompoundFrequencies}`: compounding frequency.
  - `t::TimePoint`: discount to `t`.
  - `T::TimePoint`: discount from `T`.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  When `cmpf` compoundings per year is a number, it does mean that
  the compounding occurs `cmpf` times per year.
  """
  function _discount(
                     ir::A,
                     cmpf::B,
                     t::C,
                     T::D;
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:TimePoint,D<:TimePoint}
    if isinf(cmpf)
      exp(-yearfraction(dcc,t,T) * ir * sign(cmpf))
    elseif iszero(cmpf)
      1
    else
      ^(+(1, /(ir,cmpf)), -yearfraction(dcc,t,T)*cmpf)
    end
  end
  
  function _discount(
                     ir::A,
                     cmpf::Linearly,
                     t::C,
                     T::D;
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,C<:TimePoint,D<:TimePoint}
    /(1, +(1,*(ir,yearfraction(dcc,t,T))))
  end
  
  function _discount(
                     ir::A,
                     cmpf::Continuous,
                     t::C,
                     T::D;
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,C<:TimePoint,D<:TimePoint}
    _discount(ir, Inf, t, T, dcc=dcc)
  end
  
  function _discount(
                     ir::A,
                     cmpf::B,
                     t::C,
                     T::D;
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:CompoundFrequencies,C<:TimePoint,D<:TimePoint}
    _discount(ir,_nwaar(cmpf),t,T,dcc=dcc)
  end
  
  function _discount(
                     ir::A,
                     cmpf::Union{<:Real,CompoundFrequencies};
                     dcc::DayCountConventions=ActAct()
          ) where A<:Real
    _discount(ir,cmpf,0,toyearfraction(dcc,cmpf))
  end
  
  function _discount(
                     ir::InterestRate,
                     cmpf::A,
                     t::B,
                     T::C;
                     dcc::DayCountConventions=ActAct()
        ) where {A<:Union{Real,CompoundFrequencies},B<:TimePoint,C<:TimePoint}
    _discount(S(ir)(t),cmpf,t,T,dcc=dcc)
  end
  
  function _discount(
                     ir::InterestRate,
                     t::A,
                     T::B;
                     dcc::DayCountConventions=ActAct()
           ) where {A<:TimePoint,B<:TimePoint}
    _discount(ir,_compound_freq(getfield(ir,:tenor),dcc),t,T,dcc=dcc)
  end
  
  function _discount(
                     ir::InterestRate,
                     cmpf::Union{<:Real,CompoundFrequencies};
                     dcc::DayCountConventions=ActAct()
           )
    _discount(S(ir)(),cmpf,0,yearfraction(dcc,getfield(ir,:tenor)),dcc=dcc)
  end
  
  function _discount(ir::InterestRate;dcc::DayCountConventions=ActAct())
    _discount(ir,_compound_freq(getfield(ir,:tenor),dcc),dcc=dcc)
  end
  
  """
      discount(ir::Real, t::TimePoint, T::TimePoint; [dcc::DayCountConventions])
      discount(ir::InterestRate, t::TimePoint, T::TimePoint; [dcc])
  
  Return the discount factor inherent to the interest rate `ir`, compounding
  `cmpf` into an environment when the day count conventions is `dcc`.
  
  # Arguments
  - `ir::Union{Real,InterestRate}`: interest rate.
  - `cmpf::Union{Real,CompoundFrequency}`: compounding frequency.
  - `t::TimePoint`: discount to `t`.
  - `T::TimePoint`: discount from `T`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  """
  function discount(
                    ir::Union{InterestRate,<:Real},
                    cmpf::Union{<:Real,CompoundFrequencies},
                    t::A,
                    T::B;
                    dcc::DayCountConventions=ActAct()
           ) where {A<:TimePoint,B<:TimePoint}
    _discount(ir,cmpf,t,T,dcc=dcc)
  end
  
  function discount(
                    ir::Union{InterestRate,<:Real},
                    t::A,
                    T::B;
                    dcc::DayCountConventions=ActAct()
           ) where {A<:TimePoint,B<:TimePoint}
    _discount(ir,t,T,dcc=dcc)
  end

  function discount(
                    ir::Union{InterestRate,<:Real},
                    cmpf::Union{CompoundFrequencies,<:Real};
                    dcc::DayCountConventions=ActAct()
           )
    _discount(ir,cmpf,dcc=dcc)
  end
  
  function discount(ir::InterestRate; dcc::DayCountConventions=ActAct())
    inc = toyearfraction(dcc,getfield(ir,:tenor))
    _discount(ir,/(1,inc),0,inc,dcc=dcc)
  end
  
  """
      discount(yc::YieldCurve, t, T; dcc::DayCountConventions=ActAct())
  
  Return the discount factor from `T` to `t` inferred from the yield curve `yc`.
  
  # Arguments
  - `yc::YieldCurve`: yield curve.
  - `t::TimePoint`: discount to `t`.
  - `T::TimePoint`: discount from `T`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  """
  function discount(
                    yc::YieldCurve,
                    t::TimePoint,
                    T::TimePoint;
                    dcc::DayCountConventions=ActAct()
           )
    τ, i = yearfraction(dcc,t,T), _search_last(yc,t,T,dcc=dcc)
    s = if isless(i,firstindex(yc))
      toyearfraction(dcc,getfield(first(yc),:tenor))
    elseif isequal(i,lastindex(yc))
      +(1, -(τ, toyearfraction(dcc,getfield(last(yc),:tenor))))
    else
      -(
        toyearfraction(dcc,getfield(getindex(yc,i+1),:tenor)),
        toyearfraction(dcc,getfield(getindex(yc,i),:tenor))
      )
    end
    τ1 = if isless(i,firstindex(yc))
      0
    elseif isequal(i,lastindex(yc))
      τ - 1
    else
      toyearfraction(dcc,getfield(getindex(yc,i),:tenor))
    end
    δ = /(τ - τ1, s)
    a = if isless(i,firstindex(yc))
      1
    else
      discount(getindex(yc,i),t,T,dcc=dcc)
    end
    b = discount(getindex(yc,min(1+i,lastindex(yc))),t,T,dcc=dcc)
    (1-δ) * a * dirac(i,0,lastindex(yc)) + δ * b
  end
  
  function discount(
                    yc::YieldCurve,
                    ten::Tenor;
                    dcc::DayCountConventions=ActAct()
           )
    τ, i = toyearfraction(dcc,ten), _search_last(yc,ten,dcc=dcc)
    s = if isless(i,firstindex(yc))
      toyearfraction(dcc,getfield(first(yc),:tenor))
    elseif isequal(i,lastindex(yc))
      +(1,-(τ,toyearfraction(dcc,getfield(last(yc),:tenor))))
    else
      -(
        toyearfraction(dcc,getfield(getindex(yc,i+1),:tenor)),
        toyearfraction(dcc,getfield(getindex(yc,i),:tenor))
      )
    end
    τ1 = if isless(i,firstindex(yc))
      0
    elseif isequal(i,lastindex(yc))
      τ - 1
    else
      toyearfraction(dcc,getfield(getindex(yc,i),:tenor))
    end
    δ = /(τ - τ1, s)
    a = if isless(i,firstindex(yc))
      1
    else
      discount(getindex(yc,i),dcc=dcc)
    end
    b = begin
      discount(getindex(yc,min(i+1,lastindex(yc))),dcc=dcc)
    end
    (1-δ) * a * dirac(i,0,lastindex(yc)) + δ * b
  end
  
  """
      forwardrate(yc::YieldCurve,t,t1,t2;cmpf,dcc=ActAct())
      forwardrate(yc::YieldCurve,t,t1,ten;cmpf,dcc=ActAct())
  
  Return the `cmpf`-times-per-year compounding forward rate implied by the
  term structure at `t`, to be applied from `t1` to `t2`.

  ## Arguments
  - `yc::YieldCurve`: the yield curve to proceed from.
  - `t::TimePoint`: current information time point.
  - `t1::TimePoint`: the forward rate is to be applied from `t1`.
  - `t2::TimePOint`: the forward rate is to be applied to `t2`.
  - `ten::Tenor`: the forward rate is to be applied from `T1` over `ten`.
  - `cmpf::Union{<:Real,CompoundFrequencies}`: compounding frequency of fwdr.
  - `dcc::DayCountConventions`: day count convention.

  # Notes
  The first method computes `cmpf`-times-per-year compounding forward
  rate implied by the yield curve w.r.t. the information up to `t` to
  be applied from `t1` to `t2`.  
  The second method, does the same as the first one but the time span
  is specified by the `Tenor` object `ten`.

  # Examples
  ```
  julia>

  julia>
  ```
  """
  function forwardrate(
                       yc::YieldCurve,
                       t::TimePoint,
                       t1::TimePoint,
                       t2::TimePoint;
                       cmpf::Union{R,CompoundFrequencies}=Continuous(),
                       dcc::DayCountConventions=ActAct()
           ) where R<:Real
    p = /(discount(yc,t,t1,dcc=dcc),discount(yc,t,t2,dcc=dcc))
    τ = yearfraction(dcc,t1,t2)
    if isequal(cmpf,Continuous())
      inv(τ) * log(p)
    elseif isequal(cmpf,Linearly())
      -(p,1) * inv(τ)
    else
      k = _nwaar(cmpf)
      -(^(p,inv(k*τ)),1) * k
    end
  end

  function forwardrate(
                       yc::YieldCurve,
                       t::TimePoint,
                       t1::TimePoint,
                       ten::Tenor,
                       cmpf::Union{R,CompoundFrequencies}=Continuous(),
                       dcc::DayCountConventions=ActAct()
           ) where R<:Real
    forwardrate(yc,t,t1,+(t1,ten),cmpf=cmpf,dcc=dcc)
  end

  """
      annuity(yc, t, terms::Vector{TimePoint}; dcc=ActAct())
  
  Return the *annuity* level (or colloquially the *level*) of 1 currency
  unit accrued by coverages (time span between `terms`) to be received at each
  `terms` points.
  
  # Arguments
  - `yc::Union{YieldCurve,InterestRate}`: the term structure in action.
  - `t::TimePoint` discount to `t`.
  - `terms::TimePoint`: fixings `TimePoint`s.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Examples
  ```julia
  julia> ir = InterestRate(.05);
  
  julia> annuity(yc,0,1)
  0.9592326139088729
  ```
  """
  function annuity(
                   yc::Union{InterestRate,YieldCurve},
                   t::T,
                   terms::AbstractVector{V};
                   dcc::DayCountConventions=ActAct()
            ) where {T<:TimePoint,V<:Real}
    sum(
        .*(
           vcat(yearfraction(dcc,t,first(terms)), diff(dcc,terms)),
           map(v -> discount(yc,t,v), terms)
        )
    )
  end
  
  function annuity(
                   yc::Union{InterestRate,YieldCurve},
                   t::T,
                   terms::V;
                   dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,V<:Real}
    *(
      yearfraction(dcc,t,terms),
      discount(yc,t,terms)
    )
  end
end