# This script is part of Zsofia. Soel Philippe © 2025

begin
  abstract type AbstractCurves end
  
  abstract type Curves <: AbstractCurves end
end