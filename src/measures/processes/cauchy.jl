# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      CauchyProcess <: StochasticProcesses
      
  The Cauchy process.
  
  A pure-jump process in continuous time.
  
  # Fields
  - `kinda::NTuple{3, Int}`: generic information of the process (see below).
  - `times::Vector{Real}`: timestamps, observation time points.
  - `randz::Array{Real,3}`: random kernel (increments).
  - `paths::Array{Real,3}`: simulated paths.
  
  The property/field `kinda` holds a 3-slots tuple of `Int` gathering (resp.):
  1. number of timestamps (per path) i.e. number of steps + 1.
  1. number of paths (to be) generated.
  1. dimensionality of the process.
  
  ## Process dynamic/distribution
      ``X_{t+s}-X_{t} \\sim \\text{Cauchy}(0,s)``

  # Constructors
      CauchyProcess(n::Int, times::Vector{Real}; [dims::Int])
      CauchyProcess(n::Int; [nsteps::Int], [dims::Int])
  
  See also [`BesselProcess`](@ref), [`WienerProcess`](@ref).
  
  ## Arguments
  - `n::Int`: number of paths to simulate/generate.
  - `times::Vector{Real}`: time stamps.
  - `t::Real`: simulate up to time `t`.
  - `dims::Int=1`: kwarg, dimension.
  - `nsteps::Int=360`: kwarg, process' number time steps.
  
  # Notes
  Simulated paths are stored columnwise (first path into first column...).
  
  The implementation is the one of the standard Cauchy process with scale
  parameter γ equals to 1 and location at 0.
  
  We chose to generate simulations of the Cauchy standard (increments) through
  the inverse transform from uniform distribution between [0,1].
  """
  struct CauchyProcess <: StochasticProcesses
    kinda::NTuple{3,Int}
    times::AbstractVector{R} where R<:Real
    randz::AbstractArray{A,3} where A<:Real
    paths::AbstractArray{B,3} where B<:Real

    function CauchyProcess(
                           n::Int,
                           times::Vector{R};
                           dims::Integer=1
             ) where R<:Real
      @assert <=(1,n) "invalid number of paths, got $(n)"
      @assert <=(1,dims) "invalid dimension, got $(dims)"
      k = (length(times),n,dims)
      new(k,times,zeros(Real,k),zeros(Real,k))
    end
    
    function CauchyProcess(
                           n::Int,
                           t::R;
                           nsteps::Int=ceil(Int,360t),
                           dims::Int=1
             ) where R<:Real
      CauchyProcess(n,collect(LinRange(0,t,nsteps+1)),dims=dims)
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", proc::CauchyProcess)
    n, d = getfield(proc,:kinda)[2], getfield(proc,:kinda)[3]
    print(io,"CauchyProcess[",n," path" * ifelse(1<n,"s",""),", dim=",d,"]")
  end
  
  """
      simulate(proc::CauchyProcess)
      
  Return a tuple `(times, paths)` where `times` is the vector of time stamps
  and `paths` the array of generated/simulated paths (stored columnwise, i.e.
  the first path in the first column...).
  
  # Arguments
  - `cp::CauchyProcess`: an instance of a Cauchy process.
  
  # Examples
  ```julia
  julia> dim, nop, t = 4, 10, 1.0
  (4, 10, 1.0)
  
  julia> proc = CauchyProcess(nop,t,dims=dim);
  
  julia> iszero(proc)
  true
  
  julia> length(proc)
  361
  
  julia> proc isa CauchyProcess
  true
  ```
  """
  function simulate(proc::CauchyProcess)
    getfield(proc,:times),
    cumsum(
           vcat(0,diff(getfield(proc,:times))) .*
           tan.(π .* (rand(getfield(proc,:kinda)...) .- 1/2)),
           dims=1
    )
  end
  
  """
      simulate!(proc::CauchyProcess)
  
  `simulate` the Cauchy process `proc` and return the result in-place.
  
  See also [`simulate`](@ref), [`WienerProcess`](@ref).
  """
  function simulate!(proc::CauchyProcess)
    broadcast!(
               identity,
               getfield(proc,:randz),
               tan.(π .* (rand(getfield(proc,:kinda)...) .- 1/2))
    )
    broadcast!(
               identity,
               getfield(proc,:paths),
               cumsum(
                      vcat(0,diff(getfield(proc,:times))) .*
                      getfield(proc,:randz),
                      dims=1
               )
    )
    getfield(proc,:times), getfield(proc,:paths)
  end
  
  resimulate(proc::CauchyProcess) = simulate(proc)
  
  resimulate!(proc::CauchyProcess) = simulate!(proc)
end