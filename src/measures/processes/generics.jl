# This script is part of Zsofia. Soel Philippe © 2025

begin
  function StatsBase.mean(f::Function, sp::StochasticProcesses; dims=:)
    nothing
  end
  
  function StatsBase.var(f::Function , sp::StochasticProcesses; dims=:)
    nothing
  end
  
  function StatsBase.std(f::Function, sp::StochasticProcesses; dims=:)
    nothing
  end
  
  Base.size(p::StochasticProcesses) = getfield(p,:kinda)
  
  function Base.size(p::StochasticProcesses, dim::Integer)
    size(getfield(p,:paths),dim)
  end
  
  Base.length(p::StochasticProcesses) = length(getfield(p,:times))
  
  Base.eltype(p::StochasticProcesses) = eltype(getfield(p,:paths))
end