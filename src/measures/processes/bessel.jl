# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      BesselProcess <: StochasticProcess
  
  The Bessel process is a continuous-time stochastic process.
  
  The **Bessel process** is defined to be the ``d``-dimensional euclidian
  norm of the `d`-dimensional brownian motion (wiener process) of dimension `d`.
  
  # Fields
  - `kinda::NTuple{3,Int}`: generic information of the process (see below).
  - `times::Vector{Real}`: timestamps, observation time points.
  - `wiener::WienerProcess`: underlying wiener process.
  - `randz::Array{Real,3}`: random kernel (brownian increments).
  - `paths::Array{Real,3}`: simulated paths.
  
  The field `kinda` is a 3-slots `Tuple` of `Int` gathering (resp.):
  1. number of timestamps (per path) i.e. number of steps + 1.
  1. number of paths (to be) generated.
  1. dimensionality of the process.
  
  # Constructors
      BesselProcess(n::Int, t::Real; <kwargs>)
      BesselProcess(n::Int, times::Vector{Real}; <kwarg>)
  
  See also [`WienerProcess`](@ref).

  ## Arguments
  - `n::Int`: number of paths to simulate/generate.
  - `times::Vector{Real}`: time stamps.
  - `t::Real`: simulate up to time `t`.
  - `dims::Int=1`: kwarg, dimension.
  - `nsteps::Int=360`: kwarg, process' number time steps.

  # Notes
  The implementation is built on top of the `WienerProcess`' one.
  
  # Examples
  ```julia
  julia> bp = BesselProcess(10, 1.0, nsteps=360);

  julia> size(bp)
  (361, 10, 1)

  julia> length(bp)
  361
  ```
  """
  struct BesselProcess <: StochasticProcesses
    kinda::NTuple{3,Int}
    times::AbstractVector{R} where R<:Real
    wiener::WienerProcess
    randz::AbstractArray{A,3} where A<:Real
    paths::AbstractArray{B,2} where B<:Real
    
    function BesselProcess(
                           n::Int,
                           times::AbstractVector{R};
                           dims::Int=1
             ) where R<:Real
      @assert <=(1,n) "invalid number of paths, got $(n)"
      @assert <=(1,dims) "invalid dimension, got $(dims)"
      wp = WienerProcess(n,times,dims=dims)
      new(
          getfield(wp,:kinda),
          times,
          wp,
          view(getfield(wp,:randz),:,:,:),
          zeros(Float64,first(getfield(wp,:kinda)),n)
      )
    end

    function BesselProcess(
                           n::Int,
                           t::R;
                           nsteps::Int=ceil(Int,360t),
                           dims::Int=1
             ) where R<:Real
      BesselProcess(n, collect(LinRange(0,t,nsteps+1)), dims=dims)
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", bp::BesselProcess)
    n, d = getfield(bp,:kinda)[2], getfield(bp,:kinda)[3]
    print(io,"BesselProcess[",n," path" * ifelse(1<n,"s",""),", dim=",d,"]")
  end
  
  """
      simulate(bp::BesselProcess)
  
  Return a tuple `(times, paths)` where `times` is a vector of time stamps,
  and `paths` the `Matrix` of simulated paths (stored columnwise, i.e. first
  path into the first column, etc.).

  # Arguments
  - `bp:BesselProcess`: an instance of a `BesselProcess`.

  # Examples
  ```julia
  julia> dim, nop, t = 3, 10, 1.0
  (3, 10, 1.0)

  julia> bp = BesselProcess(nop,t,dims=dim);

  julia> bp isa BesselProcess
  true
  """
  function simulate(bp::BesselProcess)
     getfield(bp, :times),
     dropdims(
              mapslices(
                        norm,
                        last(simulate(getfield(bp,:wiener))),
                        dims=3
             ),
             dims=3
     )
  end

  resimulate(bp::BesselProcess) = simulate(bp)
  
  """
      simulate!(bp::BesselProcess)
  
  `simulate` the Bessel process `bp` and store the result in-place. Return
  the tuple `(times, paths)`.

  See also [`simulate`](@ref).
  """
  function simulate!(bp::BesselProcess)
    simulate!(getfield(bp,:wiener))
    broadcast!(
               identity,
               getfield(bp,:paths),
               dropdims(
                        mapslices(
                                  norm,
                                  getfield(getfield(bp,:wiener),:paths),
                                  dims=3
                        ),
                        dims=3
               )
    )
    getfield(bp,:times), getfield(bp,:paths)
  end
  
  resimulate!(bp::BesselProcess) = simulate!(bp)
end