# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      StochasticProcesses
      
  Abstract type enclosing stochastic processes.
  """
  abstract type StochasticProcesses end
end