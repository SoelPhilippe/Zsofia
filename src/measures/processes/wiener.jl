# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      WienerProcess <: StochasticProcesses
  
  Implementation of the Wiener process, a.k.a Brownian process.

  # Fields
  - `kinda::NTuple{3,Int}`: generic information of the process (see below).
  - `times::Vector{Real}`: time stamps.
  - `randz::Array{Real,3}`: random kernel of the process (gaussian increments).
  - `paths::Array{Real,3}`: brownian paths.

  The property `kinda` holds a 3-slots tuple of `Int`s gathering (resp.):  
  1. the number of points in `times`.
  1. the number of paths (to be) simulated.
  1. process dimensionality.
  
  # Constructors
      WienerProcess(n::Int, times::AbstractVector{Real}; dims::Int=1)
      WienerProcess(n::Int, t::Real; [nsteps::Int=360t, dims::Int=1])
  
  See also [`BesselProcess`](@ref).
  
  ## Arguments
  - `n::Int`: number of paths to (be) simulate(d).
  - `times::Vector{Real}`: time stamps, observation times.
  - `t::Real`: simulate up to time `t`.
  - `nsteps::Int=360t`: kwarg, number of time stamps steps.
  - `dims::Int=1`: kwarg, dimension of the process.
  
  # Notes
  Paths are stored columnwise, the path into the first column...
  
  The implementation does not check if the provided `times` are sorted, this
  is left to the user.
  
  # Examples
  ```julia
  julia> wp = WienerProcess(10, 1.0, dims=2);
  
  julia> iszero(wp.paths)
  true
  
  julia> simulate!(wp);
  
  julia> iszero(wp.paths)
  false
  
  julia> iszero(wp.paths[begin])
  true
  
  julia> size(wp)
  (361, 10, 2)
  
  julia> length(wp)
  361
  ```
  """
  struct WienerProcess <: StochasticProcesses
    kinda::NTuple{3,Int}
    times::AbstractVector{R} where R<:Real
    randz::AbstractArray{A,3} where A<:Real
    paths::AbstractArray{B,3} where B<:Real
    
    function WienerProcess(
                           n::Int,
                           times::AbstractVector{R};
                           dims::Int=1
             ) where R<:Real
      @assert <=(1,n) "invalid number of paths, got $(n)"
      @assert <=(1,dims) "invalid dimension, got $(dims)"
      k = (length(times),n,dims)
      new(k,collect(times),zeros(Float64,k),zeros(Float64,k))
    end

    function WienerProcess(
                           n::Int,
                           t::R;
                           nsteps::Int=ceil(Int,360t),
                           dims::Int=1
             ) where R<:Real
      @assert <=(1,nsteps) "provide a valid number of steps, got $(nsteps)"
      WienerProcess(n,collect(LinRange(0,t,nsteps+1)),dims=dims)
    end
  end

  const BrownianMotion = WienerProcess
  
  function Base.show(io::IO, ::MIME"text/plain", wp::WienerProcess)
    n, d = getfield(wp,:kinda)[2], getfield(wp,:kinda)[3]
    print(io,"WienerProcess[",n," path" * ifelse(1<n,"s",""),", dim=",d,"]")
  end

  """
      simulate(wp::WienerProcess)
  
  Return a tuple `(times, paths)` where `times` is a vector of simulation
  times and `paths` is an `Array{Real,3}` of d-dimensional paths
  (stored columnwise, i.e. the first path in the first column,
  the second path in the second column...).
  
  # Arguments
  - `wp::WienerProcess`: an instance of the Wiener process.
  
  # Examples
  ```julia
  julia> dim, nop, t = 4, 10, 1.0
  (4, 10, 1.0)
  
  julia> wp = WienerProcess(nop, t, dims=dim);
  
  julia> iszero(wp)
  true
  
  julia> length(wp)
  361
  
  julia> times, paths = simulate(wp);
  
  julia> isequal(times, wp.times)
  true
  
  julia> iszero(wp)
  true
  
  julia> size(wp.paths) == size(wp)
  true
  
  julia> iszero(paths)
  false
  ```
  """
  function simulate(wp::WienerProcess)
    (
     getfield(wp,:times),
     cumsum(
            vcat(0,sqrt.(diff(getfield(wp,:times)))) .*
            randn(getfield(wp,:kinda)),dims=1
     )
    )
  end
  
  resimulate(wp::WienerProcess) = simulate(wp)
  
  
  """
      simulate!(wp::WienerProcess)
  
  `simulate` the Wiener process `wp` and store the result in-place.
  
  Return a tuple `(times, paths)` where `times` is a vector of simulation
  times and `paths` is a `Matrix{Real}` of paths (stored columnwise, i.e.
  the first path in the first column, the second path in the second column...).
  
  # Arguments
  - `wp::WienerProcess`: an instance of the Wiener process.
  
  # Examples
  ```julia
  julia> dimension, number_of_paths, term_time = 4, 10, 1.0
  (4, 10, 1.0)
  
  julia> wp = WienerProcess(number_of_paths, term_time, dimension=dimension);
  
  julia> iszero(wp)
  true
  
  julia> length(wp)
  361
  
  julia> times, paths = simulate!(wp);
  
  julia> isequal(times, wp.times)
  true
  
  julia> iszero(wp)
  false
  
  julia> size(wp.paths) == size(wp)
  true
  
  julia> iszero(paths)
  false
  ```
  """
  function simulate!(wp::WienerProcess)
    broadcast!(identity, getfield(wp,:randz), randn(getfield(wp,:kinda)))
    broadcast!(
               identity,
               getfield(wp,:paths),
               cumsum(
                      vcat(0,sqrt.(diff(getfield(wp,:times)))) .*
                      getfield(wp,:randz),
                      dims=1
               )
    )
    (getfield(wp,:times), getfield(wp,:paths))
  end
  
  resimulate!(wp::WienerProcess) = simulate!(wp)
end