# This script is part of Zsofia. Soel Philippe © 2025

begin
   """
       BernoulliProcess <: StochasticProcess
       
   The Bernoulli process.
   
   A discrete-time stochastic process taking as values either `false` or `true`.
   
   # Fields
   - `kinda::NTuple{3,Int}`: generic information (see below).
   - `times::Vector{Real}`: timestamps, observation time points.
   - `p::Real`: probability of tossing 1/true.
   - `randz::Array{Bool,3}`: kernel simulation holder.
   - `paths::Array{Bool,3}`: a view on `randz`.
   
   Property `kinda` binds to a 3-slots `Tuple` of `Int` gathering (resp.):
   1. number of observations.
   1. number of paths (to be) simulated.
   1. dimensionality of the process.
   
   # Constructors
       BernoulliProcess(n::Int, [times::Vector{Real}]; [p::Real, dims::Int])
       BernoulliProcess(n::Int, m::Int; [p::Real, dims::Int])
       
   ## Arguments
   - `n::Int`: number of paths to simulate.
   - `times::AbstractVector{Real}=collect(1:n)`: observation times.
   - `m::Integer`: number of trials, number of nodes for each paths.
   - `p::Real`: kwarg, probability of tossing `1`/`true`.
   - `dims::Int=1`: kwarg, dimensionality of the process.
 
   # Notes
   Observation times are along axis 1.  
   For uniformity across processes, we kept the `paths` field while defining
   it as a view of `randz`.
   """
   struct BernoulliProcess <: StochasticProcesses
     kinda::NTuple{3,Int}
     times::AbstractVector{R} where R<:Real
     p::S where S<:Real
     randz::AbstractArray{Bool,3}
     paths::AbstractArray{Bool,3}
     
     function BernoulliProcess(
                               n::A,
                               times::Vector{R}=collect(0:n);
                               p::S=1//2,
                               dims::Int=1
              ) where {A<:Int, R<:Real, S<:Real}
       @assert <=(1,n) "invalid number of paths, got $(n)"
       @assert <=(1,dims) "invalid dimension, got $(dimension)"
       @assert (0 <= p <= 1) "invalid mass probability, got $(p)"
       randz = falses((length(times),n,dims))
       new(size(randz),times,p,randz,view(randz,:,:,:))
     end

     function BernoulliProcess(
                               n::Int,
                               m::Int;
                               p::S=1//2,
                               dims::Int=1
              ) where S<:Real
       BernoulliProcess(n,collect(0:m),p=p,dims=dims)
     end
   end
 
   """
       simulate(ber::BernoulliProcess)
   
   Return a tuple `(times, paths)` where `times` is a vector timestamps/trials count
   and `paths` the `Array{Bool,3}` of simulated paths (stored columnwise, i.e.
   the first path in the first column, etc.)
   
   # Arguments
   - `ber::BernoulliProcess`: an instance of the *Bernoulli* process.

   # Examples
   ```julia
   julia> dim, nop, ord = 3, 10, 46
   (3, 10, 46)

   julia> bp = BernoulliProcess(nop, ord, p=1/3, dimension=dim);

   julia> size(bp.paths)
   (47, 10, 3)

   julia> bp isa BernoulliProcess
   true
   ```
   """
   function simulate(ber::BernoulliProcess)
     if isone(first(getfield(ber,:kinda)))
       getfield(ber,:times), getfield(ber,:randz)
     else
       i,j,k = getfield(ber,:kinda)
       b = rand(Bool,i,j,k)
       b[1,:,:] .= zero(Bool)
       getfield(ber,:times), b
     end
   end
   
   resimulate(ber::BernoulliProcess) = simulate(ber)
 
   """
       simulate!(ber::BernoulliProcess)
       
   Simulate the process `ber` and store the proceed in-place.
   """
   function simulate!(ber::BernoulliProcess)
     t, r = simulate(ber)
     broadcast!(identity,getfield(ber,:randz),r)
     t,r
   end
   
   function Base.show(io::IO, ::MIME"text/plain", ber::BernoulliProcess)
     n, d = getfield(ber,:kinda)[2], getfield(ber,:kinda)[3]
     print(
           io,"BernoulliProcess[",n," path" * ifelse(1<n,"s",""),
           ", dim=",d,"](",getfield(ber,:p),")"
     )
   end
   
   resimulate!(ber::BernoulliProcess) = simulate!(ber)
   
   StatsBase.fit(::BernoulliProcess, x::AbstractVector{Bool}) = (p=mean(x),)
   
   StatsBase.fit(::BernoulliProcess,x::BitVector) = (p=mean(x),)
 end