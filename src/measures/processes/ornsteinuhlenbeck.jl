# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      OrnsteinUhlenbeckProcess <: StochasticProcesses
  
  The Ornstein-Uhlenbeck process.

  # Fields
  - `kinda::NTuple{3,Int}`: generic information of the process (see below).
  - `times::Vector{Real}`: time stamps.
  - `randz::Array{Real,3}`: random kernel of the process (increments).
  - `paths::Array{Real,3}`: Ornstein-Uhlenbeck paths.
  - `θ::Real`: force/speed of reversion to the mean.
  - `σ::Real`: the (absolute) volatility level.

  The property `kinda` holds a 3-slots tuple of `Int`s gathering (resp.):  
  1. the number of points in `times`.
  1. the number of paths (to be) simulated.
  1. process dimensionality.
  
  # Constructors
      OrnsteinUhlenbeckProcess(n,times;[theta, sigma, dims])
      OrnsteinUhlenbeckProcess(n, t; [theta, sigma, nsteps, dims])
  
  See also [`WienerProcess`](@ref).
  
  ## Arguments
  - `n::Int`: number of paths to (be) simulate(d).
  - `times::Vector{Real}`: time stamps, observation times.
  - `t::Real`: simulate up to time `t`.
  - `theta::Real=1`: parameter θ.
  - `sigma::Real=1`: parameter σ.
  - `nsteps::Int=360t`: kwarg, number of time stamps steps.
  - `dims::Int=1`: kwarg, dimension of the process.

  # Notes
  The Ornstein-Uhlenbeck process is a continuous-time process following the
  dynamic:
       ``dx_{t} = -θx_t dt + σdW_t``  
  
  Model parameters ``\\theta`` and ``\\sigma`` are assumed to be
  constants. When calibrating, if any, those parameters are going to be
  regarded as (resp.) the mean-reversion speed and the absolute
  volatility level.
  
  Whether or not the `times` arg is sorted is not asserted by constructor
  user's due diligence is required here.
  """
  struct OrnsteinUhlenbeckProcess <: StochasticProcesses
    kinda::NTuple{3,Int}
    times::AbstractVector{R} where R<:Real
    randz::AbstractArray{A,3} where A<:Real
    paths::AbstractArray{B,3} where B<:Real
    θ::K where K<:Real
    σ::L where L<:Real
    
    function OrnsteinUhlenbeckProcess(
                                      n::Int,
                                      times::AbstractVector{R};
                                      theta::A=1,
                                      sigma::B=1,
                                      dims::Int=1
             ) where {R<:Real,A<:Real,B<:Real}
      @assert <=(1,n) "invalid number of paths, got $(n)"
      @assert <=(1,dims) "invalid dimension, got $(dims)"
      k = (length(times), n, dims)
      new(k,times,zeros(Float64,k),zeros(Float64,k),theta,sigma)
    end
    
    function OrnsteinUhlenbeckProcess(
                                      n::Int,
                                      t::R;
                                      nsteps::Int=ceil(Int,360t),
                                      theta::A=1,
                                      sigma::B=1,
                                      dims::Int=1
             ) where {R<:Real,A<:Real,B<:Real}
      @assert <=(1,nsteps) "invalid number of steps, got $(nsteps)"
      OrnsteinUhlenbeckProcess(
                               n,
                               collect(LinRange(0,t,nsteps+1)),
                               theta=theta,
                               sigma=sigma,
                               dims=dims
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ou::OrnsteinUhlenbeckProcess)
    s = <(1,getfield(ou,:kinda)[2]) ? "s" : ""
    print(
          io, "OrnsteinUhlenbeckProcess[",getfield(ou,:kinda)[2]," path"*s,
          ", dim=", getfield(ou,:kinda)[3],"](θ = ",getfield(ou,:θ),
          ", σ = ",getfield(ou,:σ),")"
    )
  end

  """
      simulate(ou::OrnsteinUhlenbeckProcess; x0::Real=1)
  
  Return a tuple `(times, paths)` where `times` is the vector of time stamps
  and `paths` the array of generated/simulated paths (stored columnwise, i.e.
  the first path in the first column...).
  
  # Arguments
  - `ou::OrnsteinUhlenbeckProcess`: an instance of the OU-process.
  - `x0::Real=1`: kwarg, the starting point (level) of simulations.
  
  # Examples
  ```julia
  julia> ou = OrnsteinUhlenbeckProcess(10,1.0);
  
  julia> ou isa OrnsteinUhlenbeckProcess
  true
  ```
  """
  function simulate(ou::OrnsteinUhlenbeckProcess;x0::A=1) where A<:Real
    τ = vcat(0,diff(getfield(ou,:times)))
    ω = exp.(-getfield(ou,:θ) * τ)
    m = exp(mean(log.(ω)))
    x = randn(ou.kinda)
    broadcast!(
               identity,
               x,
               .*(x,getfield(ou,:σ) * sqrt.(/(1 .- .^(ω,2), 2getfield(ou,:θ))))
    )
    accumulate!(muladd(m), x, x, dims=1, init=/(x0,m))
    getfield(ou,:times), x
  end
  
  function resimulate(ou::OrnsteinUhlenbeckProcess;x0::A=1) where A<:Real
    simulate(ou,x0=x0)
  end
  
  """
      simulate!(ou::OrnsteinUhlenbeckProcess; x0::Real=1)
  
  `simulate` the Ornstein-Uhlenbeck process `ou` and store the result in-place.
  
  # Arguments
  - `ou::OrnsteinUhlenbeckProcess`: an instance of the OU-process.
  - `x0::Real=1`: starting point.
  
  # Notes
  To void loops, which are 10 times slower (the way we implemented), we
  made the choice of leverage the `muladd` function to transform the problem
  into a plain recurrence ̀``x(t) = αx(t-1) + ε(t)``. Where ``α`` is the
  geometric mean of the damping factors ``exp(θ(t_i - t_{i-1}))``. This
  generally work because most users use constant time steps adnd it's sightly
  better because it tries to capture non-homogeneous time steps while being
  worse than the general case.
  """
  function simulate!(ou::OrnsteinUhlenbeckProcess; x0::A=1.0) where A<:Real
    τ = vcat(0,diff(getfield(ou,:times)))
    ω = exp.(-getfield(ou,:θ) * τ)
    m = exp(mean(log.(ω)))
    broadcast!(identity, getfield(ou,:randz), randn(getfield(ou,:kinda)))
    broadcast!(
               identity,
               getfield(ou,:paths),
               .*(
                  getfield(ou,:randz),
                  getfield(ou, :σ) *
                  sqrt.(/(1 .- .^(ω,2), 2getfield(ou,:θ)))
               )
    )
    accumulate!(
                muladd(m),
                getfield(ou,:paths),
                getfield(ou,:paths),
                dims=1,
                init=/(x0,m)
    )
    getfield(ou,:times), getfield(ou,:paths)
  end
  
  resimulate!(ou::OrnsteinUhlenbeckProcess) = simulate!(ou)
end