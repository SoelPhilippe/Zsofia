# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      PoissonProcess <: StochasticProcess
      
  The Poisson point process.
  
  The Poisson process is a point process in a space where values are
  discretely scattered following a Poisson distribution.
  
  # Fields
  - `kinda::NTuple{3,Int}`: generic information of the process (see below).
  - `times::Vector{Real}`: time stamps.
  - `randz::Array{Real,3}`: scaled Poisson increments.
  - `paths::Array{Real,3}`: Poisson point process paths.
  - `λ::Function`: process' intensity rate.
  
  The property `kinda` holds a 3-slots tuple of `Int`s gathering (resp.):  
  1. the number of points in `times`.
  1. the number of paths (to be) simulated.
  1. process dimensionality.
  
  # Constructors
      PoissonProcess(n::Int,times::Vector,intensity::Union{Function,Real})
      PoissonProcess(n::Int,t::Real,intensity::Union{Function,Real})
  
  ## Arguments
  - `n::Int`: number of paths to simulate.
  - `times::Vector{Real}`: time stamps, observation times.
  - `t::Real`: simulate up to time `t`.
  - `λ::Union{Function,Real}`: intensity function, `Real` -> constant function.
  
  # Notes
  For now, higher dimensions (greater than 2) are not implemented.
  
  # Examples
  ```julia
  julia> pp1 = PoissonProcess(10, 1.0, x -> x^2);
  
  julia> pp1 isa PoissonProcess
  true
  
  julia> pp2 = PoissonProcess(10, 1.0, 3//2);
  
  julia> pp2 isa PoissonProcess
  true
  ```
  """
  struct PoissonProcess <: StochasticProcesses
    kinda::NTuple{3,Int}
    times::AbstractVector{R} where R<:Real
    randz::AbstractArray{A,3} where A<:Real
    paths::AbstractArray{B,3} where B<:Real
    λ::Function
    
    function PoissonProcess(
                            n::Int,
                            times::AbstractVector{R},
                            intensity::Function
             ) where R<:Real
      @assert <=(1,n) "invalid number of paths, got $(n)"
      @assert hasmethod(intensity,Tuple{<:Real}) "intensity should be a 1-arg"
      k = (length(times),n,1)
      new(k,times,zeros(Int,k),zeros(Int,k),intensity)
    end

    function PoissonProcess(
                            n::Int,
                            times::AbstractVector{R},
                            intensity::A
             ) where {R<:Real,A<:Real}
      _f(x::B) where B<:Real = intensity 
      PoissonProcess(n,times,_f)
    end
    
    function PoissonProcess(
                            n::Int,
                            t::R,
                            intensity::Function;
                            nsteps::Int=ceil(Int,360t)
             ) where R<:Real
      PoissonProcess(n,collect(LinRange(0,t,nsteps+1)),intensity)
    end
    
    function PoissonProcess(
                            n::Int,
                            t::R,
                            intensity::A;
                            nsteps::Int=ceil(Int,360t)
             ) where {R<:Real,A<:Real}
      _f(x::B) where B<:Real = intensity
      PoissonProcess(n,t,_f,nsteps=nsteps)
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", pp::PoissonProcess)
    s = <(1,getfield(pp,:kinda)[2]) ? "s" : ""
    print(
          io, "PoissonProcess[",getfield(pp,:kinda)[2]," path"*s,
          ", dim=", getfield(pp,:kinda)[3],"]"
    )
  end
  
  """
      simulate(pp::PoissonProcess)
  
  See also [`WienerProcess`](@ref).
  """
  function simulate(pp::PoissonProcess)
    if isless(length(getfield(pp,:times)),2)
      return getfield(pp,:times),getfield(pp,:paths)
    end
    rnd = ones(Int,size(getfield(pp,:paths)))
    int = map(
              (u,v) -> integrate(getfield(pp,:λ),u,v),
              circshift(view(getfield(pp,:times),:),1),
              getfield(pp,:times)
          )
    int[1] = 0
    broadcast!(
               identity,
               rnd,
               rand.(Poisson{Float64}.(int .* repeat(int,1,pp.kinda[2])))
    )
    cumsum!(rnd, rnd, dims=1)
    getfield(pp,:times), rnd
  end
  
  resimulate(pp::PoissonProcess) = simulate(pp)
  
  """
      simulate!(pp::PoissonProcess)
  
  Return a tuple `(times, paths)` where `times` is a vector timestamps/trials count
  and `paths` the `Array{Bool,3}` of simulated paths (stored columnwise, i.e.
  the first path in the first column, etc.)
  
  # Arguments
  - `pp::PoissonProcess`: an instance of the Poisson process.
  
  See also [`BesselProcess`](@ref).
  
  # Examples
  ```
  julia> nop, t, lambda = 10, 1.0, 0.98;
  
  julia> pp = PoissonProcess(nop, t, lambda);
  
  julia> iszero(pp.paths)
  true
  
  julia> simulate!(pp);
  ```
  """
  function simulate!(pp::PoissonProcess)
    if isless(length(getfield(pp,:times)),2)
      return getfield(pp,:times),getfield(pp,:paths)
    end
    n = getfield(pp,:kinda)[2]
    int = map(
              (u,v) -> integrate(getfield(pp,:λ),u,v),
              circshift(view(getfield(pp,:times),:),1),
              getfield(pp,:times)
          )
    int[1] = 0
    broadcast!(
               identity,
               getfield(pp,:randz),
               rand.(Poisson{Float64}.(int .* repeat(int,1,n)))
    )
    cumsum!(
            getfield(pp,:paths),
            getfield(pp,:randz),
            dims=1
    )
    getfield(pp,:times), getfield(pp,:paths)
  end
  
  resimulate!(pp::PoissonProcess) = simulate!(pp)
end