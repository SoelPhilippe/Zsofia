# evaluates
begin
  """
      evaluate(m::MonteCarlo, op, ...)
      evaluate(m::BlackScholes, op, ...)
      evaluate(m::Black76, op, ...)
              ...
      evaluate(m::AbstractModel, op, ...)
  
  Evaluate and return the value of `op` under the `MonteCarlo{M}` framework.
  
  # Arguments
  - `m::MonteCarlo{AbstractModel}`: a `MonteCarlo` model instance.
  - `op::BarrierOption`: an instance of a `BarrierOption`.
  - `par::NTuple{N,Real}`: values of required M's parameters.
  - `dt::Union{Period,Real}`: required simulation time step.
  - `t::TimePoint=op.inception`: kwarg, evaluation time point.
  - `n::Integer=1000`: kwarg, number of MonteCarlo simulations.
  - `nsteps::Integer=365`: kwarg, num of steps, underlying stochastic process.
  - `confint::Bool=false`: kwarg, if `true` return a confidence interval too.
  - `confintlevel::Real=0.95`: kwarg, confidence interval level.
  - `x0::Real=1.0`: risk factor starting point of simulations.
  - `dcc::DayCountConventions=ActAct()`: the day count convention to follow.
  - `rn::Bool=true`: if `true`, the risk neutral dynamic is assumed.

  If `confint=true` the tuple `(estimated_price, (lower_ci, upper_ci))` is
  returned, otherwise the scalar `estimated_price` is returned.
  
  # Notes
  When necessary, the underlying `Observables` of the `BarrierOption` is
  sollicited, for example to get some parameters value.
  
  # Examples
  ```julia
  julia> pltr = Stock("PLTR");
  
  julia> strike, expiry = 60.00, Date(2025,1,31);
  
  julia> ccy, dcc = Currency("USD"), Thirty360();
  
  julia> bs = BlackScholes(0.041037,currency=ccy);
  
  julia> inception = Date(2025,1,3);
  
  julia> op = BarrierOption(
                             strike, expiry, option=Put,
                             underlying=pltr, inception=inception
              );
  
  julia> insertprices!(pltr, 79.89, inception);
  
  julia> setparams(pltr, "sigma", 0.6327, force=true);
  
  julia> evaluate(bs, op, sigma=getparams(pltr, "sigma"), dcc=dcc)
  0.2566547270622772
  ```
  """
  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::BarrierOption,
                    par::NTuple{2,R},
                    dt::Union{Period,<:Real};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=1.0,
                    nsteps::Integer=365,
                    confint::Bool=false,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct(),
                    rn::Bool=false
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::BarrierOption,
                    obs::Observables,
                    dt::Union{<:Real,Period};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=1.0,
                    nsteps::Integer=365,
                    confint::Bool=true,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct(),
                    rn::Bool=true
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end

  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::BarrierOption,
                    dt::Union{Period,<:Real};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=1.0,
                    nsteps::Integer=365,
                    confint::Bool=false,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct(),
                    rn::Bool=false
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  
  """
      _evaluate(m::MonteCarlo, fi::BarrierOption; <kwargs>)
  
  # Dummy evaluator for people in a hurry call it with `Zsofia._evaluate`.
  
  Return the Monte-Carlo simulation price of the given financial instrument.
  
  # Arguments
  - `m::MonteCarlo`: a monte carlo instance.
  - `op::Derivatives`: the financial instrument for which pricing is required.
  
  # Notes
  """
  function _evaluate(
                     ::MonteCarlo{M},
                     ::BarrierOption;
           ) where M<:AbstractModel
    nothing
  end
  
  """
      evaluate!(m::MonteCarlo, op, par, dt, <kwargs>)
      evaluate!(m::MonteCarlo, op, obs, dt, <kwargs>)
      evaluate!(m::MonteCarlo, op, dt, <kwargs>)
  
  `evaluate` and store the result in-place into `op`.
  
  See also [`evaluate`](@ref).
  """
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::BarrierOption,
                     par::NTuple{2,R},
                     dt::Union{Period,<:Real};
                     t::T=getfield(op, :inception),
                     n::Integer=1000,
                     x0::A=1.0,
                     nsteps::Integer=365,
                     confint::Bool=false,
                     confintlevel::B=0.95,
                     dcc::DayCountConventions=ActAct(),
                     rn::Bool=false
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::BarrierOption,
                     obs::Observables,
                     dt::Union{<:Real,Period};
                     t::T=getfield(op, :inception),
                     n::Integer=1000,
                     x0::A=1.0,
                     nsteps::Integer=365,
                     confint::Bool=true,
                     confintlevel::B=0.95,
                     dcc::DayCountConventions=ActAct(),
                     rn::Bool=true
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::BarrierOption,
                     dt::Union{<:Real,Period};
                     t::T=getfield(op, :inception),
                     n::Integer=1000,
                     x0::A=1.0,
                     nsteps::Integer=365,
                     confint::Bool=false,
                     confintlevel::B=0.95,
                     dcc::DayCountConventions=ActAct(),
                     rn::Bool=false
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  """
      evaluate(m::BlackScholes, op::BarrierOption, [t::TimePoint; <kwargs>])
  
  Return the value of the `BarrierOption` `op` under `m::BlackScholes`.
  
  See also [`BlackScholes`](@ref), [`BarrierOption`](@ref).
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  The `BlackScholes` struct type gathers relevant interest rates parameters
  to evaluate the price. `σ` is a valid for the volatility instead of `sigma`,
  for those preferring unicode notations.  
  If you need a "dummy" evaluator, struct-free evaluator, take a look
  at the `_evaluate` method, see [`_evluate`](@ref).
  
  # Examples
  ```julia-repl
  julia> 
  
  julia> 
  
  julia> 
  ```
  """
  function evaluate(
                m::BlackScholes,
                op::BarrierOption,
                t::T=getfield(op, :inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    nothing
  end
  
  function evaluate!(
                m::BlackScholes,
                op::BarrierOption,
                t::T=getfield(op, :inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    nothing
  end
  
  """
      _evaluate(::BlackScholes, ::BarrierOption; <kwargs>)
  
  # Dummy evaluator for people in a hurry call it with `Zsofia._evaluate`.
  
  """
  function _evaluate(::BlackScholes, ::BarrierOption)
    nothing # todo
  end
  
  
  """
      delta(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the delta (Δ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function delta(
                 m::BlackScholes,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      gamma(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the gamma (Γ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function gamma(
                 m::BlackScholes,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      theta(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the theta (Θ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function theta(
                 m::BlackScholes,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      vega(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the vega (V) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function vega(
                m::BlackScholes,
                op::BarrierOption,
                t::T=getfield(op,:inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      rho(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the rho (ρ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function rho(
               m::BlackScholes,
               op::BarrierOption,
               t::T=getfield(op,:inception),
               x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
               sigma::B=1.0,
               carryrate::C=0.0,
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      epsilon(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the rho (ε) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  This is the sentivity of the european price with respect to carryrate.
  """
  function epsilon(
                 m::BlackScholes,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      omega(m::BlackScholes, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the omega (ω) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  Omega, also known as elasticity or lambda, if the percentage change in
  option value per percentage change in the underlying price.

  ``\\frac{\\partial{V}}{\\partial{S}}\\times\\frac{S}{V}``
  """
  function omega(
                 m::BlackScholes,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    *(
      delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
      /(
        x,
        evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
    )
  end

  """
      commongreeks(m::BlackScholes, op::BarrierOption, [t], [x]; what, <kwargs>)
  
  Return the greek `what` of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::BlackScholes`: a Black-Scholes model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-Scholes framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  - `what::Union{Symbol,AbstractString}`: kwarg, identifying the greek.

  # Notes
  |**Params**|**Description**|
  |:---:|:---:|
  |`:all`|return all the greeks|
  |`:delta`|return the delta|
  |`:gamma`|return the gamma|
  |`:omega`|return the omeage|
  |`:rho`|return the rho|
  |`:theta`|return the theta|
  |`:vega`|return the vega|
  """
  function commongreeks(
                m::BlackScholes,
                op::BarrierOption,
                t::T=getfield(op,:inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct(),
                what::Union{AbstractString,Symbol}=:all
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
     if isequal(lowercase(string(what)), "all")
      (
       Δ = delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Γ = gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       V = vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Ρ = rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Θ = theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Λ = omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
     elseif isequal(lowercase(string(what)), "delta")
       delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "gamma")
       gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "vega")
       vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "rho")
       rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "theta")
       theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "omega")
       omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     else
       throw(
             ArgumentError(
                          "choose amongst the following \n" *
                          ":all, :delta, :gamma, :theta, :omega, :vega or :rho"
             )
       )
     end
   end
end


#> Black76
begin
  function evaluate(
                    m::MonteCarlo{Black76},
                    op::BarrierOption,
                    par::NTuple{2,R},
                    dt::Union{Period,<:Real};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=1.0,
                    nsteps::Integer=365,
                    confint::Bool=false,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct(),
                    rn::Bool=false
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  function evaluate(
                    m::MonteCarlo{Black76},
                    op::BarrierOption,
                    obs::Observables,
                    dt::Union{<:Real,Period};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=1.0,
                    nsteps::Integer=365,
                    confint::Bool=true,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct(),
                    rn::Bool=true
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end

  function evaluate(
                    m::MonteCarlo{Black76},
                    op::BarrierOption,
                    dt::Union{Period,<:Real};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=1.0,
                    nsteps::Integer=365,
                    confint::Bool=false,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct(),
                    rn::Bool=false
           ) where {T<:TimePoint,A<:Real,B<:Real}
    evaluate(
             m, op, getfield(op, :underlying), dt, t=t, n=n, x0=x0,
             nsteps=nsteps, confint=confint, confintlevel=confintlevel,
             dcc=dcc, rn=rn
    )
  end
  
  """
      evaluate!(m::MonteCarlo, op, par, dt, <kwargs>)
      evaluate!(m::MonteCarlo, op, obs, dt, <kwargs>)
      evaluate!(m::MonteCarlo, op, dt, <kwargs>)
  
  `evaluate` and store the result in-place into `op`.
  
  See also [`evaluate`](@ref).
  """
  function evaluate!(
                     m::MonteCarlo{Black76},
                     op::BarrierOption,
                     par::NTuple{2,R},
                     dt::Union{Period,<:Real};
                     t::T=getfield(op, :inception),
                     n::Integer=1000,
                     x0::A=1.0,
                     nsteps::Integer=365,
                     confint::Bool=false,
                     confintlevel::B=0.95,
                     dcc::DayCountConventions=ActAct(),
                     rn::Bool=false
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  function evaluate!(
                     m::MonteCarlo{Black76},
                     op::BarrierOption,
                     obs::Observables,
                     dt::Union{<:Real,Period};
                     t::T=getfield(op, :inception),
                     n::Integer=1000,
                     x0::A=1.0,
                     nsteps::Integer=365,
                     confint::Bool=true,
                     confintlevel::B=0.95,
                     dcc::DayCountConventions=ActAct(),
                     rn::Bool=true
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  function evaluate!(
                     m::MonteCarlo{Black76},
                     op::BarrierOption,
                     dt::Union{<:Real,Period};
                     t::T=getfield(op, :inception),
                     n::Integer=1000,
                     x0::A=1.0,
                     nsteps::Integer=365,
                     confint::Bool=false,
                     confintlevel::B=0.95,
                     dcc::DayCountConventions=ActAct(),
                     rn::Bool=false
           ) where {T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
  
  """
      evaluate(m::Black76, op::BarrierOption, [t::TimePoint; <kwargs>])
  
  Return the value of the `BarrierOption` `op` under `m::Black76`.
  
  See also [`Black76`](@ref), [`BarrierOption`](@ref).
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  The `Black76` struct type gathers relevant interest rates parameters
  to evaluate the price. `σ` is a valid for the volatility instead of `sigma`,
  for those preferring unicode notations.  
  If you need a "dummy" evaluator, struct-free evaluator, take a look
  at the `_evaluate` method, see [`_evluate`](@ref).
  
  # Examples
  ```julia-repl
  julia> 
  
  julia> 
  
  julia> 
  ```
  """
  function evaluate(
                m::Black76,
                op::BarrierOption,
                t::T=getfield(op, :inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    nothing
  end
  
  function evaluate!(
                m::Black76,
                op::BarrierOption,
                t::T=getfield(op, :inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    nothing
  end
  
  """
      _evaluate(::Black76, ::BarrierOption; <kwargs>)
  
  # Dummy evaluator for people in a hurry call it with `Zsofia._evaluate`.
  
  """
  function _evaluate(::Black76, ::BarrierOption)
    nothing # todo
  end
  
  
  """
      delta(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the delta (Δ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function delta(
                 m::Black76,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      gamma(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the gamma (Γ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function gamma(
                 m::Black76,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      theta(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the theta (Θ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function theta(
                 m::Black76,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      vega(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the vega (V) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function vega(
                m::Black76,
                op::BarrierOption,
                t::T=getfield(op,:inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      rho(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the rho (ρ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function rho(
               m::Black76,
               op::BarrierOption,
               t::T=getfield(op,:inception),
               x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
               sigma::B=1.0,
               carryrate::C=0.0,
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      epsilon(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the rho (ε) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  This is the sentivity of the european price with respect to carryrate.
  """
  function epsilon(
                 m::Black76,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    nothing
  end

  """
      omega(m::Black76, op::BarrierOption, [t], [x]; <kwargs>)
  
  Return the omega (ω) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  Omega, also known as elasticity or lambda, if the percentage change in
  option value per percentage change in the underlying price.

  ``\\frac{\\partial{V}}{\\partial{S}}\\times\\frac{S}{V}``
  """
  function omega(
                 m::Black76,
                 op::BarrierOption,
                 t::T=getfield(op,:inception),
                 x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                 sigma::B=1.0,
                 carryrate::C=0.0,
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    *(
      delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
      /(
        x,
        evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
    )
  end

  """
      commongreeks(m::Black76, op::BarrierOption, [t], [x]; what, <kwargs>)
  
  Return the greek `what` of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::Black76`: a Black-76 model instance.
  - `op::BarrierOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=1.0`: kwarg, volatility (w.r.t. Black-76 framework) level.
  - `carryrate::Real=0.0`: kwarg, continuous carry rate adjustement.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  - `what::Union{Symbol,AbstractString}`: kwarg, identifying the greek.

  # Notes
  |**Params**|**Description**|
  |:---:|:---:|
  |`:all`|return all the greeks|
  |`:delta`|return the delta|
  |`:gamma`|return the gamma|
  |`:omega`|return the omeage|
  |`:rho`|return the rho|
  |`:theta`|return the theta|
  |`:vega`|return the vega|
  """
  function commongreeks(
                m::Black76,
                op::BarrierOption,
                t::T=getfield(op,:inception),
                x::A=(isempty(op.underlying) ? op.strike : S(op.underlying)());
                sigma::B=1.0,
                carryrate::C=0.0,
                dcc::DayCountConventions=ActAct(),
                what::Union{AbstractString,Symbol}=:all
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
     if isequal(lowercase(string(what)), "all")
      (
       Δ = delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Γ = gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       V = vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Ρ = rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Θ = theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Λ = omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
     elseif isequal(lowercase(string(what)), "delta")
       delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "gamma")
       gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "vega")
       vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "rho")
       rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "theta")
       theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(lowercase(string(what)), "omega")
       omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     else
       throw(
             ArgumentError(
                          "choose amongst the following \n" *
                          ":all, :delta, :gamma, :theta, :omega, :vega or :rho"
             )
       )
     end
   end
end