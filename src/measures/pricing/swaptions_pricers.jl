# This script is part of Zsofia. Soel Philippe © 2025

begin
  function _evaluate(
                     ::Type{Black76},
                     ::Type{Swaption},
                     K::M,
                     t::V,
                     T::V,
                     x::A,
                     σ::B,
                     fixings::AbstractVector{C},
                     yc::Union{InterestRate,YieldCurve},
                     carryrate::D,
                     notional::L=1,
                     ispayer::Bool=true,
                     dcc::DayCountConventions=ActAct()
          ) where {V<:TimePoint,A<:Real,B<:Real,C<:Real,D<:Real,L<:Real,M<:Real}
    ω = 2ispayer - 1
    τ = yearfraction(dcc,t,T)
    δ = exp(-yearfraction(dcc,t,T) * carryrate)
    *(
      notional,
      annuity(yc, t, fixings, dcc=dcc),
      -(
        x * δ * Ξ(ω * _dp(K, x * exp(-τ * carryrate), sqrt(τ) * σ)),
        K * Ξ(ω * _dm(K, x * exp(-τ * carryrate), sqrt(τ) * σ))
      ),
      ω
    )
  end
         
                     
  """
      evaluate(m::MonteCarlo, op::Swaption, [t, x, <kwargs>])
      evaluate(m::Black76, op::Swaption, [t, x, <kwargs>])
  
  Evaluate the `Swaption` `op` under the `m` model framework.
  
  # Arguments
  - `m::AbstractModel`: a model instance.
  - `op::Swaption`: swaption to price.
  - `t::TimePoint`: evaluate at `t`.
  - `x::Real`: underlying swap rate level.
  - `sigma::Real=getparams(op.underlying,"sigma")`: kwarg, volatility level.
  - `carryrate::Real=0`: kwarg, carryrate.
  - `yc::Union{YieldCurve,InterestRate}`: kwarg, term-structure of rates.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  See also [`Swap`](@ref), [`EuropeanOption`](@ref).
  """
  function evaluate(
                    m::Black76,
                    op::Swaption,
                    t::T=getfield(op,:inception),
                    x::A=S(op.underlying)(t);
                    sigma::B=getparams(op.underlying,"sigma"),
                    carryrate::C=getparams(op.underlying,"carryrate"),
                    yc::Union{InterestRate,YieldCurve}=m.riskfreerate,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    ω = 2op.underlying.ispayer - 1
    δ = exp(-yearfraction(dcc,t,getfield(op,:expiry)) * carryrate)
    *(
      op.underlying.notional,
      annuity(yc, t, getfield(op.underlying,:fixingsfix), dcc=dcc),
      -(
        x * δ * Ξ(ω * _dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)),
        op.strike * Ξ(ω * _dm(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))
      ),
      (getfield(op,:inception) <= t <= getfield(op,:expiry)),
      ω
    )
  end
end