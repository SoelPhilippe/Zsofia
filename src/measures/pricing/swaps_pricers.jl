# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      evaluate(m::MonteCarlo{M},swap::Swap,par::NTuple; <kwargs>)
      evaluate(m::MonteCarlo{M},swap::Swap,par::NTuple,yc::YieldCurve;<kwargs>)
  
  Evaluate and return the value of the swap contract `swap` using
  Monte Carlo simulations under the assumptions of the framework
  model `M`.

  # Arguments
  - `m::MonteCarlo{M}`: monte carlo instance under model `M`.
  - `swap::Swap`: the swap contract to evaluate.
  - `yc::YieldCurve`: the discount curve to use.
  - `t::TimePoint`: kwarg, evaluate `swap` at time point `t`.
  - `n::Int=1000`: kwarg, number of simulation paths to generate.
  - `x0::Real=S(swap.underlying)(t)`: kwarg, risk factor starting point.
  - `nsteps::Int=ceil(Int,360t)`: kwarg, lower level number of steps.
  - `confint::Bool=false`: kwarg, if `true` return a confidence interval.
  - `confintlevel::Real=/(95,100)`: kwarg, confidence level.
  - `dcc::DayCountConventions=ActAct()`: day count conventions.

  # Notes
  When `evaluate`ing a `Swap`, the swap rate is returned instead of the
  dollar value of the contract.  
  When the method with `yc::YieldCurve` is used the yield curve `yc`
  is used for discounting purposes: this relates to multi (dual) curves
  contexts where we have a funding curve having underlying interest rates
  different from underlying contract's risk factor. 
  """
  function evaluate(
                    m::MonteCarlo{Vasicek},
                    swap::Swap,
                    par::NTuple{2,R},
                    yc::YieldCurve;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(op.underlying)(t),
                    confint::Bool=false,
                    confintlevel::B=/(95,100),
                    dcc::DayCountConventions=ActAct()
            ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    nothing
  end
end