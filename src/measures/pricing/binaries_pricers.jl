# This script is part of Zsofia. Soel Philippe © 2025

begin
  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::BinaryOption,
                    par::NTuple{2,R};
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    @assert isless(1,n)
    @assert ~confint || (0 < confintlevel < 1)
    if isless(n,30)
      @warn "low number of simulations, got $(n): accuracy concerns"
    end
    τ = yearfraction(dcc,t,getfield(op,:expiry))
    d = discount(
                 getfield(getfield(m,:model),:riskfreerate),
                 t,getfield(op,:expiry),dcc=dcc
        )
    β = first(par) - 0.5last(par)^2
    x = x0 * exp.(*(β,τ) .+ last(par) * sqrt(τ) * randn(n))
    p = map(v -> payoff(op,v,getfield(op,:expiry)),x)
    if confint
      l = /(std(p) * quantile(Normal(), (1-confintlevel)/2), sqrt(n))
      m = mean(p) * d
      m, (max(0,m+l), m-l)
    else
      mean(p) * d
    end
  end
  
  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::BinaryOption,
                    obs::Observables;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    for f in getfield(getfield(m,:model),:required)
      @assert hasparams(obs,f) "required parameter $(f) is missing"
    end
    par = [getparams(obs,f) for f in getfield(getfield(m,:model),:required)]
    opt = only(getfield(getfield(m,:model),:optional))
    par[1] -= getparams(obs,opt,0)
    par[1] += S(getfield(getfield(m,:model),:riskfreerate))(t)
    evaluate(
             m, op, Tuple(par), t=t, n=n, x0=x0, confint=confint,
             confintlevel=confintlevel, dcc=dcc
    )
  end

  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::BinaryOption;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    evaluate(
             m, op, getfield(op,:underlying), t=t, n=n, x0=x0,
             confint=confint, confintlevel=confintlevel, dcc=dcc
    )
  end
  
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::BinaryOption,
                     par::NTuple{2,R};
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, par, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::BinaryOption,
                     obs::Observables;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, obs, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::BinaryOption;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate(
                    m::BlackScholes,
                    op::BinaryOption,
                    t::T=getfield(op,:inception),
                    x::A=S(op.underlying)(t);
                    sigma::B=getparams(op.underlying,"sigma"),
                    carryrate::C=getparams(op.underlying,"carryrate"),
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    ω = 2isequal(getfield(op,:option),Call) - 1
    *(
      discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc),
      Ξ(ω * _dm(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))
    )
  end
  
  function evaluate!(
                     m::BlackScholes,
                     op::BinaryOption,
                     t::T=getfield(op,:inception),
                     x::A=S(getfield(op,:underlying))(t);
                     sigma::B=getparams(op.underlying,"sigma"),
                     carryrate::C=getparams(op.underlying,"carryrate"),
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    p = evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    insertprices!(op, p, t)
    p
  end
  
  function delta(
                 m::BlackScholes,
                 op::BinaryOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(op.underlying,"sigma"),
                 carryrate::C=getparams(op.underlying,"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:option),Call)-1
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    δ = exp(-τ * carryrate)
    /(
      ξ(_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)) * δ * ω,
      sigma * getfield(op,:strike) * sqrt(τ)
    )
  end

  function gamma(
                 m::BlackScholes,
                 op::BinaryOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:inception))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:option),Call)-1
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    δ = exp(-τ * carryrate)
    /(
      -ω * δ * _dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc) *
      ξ(_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)),
      x * getfield(op,:strike) * τ * sigma^2
    )
  end
  
  function theta(
                 m::BlackScholes,
                 op::BinaryOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(op.underlying,"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(op.option,Call)-1
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    r = S(getfield(m, :riskfreerate))(t)
    a = r * evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    b = -(
          /(_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc), 2τ),
          /(r - carryrate, sigma * sqrt(τ))
        )
    a + ω * d * ξ(_dm(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)) * b
  end
  
  function vega(
                m::BlackScholes,
                op::BinaryOption,
                t::T=getfield(op,:inception),
                x::A=S(getfield(op,:underlying))(t);
                sigma::B=getparams(op.underlying,"sigma"),
                carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(op.option,Call)-1
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    *(
      _dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc),
      -ξ(_dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      /(1, sigma) * ω * d
    )
  end
  
  function rho(
               m::BlackScholes,
               op::BinaryOption,
               t::T=getfield(op,:inception),
               x::A=S(getfield(op,:underlying))(t);
               sigma::B=getparams(op.underlying,"sigma"),
               carryrate::C=getparams(op.underlying,"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(op.option,Call)-1
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    -(
      ω * d * /(sqrt(τ),sigma) *
      ξ(_dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      τ * evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    )
  end
  
  function epsilon(
                   m::BlackScholes,
                   op::BinaryOption,
                   t::T=getfield(op,:inception),
                   x::A=S(getfield(op,:underlying))(t);
                   sigma::B=getparams(op.underlying,"sigma"),
                   carryrate::C=getparams(op.underlying,"carryrate"),
                   dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(op.option,Call)-1
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    *(
      -ω * d * ξ(_dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      /(sqrt(τ), sigma)
    )
  end
  
  function omega(
                 m::BlackScholes,
                 op::BinaryOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(op.underlying,"sigma"),
                 carryrate::C=getparams(op.underlying,"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    /(
      delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
      evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    ) * x
  end

  function vanna(
                 m::Black76,
                 op::BinaryOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:option),Call)-1
    a = *(
          ξ(_dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
          _dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc),
          _dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)
        ) - 1
    /(a * delta(m,op,t,x,sigma=sigma,carryrate=carryrate,dcc=dcc),sigma) * ω
  end

  function commongreeks(
                        m::BlackScholes,
                        op::BinaryOption,
                        t::T=getfield(op,:inception),
                        x::A=S(getfield(op,:underlying))(t);
                        what::Union{AbstractString,Symbol}=:all,
                        sigma::B=getparams(getfield(op,:underlying),"sigma"),
                        carryrate::C=getparams(op.underlying,"carryrate"),
                        dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
     gr = lowercase(string(what))
     vl = ("delta","gamma","vega","rho","theta","omega","vanna","all")
     @assert in(gr,vl) throw(ArgumentError("invalid [what] argument, see doc"))
     if isequal(gr,"all")
      (
       Δ = delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Γ = gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       V = vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Ρ = rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Θ = theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Λ = omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
     elseif isequal(gr, "delta")
       delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "gamma")
       gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "vega")
       vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "rho")
       rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "theta")
       theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "omega")
       omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "vanna")
       vanna(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     end
  end
end