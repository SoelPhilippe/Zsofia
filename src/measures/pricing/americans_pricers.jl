# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      evaluate(m::LongstaffSchwartz, op::AmericanOption, dt,<kwargs>)
  
  Compute and return an estimation of `op`'s price under the model `m`.

  # Arguments
  - `m::LongstaffSchwartz`: an instance of the model.
  - `op::AmericanOption`: the option required to be priced out.
  - `t::TimePoint`: `evaluate` the option at time `t`.
  - `from::TimePoint`: kwarg, simulate from `from` to `to`.
  - `to::TimePoint`: kwarg, simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`.
  - `n::Int`: kwarg, number of paths to simulate.
  - `nsteps::Int`: number of steps of the lower level model (`m.model`)
  - `x0::Real`: starting level point of the underlying risk factor.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `confint::Bool=false`: whether to return the confidence interval.
  - `confintlevel::Real=0.95`: confidence interval level.
  
  See also [`MonteCarlo`](@ref), [`BlackScholes`](@ref).
  
  # Notes
  If `confint` is `true`, the return value is the tuple
  `(estimated_price, (lower_ci, upper_ci))`.
  """
  function evaluate(
                    m::LongstaffSchwartz{M},
                    op::AmericanOption{U},
                    t::TimeType;
                    from::TimeType=t,
                    to::TimeType=op.expiry,
                    dt::Period=Day(1),
                    n::Int=500,
                    nsteps::Int=360,
                    x0::A=1.0,
                    dcc::DayCountConventions=ActAct(),
                    confint::Bool=false,
                    confintlevel::B=0.95
           ) where {M<:AbstractModel,U<:Observables,A<:Real,B<:Real}
      _t, _pees = simulate(
                           m.model, op.underlying, from, to, dt,
                           n=n, nsteps=nsteps, x0=x0, dcc=dcc
                  )
      _disc = discount.(m.model.riskfreerate,_t,op.expiry)
      acfls = payoff.(op, _pees, t)
      dcfls = _disc .* acfls
  end

  function evaluate(
                    m::LongstaffSchwartz{M},
                    op::AmericanOption{U},
                    t::A;
                    from::B=t,
                    to::C=op.expiry,
                    dt::D=/(-(to,from),360),
                    n::Int=500,
                    nsteps::Int=360,
                    x0::E=1.0,
                    dcc::DayCountConventions=ActAct(),
                    confint::Bool=false,
                    confintlevel::F=0.95
           ) where {
                    M<:AbstractModel,U<:Observables,A<:Real,B<:Real,
                    C<:Real,D<:Real,E<:Real,F<:Real
                   }
    nothing
  end

  function evaluate(
                    m::LongstaffSchwartz{M},
                    op::AmericanOption{U};
                    from::TimePoint=op.inception,
                    to::TimePoint=op.expiry,
                    dt::Union{Period,<:Real}=ifelse(isa(to,TimeType),Day(1),1/360),
                    n::Int=500,
                    nsteps::Int=360,
                    x0::A=1.0,
                    dcc::DayCountConventions=ActAct(),
                    confint::Bool=false,
                    confintlevel::B=0.95
           ) where {M<:AbstractModel,U<:Observables,A<:Real,B<:Real}
    evaluate(
             m, op, op.inception, from=from, to=to, dt=dt,
             n=n, nsteps=nsteps, x0=x0, dcc=dcc, confint=confint,
             confintlevel=confintlevel
    )
  end
end