# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _dummy_vasicek_zcb_price(inc, mat, notio, θ, μ, σ, dcc)
  
  # Internal Facility
  Return the `Vasicek` model, zero coupon bond price.
  
  ## Arguments
  - `mat::TimePoint`: bond maturity date.
  - `notio::Real`: bond's notional/principal.
  - `θ::Real`: first parameter of Vasicek model, see [`Vasicek`](@ref).
  - `μ::Real`: second parameter of Vasicek model, see [`Vasicek`](@ref).
  - `σ::Real`: last paramater of Vasicek model, see [`Vasicek`](@ref).
  - `dcc::DayCountConventions`: day cont convention to use.
  - `t::TimePoint`: actual time point.
  - `r::Real`: risk factor level, the short rate level at `inc`.
  """
  function _dummy_vasicek_zcb_price(
                                    mat::T,
                                    notio::A,
                                    θ::B,
                                    μ::C,
                                    σ::D,
                                    dcc::DayCountConventions,
                                    t::T,
                                    r::L
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real,D<:Real,L<:Real}
    *(
      _a_vasicek(t,mat,θ,μ,σ,dcc),
      exp(_b_vasicek(t,mat,θ,σ,dcc) * r * -1),
      notio
    )
  end
  
  """
      evaluate(m::Vasicek, zb::ZeroBond, par, <kwargs>)
      evaluate(m::Vasicek, zb::ZeroBond, <kwargs>)
  
  Evaluate and return the value of the pure-discount zero coupon bond `zb`
  under the [`Vasicek`](@ref) model assumptions.
  
  # Arguments
  - `m::Vasicek`: Vasicek model instance.
  - `zb::ZeroBond`: zero coupon bond to evaluate.
  - `par::NTuple`: required Vasicek parameters, see [`Vasicek`](@ref).
  - `r::Real`: risk factor (interest rate) level at `t`.
  - `t::TimePoint=zb.inception`: evaluate at `t`.
  - `notional::Real=1`: principal amount tied to the bond.
  - `dcc::DayCountConventions=ActAct()`: the day count convention.
  
  # Notes
  The Vasicek model has an analytic formula for the pure-discount
  zero coupon bond price ``P=Ae^{Br_t}``, the term ``r_t`` corresponding
  to the interest rate level `r` to provide as argument to the `evaluate`
  method.  
  When the second method is used, parameters are inferred from `zb`'s
  parameters container.
  """
  function evaluate(
                    m::Vasicek,
                    zb::ZeroBond,
                    par::NTuple{3,R},
                    r::A;
                    t::B=getfield(zb,:inception),
                    notional::C=getfield(zb,:notional),
                    dcc::DayCountConventions=getfield(zc,:dcc)
           ) where {R<:Real,A<:Real,B<:TimePoint,C<:Real}
    _dummy_vasicek_zcb_price(
                             getfield(zb,:maturity), notional, first(par),
                             par[2], last(par), dcc, t, r
    )
  end
  
  function evaluate(
                    m::Vasicek,
                    zb::ZeroBond,
                    r::A;
                    t::B=getfield(zb,:inception),
                    notional::C=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:TimePoint,C<:Real}
    for k in getfield(m, :required)
      @assert hasparams(zb, k) "required parameter $(k) can't be found"
    end
    evaluate(
             m, zb, Tuple(getparams(zb,k) for k=getfield(m,:required)),
             r, t=t, notional=notional, dcc=dcc
    )
  end
  
  """
      evaluate(m::Vasicek, op::EuropeanOption{ZeroBond}, t, x; <kwargs>)
  
  Evaluate and return the zero-coupon bond european option `op` under
  the vasicek model, with required parameters given as kwargs.
  
  # Arguments
  - `m::Vasicek`: Vasicek model instance.
  - `op::EuropeanOption{ZeroBond}`: the zcb option to evaluate.
  - `t::TimePoint`: evaluate at time `t`.
  - `r::Real=S(op.underlying)(t)`: underlying risk short-rate level ;
  - `theta::Real`: kwarg, first required vasicek model parameter.
  - `mu::Real`: kwarg, second required vasicek model parameter.
  - `sigma::Real`: kwarg, third required vasicek model parameter.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  The discount factor kwarg argument `df` defaults to
  """
  function evaluate(
                   m::Vasicek,
                   op::EuropeanOption{ZeroBond},
                   t::T,
                   r::A;
                   theta::K=getparams(op.underlying,"θ",0),
                   mu::L=getparams(op.underlying,"μ",0),
                   sigma::M=getparams(op.underlying,"σ",0),
                   dcc::DayCountConventions=ActAct()
          ) where {A<:Real,T<:TimePoint,K<:Real,L<:Real,M<:Real}
    δ  = <=(getfield(op,:expiry),getfield(getfield(op,:underlying),:maturity))
    τ  = yearfraction(dcc,t,getfield(op,:expiry))
    s = *(
          sigma,
          sqrt(/(1-exp(-2theta*τ), 2theta)),
          _b_vasicek(
                     getfield(op,:expiry),
                     getfield(getfield(op,:underlying),:maturity),
                     theta, mu, sigma, dcc
          )
        )
    ω = 2isequal(op.option,Call)-1
    _p2 = begin
      getfield(op,:strike) *
      _dummy_vasicek_zcb_price(
                               getfield(op,:expiry),
                               getfield(getfield(op,:underlying),:notional),
                               theta, mu, sigma, dcc, t, r 
      )
    end
    _p1 = begin
      _dummy_vasicek_zcb_price(
                               getfield(getfield(op,:underlying),:maturity),
                               getfield(getfield(op,:underlying),:notional),
                               theta, mu, sigma, dcc, t, r 
      )
    end
    h = inv(s) * log(/(_p1,_p2)) + 0.5s
    -(_p1 * Ξ(ω * h), _p2 * Ξ(ω * (h-s))) * δ
  end
end