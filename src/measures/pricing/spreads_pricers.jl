# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      evaluate(m::MonteCarlo{Tuple{M,N}},op::SpreadOption,par1,par2,<kwargs>)
  
  Evaluate and return the value of `op` using Monte Carlo simulations under the
  assumptions of framework models `M` and `N`.
  
  # Arguments
  - `m::MonteCarlo{M,N}`: monte carlo on models `M` and `N`.
  - `op::SpreadOption`: underlying spread option.
  - `par1::NTuple`: `op.underlying1`'s parameters under model `M`.
  - `par2::NTuple`: `op.underlying2`'s parameters under model `N`.
  - `dt::Union{Period,Real}`: when relevant, higher-level simulation time step.
  - `t::TimePoint=op.inception`: kwarg, evaluate `op` at time `t`.
  - `n::Int=1000`: number of MonteCarlo simulations.
  - `x01::Real=S(op.underlying1)(t)`: kwarg, starting point, risk factor 1.
  - `x02::Real=S(op.underlying2)(t)`: kwarg, starting point, risk factor 2.
  - `corr::Real=0`: correlation between the two underlying risk factors.
  - `rfr::InterestRate=m.model[1].riskfreerate`: for discounting purposes.
  - `nsteps::Int=360`: kwarg, lower level simulations number of steps.
  - `confint::Bool=false`: kwarg, if `true` return a confidence interval aside.
  - `confintlevel::Real=0.95`: kwarg, confidence level (relevant if `confint`).
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  nothing
  """
  function evaluate(
                    m::MonteCarlo{Tuple{BlackScholes,BlackScholes}},
                    op::SpreadOption,
                    par1::NTuple{2,R1},
                    par2::NTuple{2,R2};
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x01::A1=S(getfield(op,:underlying1))(t),
                    x02::A2=S(getfield(op,:underlying2))(t),
                    corr::B=0,
                    rfr::InterestRate=getfield(m,:model)[1].riskfreerate,
                    confint::Bool=false,
                    confintlevel::C=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,T<:TimePoint,A1<:Real,
                    A2<:Real,B<:Real,C<:Real
                   }
    @assert isless(1, n)
    @assert ~confint || (0 < confintlevel < 1)
    if isless(n,30)
      @warn "low number of simulations, got $(n): accuracy concerns"
    end
    τ  = yearfraction(dcc, t, getfield(op,:expiry))
    d  = discount(rfr, t, getfield(op,:expiry), dcc=dcc)
    β1 = first(par1) - 0.5last(par1)^2
    β2 = first(par2) - 0.5last(par2)^2
    ε1 = randn(n)
    ε_ = randn(n)
    ε2 = corr * ε1 .+ sqrt(1 - corr^2) * ε_
    x1 = x01 * exp.(*(β1,τ) .+ last(par1) * sqrt(τ) * ε1)
    x2 = x02 * exp.(*(β2,τ) .+ last(par2) * sqrt(τ) * ε2)
    p  = map((u,v) -> payoff(op, u, v, getfield(op,:expiry)), x1, x2)
    if confint
      l = /(std(p * d) * quantile(Normal(), (1-confintlevel)/2), sqrt(n))
      m = mean(p) * d
      m, (max(0,m+l), m-l)
    else
      mean(p) * d
    end
  end
  
  function evaluate(
                    m::MonteCarlo{Tuple{BlackScholes,BlackScholes}},
                    op::SpreadOption,
                    obs1::Observables,
                    obs2::Observables;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x01::A1=S(getfield(op,:underlying1))(t),
                    x02::A2=S(getfield(op,:underlying2))(t),
                    corr::B=0,
                    rfr::InterestRate=m.model[1].riskfreerate,
                    confint::Bool=false,
                    confintlevel::C=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A1<:Real,A2<:Real,B<:Real,C<:Real}
    for f in getfield(getfield(m,:model)[1],:required)
      @assert hasparams(obs1,f) "required parameter $(f) is missing for obs1"
    end
    for f in getfield(getfield(m,:model)[2],:required)
      @assert hasparams(obs2,f) "required parameter $(f) is missing for obs2"
    end
    par1 = [getparams(obs1,f) for f=getfield(getfield(m,:model)[1],:required)]
    par2 = [getparams(obs2,f) for f=getfield(getfield(m,:model)[2],:required)]
    opt1 = only(getfield(getfield(m,:model)[1],:optional))
    opt2 = only(getfield(getfield(m,:model)[2],:optional))
    par1[1] -= getparams(obs1,opt1,0)
    par2[1] -= getparams(obs2,opt2,0)
    par1[1] += S(rfr)(t)
    par2[1] += S(rfr)(t)
    begin
      evaluate(
               m, op, Tuple(par1), Tuple(par2), t=t, n=n,
               x01=x01, x02=x02, corr=corr, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
  end

  function evaluate(
                    m::MonteCarlo{Tuple{BlackScholes,BlackScholes}},
                    op::SpreadOption;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x01::A1=S(getfield(op,:underlying1))(t),
                    x02::A2=S(getfield(op,:underlying2))(t),
                    corr::B=0,
                    rfr::InterestRate=m.model[1].riskfreerate,
                    confint::Bool=false,
                    confintlevel::C=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A1<:Real,A2<:Real,B<:Real,C<:Real}
    evaluate(
             m, op, getfield(op,:underlying1), getfield(op,:underlying2),
             t=t, n=n, x01=x01, x02=x02, corr=corr, rfr=rfr, confint=confint,
             confintlevel=confintlevel, dcc=dcc
    )
  end

  function evaluate!(
                     m::MonteCarlo{Tuple{BlackScholes,BlackScholes}},
                     op::SpreadOption,
                     par1::NTuple{2,R1},
                     par2::NTuple{2,R2};
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x01::A1=S(getfield(op,:underlying1))(t),
                     x02::A2=S(getfield(op,:underlying2))(t),
                     corr::B=0,
                     rfr::InterestRate=m.model[1].riskfreerate,
                     confint::Bool=false,
                     confintlevel::C=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real, R2<:Real, A1<:Real, A2<:Real, T<:TimePoint,
                    B<:Real,C<:Real
                   }
    _ev = begin
      evaluate(
               m, op, par1, par2, t=t, n=n, x01=x01, x02=x02,
               corr=corr, confint=confint, confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{Tuple{BlackScholes,BlackScholes}},
                     op::SpreadOption,
                     obs1::Observables,
                     obs2::Observables;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x01::A1=S(getfield(op,:underlying1))(t),
                     x02::A2=S(getfield(op,:underlying2))(t),
                     corr::B=0,
                     rfr::InterestRate=m.model[1].riskfreerate,
                     confint::Bool=false,
                     confintlevel::C=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A1<:Real,A2<:Real,B<:Real,C<:Real}
    _ev = begin
      evaluate(
               m, op, obs1, obs2, t=t, n=n, x01=x01, x02=x02, corr=corr,
               rfr=rfr, confint=confint, confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end

  function evaluate!(
                     m::MonteCarlo{Tuple{BlackScholes,BlackScholes}},
                     op::SpreadOption;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x01::A1=S(getfield(op,:underlying1))(t),
                     x02::A2=S(getfield(op,:underlying2))(t),
                     corr::B=0,
                     rfr::InterestRate=m.model[1].riskfreerate,
                     confint::Bool=false,
                     confintlevel::C=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A1<:Real,A2<:Real,B<:Real,C<:Real}
    _ev = begin
      evaluate(
               m, op, t=t, n=n, x01=x01, x02=x02, corr=corr,
               rfr=rfr, confint=confint, confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  """
      evaluate(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>)
  
  Evaluate the `SpreadOption` `op` under the `m::Margrabe`'s model assumptions.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`Margrabe`](@ref).
  
  # Notes
  The `Margrabe` model assumes geometric brownian motion dynamics for
  each of the risk factors.  
  The pricing is made using a change of numeraire technique switching to
  the second underlying risk factor's induced numeraire before switching
  back to initial numeraire.
  """
  function evaluate(
                    m::Margrabe,
                    op::SpreadOption,
                    t::TimePoint=getfield(op,:inception),
                    x1::R1=S(getfield(op,:underlying1))(t),
                    x2::R2=S(getfield(op,:underlying2))(t);
                    sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                    sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                    carryrate1::B1=op.underlying1.params["carryrate"],
                    carryrate2::B2=op.underlying2.params["carryrate"],
                    corr::C=0,
                    dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    ω = 2isequal(op.option,Call)-1
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    δ1, δ2 = exp(-τ * carryrate1), exp(-τ * carryrate2)
    -(
      x1 * δ1 *
      Ξ(ω * _dp(
                m,op,t,x1=x1,x2=x2,σ1=sigma1,σ2=sigma2,carryrate1=carryrate1,
                carryrate2=carryrate2, corr=corr, dcc=dcc
            )
      ),
      x2 * δ2 *
      Ξ(ω * _dm(
                m, op,t,x1=x1,x2=x2,σ1=sigma1,σ2=sigma2,carryrate1=carryrate1,
                carryrate2=carryrate2, corr=corr, dcc=dcc
            )
      ) * ω * isless(0,τ)
    )
  end

  function evaluate!(
                     m::Margrabe,
                     op::SpreadOption,
                     t::TimePoint=getfield(op,:inception),
                     x1::R1=S(getfield(op,:underlying1))(t),
                     x2::R2=S(getfield(op,:underlying2))(t);
                     sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                     sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                     carryrate1::B1=op.underlying1.params["carryrate"],
                     carryrate2::B2=op.underlying2.params["carryrate"],
                     corr::C=0,
                     dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    p = begin
      evaluate(
               m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
               carryrate1=carryrate1, carryrate2=carryrate2, corr=corr,
               dcc=dcc
      )
    end
    insertprices!(op, p, t)
    p
  end


  """
      delta(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the delta (Δ) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`gamma`](@ref).
  
  # Notes
  Since spread options have two risk factors as underlyings, some
  sensitivities are multi-dimensional. This is the case for the `delta` which
  is the gradient with respect to risk factors.
  """
  function delta(
                 m::Margrabe,
                 op::SpreadOption,
                 t::TimePoint=getfield(op,:inception),
                 x1::R1=S(getfield(op,:underlying1))(t),
                 x2::R2=S(getfield(op,:underlying2))(t);
                 sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                 sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                 carryrate1::B1=op.underlying1.params["carryrate"],
                 carryrate2::B2=op.underlying2.params["carryrate"],
                 corr::C=0,
                 dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    ω = 2isequal(op.option,Call)-1
    τ  = yearfraction(dcc, t, getfield(op, :expiry))
    δ1 = exp(-τ * carryrate1)
    δ2 = exp(-τ * carryrate2)
    _u = begin
      ω * δ1 * Ξ(ω * _dp(
                         m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
                         carryrate1=carryrate1, carryrate2=carryrate2,
                         corr=corr, dcc=dcc
                     )
               )
    end
    _v = begin
      -ω * δ2 * Ξ(ω * _dm(
                         m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
                         carryrate1=carryrate1, carryrate2=carryrate2,
                         corr=corr, dcc=dcc
                     )
               )
    end
    [_u, _v]
  end

  """
      gamma(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the gamma (Γ) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  Spread options are two risk factor options so that some of their
  sensitivities are multi-dimensional. This is indeed the case for
  the gamma: the return value of this function is a matrix.
  
  See also [`omega`](@erf).
  """
  function gamma(
                 m::Margrabe,
                 op::SpreadOption,
                 t::TimePoint=getfield(op,:inception),
                 x1::R1=S(getfield(op,:underlying1))(t),
                 x2::R2=S(getfield(op,:underlying2))(t);
                 sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                 sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                 carryrate1::B1=op.underlying1.params["carryrate"],
                 carryrate2::B2=op.underlying2.params["carryrate"],
                 corr::C=0,
                 dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    τ = yearfraction(dcc,t,getfield(op, :expiry))
    sigma = sqrt(sigma1^2 + sigma2^2 - 2corr*sigma1*sigma2)
    _d = _dp(
             m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
             carryrate1=carryrate1, carryrate2=carryrate2,
             corr=corr, dcc=dcc
         )
    a = /(ξ(_d) * exp(-τ * carryrate1), sigma * sqrt(τ))
    a * [inv(x1) -inv(x2); -inv(x2) x1*inv(x2^2)]
  end

  """
      theta(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the theta (Θ) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`delta`](@ref).
  """
  function theta(
                 m::Margrabe,
                 op::SpreadOption,
                 t::TimePoint=getfield(op,:inception),
                 x1::R1=S(getfield(op,:underlying1))(t),
                 x2::R2=S(getfield(op,:underlying2))(t);
                 sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                 sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                 carryrate1::B1=op.underlying1.params["carryrate"],
                 carryrate2::B2=op.underlying2.params["carryrate"],
                 corr::C=0,
                 dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    τ = yearfraction(dcc,t,getfield(op, :expiry))
    sigma = sqrt(sigma1^2 + sigma2^2 - 2corr*sigma1*sigma2)
    _d = _dp(
             m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
             carryrate1=carryrate1, carryrate2=carryrate2,
             corr=corr, dcc=dcc
         )
    δ1 = exp(-τ * carryrate1)
    _delt = delta(
                  m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                  carryrate1=carryrate1,carryrate2=carryrate2,
                  corr=corr,dcc=dcc
            )
    a = /(sigma * x1 * δ1 * ξ(_d), 2sqrt(τ))
    carryrate1 * x1 * first(_delt) + carryrate2 * x2 * last(_delt) - a
  end

  """
      vega(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the vega (V) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`rho`](@ref).
  """
  function vega(
                m::Margrabe,
                op::SpreadOption,
                t::TimePoint=getfield(op,:inception),
                x1::R1=S(getfield(op,:underlying1))(t),
                x2::R2=S(getfield(op,:underlying2))(t);
                sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                carryrate1::B1=op.underlying1.params["carryrate"],
                carryrate2::B2=op.underlying2.params["carryrate"],
                corr::C=0,
                dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    τ = yearfraction(dcc,t,getfield(op, :expiry))
    _d = _dp(
             m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
             carryrate1=carryrate1, carryrate2=carryrate2,
             corr=corr, dcc=dcc
         )
    δ1 = exp(-τ * carryrate1)
    δ1 * sqrt(τ) * ξ(_d)
  end

  """
      rho(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the rho (V) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`vega`](@ref).
  """
  function rho(
               m::Margrabe,
               op::SpreadOption,
               t::TimePoint=getfield(op,:inception),
               x1::R1=S(getfield(op,:underlying1))(t),
               x2::R2=S(getfield(op,:underlying2))(t);
               sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
               sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
               carryrate1::B1=op.underlying1.params["carryrate"],
               carryrate2::B2=op.underlying2.params["carryrate"],
               corr::C=0,
               dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    0
  end

  """
      epsilon(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the epsilon (ε) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  Spread options having two risk factors, some of their sensitivities
  are multi-dimensional. This is indeed the case for the `epsilon` which
  is the gradient with respect to `carryrate`s.
  
  See also [`delta`](@ref).
  """
  function epsilon(
                   m::Margrabe,
                   op::SpreadOption,
                   t::TimePoint=getfield(op,:inception),
                   x1::R1=S(getfield(op,:underlying1))(t),
                   x2::R2=S(getfield(op,:underlying2))(t);
                   sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                   sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                   carryrate1::B1=op.underlying1.params["carryrate"],
                   carryrate2::B2=op.underlying2.params["carryrate"],
                   corr::C=0,
                   dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    τ = yearfraction(dcc,t,getfield(op, :expiry))
    _delt = delta(
                  m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                  carryrate1=carryrate1,carryrate2=carryrate2,
                  corr=corr,dcc=dcc
            )
    -τ * (x1 * first(_delt) + x2 * last(_delt))
  end

  """
      omega(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the omega (ω) of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  Omega, also known as elasticity or lambda, if the percentage change in
  option value per percentage change in the underlying price.

  ``\\frac{\\partial{V}}{\\partial{S}}\\times\\frac{S}{V}``
  
  See also [`gamma`](@ref).
  """
  function omega(
                 m::Margrabe,
                 op::SpreadOption,
                 t::TimePoint=getfield(op,:inception),
                 x1::R1=S(getfield(op,:underlying1))(t),
                 x2::R2=S(getfield(op,:underlying2))(t);
                 sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                 sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                 carryrate1::B1=op.underlying1.params["carryrate"],
                 carryrate2::B2=op.underlying2.params["carryrate"],
                 corr::C=0,
                 dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    _ev = evaluate(
                   m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                   carryrate1=carryrate1,carryrate2=carryrate2,
                   corr=corr,dcc=dcc
          )
    _delt = delta(
                  m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                  carryrate1=carryrate1,carryrate2=carryrate2,
                  corr=corr,dcc=dcc
            )
    [first(_delt) * /(x1, _ev), last(_delt) * /(x2, _ev)]
  end
  
  """
      vanna(m::Margrabe, op::SpreadOption, t, x1, x2; <kwargs>
  
  Return the vanna of `op` at time `t` for the underlying risk factors
  levels `x1` and `x2`.
  
  # Arguments
  - `m::Margrabe`: a model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: risk factor 2 level.
  - `sigma1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `sigma2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  This greek is also known as DvegaDspot or DdeltaDvol.
  
  See also [`gamma`](@ref).
  """
  function vanna(
                 m::Margrabe,
                 op::SpreadOption,
                 t::TimePoint=getfield(op,:inception),
                 x1::R1=S(getfield(op,:underlying1))(t),
                 x2::R2=S(getfield(op,:underlying2))(t);
                 sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                 sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                 carryrate1::B1=op.underlying1.params["carryrate"],
                 carryrate2::B2=op.underlying2.params["carryrate"],
                 corr::C=0,
                 dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
    sigma = sqrt(sigma1^2 + sigma2^2 - 2corr*sigma1*sigma2)
    a = _dp(
             m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
             carryrate1=carryrate1, carryrate2=carryrate2,
             corr=corr, dcc=dcc
         )
    b = _dm(
             m, op, t, x1=x1, x2=x2, σ1=sigma1, σ2=sigma2,
             carryrate1=carryrate1, carryrate2=carryrate2,
             corr=corr, dcc=dcc
         )
    δ1 = exp(-τ * carryrate1)
    δ2 = exp(-τ * carryrate2)
    [-δ1 * /(b, sigma) * ξ(a), δ2 * /(a, sigla) * ξ(b)]
  end
                 

  """
      commongreeks(m::BlackScholes,op::EuropeanOption,[t],[x]; what,<kwargs>)
      commongreeks(m::Black76,op::EuropeanOption,[t],[x]; what,<kwargs>)
  
  Return the greek `what` of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  - `what::Symbol=:all`: kwarg, required greeks.

  # Notes
  When `what=:all`, return the following greeks as a [`NamedTuple`](@ref):
  delta, gamma, omega, rho, theta, vega.
  
  Valid arguments for `what` are :all, :delta, :vega, :rho, :theta, :omega and
  :gamma.
  """
  function commongreeks(
                        m::Margrabe,
                        op::SpreadOption,
                        t::TimePoint=getfield(op,:inception),
                        x1::R1=S(getfield(op,:underlying1))(t),
                        x2::R2=S(getfield(op,:underlying2))(t);
                        what::Union{AbstractString,Symbol}=:all,
                        sigma1::A1=getparams(getfield(op,:underlying1),"sigma"),
                        sigma2::A2=getparams(getfield(op,:underlying2),"sigma"),
                        carryrate1::B1=op.underlying1.params["carryrate"],
                        carryrate2::B2=op.underlying2.params["carryrate"],
                        corr::C=0,
                        dcc::DayCountConventions=ActAct()
           ) where {
                    R1<:Real,R2<:Real,A1<:Real,A2<:Real,
                    B1<:Real,B2<:Real,C<:Real
                   }
     gr = lowercase(string(what))
     vl = ("delta","gamma","vega","rho","theta","omega","vanna","all")
     @assert in(gr,vl) throw(ArgumentError("invalid [what] argument, see doc"))
     if isequal(gr,"all")
      (
       Δ = delta(
                 m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                 carryrate1=carryrate1,carryrate2=carryrate2,
                 corr=corr,dcc=dcc
            ),
       Γ = gamma(
                 m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                 carryrate1=carryrate1,carryrate2=carryrate2,
                 corr=corr,dcc=dcc
           ),
       V = vega(
                m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                carryrate1=carryrate1,carryrate2=carryrate2,
                corr=corr,dcc=dcc
           ),
       Ρ = rho(
               m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
               carryrate1=carryrate1,carryrate2=carryrate2,
               corr=corr,dcc=dcc
           ),
       Θ = theta(
                  m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                  carryrate1=carryrate1,carryrate2=carryrate2,
                  corr=corr,dcc=dcc
            ),
       Λ = omega(
                 m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
                 carryrate1=carryrate1,carryrate2=carryrate2,
                 corr=corr,dcc=dcc
           )
      )
     elseif isequal(gr, "delta")
       delta(
             m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
             carryrate1=carryrate1,carryrate2=carryrate2,
             corr=corr,dcc=dcc
       )
     elseif isequal(gr, "gamma")
       gamma(
             m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
             carryrate1=carryrate1,carryrate2=carryrate2,
             corr=corr,dcc=dcc
       )
     elseif isequal(gr, "vega")
       vega(
            m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
            carryrate1=carryrate1,carryrate2=carryrate2,
            corr=corr,dcc=dcc
       )
     elseif isequal(gr, "rho")
       rho(
           m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
           carryrate1=carryrate1,carryrate2=carryrate2,
           corr=corr,dcc=dcc
       )
     elseif isequal(gr, "theta")
       theta(
             m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
             carryrate1=carryrate1,carryrate2=carryrate2,
             corr=corr,dcc=dcc
       )
     elseif isequal(gr, "omega")
       omega(
             m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
             carryrate1=carryrate1,carryrate2=carryrate2,
             corr=corr,dcc=dcc
           )
     elseif isequal(gr, "vanna")
       omega(
             m, op, t, x1, x2, sigma1=sigma1, sigma2=sigma2,
             carryrate1=carryrate1,carryrate2=carryrate2,
             corr=corr,dcc=dcc
       )
     end
  end
end