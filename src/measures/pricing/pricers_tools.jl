# This script is part of Zsofia. Soel Philippe © 2025

#> tools
begin
  """
      Ξ(x)
  
  # Internal Facility
  
  Return the cumulutative distribution function of the standard normal
  distribution at `x`∈Real.
  """
  Ξ(x::R) where R<:Real = cdf(Normal(0,1),x)
  
  """
      ξ(x)
  
  # Internal Facility
  
  Return the probability distribution function of the standard normal
  distribution at `x`∈Real.
  """
  ξ(x::R) where R<:Real = pdf(Normal(0,1),x)
end


#> raw dp, dm
begin
  """
      _dp(K, F, v)
  
  # Internal Facility
  ``d_p`` and ``d_m`` from the Black(-Scholes) model, this is intended
  to ease the computation of vanillas and to operate within the Black-Scholes
  framework.
  
  The definitions are the following:
  
  ``d_p(K, F, v) = \\frac{ln(F/K) + v^2 / 2}{v}``
  
  ``d_m(K, F, v) = \\frac{ln(F/K) - v^2 / 2}{v}``
  
  ``v = σ\\sqrt{T}`` where σ is common volatility parameter retrieved from
  market quotes.
  """
  function _dp(K::R, F::V, v::W) where {R<:Real,V<:Real,W<:Real}
    /(log(/(F, K)) + 0.5v^2, v)
  end

  """
      _dm(K, F, v)
  
  # Internal Facility
  ``d_p`` and ``d_m`` from the Black(-Scholes) model, this is intended
  to ease the computation of vanillas and to operate within the Black-Scholes
  framework.
  
  The definitions are the following:
  
  ``d_p(K, F, v) = \\frac{ln(F/K) + v^2 / 2}{v}``
  
  ``d_m(K, F, v) = \\frac{ln(F/K) - v^2 / 2}{v}``
  
  ``v = σ\\sqrt{T}`` where σ is common volatility parameter retrieved from
  market quotes.
  """
  _dm(K::R, F::V, v::W) where {R<:Real,V<:Real,W<:Real} = _dp(K, F, v) - v
end

# BlackScholes, european options
begin
  """
      _dp(m::Black76, op::EuropeanOption, t::TimePoint; <kwargs>)
      _dp(m::BlackScholes, op::BinaryOption, t::TimePoint; <kwargs>)
      _dp(m::BlackScholes, op::EuropeanOption, t::TimePoint; <kwargs>)
  
  # Internal Facility
  Return the well-known and well worn ``d_1`` of the Black-Scholes options
  pricing model (european-style options).
  
  # Arguments
  - `m::BlackScholes`: Black-Scholes model instance.
  - `op::EuropeanOption`: an instance of a `EuropeanOption`.
  - `t::TimePoint`: compute ``d_1`` at time `t`.
  - `x::Real=S(op.underlying)(t)`: kwarg, underlying price level.
  - `σ::Real=getparams(op.underlying,"sigma")`: kwarg, volatility level.
  - `carryrate::real=getparams(op.underlying,"carryrate")`: kwarg, carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function _dp(
               m::BlackScholes,
               op::EuropeanOption,
               t::T;
               x::A=S(getfield(op,:underlying))(t),
               σ::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    d = discount(m.riskfreerate, t, getfield(op,:expiry), dcc=dcc)
    δ = exp(-carryrate * τ)
    _dp(getfield(op,:strike), x * δ * inv(d), sqrt(τ) * σ)
  end
  
  """
      _dm(m::Black76, op::EuropeanOption, t::TimePoint; <kwargs>)
      _dm(m::BlackScholes, op::BinaryOption, t::TimePoint; <kwargs>)
      _dm(m::BlackScholes, op::EuropeanOption, t::TimePoint; <kwargs>)
      
  
  # Internal Facility
  Return the well-known and well worn ``d_2`` of the Black-Scholes options
  pricing model (european-style options).
  
  # Arguments
  - `m::BlackScholes`: Black-Scholes model instance.
  - `op::EuropeanOption`: an instance of a `EuropeanOption`.
  - `t::TimePoint`: compute ``d_2`` at time `t`..
  - `x::Real=S(op.underlying)(t)`: kwarg, underlying price level.
  - `σ::Real=getparams(op.underlying,"sigma")`: kwarg, volatility level.
  - `carryrate::real=getparams(op.underlying,"carryrate")`: kwarg, carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function _dm(
               m::BlackScholes,
               op::EuropeanOption,
               t::T;
               x::A=S(getfield(op,:underlying))(t),
               σ::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    d = discount(m.riskfreerate, t, getfield(op,:expiry), dcc=dcc)
    δ = exp(-carryrate * τ)
    _dm(getfield(op,:strike), x * δ * inv(d), sqrt(τ) * σ)
  end
end

# Black76, european options
begin
  function _dp(
               m::Black76,
               op::EuropeanOption,
               t::T;
               x::A=S(getfield(op,:underlying))(t),
               σ::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    _dp(getfield(op, :strike), x * exp(-τ * carryrate), sqrt(τ) * σ)
  end
  
  function _dm(
               m::Black76,
               op::EuropeanOption,
               t::T;
               x::A=S(getfield(op,:underlying))(t),
               σ::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    _dm(getfield(op,:strike), x * exp(-τ * carryrate), sqrt(τ) * σ)
  end
end

# BlackScholes, binary options
begin
  function _dp(
               m::BlackScholes,
               op::BinaryOption,
               t::T;
               x::A=S(getfield(op,:underlying))(t),
               σ::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    d = discount(m.riskfreerate, t, getfield(op,:expiry), dcc=dcc)
    δ = exp(-carryrate * τ)
    _dp(getfield(op,:strike), x * δ * inv(d), sqrt(τ) * σ)
  end
  
  function _dm(
               m::BlackScholes,
               op::BinaryOption,
               t::T;
               x::A=S(getfield(op,:underlying))(t),
               σ::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    d = discount(m.riskfreerate, t, getfield(op,:expiry), dcc=dcc)
    δ = exp(-carryrate * τ)
    _dm(getfield(op,:strike), x * δ * inv(d), sqrt(τ) * σ)
  end
end

# Margrabe, spread options
begin
  """
      _dp(m::Margrabe, op::SpreadOption, t::TimePoint; <kwargs>)
  
  # Internal Facility
  Return the famous ``d_1`` parameter for `Margrabe`'s formula. 
  
  # Arguments
  - `m::Margrabe`: Margrabe's model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: kwarg, risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: kwarg, risk factor 2 level.
  - `σ1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `σ2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function _dp(
               m::Margrabe,
               op::SpreadOption,
               t::T;
               x1::A1=S(getfield(op,:underlying1))(t),
               x2::A2=S(getfield(op,:underlying2))(t),
               σ1::B1=getparams(getfield(op,:underlying1),"sigma"),
               σ2::B2=getparams(getfield(op,:underlying2),"sigma"),
               carryrate1::C1=getparams(getfield(op,:underlying1),"carryrate"),
               carryrate2::C2=getparams(getfield(op,:underlying2),"carryrate"),
               corr::D=0,
               dcc::DayCountConventions=ActAct()
           ) where {
                    A1<:Real,A2<:Real,B1<:Real,B2<:Real,
                    C1<:Real,C2<:Real,D<:Real,T<:TimePoint
                   }
    τ = yearfraction(dcc, t, getfield(op, :expiry))
    σ = sqrt(σ1^2 + σ2^2 - 2corr * σ1 * σ2)
    carryrate = carryrate1 - carryrate2
    _dp(x2, x1 * exp(-τ * carryrate), sqrt(τ) * σ)
  end

  """
      _dm(m::Margrabe, op::SpreadOption, t::TimePoint; <kwargs>)
  
  # Arguments
  - `m::Margrabe`: Margrabe's model instance.
  - `op::SpreadOption`: the spread option to price.
  - `t::TimePoint=op.inception`: evaluate `op` at `t`.
  - `x1::Real=S(op.underlying1)(t)`: kwarg, risk factor 1 level.
  - `x2::Real=S(op.underlying2)(t)`: kwarg, risk factor 2 level.
  - `σ1::Real=getparams(op.underlying1,"sigma")`: kwarg, volatility 1.
  - `σ2::Real=getparams(op.underlying2,"sigma")`: kwarg, volatility 2.
  - `carryrate1::Real=op.underlying1.params["carryrate"]: kwarg, carry 1.
  - `carryrate2::Real=op.underlying2.params["carryrate"]: kwarg, carry 2.
  - `corr::Real=0`: correlation between two risk factors.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  """
  function _dm(
               m::Margrabe,
               op::SpreadOption,
               t::T;
               x1::A1=S(getfield(op,:underlying1))(t),
               x2::A2=S(getfield(op,:underlying2))(t),
               σ1::B1=getparams(getfield(op,:underlying1),"sigma"),
               σ2::B2=getparams(getfield(op,:underlying2),"sigma"),
               carryrate1::C1=getparams(getfield(op,:underlying1),"carryrate"),
               carryrate2::C2=getparams(getfield(op,:underlying2),"carryrate"),
               corr::D=0,
               dcc::DayCountConventions=ActAct()
           ) where {
                    A1<:Real,A2<:Real,B1<:Real,B2<:Real,
                    C1<:Real,C2<:Real,D<:Real,T<:TimePoint
                   }
    τ = yearfraction(dcc,t,getfield(op, :expiry))
    σ = sqrt(σ1^2 + σ2^2 - 2corr * σ1 * σ2)
    carryrate = carryrate1 - carryrate2
    _dm(x2, x1 * exp(-τ * carryrate), sqrt(τ) * σ)
  end

  """
      _b_vasicek(t,T,θ,μ,σ,dcc::DayCountConventions=ActAct())
  
  # Internal Facility
  The ``B(t,T)``-term of analytical price of pure-discount bond under the affine
  term structure model known as the **Vasicek model**, modelling  the short-rate
  rate with an *Ornstein-Uhlenbeck dynamic*.
  
  See also [`OrnsteinUhlenbeckProcess`](@ref), [`Vasicek`](@ref).
  
  Reference: *Interest Rate Models - Theory and Practice: With Smile,
  Inflation and Credit* by from Damiano Brigo and Fabio Mercurio,
  Springer, 2006 ; chapter 3 - pp. 59.
  """
  function _b_vasicek(
                      t::T1,
                      T::T2,
                      θ::A,
                      μ::B,
                      σ::C,
                      dcc::DayCountConventions=ActAct()
           ) where {T1<:TimePoint,T2<:TimePoint,A<:Real,B<:Real,C<:Real}
    /(1-exp(-yearfraction(dcc,t,T)*θ), θ)
  end

  """
      _a_vasicek(t,T,θ,μ,σ,dcc::DayCountConventions=ActAct())
  
  # Internal Facility
  The ``A(t,T)``-term of analytical price of pure-discount bond under the affine
  term structure model known as the **Vasicek model**, modelling  the short-rate
  rate with an *Ornstein-Uhlenbeck dynamic*.
  
  See also [`OrnsteinUhlenbeckProcess`](@ref), [`Vasicek`](@ref).
  
  Reference: *Interest Rate Models - Theory and Practice: With Smile,
  Inflation and Credit* by from Damiano Brigo and Fabio Mercurio,
  Springer, 2006 ; chapter 3 - pp. 59.
  """
  function _a_vasicek(
                      t::T1,
                      T::T2,
                      θ::A,
                      μ::B,
                      σ::C,
                      dcc::DayCountConventions=ActAct()
           ) where {T1<:TimePoint,T2<:TimePoint,A<:Real,B<:Real,C<:Real}
    exp(
        -(
          *(
            μ - /(σ^2,2θ^2),
            _b_vasicek(t,T,θ,μ,σ,dcc) - yearfraction(dcc,t,T)
          ),
          *(/(σ^2,4θ), _b_vasicek(t,T,θ,μ,σ,dcc)^2)
        )
    )
  end
end