# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _ivol_raw(::BlackScholes, K, T, F, r, t, oprice, ocolor, dcc)

  # Internal Facility
  This internal facility is intended for implied volatility computations.

  # Arguments
  - `::AbstractModel`: underlying pricing model.
  - `K::Real`: option's strike price.
  - `T::TimePoint`: maturity.
  - `F::Real`: (forward) price level.
  - `t::TimePoint`: pricing time.
  - `r::Real`: constant interest rate level.
  - `oprice::Real`: option's price.
  - `ocolor::OptionColor`: either `Put` or Call`, defaulting to `Call`.
  - `dcc::DayCountConventions`: defaulting to `ActAct()`
  - `low::Real`: positive real number bounding below the search region.
  - `upp::Real` kwarg, upper bound of solver's search region ([low, upps]).

  # Notes
  The minimization/root finding algorithm is the **Brent-Dekker method**,
  the `Optim.Brent()` instance.  
  See [Wikipedia](https://en.wikipedia.org/wiki/Brent's_method)
  """
  function _ivol_raw(
                     ::BlackScholes,
                     K::A,
                     T::V,
                     F::B,
                     t::V,
                     r::D,
                     oprice::W,
                     ocolor::OptionColor=Call,
                     dcc::DayCountConventions=ActAct(),
                     low::C=1e-8,
                     upp::Real=5.0
           ) where {A<:Real,B<:Real,C<:Real,D<:Real,V<:TimePoint,W<:Real}
    ω, τ = ifelse(isequal(ocolor,Call),one(B),-one(B)), yearfraction(dcc,t,T)
    κ = K*exp(-r*τ)
    _f(x::Real) = (oprice-F*ω*Ξ(ω*_dp(κ,F,x * √τ))+κ*ω*Ξ(ω*_dm(κ,F,x * √τ)))^2
    getfield(optimize(_f, low, upp, Brent()), :minimizer)
  end

  function _ivol_raw(
                     ::Black76,
                     K::A,
                     T::V,
                     F::B,
                     t::V,
                     r::D,
                     oprice::W,
                     ocolor::OptionColor=Call,
                     dcc::DayCountConventions=ActAct(),
                     low::C=1e-8,
                     upp::Real=5.0
           ) where {A<:Real,B<:Real,C<:Real,D<:Real,V<:TimePoint,W<:Real}
    ω, τ = 2isequal(ocolor,Call)-1, yearfraction(dcc,t,T)
    function _f(x::Real)
      -(
        oprice,
        *(
          exp(-r*τ),
          F*ω*Ξ(ω*_dp(K,F,x * √τ)) - K*ω*Ξ(ω*_dm(K,F,x * √τ))
        )
      ) ^ 2
    end
    getfield(optimize(_f, low, upp, Brent()), :minimizer)
  end
end

#> exposed foos
begin
  """
      ivol(m::AbstractModel, op::EuropeanOption, [K, [T, [F, [t, [r; [dcc]]]]]])
  
  Return a function returning the implied volality of the provided option
  `op` of strike `K`, maturing at `T`, given the underlying price level `F`
  for the time point `t` and the day count convention `dcc`.

  # Arguments
  - `m::AbstractModel`: underlying model.
  - `op::EuropeanOption`: underlying european option.
  - `K::Real`: option's strike.
  - `T::TimePoint`: maturity time point.
  - `F::Real`: underlying price level.
  - `t::TimePoint`: pricing time.
  - `r::Union{Real,InterestRate}`: the relevant interest rate.
  - `dcc`: kwarg, day count convention defaulting to `ActAct()`.
  - `low::Real`: kwarg, lower bound of solver's search region. 
  - `upp::Real` kwarg, upper bound of solver's search region.

  # Notes
  This function returns a function taking option prices and returning
  the implied volatility associated to that price, under the relevant model.
  The underlying root-finding/optimization procedure is the Brent-Dekker's
  algorithm.
  
  # Important
  Make sure to properly set the `low` and `upp` parameters, which are
  respectively the *lower* and *upper* bounds of the root-search region.

  # Examples
  ```julia
  julia> begin
           pltr = Stock("PLTR")
           strike, expiry = 60.00, Date(2025,1,31)
           ccy, dcc = Currency("USD"), Thirty360()
           bs = BlackScholes(0.041037, currency=ccy)
           inception = Date(2025, 1, 3)
           op = EuropeanOption(
                               strike, expiry, option=Put,
                               underlying=pltr, inception=inception
                )
           insertprices!(pltr, 79.89, inception)
           setparams!(pltr, "sigma", 0.6327, force=true)
           p = evaluate(bs, op, sigma=getparams(pltr, "sigma"), dcc=dcc)
        end
  0.2566547270622772
  ```
  Let's see how the ivol fluctuates when we vary that upper bound... remember
  that our true volatility level (sigma) is 0.6327.
  ```julia
  julia> realsigma = getparams(pltr, "sigma")
  0.6327
  
  julia> ivol(bs, op, upp=1.0)(p)
  0.6369990065866554
  
  julia> realsigma < ivol(bs, op, upp=1.0)(p)
  true
  
  julia> upps = collect(1:0.1:2);
  
  julia> function foo(u::Real)
           1_000_000 * (ivol(bs,op,upp=u)(p) - realsigma)
         end
  foo (generic function with 1 method)
  
  julia> [foo(u) for u in upps]
  11-element Vector{Float64}:
   4299.006586655385
   4299.006739551192
   4299.0064377929075
   4299.006587002663
   4299.00834695307
   4299.0044618473885
   4299.003767792242
   4299.00446471565
   4299.007069109018
   4299.005995788474
   4299.009766253525
  ```
  """
  function ivol(
                m::BlackScholes,
                op::EuropeanOption,
                K::R,
                T::A,
                F::V,
                t::A,
                r::W;
                dcc::DayCountConventions=ActAct(),
                low::Real=1e-8,
                upp::Real=5.0
           ) where {R<:Real,A<:TimePoint,V<:Real,W<:Real}
    _iv(p::R) where R<:Real = _ivol_raw(m,K,T,F,t,r,p,op.option,dcc,low,upp)
  end

  function ivol(
                m::Black76,
                op::EuropeanOption,
                K::R,
                T::A,
                F::V,
                t::A,
                r::W;
                dcc::DayCountConventions=ActAct(),
                low::Real=1e-8,
                upp::Real=5.0
           ) where {R<:Real,A<:TimePoint,V<:Real,W<:Real}
    _iv(p::R) where R<:Real = _ivol_raw(m,K,T,F,t,r,p,op.option,dcc,low,upp)
  end
  
  function ivol(
                m::BlackScholes,
                op::EuropeanOption,
                K::R=getfield(op, :strike),
                T::A=getfield(op, :expiry),
                F::V=last(getfield(getfield(op, :underlying), :prices)),
                t::A=last(getfield(getfield(op, :underlying), :times)),
                r::InterestRate=getfield(m, :riskfreerate);
                dcc::DayCountConventions=ActAct(),
                low::Real=1e-8,
                upp::Real=5.0
           ) where {R<:Real,A<:TimePoint,V<:Real}
    ivol(m, op, K, T, F, t, S(r)(t), dcc=dcc, low=low, upp=upp)
  end

  function ivol(
                m::Black76,
                op::EuropeanOption,
                K::R=getfield(op, :strike),
                T::A=getfield(op, :expiry),
                F::V=last(getfield(getfield(op, :underlying), :prices)),
                t::A=last(getfield(getfield(op, :underlying), :times)),
                r::InterestRate=getfield(m, :riskfreerate);
                dcc::DayCountConventions=ActAct(),
                low::Real=1e-8,
                upp::Real=5.0
           ) where {R<:Real,A<:TimePoint,V<:Real}
    ivol(m, op, K, T, F, t, S(r)(t), dcc=dcc, low=low, upp=upp)
  end
  
  function ivol(
                m::Black76,
                op::Swaption,
                K::R=getfield(op,:strike),
                T::A=getfield(op,:expiry),
                F::V=last(op.underlying.prices),
                t::A=last(op.underlying.times),
                yc::Union{YieldCurve,InterestRate}=m.riskfreerate;
                carryrate=getparams(op.underlying,"carryrate"),
                dcc::DayCountConventions=ActAct(),
                low::Real=1e-8,
                upp::Real=5.0
          ) where {R<:Real,A<:TimePoint,V<:Real}
    _f(σ::Real) = evaluate(m,op,t,F,sigma=σ,carryrate=carryrate,yc=yc,dcc=dcc)
    function _iv(price::Real)
      getfield(optimize(v -> (_f(v)-price)^2, low, upp, Brent()), :minimizer)
    end
  end
end