# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      evaluate(m::MonteCarlo{M}, op, par, <kwargs>)
      evaluate(m::MonteCarlo{M}, op, obs::Observables, <kwargs>)
      evaluate(m::MonteCarlo{M}, op, <kwargs>)
  
  Evaluate and return the value of `op` using Monte Carlo simulations under
  the assumptions of the framework model `M`.
  
  # Arguments
  - `m::Montecarlo{M<:AbstractModel}`: monte carlo instance under model `M`.
  - `op::Derivatives`: derivatives required for pricing.
  - `par::NTuple{N,Real}`: parameters of underlying model `M`.
  - `dt::Union{Period,Real}`: when relevant, higher-level simulation time step.
  - `t::TimePoint=op.inception`: kwarg, evaluate `op` at time `t`.
  - `n::Int=1000`: kwarg, number of MonteCarlo simulations.
  - `x0::Real=S(op.underlying)(t)`: risk factor starting point of simulations.
  - `nsteps::Int=360`: kwarg, lower level simulations number of steps.
  - `confint::Bool=false`: kwarg, if `true` return a confidence interval aside.
  - `confintlevel::Real=0.95`: kwarg, confidence level (relevant if `confint`).
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  If `confint=true` the tuple `(estimated_price, (lower_ci, upper_ci))` is
  returned, otherwise the scalar `estimated_price` is returned.
  
  When necessary  the underlying `Observables` is sollicited to infer
  required parameters. This is triggered explicitely when calling
  the second method (required models parameters are to be provided in
  `Observables`' parameters container).
  
  # Examples
  ```julia
  julia> begin
           pltr = Stock("PLTR")
           strk, expy = 60.0, Date(2025,1,31)
           ccy, dcc = Currency("USD"), Thirty360()
           bs = BlackScholes(0.041037, currency=ccy)
           inc = Date(2025,1,3)
           op=EuropeanOption(strk,expy,option=Put,underlying=pltr,inception=inc)
           insertprices!(pltr, 79.89, inc)
           setparams!(pltr,"sigma",0.6327,force=true)
         end;
  
  julia> mc = MonteCarlo(bs);
  
  julia> v = evaluate(mc, op);
  ```
  """
  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::EuropeanOption,
                    par::NTuple{2,R};
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    @assert isless(1,n)
    @assert ~confint || (0 < confintlevel < 1)
    if isless(n,30)
      @warn "low number of simulations, got $(n): accuracy concerns"
    end
    τ = yearfraction(dcc,t,getfield(op,:expiry))
    d = discount(
                 getfield(getfield(m,:model),:riskfreerate),
                 t,getfield(op,:expiry),dcc=dcc
        )
    β = first(par) - 0.5last(par)^2
    x = x0 * exp.(*(β,τ) .+ last(par) * sqrt(τ) * randn(n))
    p = map(v -> payoff(op,v,getfield(op,:expiry)),x)
    if confint
      l = /(std(p * d) * quantile(Normal(), (1-confintlevel)/2), sqrt(n))
      m = mean(p) * d
      m, (max(0,m+l), m-l)
    else
      mean(p) * d
    end
  end
  
  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::EuropeanOption,
                    obs::Observables;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    for f in getfield(getfield(m,:model),:required)
      @assert hasparams(obs,f) "required parameter $(f) is missing"
    end
    par = [getparams(obs,f) for f in getfield(getfield(m,:model),:required)]
    opt = only(getfield(getfield(m,:model),:optional))
    par[1] -= getparams(obs,opt,0)
    par[1] += S(getfield(getfield(m,:model),:riskfreerate))(t)
    evaluate(
             m, op, Tuple(par), t=t, n=n, x0=x0, confint=confint,
             confintlevel=confintlevel, dcc=dcc
    )
  end

  function evaluate(
                    m::MonteCarlo{BlackScholes},
                    op::EuropeanOption;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    evaluate(
             m, op, getfield(op,:underlying), t=t, n=n, x0=x0,
             confint=confint, confintlevel=confintlevel, dcc=dcc
    )
  end
  
  
  """
      evaluate!(m::MonteCarlo{M}, op, par, <kwargs>)
      evaluate!(m::MonteCarlo{M}, op, obs::Observables, <kwargs>)
      evaluate!(m::MonteCarlo{M}, op, <kwargs>)
  
  `evaluate` and store the result in-place into `op`. The return value is
  the same as the `evluate` method.
  
  See also [`evaluate`](@ref).
  
  # Notes
  The average price is stored into `op`'s prices container and timestamped at
  `t` into `op`'s times container.
  
  Any pre-existing price/observation at `t` is overwritten.
  """
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::EuropeanOption,
                     par::NTuple{2,R};
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, par, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::EuropeanOption,
                     obs::Observables;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, obs, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{BlackScholes},
                     op::EuropeanOption;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  """
      evaluate(m::BlackScholes, op::EuropeanOption, [t, x; <kwargs>])
      evaluate(m::Black76, op::EuropeanOption, [t, x; <kwargs>])
  
  Evaluate the `EuropeanOption` `op` under the `m` model framework.
  
  # Arguments
  - `m::AbstractModel`: a model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=op.underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`BlackScholes`](@ref), [`EuropeanOption`](@ref).
  
  # Notes
  The `BlackScholes` model gathers relevant parameters related to the state
  of the world according to Black-Scholes model assumptions.
  
  # Examples
  
  ## Example 1
  ```
  julia> begin
           pltr = Stock("PLTR")
           strk, expy = 60.0, Date(2025,1,31)
           ccy, dcc = Currency("USD"), Thirty360()
           bs = BlackScholes(0.041037, currency=ccy)
           inc = Date(2025,1,3)
           op=EuropeanOption(strk,expy,option=Put,underlying=pltr,inception=inc)
           insertprices!(pltr, 79.89, inc)
           setparams!(pltr,"sigma",0.6327,force=true)
         end;
  
  julia> evaluate(bs, op, sigma=getparams(pltr, "sigma"), dcc=dcc)
  0.2566547270622772
  ```
  
  # Example 2
  ```julia
  julia> begin
           fut = Futures(600.0, 0.5)
           setparams!(fut, "sigma", 0.1925, force=true)
           insertprices!(fut, 603.75, 0.0)
           blk = Black76(0.0500, currency=Currency("USD"))
           op = EuropeanOption(625,0.5,underlying=fut,inception=0.0)
        end;
  
  julia> v = evaluate(blk, op);
  ```
  """
  function evaluate(
                    m::BlackScholes,
                    op::EuropeanOption,
                    t::T=getfield(op,:inception),
                    x::A=S(op.underlying)(t);
                    sigma::B=getparams(op.underlying,"sigma"),
                    carryrate::C=getparams(op.underlying,"carryrate"),
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    ω = 2isequal(getfield(op,:options),Call)-1
    τ = yearfraction(dcc,t,getfield(op,:expiry))
    δ = exp(-τ * carryrate)
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    -(
      x * δ * Ξ(ω * _dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      getfield(op,:strike) * d *
      Ξ(ω * _dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc))
    ) * ω * isless(0,τ)
  end
  
  function evaluate!(
                     m::BlackScholes,
                     op::EuropeanOption,
                     t::T=getfield(op,:inception),
                     x::A=S(getfield(op,:underlying))(t);
                     sigma::B=getparams(op.underlying,"sigma"),
                     carryrate::C=getparams(op.underlying,"carryrate"),
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    p = evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    insertprices!(op, p, t)
    p
  end
  
  
  """
      delta(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      delta(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the delta (Δ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: a model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`gamma`](@ref).
  """
  function delta(
                 m::BlackScholes,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(op.underlying,"sigma"),
                 carryrate::C=getparams(op.underlying,"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(op.option,Call)-1
    τ = yearfraction(dcc,t,op.expiry)
    δ = exp(-τ * carryrate)
    -(
      Ξ(_dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      0.5 - 0.5ω
    ) * δ
  end

  """
      gamma(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      gamma(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the gamma (Γ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`omega`](@erf).
  """
  function gamma(
                 m::BlackScholes,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(op.underlying,"sigma"),
                 carryrate::C=getparams(op.underlying,"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    δ = exp(-τ * carryrate)
    /(
      ξ(_dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      sigma * x * sqrt(τ)
    ) * δ
  end

  """
      theta(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      theta(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the theta (Θ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`delta`](@ref).
  """
  function theta(
                 m::BlackScholes,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(op.option,Call)-1
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    δ = exp(-τ * carryrate)
    r = S(getfield(m, :riskfreerate))(t)
    a = /(
          *(δ,sigma,x,ξ(ω*_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))),
          2sqrt(τ)
        )
    b = -(
          *(
            carryrate, x, δ,
            Ξ(ω*_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))
          ),
          *(
            r,
            getfield(op,:strike),
            Ξ(ω*_dm(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)),
            discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
          )
        ) * ω
    b - a
  end

  """
      vega(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      vega(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the vega (V) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`rho`](@ref).
  """
  function vega(
                m::BlackScholes,
                op::EuropeanOption,
                t::T=getfield(op,:inception),
                x::A=S(getfield(op,:underlying))(t);
                sigma::B=getparams(getfield(op,:underlying),"sigma"),
                carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    δ = exp(-τ*carryrate)
    x*ξ(_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))*δ*sqrt(τ)
  end

  """
      rho(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      rho(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the rho (ρ) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  See also [`vega`](@ref).
  """
  function rho(
               m::BlackScholes,
               op::EuropeanOption,
               t::T=getfield(op,:inception),
               x::A=S(getfield(op,:underlying))(t);
               sigma::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:option),Call) - 1
    τ, r = yearfraction(dcc,t,op.expiry), m.riskfreerate
    *(
      Ξ(ω * _dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      op.strike, ω, τ,
      discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    )
  end

  """
      epsilon(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      epsilon(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the rho (ε) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  This is the sentivity of the option price with respect to the carryrate.
  
  See also [`theta`](@ref).
  """
  function epsilon(
                   m::BlackScholes,
                   op::EuropeanOption,
                   t::T=getfield(op,:inception),
                   x::A=S(getfield(op,:underlying))(t);
                   sigma::B=getparams(getfield(op,:underlying),"sigma"),
                   carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                   dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:option),Call) - 1
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    δ = exp(-τ * carryrate) * ω
    -τ * x * δ * Ξ(ω *_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))
  end

  """
      omega(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      omega(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the omega (ω) of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  Omega, also known as elasticity or lambda, if the percentage change in
  option value per percentage change in the underlying price.

  ``\\frac{\\partial{V}}{\\partial{S}}\\times\\frac{S}{V}``
  
  See also [`gamma`](@ref).
  """
  function omega(
                 m::BlackScholes,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    *(
      delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
      /(
        x,
        evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
    )
  end

  """
      vanna(m::BlackScholes, op::EuropeanOption, [t], [x]; <kwargs>)
      vanna(m::Black76, op::EuropeanOption, [t], [x]; <kwargs>)
  
  Return the vanna of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.

  # Notes
  This greek is also known as DvegaDspot or DdeltaDvol.
  
  See also [`gamma`](@ref).
  """
  function vanna(
                 m::BlackScholes,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    *(
      -exp(-τ * carryrate), /(1, sigma),
      ξ(_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)),
      _dm(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)
    )
  end
                 

  """
      commongreeks(m::BlackScholes,op::EuropeanOption,[t],[x]; what,<kwargs>)
      commongreeks(m::Black76,op::EuropeanOption,[t],[x]; what,<kwargs>)
  
  Return the greek `what` of `op` at time `t` for the underlying price level `x`.
  
  # Arguments
  - `m::AbstractModel`: model instance.
  - `op::EuropeanOption`: the european option to price.
  - `t::TimePoint=op.inception`: time at which to evaluate `op`.
  - `x::Real=op.strike`: underlying price level.
  - `sigma::Real=underlying.params["sigma"]`: kwarg, volatility.
  - `carryrate::Real=underlying.params["carryrate"]`: kwarg, continuous carry.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  - `what::Symbol=:all`: kwarg, required greeks.

  # Notes
  When `what=:all`, return the following greeks as a [`NamedTuple`](@ref):
  delta, gamma, omega, rho, theta, vega.
  
  Valid arguments for `what` are :all, :delta, :vega, :rho, :theta, :omega and
  :gamma.
  """
  function commongreeks(
                        m::BlackScholes,
                        op::EuropeanOption,
                        t::T=getfield(op,:inception),
                        x::A=S(getfield(op,:underlying))(t);
                        what::Union{AbstractString,Symbol}=:all,
                        sigma::B=getparams(op.underlying,"sigma"),
                        carryrate::C=getparams(op.underlying,"carryrate"),
                        dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
     gr = lowercase(string(what))
     vl = ("delta","gamma","vega","rho","theta","omega","vanna","all")
     @assert in(gr,vl) throw(ArgumentError("invalid [what] argument, see doc"))
     if isequal(gr,"all")
      (
       Δ = delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Γ = gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       V = vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Ρ = rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Θ = theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Λ = omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
     elseif isequal(gr, "delta")
       delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "gamma")
       gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "vega")
       vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "rho")
       rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "theta")
       theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "omega")
       omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "vanna")
       vanna(m, opt, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     end
  end
end


#> Black76
begin
  function evaluate(
                    m::MonteCarlo{Black76},
                    op::EuropeanOption,
                    par::NTuple{1,R};
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying))(t),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    @assert isless(1,n)
    @assert ~confint || (0 < confintlevel < 1)
    if isless(n,30)
      @warn "low number of simulations, got $(n): accuracy concerns"
    end
    τ = yearfraction(dcc,t,getfield(op,:expiry))
    d = discount(
                 getfield(getfield(m,:model),:riskfreerate),
                 t,getfield(op,:expiry),dcc=dcc
        )
    β = -0.5first(par)^2
    x = x0 * exp.(*(β,τ) .+ first(par) * sqrt(τ) * randn(n))
    p = map(v -> payoff(op,v,getfield(op,:expiry)),x)
    if confint
      l = /(std(p) * quantile(Normal(), (1-confintlevel)/2), sqrt(n))
      m = mean(p) * d
      m, (max(0,m+l), m-l)
    else
      mean(p) * d
    end
  end
  
  function evaluate(
                    m::MonteCarlo{Black76},
                    op::EuropeanOption,
                    obs::Observables,
                    dt::Union{<:Real,Period};
                    t::T=getfield(op,:inception),
                    n::Integer=1000,
                    x0::A=S(obs)(t),
                    nsteps::Integer=365,
                    confint::Bool=true,
                    confintlevel::B=0.95,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    for f in getfield(getfield(m,:model),:required)
      @assert hasparams(obs,f) "required parameter $(f) is missing"
    end
    par = [getparams(obs,f) for f in getfield(getfield(m,:model),:required)]
    opt = only(getfield(getfield(m,:model), :optional))
    par[1] -= getparams(obs,opt,0)
    evaluate(
             m, op, Tuple(par), t=t, n=n, x0=x0, confint=confint,
             confintlevel=confintlevel, dcc=dcc
    )
  end

  function evaluate(
                    m::MonteCarlo{Black76},
                    op::EuropeanOption;
                    t::T=getfield(op,:inception),
                    n::Int=1000,
                    x0::A=S(getfield(op,:underlying)(t)),
                    confint::Bool=false,
                    confintlevel::B=95/100,
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    evaluate(
             m, op, getfield(op,:underlying), t=t, n=n, x0=x0,
             confint=confint, confintlevel=confintlevel, dcc=dcc
    )
  end
  
  
  #> evaluate! and store
  #
  
  function evaluate!(
                     m::MonteCarlo{Black76},
                     op::EuropeanOption,
                     par::NTuple{1,R};
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, par, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{Black76},
                     op::EuropeanOption,
                     obs::Observables;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, obs, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices!(op, first(_ev), t)
    _ev
  end
  
  function evaluate!(
                     m::MonteCarlo{Black76},
                     op::EuropeanOption;
                     t::T=getfield(op,:inception),
                     n::Int=1000,
                     x0::A=S(getfield(op,:underlying))(t),
                     confint::Bool=false,
                     confintlevel::B=95/100,
                     dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real}
    _ev = begin
      evaluate(
               m, op, t=t, n=n, x0=x0, confint=confint,
               confintlevel=confintlevel, dcc=dcc
      )
    end
    insertprices(op,first(_ev),t)
    _ev
  end
  
  ##############################################################
  #################     Black76, closed pricers  ###############
  ##############################################################
  
  function evaluate(
                  m::Black76,
                  op::EuropeanOption,
                  t::T=op.inception,
                  x::A=S(getfield(op,:underlying))(t);
                  sigma::B=getparams(getfield(op,:underlying),"sigma"),
                  carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                  dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    ω = 2isequal(getfield(op,:option),Call) - 1
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    δ = exp(-yearfraction(dcc,t,getfield(op,:expiry)) * carryrate)
    -(
      x * δ * Ξ(ω * _dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      getfield(op,:strike) *
      Ξ(ω * _dm(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc))
    ) * ω * d * (getfield(op,:inception) <= t <= getfield(op,:expiry))
  end
  
  function evaluate!(
                    m::Black76,
                    op::EuropeanOption,
                    t::T=getfield(op,:inception),
                    x::A=S(getfield(op,:underlying))(t);
                    sigma::B=getparams(getfield(op,:underlying),"sigma"),
                    carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,A<:Real,B<:Real,C<:Real}
    p = evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    insertprices!(op, p, t)
    p
  end
  
  function delta(
                 m::Black76,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:option),Call) - 1
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    δ = exp(-yearfraction(dcc,t,getfield(op,:expiry)) * carryrate)
    -(
      Ξ(_dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
      0.5 - 0.5ω
    ) * δ * d
  end
  
  function gamma(
                 m::Black76,
                 op::EuropeanOption,
                 t::T=op.inception,
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(op.underlying,"sigma"),
                 carryrate::C=getparams(op.underlying,"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    d = discount(getfield(m,:riskfreerate), t, getfield(op,:expiry), dcc=dcc)
    τ = yearfraction(dcc,t,getfield(op,:expiry))
    δ = exp(-τ * carryrate)
    /(
      ξ(_dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)) * d * δ,
      sigma * x * sqrt(τ)
    )
  end
  
  function theta(
                 m::Black76,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:options),Call) - 1
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    r = S(getfield(m,:riskfreerate))(t)
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    δ = exp(-τ * carryrate)
    p = evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
    c = *(d, δ, x)
    b = *(carryrate,ω,Ξ(ω*_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)))
    a = *(
          ξ(_dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc)),
          sigma, /(1, 2sqrt(τ))
        )
    r * p + *(c, b-a)
  end
  
  function vega(
                m::Black76,
                op::EuropeanOption,
                t::T=op.inception,
                x::A=S(getfield(op,:underlying))(t);
                sigma::B=getparams(op.underlying,"sigma"),
                carryrate::C=getparams(op.underlying,"carryrate"),
                dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    *(
      discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc),
      exp(-τ * carryrate), x, sqrt(τ),
      ξ(ω * _dp(m, op, t, x=x, σ=sigma, carryrate=carryrate, dcc=dcc))
    )
  end
  
  function rho(
               m::Black76,
               op::EuropeanOption,
               t::T=op.inception,
               x::A=S(getfield(op,:underlying))(t);
               sigma::B=getparams(getfield(op,:underlying),"sigma"),
               carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
               dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    *(
      evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
      -S(getfield(m,:riskfreerate))(t)
    )
  end
  
  function epsilon(
                   m::Black76,
                   op::EuropeanOption,
                   t::T=op.inception,
                   x::A=S(getfield(op,:underlying))(t);
                   sigma::B=getparams(getfield(op,:underlying),"sigma"),
                   carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                   dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    ω = 2isequal(getfield(op,:options),Call) - 1
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    δ = exp(-τ * carryrate)
    d = discount(getfield(m,:riskfreerate), t, getfield(op,:expiry), dcc=dcc)
    *(
      d, δ, τ, x, ω,
      Ξ(ω * _dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc))
    )
  end
  
  function omega(
                 m::Black76,
                 op::EuropeanOption,
                 t::T=op.inception,
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    *(
      delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
      /(
        x,
        evaluate(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
    )
  end

  function vanna(
                 m::Black76,
                 op::EuropeanOption,
                 t::T=getfield(op,:inception),
                 x::A=S(getfield(op,:underlying))(t);
                 sigma::B=getparams(getfield(op,:underlying),"sigma"),
                 carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                 dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
    τ = yearfraction(dcc, t, getfield(op,:expiry))
    d = discount(getfield(m,:riskfreerate),t,getfield(op,:expiry),dcc=dcc)
    *(
      -exp(-τ * carryrate), /(1, sigma), d,
      ξ(_dp(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)),
      _dm(m,op,t,x=x,σ=sigma,carryrate=carryrate,dcc=dcc)
    )
  end

  function commongreeks(
                        m::Black76,
                        op::EuropeanOption,
                        t::T=getfield(op,:inception),
                        x::A=S(getfield(op,:underlying))(t);
                        what::Union{AbstractString,Symbol}=:all,
                        sigma::B=getparams(getfield(op,:underlying),"sigma"),
                        carryrate::C=getparams(getfield(op,:underlying),"carryrate"),
                        dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T<:TimePoint}
     gr = lowercase(string(what))
     vl = ("delta","gamma","vega","rho","theta","omega","vanna","all")
     @assert in(gr,vl) throw(ArgumentError("invalid [what] argument, see doc"))
     if isequal(gr,"all")
      (
       Δ = delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Γ = gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       V = vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Ρ = rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Θ = theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc),
       Λ = omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
      )
     elseif isequal(gr, "delta")
       delta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "gamma")
       gamma(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "vega")
       vega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "rho")
       rho(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "theta")
       theta(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "omega")
       omega(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     elseif isequal(gr, "vanna")
       vanna(m, op, t, x, sigma=sigma, carryrate=carryrate, dcc=dcc)
     end
  end
end