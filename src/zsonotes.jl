### A Pluto.jl notebook ###
# v0.20.4

using Markdown
using InteractiveUtils

# ╔═╡ 23ff20be-30fa-4b99-afdc-e2b8a48db8fc
# ╠═╡ show_logs = false
begin
  import Pkg
  Pkg.activate("../")
  using Zsofia, Plots, LaTeXStrings
end

# ╔═╡ 2b175663-02cd-4134-bea8-bdb7af491626
md"""
# HW1F tweaks

```math
r_t = \alpha_t + x_t
```

$$dx_t = -\theta_t x_t dt +\sigma dW_t$$

$$\alpha_t = f^{M}(0,t) + \frac{\sigma^2}{2\theta^2}\left(1-e^{-\theta t}\right)^2$$
"""

# ╔═╡ 58297162-1aa2-4ebe-8b18-d59eec880a1a
md"""
### Context
|||
|:---:|:---|
|**currency**|USD|
|**yield curve**|USTreasuryYields, March 10, 2025|
|**rates, range**|3.89% (3Y) - 4.58% (20Y)|
|**tenors**|4w,6w,2m,3m,6m,1y,2y,3y,5y,7y,10y,20y,30y|
"""

# ╔═╡ 1ec1a1ac-e67e-436c-a9b4-0358d53f628f
begin
  ccy  = Currency("USD")
  _byc = (
          4.37, 4.35, 4.34, 4.33, 4.28, 4.25, 3.98,
          3.89, 3.91, 3.98, 4.10, 4.22, 4.58, 4.54
         )
  _btn = (1//12,3//24,1//6,1//4,1//2,1,2,3,5,7,10,20,30)
  x0  = 0.0425
  dcc = Thirty360()
  irs = InterestRate[]

  for (r,tn) in zip(_byc,_btn)
    push!(irs,InterestRate(Tenor(tn),currency=ccy))
    insertprices!(irs[end],0.01r,.0)
  end
  
  yc = YieldCurve(irs,title="US-Yields")
end

# ╔═╡ 4f92cca1-5e5c-4e3a-9f95-87536be9d7a3
begin
  _curve = getcurve(yc,.0,dcc=dcc,yf=true)
  plot(
	   _curve[1],100.0*_curve[2],legend=nothing,
	   title="US-Yields, March 10, 2025",xaxis=(L"T",0:2:30),
	   yaxis=("in %",3.9:0.1:4.75),lw=1.25,color=:gray3,
	   titlefontsize=10
  )
end

# ╔═╡ aa5ac8a4-7812-4613-a23a-8bffac2276d9
md"""
### Instruments
"""

# ╔═╡ 223bf7c1-cfdd-44d1-b8df-78aea0ba1bbe
begin
  strike = 0.0425
  ir = InterestRate(Tenor(1//4),currency=ccy)
  swap = Swap(0.05,11.0,ir,inception=1.0,dccflt=dcc)

  swaption = Swaption(strike,1.0,swap,inception=0.0)
end

# ╔═╡ a06a0cb5-7179-4799-9b50-f0af5dba36c9
md"""
### Pricers
"""

# ╔═╡ b273d932-aaa4-4607-b5e2-f4d98349c29a
begin
  blk76 = Black76(x0, currency=ccy)
  function σprice(σ::Real,T::Real,fixings::AbstractVector{R}) where R<:Real
    Zsofia._evaluate(
		             Black76, Swaption, x0, .0, T, x0, σ, fixings,
	                 yc, 0.0, 1.0, true, dcc
	)
  end
end

# ╔═╡ d3d39d10-1801-405f-bd9e-9846add52078
begin
  function dummyfixing(T::Real,swapexp::Real)
    collect(T+0.25k for k in 1:floor(Int,swapexp/0.25))
  end

  foo(σ::Real, T::Real,swapexp::Real) = σprice(σ,T,dummyfixing(T,swapexp))
end

# ╔═╡ 0c05fa6b-81e1-45fb-8c3f-7559c27a310c
begin
 _axis1 = [1/12,3/12,6/12,1,2,3,5,7,10] # swaptions (T)
 _axis2 = [1,2,3,5,7,10,15,20,30] * 1.0 # underlying swap (Ts)
 ATM = [
	    71.7 68.2 61.8 56.0 50.6 49.7 44.4 43.4 43.0;
	    73.6 68.8 61.1 55.9 51.5 49.6 44.0 42.8 42.8;
	    72.2 70.0 60.0 55.6 51.9 49.0 42.9 42.1 41.9;
	    73.8 65.5 57.1 52.9 49.7 46.6 40.8 40.3 40.0;
	    73.7 59.0 51.4 47.2 45.4 42.0 37.0 36.6 36.4;
	    57.8 49.0 44.3 41.8 40.4 37.9 34.3 34.1 33.4;
	    39.7 37.8 36.6 35.6 34.8 33.5 31.7 31.6 31.3;
	    34.4 33.4 32.4 31.7 31.2 30.7 30.0 29.7 29.2;
	    29.8 29.4 29.1 28.8 28.7 28.8 28.2 27.5 26.8
      ] * 0.01
end

# ╔═╡ 86dd43cd-ee20-4362-8d4a-d04110fb7fed
begin
  surface(
	      _axis1, _axis2, 100*ATM, xlabel=L"T",ylabel=L"T_s",
	      zlabel="in %", camera=(47,15), fc=:heat,
	      title="ATM volatilities, quotes", titlefontsize=10
  )
end

# ╔═╡ dc28e146-6a95-43ba-bead-cd1f3928b6fb
_axis = collect(Iterators.product(_axis1, _axis2))

# ╔═╡ 3267f6e2-711e-4267-8e27-4f0ab0173b9c
blackPrices = map((x,y) -> foo(x,y...), ATM, _axis)

# ╔═╡ 3ca421e5-9557-41a7-b2ee-0c1b3161fa26
begin
  surface(
	      _axis1, _axis2, blackPrices, xlabel=L"T", ylabel=L"T_s",
	      title="Black76 impl. prices", titlefontsize=10, fc=:heat
  )
end

# ╔═╡ 95c2283d-0694-46f7-afb9-ec43979c954c


# ╔═╡ Cell order:
# ╟─23ff20be-30fa-4b99-afdc-e2b8a48db8fc
# ╟─2b175663-02cd-4134-bea8-bdb7af491626
# ╠═58297162-1aa2-4ebe-8b18-d59eec880a1a
# ╠═1ec1a1ac-e67e-436c-a9b4-0358d53f628f
# ╠═4f92cca1-5e5c-4e3a-9f95-87536be9d7a3
# ╠═aa5ac8a4-7812-4613-a23a-8bffac2276d9
# ╠═223bf7c1-cfdd-44d1-b8df-78aea0ba1bbe
# ╠═a06a0cb5-7179-4799-9b50-f0af5dba36c9
# ╠═b273d932-aaa4-4607-b5e2-f4d98349c29a
# ╠═d3d39d10-1801-405f-bd9e-9846add52078
# ╠═0c05fa6b-81e1-45fb-8c3f-7559c27a310c
# ╠═86dd43cd-ee20-4362-8d4a-d04110fb7fed
# ╠═dc28e146-6a95-43ba-bead-cd1f3928b6fb
# ╠═3267f6e2-711e-4267-8e27-4f0ab0173b9c
# ╠═3ca421e5-9557-41a7-b2ee-0c1b3161fa26
# ╠═95c2283d-0694-46f7-afb9-ec43979c954c
