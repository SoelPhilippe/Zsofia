# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Black76 <: GeneralModels
  
  Black (aka Black-76) model.


  
  # Fields
  - `required::NTuple{1,String}`: required parameters' names, for the case  "σ". 
  - `optional::Tuple{String}`: optional parameters,  "carryrate".
  - `riskfreerate::InterestRate`: the risk-free interest rate.

  # Constructors
      Black76(riskfreerate::Real; [currency])
      Black76(riskfreerate::InterestRate)

  ## Arguments
  - `riskfreerate::Union{Real,IntrestRate}`: risk-free rate.
  - `currency::Currency=Currency()`: kwarg, the currency.
  
  See also [`BlackScholes`](@ref)
  
  # Notes
  The Black-76 model assumes a zero-drift dynamic for the futures (strike)
  contract price level:  
      ``dF_t = σF_t dW_t``
  
  The model assumes a constant interest rate, but this is not enforced
  by the API: the argument `riskfreerate::InterestRate` can be such that
  `riskfreerate.isfixed == false` evaluates to `true`.
  
  In different methods using the model, whevener an `Observables` is used
  in conjunction with the model, the `Black76.required` parameters are
  meant to be found in `Observables`' parameter containers. See
  [`Observables`](@ref), [`setparams!`](@ref) or [`getparams`](@ref).
  
  The optional parameter we ubiquituously and invariably call "carryrate"
  is also known in the literature as the "convenience yield", "portage cost"...
  
  We chose to classify the model into the `EquityModels` silo, this does not
  restruc it's use.
  """
  struct Black76 <: EquityModels
    required::NTuple{1,AbstractString}
    optional::NTuple{1,AbstractString}
    riskfreerate::InterestRate
    
    function Black76(rate::InterestRate)
      new(("σ",), ("carryrate",), rate)
    end
    
    function Black76(rate::R; currency::Currency=Currency()) where R<:Real
      Black76(InterestRate(rate,currency=currency))
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", m::Black76)
    print(
          io, "Black76", getfield(m,:required), " [",
          getfield(m,:optional), "]\t rate level: ",
          S(getfield(m,:riskfreerate))()
    )
  end
  
  Base.string(::Black76) = "Black76"
  
  """
      fit(m::Black76, prices, times; <kwargs>)
      fit(m::Black76, obs, from::TimePoint, to::TimePoint; <kwargs>)
      fit(m::Black76, obs, from::TimePoint; <kwargs>)
      fit(m::Black76, obs, <kwargs>)
  
  Return a `NamedTuple` of parameters estimates.
  
  # Arguments
  - `m::Black76`: an instance of the `Black76` model.
  - `prices::Vector{Real}`: observed prices to fit.
  - `times::Vector{TimePoint}`: observation times.
  - `obs::Observables`: financial instrument in consideration.
  - `from::TimePoint`: use `obs`' prices from `from` to `to`.
  - `to::TimePoint`: use `obs` prices from `from` to `to`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  The methods are either `fit(m::Black76, obs::Observables, ...)` or
  `fit(m::Black76, prices::Vector{Real}, times::Vector{TimePoint}, ...)`.
  
  The current implementation does not restrict `::Observables`
  to any of its subtypes though the Black being mostly categorized (in the
  types graph) in `EquityModels`.
  
  Note that whenever the method `obs` is used, `m.required` parameters are
  used/stored into `obs`' parameters container. appropriate containers.
  
  As most of the time throughout the package, the implementation does not
  perform sorting checks.
  """
  function StatsBase.fit(
                         m::Black76,
                         prices::AbstractVector{R},
                         times::AbstractVector{T};
                         dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint}
    @assert isequal(length(prices),length(times))
    n = length(prices)
    @assert isless(n,3) throw(DomainError(prices,"too few data ($(n) < 3)"))
    (σ = std(./(diff(log.(prices)), sqrt.(diff(dcc,times)))),)
  end
  
  function StatsBase.fit(
                         m::Black76,
                         obs::Observables,
                         from::T,
                         to::T;
                         dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint}
    i = searchsortedfirst(getfield(obs,:times), from)
    j = searchsortedlast(getfield(obs,:times), to)
    c = Colon()(i,j)
    fit(m,view(getfield(obs,:prices),c),view(getfield(obs,:times),c),dcc=dcc)
  end
  
  function StatsBase.fit(
                         m::Black76,
                         obs::Observables,
                         from::T;
                         dcc::DayCountConventions=ActAct()
                     ) where T<:TimePoint
    i = searchsortedfirst(getfield(obs,:times),from)
    c = Colon()(i,length(getfield(obs,:times)))
    fit(m,view(getfield(obs,:prices),c),view(getfield(obs,:times),c),dcc=dcc)
  end
  
  function StatsBase.fit(
                         m::Black76,
                         obs::Observables,
                         dcc::DayCountConventions=ActAct()
                     )
    fit(m,getfield(obs,:prices),getfield(obs,:times),dcc=dcc)
  end
  
  """
      fit!(m::Black76, obs, prices, times; <kwargs>)
      fit!(m::Black76, obs, [from::TimePoint, to::TimePoint]; <kwargs>)
  
  `fit` the model `m` to `obs` risk factor levels and store result in-place.
  The return value is the same as `fit`.
  
  # Arguments
  - `m::Black76`: Black model instance.
  - `prices::Vector{Real}`: risk factor levels (prices).
  - `times::Vector{TimePoint}`: `prices` observed at `times`.
  - `obs::Observables`: observable to get prices from, and fit parameters into.
  - `from::TimePoint`: use `obs`' prices from `from` to `to`.
  - `to::TimePoint`: use `obs` prices from `from` to `to`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `force::Bool=false`: kwarg, if `false` throw error if params already exist.
  
  # Notes
  The methods are either `fit(m::Black76, obs::Observables, ...)` or
  `fit(m::Black76, prices::Vector{Real}, times::Vector{TimePoint}, ...)`.
  
  The implementation does not perform sorting check, this is left to users'
  discretion.
  """
  function StatsBase.fit!(
                          m::Black76,
                          obs::Observables,
                          prices::AbstractVector{R},
                          times::AbstractVector{T};
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where {R<:Real,T<:TimePoint}
    _f = fit(m, prices, times, dcc=dcc)
    for (k,v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::Black76,
                          obs::Observables,
                          from::T,
                          to::T;
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where {T<:TimePoint}
    _f = fit(m, obs, from, to, dcc=dcc)
    for (k, v) in zip(getfield(m, :required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::Black76,
                          obs::Observables,
                          from::T;
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where T<:TimePoint
    _f = fit(m, obs, from, dcc=dcc)
    for (k, v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::Black76,
                          obs::Observables;
                          dcc::DayCountConventions=ActAct(),
                          foce::Bool=false
           )
    _f = fit(m, obs, dcc=dcc)
    for (k, v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  """
      simulate(m::Black76, par::Tuple{Real,Real}, from, to, dt; <kwargs>)
      simulate(m::Black76, obs::Observables, from, to, dt; <kwargs>)
  
  Return a `Tuple{Vector{TimePoint},Vector{Real}}` holding the vector of
  simulation timestamps and the vector of simulated prices, respectively.
  
  # Arguments
  - `m::Black76`: Black model.
  - `par::Tuple{Real,Real}`: tuple of required parameters, ("μ", "σ").
  - `obs::Observables`: underlying financial instrument.
  - `from::TimePoint`: simulate from `from` to `to`.
  - `to::TimePoint`: simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`. 
  - `n::Int64=1`: kwarg, number of paths to simulate.
  - `nsteps::Integer=360`: kwarg, lower-level (WienerProcess) simulation steps.
  - `x0::Real=1.0`: kwarg, simulation starting point level.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When `obs::Observables` is provided, the `x0` kwarg
  defaults to the very last observation. When `obs::Observables`
  is provided, its parameters container should contain the
  required parameters otherwise an error is thrown.
  
  The interface doesn't not provide a special purpose facility for
  the (continuous) carry-rate! It is up to the user to adapt `par[1]` in
  that regard.
  """
  function simulate(
                    m::Black76,
                    par::Tuple{A,B},
                    from::T,
                    to::T,
                    dt::Period;
                    n::Int=1,
                    nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                    x0::R=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,T<:TimeType,R<:Real}
    @assert isless(from,to)
    u = Colon()(from, dt, to)
    t = in(to,u) ? collect(u) : vcat(u,to)
    τ = vcat(0, diff(dcc, t))
    s = yearfraction(dcc,first(t),last(t))
    p = length(t)
    q = max(nsteps, ceil(Int, /(s, maximum(τ))))
    @assert isless(1,min(p,q)) "too few time points ($(p))"
    r = -0.5first(par)^2
    inds = map(i -> ceil(Int, /(q,p-1)*(i-1) + 1), Colon()(1,p))
    wi   = WienerProcess(n, s, nsteps=q, dims=1)
    simulate!(wi)
    (t, x0*exp.(.+(r*cumsum(τ),first(par)*view(getfield(wi,:paths),inds,:))))
  end
  
  function simulate(
                    m::Black76,
                    par::Tuple{A,B},
                    from::T1,
                    to::T2,
                    dt::P;
                    n::Int=1,
                    nsteps::Int=ceil(Int,360*(to-from)),
                    x0::R=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,T1<:Real,T2<:Real,P<:Real,R<:Real}
    @assert isless(0, dt)
    @assert isless(from, to)
    p = ceil(Int,/(to-from,dt)) + 1
    q = max(nsteps,p)
    t = collect(LinRange(from,to,q))
    r = -0.5first(par)^2
    w = WienerProcess(n,last(t),nsteps=q-1, dims=1)
    simulate!(w)
    (t,x0*exp.(.+(r*t,first(par)*getfield(w,:path))))
  end
  
  function simulate(
                    m::Black76,
                    obs::Observables,
                    from::T,
                    to::T,
                    dt::Union{Period,<:Real};
                    n::Int=1,
                    nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                    x0::R=S(obs)(from),
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,R<:Real}
    for k in getfield(m, :required)
      @assert hasparam(obs, k) "required parameter $(k) can't be found"
    end
    opt = only(getfield(m, :optional))
    par = [getparams(obs, k) for k in getfield(m, :required)]
    par[1] -= getparams(obs,opt,0)
    simulate(m, Tuple(par), from, to, dt, n=n, nsteps=nsteps, x0=x0, dcc=dcc)
  end

  """
      simulate!(m::Black76, obs, par::Tuple{Real,Real}, from, to, dt; <kwargs>)
      simulate!(m::Black76, obs::Observables, from, to, dt; <kwargs>)
  
  `simulate` and store the proceed in-place in `obs`' containers.
  If more than one paths is simulated, the averaged process is stored.
  
  # Arguments
  - `m::Black76`: Black model instance.
  - `par::Tuple{Real,Real}`: a tuple of required parameters values, ("μ", "σ").
  - `obs::Observables`: underlying observables.
  - `from::TimePoint`: simulate from `from` to `to`.
  - `to::TimePoint`: simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`. 
  - `n::Integer=1`: kwarg, number of simulation paths.
  - `nsteps::Integer=360`: kwarg, simulations time granularity.
  - `x0::Real=obs.prices[end]`: kwarg, starting point.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When more than one path is simulated, the time-sectional average is stored,
  while the whole bunch of different simulation paths are still returned.
  """
  function simulate!(
                     m::Black76,
                     obs::Observables,
                     par::Tuple{A,B},
                     from::T,
                     to::T,
                     dt::Union{Period,Real};
                     n::Int=1,
                     nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                     x0::R=S(obs)(from),
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,R<:Real,T<:TimePoint}
    _sim = simulate(m,par,from,to,dt,n=n,nsteps=nsteps,x0=x0,dcc=dcc)
    inserprices!(obs,dropdims(mean(last(_sim),dims=2),dims=2),first(_sim))
    _sim
  end

  function simulate!(
                     m::Black76,
                     obs::Observables,
                     from::T,
                     to::T,
                     dt::Union{Period,Real};
                     n::Int=1,
                     nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                     x0::R=S(obs)(from),
                     rn::Bool=false,
                     dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint}
    _sim = simulate(m,obs,from,to,dt,n=n,nsteps=nsteps,x0=x0,dcc=dcc)
    insertprices!(obs,dropdims(mean(last(_sim),dims=2),dims=2),first(_sim))
    _sim
  end
end