# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      HullWhite1F <: RatesModels
  
  The one-factor Hull-White model.
  
  # Fields
  - `required::NTuple{2,String}`: required parameters' names, "θ", and "σ".
  - `optional::Tuple{String}`: optinal parameters' names, void string.
  
  # Constructors
      HullWhite1F()
  
  # Arguments
  None
  
  # Notes
  What we implement as being the Hull-White one-factor model is actually
  the model analyzed by Hull and White (1994) as the model colloquially being
  known as the extended Vasicek; modelling the short rate to have the
  following dynamic:
  
      ```math
      dr_t = (φ_t - θr_t)dt + σdW_t
      ```
  The function ``φ_t`` being a deterministic function and θ and σ being
  constant (positive) parameters.
  
  The short rate ``r_t`` is then built such that:
      
      ```math
      r_t = g_t + x_t
      ```
  
  Where ``x_t`` has the Ornstein-Uhlenbeck dynamic with θ and σ parameters and
  ``g_t`` is a deterministic function (see below), basically a deterministic
  term related to the integral of ``φ_t`` (see Interest Rates Models - Theory
  and Practice, with smile, inflation and credit; by Damiano Brigo and Fabio
  Mercurio, Springer - 2006, Chp.3 pp. 73 for more details).
  
  ```math
  g_t = f^{M}(0,t) + \\frac{σ^2}{2θ^2}\\left( 1-exp(-θt) \\right)^2
  ```
  
  Where ``f^{M}(0,t)`` is the (market) instantaneous forward rate at time
  0 for the maturity ``t``.
  """
  struct HullWhite1F <: RatesModels
    required::NTuple{2,AbstractString}
    optional::Tuple{AbstractString}
    
    HullWhite1F() = new(("θ","σ"),("",))
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ::HullWhite1F)
    print(io, "HullWhite1F[θ,σ]")
  end
end
