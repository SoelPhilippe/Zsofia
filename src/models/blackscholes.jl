# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      BlackScholes <: EquityModels
  
  The **Black-Scholes** model.
  
  # Fields
  - `required::NTuple{2,String}`: required parameters' names,  "μ" and "σ". 
  - `optional::Tuple{String}`: optional parameters,  "carryrate".
  - `riskfreerate::InterestRate`: assuming model riskfree rate.
  
  # Constructors
      BlackScholes(rate::Real; [currency])
      BlackScholes(rate::InterestRate)
  
  See also [`Black76`](@ref).
  
  ## Arguments
  - `rate::Union{Real,InterestRate}`: (risk-free) rate.
  - `currency::Currency=Currency()`: when `rate` is a number, specify currency.
  
  # Notes
  The Black-Scholes model assumes the underlying asset to have
  its price process following a geometric brownian motion having
  the dynamic:
  
  ```math
  dS_t = μS_t dt + σS_tdW_t
  ```
  
  Where ``(W_t)_t`` is the **Wiener process**. For a quick glance
  on the model, watch out the
  [wikipedia](https://en.wikipedia.org/wiki/Black%E2%80%93Scholes_model)
  page.
  
  See also [`WienerProcess`](@ref)
  
  # Examples
  ```julia
  julia> bs = BlackScholes(0.05, currency=Currency("USD"));
  
  julia> bs isa BlackScholes
  true
  
  julia> BlackScholes <: EquityModels
  true
  ```
  """
  struct BlackScholes <: EquityModels
    required::NTuple{2,AbstractString}
    optional::Tuple{AbstractString}
    riskfreerate::InterestRate
    
    function BlackScholes(rate::InterestRate)
      new(("μ", "σ"), ("carryrate",), rate)
    end
    
    function BlackScholes(rate::R; currency::Currency=Currency()) where R<:Real
      BlackScholes(InterestRate(rate,currency=currency))
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", m::BlackScholes)
    print(
          io, "BlackScholes",m.required," [",m.optional,"]\t rate level: ",
          S(getfield(m,:riskfreerate))()
    )
  end
  
  Base.string(::BlackScholes) = "BlackScholes"
  
  """
      fit(m::BlackScholes, prices, times; <kwargs>)
      fit(m::BlackScholes, obs, [from::TimePoint, to::TimePoint]; <kwargs>)
  
  `fit` the model `m` to `obs` risk factor levels.
  
  # Arguments
  - `m::BlackScholes`: an instantiated Black-Scholes model.
  - `prices::Vector{Real}`: observed prices to fit.
  - `times::Vector{TimePoint}`: observation times.
  - `obs::Observables`: financial instrument in consideration.
  - `from::TimePoint`: use `obs`' prices from `from` to `to`.
  - `to::TimePoint`: use `obs` prices from `from` to `to`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  The methods are either `fit(m::BlackScholes, obs::Observables, ...)` or
  `fit(m::BlackScholes, prices::Vector{Real}, times::Vector{TimePoint}, ...)`.
  
  The implementation does not perform sorting check, this is left to users'
  discretion.
  """
  function StatsBase.fit(
                         m::BlackScholes,
                         prices::AbstractVector{R},
                         times::AbstractVector{T};
                         dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint}
    @assert isequal(length(prices),length(times))
    n = length(prices)
    @assert isless(2,n) throw(DomainError(prices,"too few data ($(n) < 3)"))
    r ,τ = diff(log.(prices)), diff(dcc,times)
    σ2,α = var(./(r, sqrt.(τ))), mean(./(r,τ))
    (μ = α + 0.5σ2, σ = sqrt(σ2))
  end
  
  function StatsBase.fit(
                         m::BlackScholes,
                         obs::Observables,
                         from::T,
                         to::T;
                         dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint}
    i = searchsortedfirst(getfield(obs,:times),from)
    j = searchsortedlast(getfield(obs,:times),to)
    c = Colon()(i,j)
    fit(m,view(getfield(obs,:prices),c),view(getfield(obs,:times),c),dcc=dcc)
  end
  
  function StatsBase.fit(
                         m::BlackScholes,
                         obs::Observables,
                         from::T;
                         dcc::DayCountConventions=ActAct()
           ) where T<:TimePoint
    i = searchsortedfirst(getfield(obs,:times), from)
    c = Colon()(i, length(getfield(obs,:times)))
    fit(m,view(getfield(obs,:prices),c),view(getfield(obs,:times),c),dcc=dcc)
  end
  
  function StatsBase.fit(
                         m::BlackScholes,
                         obs::Observables,
                         dcc::DayCountConventions=ActAct()
                     )
    fit(m,getfield(obs,:prices),getfield(obs,:times),dcc=dcc)
  end
  
  """
      fit!(m::BlackScholes, obs, prices, times; <kwargs>)
      fit!(m::BlackScholes, obs, [from::TimePoint, to::TimePoint]; <kwargs>)
  
  `fit` the model `m` to `obs` risk factor levels and store result in-place.
  The return value is the same as `fit`.
  
  # Arguments
  - `m::BlackScholes`: Black-Scholes model instance.
  - `prices::Vector{Real}`: risk factor levels (prices).
  - `times::Vector{TimePoint}`: `prices` observed at `times`.
  - `obs::Observables`: observable to get prices from, and fit parameters into.
  - `from::TimePoint`: use `obs`' prices from `from` to `to`.
  - `to::TimePoint`: use `obs` prices from `from` to `to`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `force::Bool=false`: kwarg, if `false` throw error if params already exist.
  
  # Notes
  The methods are either `fit(m::BlackScholes, obs::Observables, ...)` or
  `fit(m::BlackScholes, prices::Vector{Real}, times::Vector{TimePoint}, ...)`.
  
  The implementation does not perform sorting check, this is left to users'
  discretion.
  """
  function StatsBase.fit!(
                          m::BlackScholes,
                          obs::Observables,
                          prices::AbstractVector{R},
                          times::AbstractVector{T};
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where {R<:Real,T<:TimePoint}
    _f = fit(m, prices, times, dcc=dcc)
    for (k,v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::BlackScholes,
                          obs::Observables,
                          from::T,
                          to::T;
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where {T<:TimePoint}
    _f = fit(m, obs, from, to, dcc=dcc)
    for (k, v) in zip(getfield(m, :required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::BlackScholes,
                          obs::Observables,
                          from::T;
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where T<:TimePoint
    _f = fit(m, obs, from, dcc=dcc)
    for (k, v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::BlackScholes,
                          obs::Observables;
                          dcc::DayCountConventions=ActAct(),
                          foce::Bool=false
           )
    _f = fit(m, obs, dcc=dcc)
    for (k, v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  """
      simulate(m::BlackScholes, par::Tuple{Real,Real}, from, to, dt; <kwargs>)
      simulate(m::BlackScholes, obs::Observables, from, to, dt; <kwargs>)
  
  Return a `Tuple{Vector{TimePoint},Vector{Real}}` holding the vector of
  simulation timestamps and the vector of simulated prices, respectively.
  
  # Arguments
  - `m::BlackScholes`: Black-Scholes model.
  - `par::Tuple{Real,Real}`: tuple of required parameters, ("μ", "σ").
  - `obs::Observables`: underlying financial instrument.
  - `from::TimePoint`: simulate from `from` to `to`.
  - `to::TimePoint`: simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`. 
  - `n::Int64=1`: kwarg, number of paths to simulate.
  - `nsteps::Integer=360`: kwarg, lower-level (WienerProcess) simulation steps.
  - `x0::Real=1.0`: kwarg, simulation starting point level.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When `obs::Observables` is provided, the `x0` kwarg
  defaults to the very last observation. When `obs::Observables`
  is provided, its parameters container should contain the
  required parameters otherwise an error is thrown.
  
  The interface doesn't not provide a special purpose facility for
  the (continuous) carry-rate! It is up to the user to adapt `par[1]` in
  that regard.
  """
  function simulate(
                    m::BlackScholes,
                    par::Tuple{A,B},
                    from::T1,
                    to::T2,
                    dt::Period;
                    n::Int=1,
                    nsteps::Int=ceil(Int,360*yearfraction(ActAct(),from,to)),
                    x0::R=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,T1<:TimeType,T2<:TimeType,R<:Real}
    @assert isless(from,to)
    u = Colon()(from, dt, to)
    t = in(to,u) ? collect(u) : vcat(u,to)
    τ = vcat(0, diff(dcc, t))
    s = yearfraction(dcc,first(t),last(t))
    p = length(t)
    q = max(nsteps, ceil(Int, /(s, maximum(τ))))
    @assert isless(1,min(p,q)) "too few time points ($(p))"
    inds = map(i -> ceil(Int, /(q,p-1)*(i-1) + 1), Colon()(1,p))
    r = first(par) - 0.5last(par)^2
    wi   = WienerProcess(n, s, nsteps=q, dims=1)
    simulate!(wi)
    (t, x0*exp.(.+(r*cumsum(τ),last(par)*view(getfield(wi,:paths),inds,:))))
  end
  
  function simulate(
                    m::BlackScholes,
                    par::Tuple{A,B},
                    from::T1,
                    to::T2,
                    dt::P;
                    n::Int=1,
                    nsteps::Int=ceil(Int,360*(to-from)),
                    x0::R=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,T1<:Real,T2<:Real,P<:Real,R<:Real}
    @assert isless(0, dt)
    @assert isless(from, to)
    p = ceil(Int,/(to-from,dt)) + 1
    q = max(nsteps,p)
    t = collect(LinRange(from,to,q))
    r = first(par) - 0.5last(par)^2
    w = WienerProcess(n,last(t),nsteps=q-1,dims=1)
    simulate!(w)
    (t,x0*exp.(.+(r*t,last(par) * getfield(w,:paths))))
  end
  
  function simulate(
                    m::BlackScholes,
                    obs::Observables,
                    from::T,
                    to::T,
                    dt::Union{Period,<:Real};
                    n::Int=1,
                    nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                    x0::R=S(obs)(from),
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,R<:Real}
    for k in getfield(m, :required)
      @assert hasparams(obs, k) "required parameter $(k) can't be found"
    end
    opt = only(getfield(m, :optional))
    par = [getparams(obs, k) for k in getfield(m, :required)]
    par[1] -= getparams(obs,opt,0)
    simulate(m, Tuple(par), from, to, dt, n=n, nsteps=nsteps, x0=x0, dcc=dcc)
  end

  """
      simulate!(m::BlackScholes, obs, par::Tuple{Real,Real}, from, to, dt; ...)
      simulate!(m::BlackScholes, obs::Observables, from, to, dt; <kwargs>)
  
  `simulate` and store the proceed in-place (`obs`).
  If more than one paths is simulated, the averaged process is stored.
  
  # Arguments
  - `m::BlackScholes`: Black-Scholes model instance.
  - `par::Tuple{Real,Real}`: a tuple of required parameters values, ("μ", "σ").
  - `obs::Observables`: underlying observables.
  - `from::TimePoint`: simulate from `from` to `to`.
  - `to::TimePoint`: simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`. 
  - `n::Integer=1`: kwarg, number of simulation paths.
  - `nsteps::Integer=360`: kwarg, simulations time granularity.
  - `x0::Real=obs.prices[end]`: kwarg, starting point.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When more than one path is simulated, the time-sectional average is stored,
  while the whole bunch of different simulation paths are still returned.
  """
  function simulate!(
                     m::BlackScholes,
                     obs::Observables,
                     par::Tuple{A,B},
                     from::T,
                     to::T,
                     dt::Union{Period,Real};
                     n::Int=1,
                     nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                     x0::R=S(obs)(from),
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,R<:Real,T<:TimePoint}
    _sim = simulate(m,par,from,to,dt,n=n,nsteps=nsteps,x0=x0,dcc=dcc)
    inserprices!(obs,dropdims(mean(last(_sim),dims=2),dims=2),first(_sim))
    _sim
  end

  function simulate!(
                     m::BlackScholes,
                     obs::Observables,
                     from::T,
                     to::T,
                     dt::Union{Period,Real};
                     n::Int=1,
                     nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                     x0::R=S(obs)(from),
                     rn::Bool=false,
                     dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint}
    _sim = simulate(m,obs,from,to,dt,n=n,nsteps=nsteps,x0=x0,dcc=dcc)
    insertprices!(obs,dropdims(mean(last(_sim),dims=2),dims=2),first(_sim))
    _sim
  end
end