# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Vasicek <: RatesModels

  The **Vasicek** model.

  # Fields
  - `required::NTuple{3,String}`: required parameters' names, "θ", "μ" and "σ".
  - `optional::Tuple{String}`: optional parameters' names, void string.

  # Constructors
      Vasicek()
  
  # Arguments
  None

  # Notes
  The Vasicek model (1977) assumes that the instantaneous spot rate
  under the real-world measure evolves as an Ornstein-Uhlenbeck process
  with constant coefficients. The dynamic is the following (for suitable
  parameters, this is also valid under the risk-neutral measure):

  ``dr_t = θ(μ-r_t)dt + σdW_t``

  Where ``(W_t)_t`` is the wiener process, see [`WienerProcess`](@ref).

  ## Parameters description
  |**Parameters**|**Description**|
  |:---:|:---:|
  |`θ`|reversion rate/speed/force|
  |`μ`|long-term mean|
  |`σ`|volatility level|

  # Examples
  ```julia
  julia> vasi = Vasicek();
  
  julia> vasi isa Vasicek
  true
  ```
  """
  struct Vasicek <: RatesModels
    required::NTuple{3,AbstractString}
    optional::Tuple{AbstractString}
    
    Vasicek() = new(("θ","μ", "σ"), ("",))
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ::Vasicek)
    print(io, "Vasicek[θ, μ, σ]")
  end
  
  """
      fit(m::Vasicek, prices, times; <kwargs>)
      fit(m::Vasicek, obs, [from::TimePoint, [to::TimePoint; [<kwargs>]]])

  Return an `NamedTuple{3,Real}` holding estimated model's required parameters
  resulting from the fitting procedure of the `Vasicek` model `m` to observed
  prices `prices` observed at `times`.

  # Arguments
  - `m::Vasicek`: Vasicek model instance.
  - `prices::Vector{Real}`: vector of observed underlying risk factor levels.
  - `times::Vector{TimePoint}`: observation times.
  - `obs::Observables`: the observable to get `prices` from.
  - `from::TimePoint`: when `obs` is provided, get `prices` from `from` to `to`.
  - `to::TimePoint`: when `obs` is provided, get `prices` from `from` to `to`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.

  # Notes
  When `obs` is provided, prices and times into `prices` and `times` containers.
  """
  function StatsBase.fit(
                         m::Vasicek,
                         prices::AbstractVector{R},
                         times::AbstractVector{T};
                         dcc::DayCountConventions=ActAct()
                     ) where {R<:Real,T<:TimePoint}
    @assert isequal(length(prices),length(times))
    n = length(prices)-1
    @assert isless(1,n) throw(DomainError(prices,"too few data (n=$(n+1) < 3)"))
    δ = /(sum(diff(dcc,times)),n)
    j, i = view(prices,Colon()(2,n+1)), view(prices,Colon()(1,n))
    α = /(-(n*sum(i .* j), sum(i) * sum(j)), n*sum(i .^ 2) - sum(i)^2)
    β = /(sum(j .- α*i), n*(1-α))
    v = /(sum((j .- α*i - β*(1-α)) .^ 2),n)
    (θ=/(-log(α),δ), μ=β, σ=sqrt(/(2*/(-log(α),δ),1-α^2)))
  end

  function StatsBase.fit(
                         m::Vasicek,
                         obs::Observables,
                         from::T,
                         to::T;
                         dcc::DayCountConventions=ActAct()
                     ) where T<:TimePoint
    i = searchsortedfirst(getfield(obs,:times),from)
    j = searchsortedlast(getfield(obs,:times),to)
    c = Colon()(i,j)
    fit(m,view(getfield(obs,:prices),c),view(getfield(obs,:times),c),dcc=dcc)
  end

  function StatsBase.fit(
                         m::Vasicek,
                         obs::Observables,
                         from::T;
                         dcc::DayCountConventions=ActAct()
                     ) where T<:TimePoint
    i = searchsortedfirst(getfield(obs,:times), from)
    c = Colon()(i, length(getfield(obs,:times)))
    fit(m,view(getfield(obs,:prices),c),view(getfield(obs,:times),c),dcc=dcc)
  end

  function StatsBase.fit(
                         m::Vasicek,
                         obs::Observables;
                         dcc::DayCountConventions=ActAct()
                     )
    fit(m,getfield(obs,:prices),getfield(obs,:times),dcc=dcc)
  end

  """
      fit!(m::Vasicek, obs, prices, times; <kwargs>)
      fit!(m::Vasicek, obs, [from::TimePoint, to::TimePoint]; <kwargs>)
  
  `fit` the model `m` to `obs` risk factor levels and store result in-place.
  The return value is the same as `fit`.
  
  # Arguments
  - `m::Vasicek`: Black-Scholes model instance.
  - `prices::Vector{Real}`: risk factor levels (prices).
  - `times::Vector{TimePoint}`: `prices` observed at `times`.
  - `obs::Observables`: observable to get prices from, and fit parameters into.
  - `from::TimePoint`: use `obs`' prices from `from` to `to`.
  - `to::TimePoint`: use `obs` prices from `from` to `to`.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `force::Bool=false`: kwarg, if `false` throw error if params already exist.
  
  # Notes
  The methods are either `fit(m::Vasicek, obs::Observables, ...)` or
  `fit(m::Vasicek, prices::Vector{Real}, times::Vector{TimePoint}, ...)`.
  
  The implementation does not perform sorting check, this is left to users'
  discretion.
  """
  function StatsBase.fit!(
                          m::Vasicek,
                          obs::Observables,
                          prices::AbstractVector{R},
                          times::AbstractVector{T};
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where {R<:Real,T<:TimePoint}
    _f = fit(m, prices, times, dcc=dcc)
    for (k,v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::Vasicek,
                          obs::Observables,
                          from::T,
                          to::T;
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where {T<:TimePoint}
    _f = fit(m, obs, from, to, dcc=dcc)
    for (k, v) in zip(getfield(m, :required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::Vasicek,
                          obs::Observables,
                          from::T;
                          dcc::DayCountConventions=ActAct(),
                          force::Bool=false
           ) where T<:TimePoint
    _f = fit(m, obs, from, dcc=dcc)
    for (k, v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end
  
  function StatsBase.fit!(
                          m::Vasicek,
                          obs::Observables;
                          dcc::DayCountConventions=ActAct(),
                          foce::Bool=false
           )
    _f = fit(m, obs, dcc=dcc)
    for (k, v) in zip(getfield(m,:required), _f)
      setparams!(obs, k, v, force=force)
    end
    _f
  end

  """
      simulate(m::Vasicek, par::Tuple{Real,Real,Real}, from, to, dt; <kwargs>)
      simulate(m::Vasicek, obs::Observables, from, to, dt; <kwargs>)
  
  Return a `Tuple{Vector{TimePoint},Vector{Real}}` holding the vector of
  simulation timestamps and the vector of simulated prices, respectively.
  
  # Arguments
  - `m::Vasicek`: Black-Scholes model.
  - `par::Tuple{Real,Real}`: tuple of required parameters, ("θ","μ", "σ").
  - `obs::Observables`: underlying financial instrument.
  - `from::TimePoint`: simulate from `from` to `to`.
  - `to::TimePoint`: simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`. 
  - `n::Int64=1`: kwarg, number of paths to simulate.
  - `nsteps::Integer=360`: kwarg, lower-level (WienerProcess) simulation steps.
  - `x0::Real=1.0`: kwarg, simulation starting point level.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When `obs::Observables` is provided, the `x0` kwarg
  defaults to the very last observation. When `obs::Observables`
  is provided, its parameters container should contain the
  required parameters otherwise an error is thrown.
  
  The interface doesn't not provide a special purpose facility for
  the (continuous) carry-rate! It is up to the user to adapt `par[1]` in
  that regard.
  """
  function simulate(
                    m::Vasicek,
                    par::Tuple{A,B,C},
                    from::T1,
                    to::T2,
                    dt::Period;
                    n::Int=1,
                    nsteps::Int=ceil(Int,360*yearfraction(ActAct(),from,to)),
                    x0::R=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T1<:TimeType,T2<:TimeType,R<:Real}
    @assert isless(from,to)
    u = Colon()(from, dt, to)
    t = in(to,u) ? collect(u) : vcat(u,to)
    τ = vcat(0, diff(dcc, t))
    s = yearfraction(dcc,first(t),last(t))
    p = length(t)
    q = max(nsteps, ceil(Int, /(s, maximum(τ))))
    @assert isless(1,min(p,q)) "too few time points ($(p))"
    inds = map(i -> ceil(Int, /(q,p-1)*(i-1) + 1), Colon()(1,p))
    ou = OrnsteinUhlenbeckProcess(
                                  n, s, theta=first(par), sigma=last(par),
                                  nsteps=q, dims=1
         )
    simulate!(ou, x0=x0)
    x = getfield(ou,:paths) .+ par[2]*(1 .- exp.(-ou.θ * ou.times))
    (t, view(x,inds,:))
  end

  function simulate(
                    m::Vasicek,
                    par::Tuple{A,B,C},
                    from::T1,
                    to::T2,
                    dt::P;
                    n::Int=1,
                    nsteps::Int=ceil(Int,360*(to-from)),
                    x0::R=1,
                    dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,T1<:Real,T2<:Real,P<:Real,R<:Real}
    @assert isless(0, dt)
    @assert isless(from, to)
    p = ceil(Int,/(to-from,dt)) + 1
    q = max(nsteps,p)
    t = collect(LinRange(from,to,q))
    ou = OrnsteinUhlenbeckProcess(
                                  n, last(t), nsteps=q-1, theta=first(par),
                                  sigma=last(par), dims=1
         )
    s = last(t)
    simulate!(ou, x0=x0)
    (t,getfield(ou,:paths) .+ par[2]*(1 .- exp.(-ou.θ * ou.times)))
  end
  
  function simulate(
                    m::Vasicek,
                    obs::Observables,
                    from::T,
                    to::T,
                    dt::Union{Period,<:Real};
                    n::Int=1,
                    nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                    x0::R=S(obs)(from),
                    dcc::DayCountConventions=ActAct()
           ) where {T<:TimePoint,R<:Real}
    for k in getfield(m, :required)
      @assert hasparams(obs, k) "required parameter $(k) can't be found"
    end
    opt = only(getfield(m, :optional))
    par = [getparams(obs, k) for k in getfield(m, :required)]
    simulate(m, Tuple(par), from, to, dt, n=n, nsteps=nsteps, x0=x0, dcc=dcc)
  end

  """
      simulate!(m::Vasicek, obs, par::Tuple{Real,Real}, from, to, dt; ...)
      simulate!(m::Vasicek, obs::Observables, from, to, dt; <kwargs>)
  
  `simulate` and store the proceed in-place (`obs`).
  If more than one paths is simulated, the averaged process is stored.
  
  # Arguments
  - `m::Vasicek`: Black-Scholes model instance.
  - `par::Tuple{Real,Real,Real}`: a tuple of required parameters ("θ","μ", "σ").
  - `obs::Observables`: underlying observables.
  - `from::TimePoint`: simulate from `from` to `to`.
  - `to::TimePoint`: simulate from `from` to `to`.
  - `dt::Union{Period,Real}`: time delta, simulate `from` `to` with step `dt`. 
  - `n::Integer=1`: kwarg, number of simulation paths.
  - `nsteps::Integer=360`: kwarg, simulations time granularity.
  - `x0::Real=obs.prices[end]`: kwarg, starting point.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  When more than one path is simulated, the time-sectional average is stored,
  while the whole bunch of different simulation paths are still returned.
  """
  function simulate!(
                     m::Vasicek,
                     obs::Observables,
                     par::Tuple{A,B,C},
                     from::T,
                     to::T,
                     dt::Union{Period,Real};
                     n::Int=1,
                     nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                     x0::R=S(obs)(from),
                     dcc::DayCountConventions=ActAct()
           ) where {A<:Real,B<:Real,C<:Real,R<:Real,T<:TimePoint}
    _sim = simulate(m,par,from,to,dt,n=n,nsteps=nsteps,x0=x0,dcc=dcc)
    inserprices!(obs,dropdims(mean(last(_sim),dims=2),dims=2),first(_sim))
    _sim
  end

  function simulate!(
                     m::Vasicek,
                     obs::Observables,
                     from::T,
                     to::T,
                     dt::Union{Period,Real};
                     n::Int=1,
                     nsteps::Int=ceil(Int,360yearfraction(ActAct(),from,to)),
                     x0::R=S(obs)(from),
                     rn::Bool=false,
                     dcc::DayCountConventions=ActAct()
           ) where {R<:Real,T<:TimePoint}
    _sim = simulate(m,obs,from,to,dt,n=n,nsteps=nsteps,x0=x0,dcc=dcc)
    insertprices!(obs,dropdims(mean(last(_sim),dims=2),dims=2),first(_sim))
    _sim
  end
end