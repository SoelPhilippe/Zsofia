# This script is part of Zsofia. Soel Philippe © 2025

if !(isdefined(@__MODULE__, :AbstractModel))
  abstract type AbstractModel end
end

begin
  abstract type GeneralModels     <: AbstractModel end
  abstract type EquityModels      <: AbstractModel end
  abstract type RatesModels       <: AbstractModel end
  abstract type VolatilityModels  <: AbstractModel end
end

begin
  struct DummyModel <: AbstractModel end
end