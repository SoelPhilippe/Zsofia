# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      MonteCarlo{M} <: AbstractModel where M<:AbstractModel
  
  Monte-Carlo model.
  
  The struct `MonteCarlo` gathers a single field: the underlying
  `model` specifying the operating framework.
  
  # Fields
  - `model::AbstractModel`: the underlying model.
  
  # Constructors
      MonteCarlo(model::AbstractModel)
  
  See [`BlackScholes`](@ref), [`Black76`](@ref).
  
  ## Arguments
  - `model::AbstractModel`: the framework to perform monte-carlo upon.
  
  # Examples
  ```julia
  julia> mc = MonteCarlo(BlackScholes(0.04528,currency=Currency("USD"));
  
  julia> mc isa MonteCarlo
  true
  ```
  """
  struct MonteCarlo{M} <: AbstractModel
    model::M where M<:Union{<:AbstractModel,Tuple{<:AbstractModel,Vararg}}
    
    function MonteCarlo(m::A...) where A<:AbstractModel
      if iszero(length(m))
        new{DummyModel}(DummyModel())
      elseif isone(length(m))
        new{A}(only(m))
      else
        new{typeof(m)}(m)
      end
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", mc::MonteCarlo)
    _b = reduce(*, string(h) * "," for h in getfield(mc,:model))
    print(io, "MonteCarlo{",strip(_b,','),"}")
  end
end