# This script is part of Zsofia. Soel Philippe © 2025


begin
  """
      NoArbitrage <: AbstractModel
  
  Technically, we define this as an `AbstractModel` since it's the most robust
  and most natural technique for pricing derivatives. It is the essence of
  the derivatives pricing theory.
  
  See also [`MonteCarlo`](@ref).
  
  # Notes
  This is a singleton type to be given to functions like `evaluate` for
  derivatives pricing.
  
  # Examples
  ```julia
  julia> pltr = Stock("PLTR");
  
  julia> setprices!(pltr, [105.25,106.57,109.97], [0.01,0.02,0.03]);
  
  julia> forward = Forward(105.75, 0.03, pltr, islong=true, inception=0.0);
  
  julia> isa(forward, Forward) && isa(forward, Forward{Stock})
  true
  
  julia> r = 0.05;
  
  julia> v = S(pltr)(forward.inception)*exp(r*forward.expiry) - forward.strike; 
  
  julia> isequal(evaluate(NoArbitrage(), forward, r), v)
  true
  ```
  """
  struct NoArbitrage <: AbstractModel end
end