# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Margrabe <: EquityModels
  
  A type intended to represent the Margrabe's change of numeraire technique
  to price outperformance options, see [`SpreadOption`](@ref).
  
  # Constructors
      Margrabe
  
  See also [`MonteCarlo`](@ref).
  
  # Notes
  This type was intially built to represent what's known as the
  Margrabe's formula for `SpreadOption`s pricing.
  """
  struct Margrabe <: AbstractModel end
end