# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      LongstaffSchwartz{M} <: AbstractModel
  
  LongstaffSchwartz model for applying the eponumous algorithm.
  
  # Fields
  - `m::M`: underlying model for the risk factor dynamic.

  # Constructors
      LongstaffSchwartz(model::AbstractModel)
  
  Pass `model` as parameter.

  # Notes
  The `LongstaffSchwartz` model is intended to be applied for
  derivatives pricing.
  """
  struct LongstaffSchwartz{M} <: AbstractModel
    m::M where M<:AbstractModel
    LongstaffSchwartz(model::M) where M<:AbstractModel = new{M}(m)
  end

  function Base.show(io::IO, ::MIME"text/plain", lssc::LongstaffSchwartz)
    print(io,"LongstaffSchwartz[",getfield(lssc,:m),"]")
  end
end