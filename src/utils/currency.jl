# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Currency <: Conventions
  
  A type for managing currencies.
  
  # Fields
  - `ticker::AbstractString`: currency code, typically ISO 4217.
  
  # Constructors
      Currency([ticker::AbstractString])
  
  ## Arguments
  - `ticker::AbstractString`: currency code symbol.
  
  # Notes
  Only ascii-only three-characters strings are allowed.
  The symbol is stored as uppercased 3-char codes except for the dummy ccy.
  
  # Examples
  ```julia
  julia> ccy = Currency("USD");
  
  julia> isa(ccy, Currency)
  true
  ```
  """
  struct Currency <: Conventions
    ticker::AbstractString
    
    function Currency(s::AbstractString)
      m = getfield(match(r"[A-Z]{3}", uppercase(s)), :match)
      if ~isequal(m, uppercase(s))
        throw(ArgumentError("3-char strings only allowed, got $(s)"))
      end
      new(string(m))
    end
    Currency() = new("dummy")
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ccy::Currency)
    print(io, ccy.ticker)
  end
  
  Base.:(==)(c1::A, c2::A) where A<:Currency = isequal(c1.ticker, c2.ticker)
  Base.:(<=)(c1::A, c2::A) where A<:Currency = <=(c1.ticker, c2.ticker)
  Base.:(>=)(c1::A, c2::A) where A<:Currency = >=(c1.ticker, c2.ticker)
  Base.:(<)(c1::A, c2::A) where A<:Currency  = isless(c1.ticker, c2.ticker)
  Base.:(>)(c1::A, c2::A) where A<:Currency  = >(c1.ticker, c2.ticker)
end