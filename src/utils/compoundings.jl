# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Annually <: CompoundFrequencies
  
  A singleton type representing *annual* compounding frequency.
  
  See also [`Quarterly`](@ref).
  
  # Examples
  ```julia
  julia> isa(Annually(), CompoundFrequencies)
  true
  ```
  """
  struct Annually <: CompoundFrequencies end
  
  const Yearly = Annually
  
  function Base.show(io::IO, ::MIME"text/plain", ::Annually)
    print(io, "AnnualCompounding")
  end
  
  """
      SemiAnnually <: CompoundFrequencies
  
  A singleton type representing *semi-annual* compounding frequency.
  
  See also [`Annually`](@ref).

  # Examples
  ```julia
  julia> isa(SemiAnnually(), CompoundFrequencies)
  true
  ```
  """
  struct SemiAnnually <: CompoundFrequencies end

  function Base.show(io::IO, ::MIME"text/plain", ::SemiAnnually)
    print(io, "SemiAnnualCompounding")
  end
  
  """
      Quarterly <: CompoundFrequencies
  
  A singleton type representing *quarterly* compounding frequency.
  
  See also [`SemiAnnually`](@ref).

  # Examples
  ```julia
  julia> isa(Quarterly(), CompoundFrequencies)
  true
  ```
  """
  struct Quarterly <: CompoundFrequencies end

  function Base.show(io::IO, ::MIME"text/plain", ::Quarterly)
    print(io, "QuarterlyCompounding")
  end
  
  """
      Monthly <: Compoundinds
  
  A singleton type representing *monthly* compounding frequency.
  
  See also [`Quarterly`](@ref).

  # Examples
  ```julia
  julia> isa(Monthly(), CompoundFrequencies)
  true
  ```
  """
  struct Monthly <: CompoundFrequencies end

  function Base.show(io::IO, ::MIME"text/plain", ::Monthly)
    print(io, "MonthlyCompounding")
  end
  
  """
      Weekly <: CompoundFrequencies
  
  A singleton type representing *weekly* compounding frequency.
  
  See also [`Monthly`](@ref).

  # Examples
  ```julia
  julia> isa(Weekly(), CompoundFrequencies)
  true
  ```
  """
  struct Weekly <: CompoundFrequencies end
  
  function Base.show(io::IO, ::MIME"text/plain", ::Weekly)
    print(io, "WeeklyCompounding")
  end

  """
     Daily <: CompoundFrequencies
  
  A singleton type representing *daily* compounding frequency.
  
  See also ['Weekly`](@ref).

  # Examples
  ```julia
  julia> isa(Daily(), CompoundFrequencies)
  true
  ```
  """
  struct Daily <: CompoundFrequencies end
  
  function Base.show(io::IO, ::MIME"text/plain", ::Daily)
    print(io, "DailyCompounding")
  end
    
  """
      Linearly <: CompoundFrequencies
  
  A singleton type representing the *linear* compounding style.
  """
  struct Linearly <: CompoundFrequencies end

  function Base.show(io::IO, ::MIME"text/plain", ::Linearly)
    print(io, "LinearCompounding")
  end
  
  """
      Continuous <: CompoundFrequencies
  
  A singleton type representing the *continous* compounding style.
  """
  struct Continuous <: CompoundFrequencies end
  
  function Base.show(io::IO, ::MIME"text/plain", ::Continuous)
    print(io, "ContinuousCompounding")
  end
  
  """
      CompoundFrequency <: CompoundFrequencies
  
  A type for representing custom compounding frequency.
  
  # Constructors
      CompoundFrequency(value::Real)
      CompoundFrequency(value::Tenor, [dcc::DayCountConventions])
  
  ## Arguments
  - `value::Real`: compounding frequency (`value` times per year).
  - `dcc::DayCountConventions=ActAct()`: day count conventions.
  
  # Notes
  This type generalizes `Monthly`, `Weekly`, `Quarterly`... indeed,
  `Monthly` would be constructed as `CompoundFrequency(12)`, `Quarterly` as
  `CompoundFrequency(4)` etc. However, there's a catch for `Daily` because
  some facilities (methods/functions) need another information such as the
  `DayCountConventions` in order to function properly. That said, constructing
  a daily compounding frequency using `CompoundFrequency(365)` may differ
  from using `Daily` is some applications; make sure of what best fits your
  needs.
  
  An important fact to remember is that the compounding frequency uses the
  common standard widely used in the finance industry of taking the year
  as the basis for time unit.
  
  An important fact to bear in mind is that the constructor takes as input
  the compounding frequency, when using `CompoundFrequency(value)`, the
  meaning is "...the underlying event compounds `value` times per year".
  """
  struct CompoundFrequency <: CompoundFrequencies
    value::R where R<:Real
    
    function CompoundFrequency(value::R) where R<:Real
      if isinf(value)
        Continuous()
      else
        new(value)
      end
    end
    
    CompoundFrequency(value::CompoundFrequencies) = value
  end

  function Base.show(io::IO, m::MIME"text/plain", cf::CompoundFrequency)
    if isequal(getfield(cf,:value), 1)
      show(io, m, Yearly())
    elseif isequal(getfield(cf,:value), 4)
      show(io, m, Quarterly())
    elseif isequal(getfield(cf,:value), 12)
      show(io, m, Monthly())
    elseif isequal(getfield(cf,:value), 52)
      show(io, m, Weekly())
    else
      print(io,"Compounding(",value,")")
    end
  end

  """
      _nwaar(::CompoundFrequencies)
      _nwaar(::Daily,dcc::DayCountConventions=ActAct(),isleap::Bool=false)
      _nwaar(x::Real)
  
  # Internal Facility
  Return the corresponding (numeric) compounding frequency of the
  `CompoundFrequencies` (sub)type.
  
  # Notes
  When `_nwaar` receives as argument the `Daily` compounding frequency,
  a `DayCountConventions` type can be provided for refinements, there's
  another argument `isleap::Bool` which defaults to `false` to adjust for
  leap years.
  
  # Examples
  ```julia
  julia> Zsofia._nwaar(Daily())
  365
  
  julia> Zsofia._nwaar(Daily(),Act360())
  360
  
  julia> Zsofia._nwaar(Daily(),Act360(),true)
  360
  
  julia> Zsofia._nwaar(Daily(),ActAct(),true)
  366
  ```
  """
  _nwaar(::CompoundFrequencies) = 365
  
  _nwaar(cmpf::CompoundFrequency) = getfield(cmpf, :value)
  
  _nwaar(::Daily,::OneOne,::Bool) = 1
  
  _nwaar(::Daily,::Thirty360,::Bool) = 360
  
  _nwaar(::Daily,::ThirtyE360,::Bool) = 360
  
  _nwaar(::Daily,::ActAct,isleap::Bool=false) = 365 + isleap
  
  _nwaar(::Daily,::Act360,::Bool) = 360
  
  _nwaar(::Daily,::Act365,::Bool) = 365
  
  function _nwaar(::Daily, dcc::DayCountConventions)
    _nwaar(Daily(), dcc, false)
  end
  
  _nwaar(::Daily) = _nwaar(Daily(),ActAct())
  
  _nwaar(::Annually) = 1
  
  _nwaar(::SemiAnnually) = 2
  
  _nwaar(::Quarterly) = 4
  
  _nwaar(::Monthly) = 12
  
  _nwaar(::Weekly) = 52
  
  _nwaar(::Linearly) = Linearly()
  
  _nwaar(::Continuous) = Continuous()
  
  _nwaar(x::R) where R<:Real = x

  toyearfraction(::OneOne,cmpf::B) where B<:CompoundFrequencies = 1
  
  function toyearfraction(dcc::DayCountConventions,cmpf::Daily,isleap::Bool)
    /(1, _nwaar(cmpf,dcc,isleap))
  end
  
  toyearfraction(dcc::DayCountConventions,cmpf::Daily) = /(1,_nwaar(cmpf,dcc))
  
  toyearfraction(cmpf::CompoundFrequencies) = /(1,_nwaar(cmpf))
  
  function toyearfraction(dcc::DayCountConventions,cmpf::CompoundFrequencies)
    toyearfraction(cmpf)
  end
  
  function toyearfraction(
                          dcc::DayCountConventions,
                          cmpf::CompoundFrequencies,
                          isleap::Bool
           )
    toyearfraction(cmpf)
  end
end