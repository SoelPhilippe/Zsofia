# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      CreditRating <: Ratings
  
  A credit rating type.
  
  # Fields
  - `rating::AbstractString`: rating notation.
  """
  struct CreditRating <: Ratings
    rating::AbstractString
    CreditRating(rating::AbstractString=DummyTicker()) = new(rating)
  end
  
  """
      CreditRatingAgency
  
  Data structure for credit rating agencies modeling.
  
  The credit rating agency data structure is intended to facilitate credit
  derivatives management, especially credit default swaps.
  
  See also [`CreditRating`](@ref).
  
  # Fields
  - `name::AbstractString`: name of the credit agency.
  
  # Constructors
      CreditRatingAgency(::String, ::Dict{CreditRating,Real})
      CreditRatingAgency(::String, ::Vector{String}, ::Vector{Real})
  
  ## Arguments
  - `name::AbstractString=DummyTicker()`: credit agency's name.
  """
  struct CreditRatingAgency
    name::AbstractString
    notations::Dict{CreditRating,Real}
    
    function CreditRatingAgency(
                                name::AbstractString,
                                notations::Dict{CreditRating,Real}
             )
      new(name, notations)
    end
    
    function CreditRatingAgency(notations::Dict{CreditRating,Real})
      CreditRatingAgency(DummyTicker(), notations)
    end
    
    function CreditRatingAgency(
                                name::AbstractString,
                                ratings::AbstractVector{AbstractString},
                                probs::AbstractVector{R}
             ) where R<:Real
      if ~isequal(length(ratings), length(probs))
        throw(DimensionMismatch("ratings and probs don't have the same size!"))
      elseif any(<(0), probs) && ~isone(sum(probs))
        throw(ArgumentError("probs are meant to be probability masses!"))
      end
      new(
          name,
          Dict{
               CreditRating,
               Real
          }(CreditRating(r) => p for (r,p) in zip(ratings,probs))
      )
    end
    
    function CreditRatingAgency(
                                ratings::AbstractVector{AbstractString},
                                probs::AbstractVector{R}
             ) where R<:Real
      CreditRatingAgency(DummyTicker(), ratings, probs)
    end
  end
end