# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Cashflow
  
  `Cashflow`, a data structure for cashflow occurrences modeling.
  
  # Fields
  - `amount::Real`: cashflow value, can be negative.
  - `t::TimePoint`: time of occurrence.
  - `currency::Currency`: cashflow currency.
  
  # Contructors
      Cashflow(amount::Real, t::TimePoint, [currency::Currency])
  
  ## Arguments
  - `amt::Real`: cashflow value.
  - `t::TimePoint`: time of occurrence.
  - `ccy::Currency=Currency()`: cashflow currency.
  
  # Examples
  ̀```julia
  julia> cflw = Cashflow(1_000_000, today(), Currency("USD"));
  
  julia> cflw isa Cashflow
  true
  ```
  """
  struct Cashflow <: Actions
    amount::R where R<:Real
    t::T where T<:TimePoint
    currency::Currency
    
    function Cashflow(amt::R,t::T,ccy::Currency) where {R<:Real,T<:TimePoint}
      new(amt,t,ccy)
    end
  end
  
  Cashflow(amt, t) = Cashflow(amt, t, Currency())
  
  """
      _are_comparable(c1::Cashflow, c2::Cashflow)
  
  # Internal Facility
  Return an error if `c1` and `c2` are not comparable: same `time` and
  same `currency`.
  
  # Notes
  `Cashflow`s are compared through their `amount` properties.
  """
  function _are_comparable(c1::Cashflow, c2::Cashflow)
    if ~isequal(c1.currency, c2.currency) || ~isequal(c1.t, c2.t)
      throw(AssertionError("Non comparable Cashflows\n $(c1) -|- $(c2)\n"))
    end
  end
  
  function Base.:(==)(c1::Cashflow, c2::Cashflow)
    isequal(c1.currency, c2.currency) &&
    isequal(c1.t, c2.t) &&
    isequal(c1.amount, c2.amount)
  end
  
  function Base.:(<=)(c1::Cashflow, c2::Cashflow)
    _are_comparable(c1, c2)
    <=(c1.amount, c2.amount)
  end
  
  function Base.:(<)(c1::Cashflow, c2::Cashflow)
    _are_comparable(c1, c2)
    isless(c1.amount, c2.amount)
  end
  
  function Base.:(>=)(c1::Cashflow, c2::Cashflow)
    _are_comparable(c1, c2)
    >=(c1.amount, c2.amount)
  end
  
  function Base.:(>)(c1::Cashflow, c2::Cashflow)
    _are_comparable(c1, c2)
    >(c1.amount, c2.amount)
  end
  
  function Base.sort(cflws::AbstractVector{Cashflow}, sym::Symbol)
    if ~in(sym, (:amount, :t))
      throw(ArgumentError("invalid sym, should be either :amount or :t"))
    end
    getindex(cflws, sortperm(getfield.(cflws,sym)))
  end
  
  function Base.sort!(
                      cflws::AbstractVector{Cashflow},
                      sym::Symbol;
                      rev::Bool=false
                )
    _ord = sortperm(getfield.(cflws,sym), rev=rev)
    cflws .= getindex(cflws, _ord)
  end
end