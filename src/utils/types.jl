# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Conventions
  
  An abstract supertype for all type of conventions.

  # Examples
  ```julia
  julia> DayCountConventions <: Conventions
  true
  """
  abstract type Conventions end
  
  """
      DayCountConventions <: Conventions
  
  An abstract type for day count conventions implementation.

  See also [`Thirty360`](@ref), [`ActAct`](@ref).
  """
  abstract type DayCountConventions <: Conventions end
  
  """
     Ratings <: Conventions
  
  An abstract type for rating systems.

  See also [`CreditRatingAgency`](@ref).
  """
  abstract type Ratings <: Conventions end
  
  """
      CompoundFrequency <: Conventions
  
  An abstract type for compounding frequency (sub)types.
  
  See [`Annually`](@ref), [`Monthly`](@ref), [`Daily`](@ref).
  """
  abstract type CompoundFrequencies <: Conventions end
  
  """
      TimePoint === Union{TimeType,Real}
  
  A constant intented to represent the time line.
  
  # Notes
  Sometimes we do need real dates for real life application,
  but for many simulations/analysis we represent the time line by real numbers,
  this type alias emcompasses both case.
  
  # Exampes
  ```julia-repl
  julia> Date <: TimePoint
  true
  
  julia> DateTime <: TimePoint
  true
  
  julia> Real <: TimePoint
  true
  
  julia> Rational <: TimePoint
  true
  ```
  """
  const TimePoint = Union{TimeType,Real}
  
  """
      Actions
  
  An abstract super type for actions.
  
  # Notes
  An example of an action is a `Cashflow`.
  """
  abstract type Actions end
end