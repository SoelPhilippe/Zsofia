# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Tenor{P} where P<:Union{Period,Real}
  
  Data structure for `Tenor` modeling.
  
  # Fields
  - `p::Union{Period,Real}`: underlying periodicity.
  
  # Constructors
      Tenor{Real}(r::Real)
      Tenor{Period}(s::Union{Real,AbstractString,Period})
      Tenor{Period}(s::AbstractString)
      Tenor(p::Period)
      Tenor(p::Real)
      Tenor(s::AbstractString)
      Tenor(cmpf::CompoundFrequency)
  
  ## Arguments
  - `p::Real`: tenor value, relevant for `Tenor{Real}`.
  - `p::AbstractString`: one of "[n]d","[n]w","[n]m","[n]y", `Tenor{Period}`
  - `p::Period`: an instance of any subtype of `Dates.Period`
  
  # Notes
  An algebra is defined for `Tenor`s: addition (`+`) with another `Tenor`,
  addition with a `Date`, addition with a `Number`.  
  The multiplication (`*`) operator, when relevant, is implemented. When the
  `Tenor{Period}(ten::AbstractString)` constructor is used, the
  `AbstractString` argument should have a pattern [number][periodcode] where
  "periodcode" is one of "d", "w", "m", and "y".  
  A Tenor object can directly be constructed using `Period` objects.
  
  # Examples
  ```julia-repl
  julia> ten = Tenor("3m");
  
  julia> isa(ten, Tenor)
  true
  ```
  """
  struct Tenor{P}
    p::P where P<:Union{<:Real,<:Period}
    
    Tenor{P}(p::P) where P<:Real = new{P}(p)

    Tenor{P}(p::P) where P<:Period = new{P}(p)
    
    Tenor(p::P) where P<:Period = new{P}(p)
    
    Tenor(p::P) where P<:Real = new{P}(p)
    
    Tenor{P}(::Annually) where P<:Real = new{P}(1)
    
    Tenor{P}(::SemiAnnually) where P<:Real = new{P}(1/2)
    
    Tenor{P}(::Quarterly) where P<:Real = new{P}(1/4)
    
    Tenor{P}(::Monthly) where P<:Real = new{P}(1/12)
    
    Tenor{P}(::Weekly) where P<:Real = new{P}(1/52)
    
    function Tenor{P}(
                      cmpf::Daily,
                      dcc::DayCountConventions,
                      isleap::Bool
             ) where P<:Real
      new{P}(inv(_nwaar(cmpf,dcc,isleap)))
    end
    
    function Tenor{P}(cmpf::Daily,dcc::DayCountConventions) where P<:Real
      Tenor{P}(cmpf,dcc,false)
    end
    
    function Tenor{P}(cmpf::Daily) where P<:Real
      Tenor{P}(cmpf,ActAct())
    end
    
    function Tenor{P}(cmpf::CompoundFrequency) where P<:Real
      new{P}(inv(getfield(cmpf,:value)))
    end
    
    function Tenor(p::AbstractString)
      m = match(r"(\d+)([dwmy])",lowercase(p))
      if isnothing(m) || ~isequal(m.match,lowercase(p))
        throw(ArgumentError("invalid Tenor format... try [numbder][tag]"))
      end
      if isequal(getfield(m,:captures)[2],"d")
        Tenor{Day}(Day(parse(Int,getfield(m,:captures)[1])))
      elseif isequal(getfield(m,:captures)[2],"w")
        Tenor{Week}(Week(parse(Int,getfield(m,:captures)[1])))
      elseif isequal(getfield(m,:captures)[2],"m")
        Tenor{Month}(Month(parse(Int,getfield(m,:captures)[1])))
      elseif isequal(getfield(m,:captures)[2],"y")
        Tenor{Year}(Year(parse(Int,getfield(m,:captures)[1])))
      else
        throw(ArgumentError("can't parse the input $(p)"))
      end
    end
    
    Tenor(::Annually) = Tenor("1y")
    
    Tenor(::SemiAnnually) = Tenor("6m")
    
    Tenor(::Quarterly) = Tenor("3m")
    
    Tenor(::Monthly) = Tenor("1m")
    
    Tenor(::Weekly) = Tenor("1w")
    
    Tenor(::Daily) = Tenor("1d")
  end
  
  function Base.show(io::IO, ::MIME"text/plain", tenor::Tenor)
    print(io, "Tenor[", tenor.p, "]")
  end
  
  # Period algebra
  Base.:+(a::Date, b::Tenor{P}) where P<:Period           = +(a, b.p)
  Base.:+(a::DateTime, b::Tenor{P}) where P<:Period       = +(a, b.p)
  Base.:+(b::Tenor{P}, a::DateTime) where P<:Period       = Base.:+(a, b)
  Base.:-(a::Date, b::Tenor{P}) where P<:Period           = -(a,b.p)
  Base.:-(a::DateTime, b::Tenor{P}) where P<:Period       = -(a,b.p)
  Base.:+(b::Tenor{P}, a::Date) where P<:Period           = Base.:+(a, b)
  Base.:+(a::Tenor{P}, b::Tenor{P}) where P<:Period       = Tenor(+(a.p,b.p))
  Base.:-(a::Tenor{P}, b::Tenor{P}) where P<:Period       = Tenor(-(a.p,b.p))
  Base.:*(a::N, b::Tenor{P}) where {P<:Period,N<:Int} = Tenor(*(a,b.p))
  Base.:*(a::Tenor{P}, b::N) where {P<:Period,N<:Int} = Tenor(*(a.p,b))
  Base.:/(a::Tenor{P}, b::Tenor{P}) where P<:Period       = /(a.p, b.p)
  Base.:/(a::Tenor{P}, b::N) where {P<:Period,N<:Int} = Tenor(/(a.p, b))
  function Base.:*(a::Tenor{P}, b::AbstractArray{<:Int}) where P<:Period
    Tenor.(.*(a.p,b))
  end
  function Base.:*(a::AbstractArray{<:Int},b::Tenor{P}) where P<:Period
    Tenor.(.*(a,b.p))
  end
  
  # Real algebra
  Base.:+(a::R, b::Tenor{P}) where {P<:Real,R<:Real}   = +(a, b.p)
  Base.:-(a::R, b::Tenor{P}) where {P<:Real,R<:Real}   = -(a, b.p)
  Base.:+(b::Tenor{P}, a::R) where {P<:Real,R<:Real}   = +(b.p, a)
  Base.:+(a::Tenor{P}, b::Tenor{P}) where P<:Real      = Tenor(+(a.p,b.p))
  Base.:-(a::Tenor{P}, b::Tenor{P}) where P<:Real      = Tenor(-(a.p,b.p))
  Base.:*(a::N, b::Tenor{P}) where {P<:Real,N<:Int}= Tenor(*(a,b.p))
  Base.:*(a::Tenor{P}, b::N) where {P<:Real,N<:Int}= Tenor(*(a.p,b))
  Base.:/(a::Tenor{P}, b::Tenor{P}) where P<:Real      = /(a.p, b.p)
  Base.:/(a::Tenor{P}, b::N) where {P<:Real,N<:Int}= Tenor(/(a.p, b))
  function Base.:*(a::Tenor{P}, b::AbstractArray{<:Int}) where P<:Real
    Tenor.(.*(a.p,b))
  end
  function Base.:*(a::AbstractArray{<:Int},b::Tenor{P}) where P<:Real
    Tenor.(.*(a,b.p))
  end
  
  # Boolean algebra
  Base.:(==)(a::Tenor, b::Tenor)   = isequal(a.p, b.p)
  Base.isequal(a::Tenor, b::Tenor) = isequal(a.p, b.p)
  Base.isless(a::Tenor, b::Tenor)  = Base.:(<)(a, b)
  Base.:(<)(a::Tenor, b::Tenor)    = isless(a.p, b.p)
  Base.:(>)(a::Tenor, b::Tenor)    = isless(b.p, a.p)
  Base.:(<=)(a::Tenor, b::Tenor)   = <=(a.p, b.p)
  Base.:(>=)(a::Tenor, b::Tenor)   = >=(a.p, b.p)
  
  function Base.one(tenor::Tenor{R}) where R<:Real
    Tenor(one(R))
  end
  
  function Base.one(tenor::Tenor{R}) where R<:Period
    Tenor(R(one(tenor.p.value)))
  end
  
  function Base.zero(tenor::Tenor{R}) where R<:Real
    Tenor(zero(tenor.p))
  end
  
  function Base.zero(tenor::Tenor{R}) where R<:Period
    Tenor(R(zero(tenor.p.value)))
  end

  Base.iszero(tenor::Tenor) = iszero(tenor.p)
  Base.isone(tenor::Tenor) = isone(tenor.p)

  """
      toyearfraction([dcc::DayCountConventions], tenor::Tenor)
      toyearfraction([dcc::DayCountConventions], cmpf::CompoundingFrequencies)
  
  Return the equivalent year fraction of `tenor`/`cmpf` according to the
  provided day count convention `dcc`.
  
  # Arguments
  - `dcc::DayCountConventions=ActAct()`: the day count convention.
  - `tenor::Tenor`: the tenor to convert into year fraction.
  - `cmpf::CompoundingFrequencies`: for the second method, the compounding frq.
  
  See [`CompoundingFrequencies`](@ref) too.
  
  # Notes
  If not specified, the day count convention `dcc` is taken to be `ActAct`.
  """
  toyearfraction(::OneOne, tenor::Tenor) = one(Real)
  
  function toyearfraction(::U,tenor::Tenor) where U<:Union{Thirty360,ThirtyE360}
    p = getfield(tenor, :p)
    if isa(p, Day)
      /(getfield(p, :value), 360)
    elseif isa(p, Week)
      /(getfield(p, :value), 52)
    elseif isa(p, Month)
      /(getfield(p, :value), 12)
    elseif isa(p, Quarter)
      /(getfield(p, :value), 4)
    elseif isa(p, Year)
      getfield(p, :value)
    elseif isa(p, Real)
      p
    else
      throw(ArgumentError("unparsable period -> $(p)"))
    end
  end

  function toyearfraction(::Act360, tenor::Tenor)
    p = getfield(tenor, :p)
    if isa(p, Day)
      /(getfield(p, :value), 360)
    elseif isa(p, Week)
      /(getfield(p, :value), 52)
    elseif isa(p, Month)
      /(getfield(p, :value), 12)
    elseif isa(p, Quarter)
      /(getfield(p, :value), 4)
    elseif isa(p, Year)
      getfield(p, :value)
    elseif isa(p, Real)
      p
    else
      throw(ArgumentError("unparsable period: $(p)"))
    end
  end

  function toyearfraction(::Act365, tenor::Tenor)
    p = getfield(tenor, :p)
    if isa(p, Day)
      /(getfield(p, :value), 365)
    elseif isa(p, Week)
      /(getfield(p, :value), 52)
    elseif isa(p, Month)
      /(getfield(p, :value), 12)
    elseif isa(p, Quarter)
      /(getfield(p, :value), 4)
    elseif isa(p, Year)
      getfield(p, :value)
    elseif isa(p, Real)
      p
    else
      throw(ArgumentError("unparsable period -> $(p)"))
    end
  end

  function toyearfraction(::ActAct, tenor::Tenor)
    p = getfield(tenor, :p)
    if isa(p, Day)
      /(getfield(p, :value), 365) #> ambiguous, think bout' it
    elseif isa(p, Week)
      /(getfield(p, :value), 52)
    elseif isa(p, Month)
      /(getfield(p, :value), 12)
    elseif isa(p, Quarter)
      /(getfield(p, :value), 4)
    elseif isa(p, Year)
      getfield(p, :value)
    elseif isa(p, Real)
      p
    else
      throw(ArgumentError("unparsable period -> $(p)"))
    end
  end
  
  toyearfraction(tenor::Tenor) = toyearfraction(ActAct(),tenor)
  
  function CompoundFrequency(
                             tenor::Tenor,
                             dcc::DayCountConventions=ActAct()
           )
    if iszero(tenor)
      Continuous()
    else
      CompoundFrequency(inv(toyearfraction(dcc,tenor)))
    end
  end
  
  toyearfraction(::DayCountConventions, a::A) where A <:Real = a
  
  toyearfraction(a::A) where A<:Real = toyearfraction(ActAct(),a)
  
  """
      _compound_freq(tenor::Tenor)
  
  # Internal Facility
  Return the associated compound frequency value of a given `tenor`.
  """
  function _compound_freq(tenor::Tenor,dcc::DayCountConventions=ActAct())
    iszero(tenor) ? Inf : inv(toyearfraction(dcc,tenor))
  end
end