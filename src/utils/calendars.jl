# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      AustraliaEx <: HolidayCalendar
      
  Public holidays for the Commonwealth of Australia.
  The focus is on holidays observed in the financial sector.
  """
  struct AustraliaEx <: HolidayCalendar end
  
  function BusinessDays.isholiday(::AustraliaEx, dt::Date)
    isholiday(AustraliaASX(), dt)
  end

  """
      AU
  
  Constant, short code for the `AustraliaEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const AU = AustraliaASX()
end

begin
  """
      BelgiumEx <: HolidayCalendar

  Public holidays for the Kingdom of Belgium.
  The focus is on holidays observed in the financial sector.

  # References
  https://en.wikipedia.org/wiki/Public_holidays_in_Belgium
  https://www.belgium.be/nl/over_belgie/land/belgie_in_een_notendop/feestdagen
  https://nl.wikipedia.org/wiki/Feestdagen_in_Belgi%C3%AB
  https://www.nbb.be/en/about-national-bank/national-bank-belgium/
  """
  struct BelgiumEx <: HolidayCalendar end

  """
      BE
  
  Constant, short code for the `BelgiumEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const BE = BelgiumEx()

  function BusinessDays.isholiday(::BelgiumEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Labor Day
    (isequal(m,5) && isone(d)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Whit Monday
    isequal(dt, es+Day(50)) ||
    # National Day
    (==(d, 21) && ==(m,7)) ||
    # Assumption Day
    (==(d, 15) && ==(m,8)) ||
    # Friday after Ascension Day
    isequal(dt, tonext(Date(y,8,15), Fri)) ||
    # All Saints' Day
    (isone(d) && ==(m, 11)) ||
    # Armistice Day
    (==(d, 11) && ==(m, 11)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      CanadaEx <: HolidayCalendar
  
  Public holidays for the Canada.
  The focus is on holidays observed in the financial sector.
  """
  const CanadaEx = CanadaTSX()

  """
      CA
  
  Constant, short code for the `CanadaEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const CA = CanadaEx
end


begin
  """
      ChinaEx <: HolidayCalendar
  
  Public holidays for the People's Republic of China.
  The focus is on holidays observed in the financial sector.
  """
  struct ChinaEx <: HolidayCalendar end

  """
      CN
  
  Constant, short code for the `ChinaEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const CN = ChinaEx()

  function BusinessDays.isholiday(::ChinaEx, dt::Date)
    y, m, d = year(dt), month(dt), day(dt)
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Labor Day
    (isequal(m,5) && isone(d)) ||
    # National Day
    (isone(d) && ==(10, m))
  end
end


begin
  """
      DenmarkEx <: HolidayCalendar
  
  Public holidays for the Kingdom of Denmark.
  The focus is on holidays observed in the financial sector.
  """
  struct DenmarkEx <: HolidayCalendar end

  """
      DK
  
  Constant, short code for the `DenmarkEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const DK = DenmarkEx()

  function BusinessDays.isholiday(::DenmarkEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Maundy Thursday
    isequal(dt, es-Day(3)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Whit Monday
    isequal(dt, es+Day(50)) ||
    # Constitution Day
    (==(d, 5) && ==(m, 6)) ||
    # Christmas Eve.
    (==(d, 24) && ==(m, 12)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      EstoniaEx <: HolidayCalendar
  
  Public holidays for the Republic of Estonia.
  The focus is on holidays observed in the financial sector.
  """
  struct EstoniaEx <: HolidayCalendar end

  """
      EE
  
  Constant, short code for the `EstoniaEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const EE = EstoniaEx()

  function BusinessDays.isholiday(::EstoniaEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Independence Day
    (==(d, 24) && ==(m, 2)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Spring Day
    (isone(d) && ==(m, 5)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Victory Day
    (==(d, 23) && ==(m, 6)) ||
    # Midsummer Day
    (==(d, 24) && ==(m, 6)) ||
    # Independence Restoration Day
    (==(d, 20) && ==(m, 8)) ||
    # Christmas Eve
    (==(d, 24) && ==(m, 12)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      FinlandEx <: HolidayCalendar
  
  Public holidays for the Republic of Finland.
  The focus is on holidays observed in the financial sector.
  """
  struct FinlandEx <: HolidayCalendar end

  """
      FI
  
  Constant, short code for the `FinlandEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const FI = FinlandEx()

  function BusinessDays.isholiday(::FinlandEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    if 1973 <= y <= 1990
      # Epiphany
      return isequal(dt, tonext(Date(y, 1, 5), Sat)) 
    end
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Epiphany
    (isequal(m, 1) && isequal(d, 6)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # May Day
    (isequal(m,5) && isone(d)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Midsummer Eve
    isequal(dt, tonext(Date(y, 6, 19), Fri)) ||
    # Midsummer Day
    isequal(dt, tonext(Date(y, 6, 20), Sat)) ||
    # All Saints' Day
    isequal(dt, tonext(Date(y, 10, 31), Sat)) ||
    # Independence Day
    isequal(dt, Date(y, 12, 6)) ||
    # Christmas Eve
    (==(d, 24) && ==(m, 12)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      FranceEx <: HolidayCalendar
  
  Public holidays for the French Republic.
  The focus is on holidays observed in the financial sector.
  """
  struct FranceEx <: HolidayCalendar end

  """
      FR
  
  Constant, short code for the `FranceEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const FR = FranceEx()

  function BusinessDays.isholiday(::FranceEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    ((isone(d) && isone(m)) && y >= 1811) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Labor Day
    ((isequal(m,5) && isone(d)) && (y >= 1919)) ||
    # Victory Day
    (==(m,5) && ==(d,8)) && (1953 <= y <= 1959) && (y >= 1982) ||
    # Ascension Day
    (isequal(dt, es+Day(40)) && (y >= 1802)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Whit Monday
    isequal(dt, es+Day(50)) ||
    # National Day
    ((==(d, 14) && ==(m, 7)) && (y >= 1880)) ||
    # Assumption Day
    (==(d, 15) && ==(m, 8)) ||
    # Friday after Ascension Day
    isequal(dt, tonext(Date(y,8,15), Fri)) ||
    # All Saints' Day
    (isone(d) && ==(m, 11)) ||
    # Armistice Day
    (==(d, 11) && ==(m, 11)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Saint Stephen's Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  BusinessDays.Germany() = BusinessDays.Germany(:HE)
  
  """
      GermanyEx <: HolidayCalendar
  
  Public holidays for the Federal Republic of Germany.
  The focus is on holidays observed in the financial sector.
  """
  const GermanyEx = Germany

  """
      DE
  
  Constant, short code for the `GermanyEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const DE = GermanyEx(:HE)
end


begin
  """
      HongKongEx <: HolidayCalendar
  
  Public holidays for the Hong Kong territory.
  The focus is on holidays observed in the financial sector.
  """
  struct HongKongEx <: HolidayCalendar end

  """
      HK
  
  Constant, short code for the `HongKongEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const HK = HongKongEx()

  function BusinessDays.isholiday(::HongKongEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Labour Day
    (isequal(m,5) && isone(d)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Whit Monday
    isequal(dt, es+Day(50)) ||
    # Hong Kong Special Administrative Region Establishment
    (==(d, 1) && ==(m, 7)) ||
    # Assumption Day
    (==(d, 15) && ==(m, 8)) ||
    # National Day
    (==(m, 10) && ==(d, 1)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      IcelandEx <: HolidayCalendar
  
  Public holidays for Iceland.
  The focus is on holidays observed in the financial sector.
  """
  struct IcelandEx <: HolidayCalendar end

  """
      IS
  
  Constant, short code for the `IcelandEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const IS = IcelandEx()

  function BusinessDays.isholiday(::IcelandEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Maundy Thursday
    isequal(dt, es-Day(3)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # First Day Of Summer
    ==(dt, tonext(Date(y, 4, 19), Thu)) ||
    # May Day
    (isequal(m,5) && isone(d)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Whit Monday
    isequal(dt, es+Day(50)) ||
    # National Day
    (==(d, 17) && ==(m, 6)) ||
    # Commerce Day
    ==(dt, tonext(Date(y, 9, 30), Mon)) ||
    # Christmas Eve.
    (==(d, 24) && ==(m, 12)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12)) ||
    # New Year's Eve
    (==(d,31) && ==(m,12))
  end
end


begin
  """
      IndiaEx <: HolidayCalendar
  
  Public holidays for the Republic of India.
  The focus is on holidays observed in the financial sector.
  """
  struct IndiaEx <: HolidayCalendar end

  """
      IN
  
  Constant, short code for the `IndiaEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const IN = IndiaEx()

  function BusinessDays.isholiday(::IndiaEx, dt::Date)
    y, m, d = year(dt), month(dt), day(dt)
    # Republic Day
    (isone(m) && ==(d,26)) ||
    # Independence Day
    (==(m, 8) && ==(d, 15)) ||
    # Easter Sunday
    (==(m, 10) && ==(d, 2))
  end
end


begin
  """
      IrelandEx <: HolidayCalendar
  
  Public holidays for the Republic of Ireland.
  The focus is on holidays observed in the financial sector.
  """
  struct IrelandEx <: HolidayCalendar end

  """
      IE
  
  Constant, short code for the `IrelandEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const IE = IrelandEx()

  function BusinessDays.isholiday(::IrelandEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Saint Patrick's Day
    (==(d, 17) && ==(m, 3)) ||
    # Saint Brigid's Day
    (xor(
        ==(d, 1) && ==(m, 2) && dow(dt,5),
        tonext(Date(y,1,31), Mon)
    ) && (y >= 2023)) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # May Day
    ==(dt, tonext(Date(y,4,30), Mon)) ||
    # June Holiday
    ==(dt, tonext(Date(y,5,31), Mon)) ||
    # August Holiday
    ==(dt, tonext(Date(y, 7, 31), Mon)) ||
    # October Holiday
    ==(dt, toprev(Date(y, 10, 31), Mon)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Saint Stephen's Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      IsraelEx <: HolidayCalendar
  
  Public holidays for the State of Israel.
  The focus is on holidays observed in the financial sector.
  """
  struct IsraelEx <: HolidayCalendar end
  
  """
      IL
  
  Constant, short code for the `IsraelEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const IL = IsraelEx()

  function BusinessDays.isholiday(::IsraelEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # Sabbath
    ==(dow(dt), 5) ||
    # Civi New Year's Day
    (isone(d) && isone(m)) ||
    # Christian New Year's Day
    (isone(m) && ==(d,14)) ||
    # Ziyarat al-Nabi Al-Khadir
    (isone(m) && ==(d,25)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Echad BeMay
    (==(m,5) && ==(d,1))
  end
end


begin
  """
      ItalyEx <: HolidayCalendar
  
  Public holidays for the Italian Republic.
  The focus is on holidays observed in the financial sector.
  """
  struct ItalyEx <: HolidayCalendar end

  """
      IT
  
  Constant, short code for the `ItalyEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const IT = ItalyEx()

  function BusinessDays.isholiday(::ItalyEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Epiphany
    (isone(m) && ==(d, 6)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Liberation Day
    (==(d,25) && ==(m,4)) ||
    # Labor Day
    (isequal(m,5) && isone(d)) ||
    # Republic Day
    (==(m,6) && ==(d,2)) ||
    # Assumption Day
    (==(d, 15) && ==(m, 8)) ||
    # All Saints' Day
    (isone(d) && ==(m, 11)) ||
    # Immaculate Conception
    (==(m,12) && ==(d,8)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Saint Stephen's Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      JapanEx <: HolidayCalendar
  
  Public holidays for Japan.
  The focus is on holidays observed in the financial sector.
  """
  struct JapanEx <: HolidayCalendar end

  """
      JP
  
  Constant, short code for the `JapanEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const JP = JapanEx()

  function BusinessDays.isholiday(::JapanEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day
    (isone(d) && isone(m)) ||
    # Second Monday of January
    ==(dt,tonext(Date(y-1,12,31),Mon)+Day(7)) ||
    # February 11
    (==(m,2) && ==(d,11)) ||
    # Emperor Birthday
    (==(m,2) && ==(d,23)) ||
    # Showa Day
    (==(m,4) && ==(d,29)) ||
    # Constitution Memorial Day, Greenery Day and Children's Day
    (==(m,5) && in(d,3:5)) ||
    # Marine Day (Third Monday Of July)
    ==(dt, tonext(Date(y,6,30),Mon)+Day(14)) ||
    # Mountain Day
    (==(m,8) && ==(d,11)) ||
    # Respect For the Aged Day
    ==(dt,tonext(Date(y,8,31),Mon)+Day(14)) ||
    # Sports Day
    ==(dt,tonext(Date(y,9,30),Mon)+Day(7)) ||
    # Culture Day
    (==(m,11) && ==(d,3)) ||
    # Labor Thanksgiving Day
    (==(m,11) && ==(d,23))
  end
end


begin
  """
     LatviaEx <: HolidayCalendar
  
  Public holidays for the Republic of Latvia.
  The focus is on holidays obvserved in the financial sector.
  """
  struct LatviaEx <: HolidayCalendar end

  """
      LV
  
  Constant, short code for the `LatviaEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const LV = LatviaEx()

  function BusinessDays.isholiday(::LatviaEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Labour Day
    (isequal(m,5) && isone(d)) ||
    # Restoration of Independence Day
    (==(m,5) && ==(d,4)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Whit Sunday
    isequal(dt, es+Day(49)) ||
    # Midsummer Eve
    (==(m, 6) && ==(d, 23)) ||
    # Midsummer Day
    (==(m, 6) && ==(d, 24)) ||
    # Proclamation Day of The Republic of Latvia
    (==(m, 11) && ==(d, 18)) ||
    # Christmas Eve
    (==(d, 24) && ==(m, 12)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Second Day of Christmas
    (==(d, 26) && ==(m, 12)) ||
    # New Year's Eve
    (==(m, 12) && ==(d, 31))
  end
end


begin
  """
      LithuaniaEx <: HolidayCalendar
  
  Public holidays for the Republic of Lithuania.
  The focus is on holidays observed in the financial sector.
  """
  struct LithuaniaEx <: HolidayCalendar end

  """
      LT
  
  Constant, short code for the `LithuaniaEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const LT = LithuaniaEx()

  function BusinessDays.isholiday(::LithuaniaEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Day of Restoration of State of Lithuania
    (==(m, 2) && ==(d, 16)) ||
    # Day of Restoration of Independence of Lithuania
    (==(m, 3) && ==(d, 11)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # Labor Day
    (isequal(m,5) && isone(d)) ||
    # Mother's Day
    isequal(dt, tonext(Day(y, 4, 30), Sun)) ||
    # Father's Day
    isequal(dt, tonext(Day(y, 5, 31), Sun)) ||
    # St John's Day
    (==(m,6) && ==(d,24)) ||
    # Statehood Day
    (==(m,7) && ==(d,6)) ||
    # Assumption Day
    (==(m,8) && ==(d,15)) ||
    # All Saints' Day
    (==(m,11) && ==(d,1)) ||
    # All Souls' Day
    (==(m,11) && ==(d,2)) ||
    # Christmas Eve
    (==(m, 12) && ==(d, 24)) ||
    # Christmas Day
    (==(m, 12) && ==(d, 25)) ||
    # Day following Christmas Day
    (==(m, 12) && ==(d, 26))
  end
end


begin
  """
      NetherlandsEx <: HolidayCalendar
  
  Public holidays for the Kingdom of the Netherlands.
  The focus is on holidays observed in the financial sector.
  """
  struct NetherlandsEx <: HolidayCalendar end

  """
      NL
  
  Constant, short code for the `NetherlandsEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const NL = NetherlandsEx()

  function BusinessDays.isholiday(::NetherlandsEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # King's Day
    (==(m,4) && ==(d,27)) ||
    # Liberation Day
    (==(m,5) && ==(d,5)) ||
    # Ascension Day
    isequal(dt, es+Day(40)) ||
    # Pentecost
    isequal(dt, es+Day(40)+Week(7)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Day following Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      NorwayEx <: HolidayCalendar
  
  Public holidays for the Kingdom of Norway.
  The focus is on holidays observed in the financial sector.
  """
  struct NorwayEx <: HolidayCalendar end

  """
      NO
  
  Constant, short code for the `NorwayEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const NO = NorwayEx()

  function BusinessDays.isholiday(::NorwayEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day.
    (isone(d) && isone(m)) ||
    # Maundy Thursday
    isequal(dt, es-Day(3)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Easter Monday
    ==(dt, es+Day(1)) ||
    # May Day
    (==(m,5) && ==(d,1)) ||
    # Ascension Day
    isequal(dt, es+Day(39)) ||
    # Pentecost
    isequal(dt, es+Day(49)) ||
    # Whit Monday
    isequal(dt, es+Day(50)) ||
    # Christmas Day
    (==(d, 25) && ==(m, 12)) ||
    # Second Day of Christmas Day
    (==(d, 26) && ==(m, 12))
  end
end


begin
  """
      PortugalEx <: HolidayCalendar
  
  Public holidays for the Portuguese Republic.
  The focus is on holidays observed in the financial sector.
  """
  struct PortugalEx <: HolidayCalendar end

  """
      PT
  
  Constant, short code for the `PortugalEx` calendar,
  based on the ISA 3166-1 alpha-2 standard.
  """
  const PT = PortugalEx()

  function BusinessDays.isholiday(::PortugalEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day
    (==(m, 1) && ==(d, 1)) ||
    # Carnival (47 days before easter sunday)
    isequal(dt, es - Day(74)) ||
    # Good Friday
    isequal(dt, es - Day(2)) ||
    # Easter Sunday
    isequal(dt, es) ||
    # Freedom Day
    (==(m, 4) && ==(d, 25)) ||
    # Labour Day
    (==(m,5) && ==(d,1)) ||
    # Corpus Christi (60 days after easter sunday)
    isequal(dt, es+Day(60)) ||
    # Portugal Day
    (==(m,6) && ==(d,10)) ||
    # Assumption Day
    (==(m,8) && ==(d,15)) ||
    # Republic Day
    (==(m,10) && ==(d,5)) ||
    # All Saints' Day
    (==(m,11) && ==(d,1)) ||
    # Restoration of Independence
    (==(m,12) && ==(d,1)) ||
    # Immaculate Conception
    (==(m,12) && ==(d,8)) ||
    # Christmas day
    (==(m,12) && ==(d,25)) ||
    # Boxing Day
    (==(m,12) && ==(d,26))
  end
end


begin
  """
      SaudiArabiaEx <: HolidayCalendar
      
  Public holidays for the Kingdom of Saudi Arabia.
  The focus is on holidays observed in the financial sector.
  """
  struct SaudiArabiaEx <: HolidayCalendar end
  
  """
      SA
  
  Constant, short code for the `SaudiArabiaEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const SA = SaudiArabiaEx()
  
  function BusinessDays.isholiday(::SaudiArabiaEx, dt::Date)
    y, m, d = year(dt), month(dt), day(dt)
    # Prayer Day
    ==(dow(dt), 5) ||
    # Founding Day
    (==(m,2) && ==(d,22)) ||
    # National Day
    (==(m,9) && ==(d,23))
  end
end


begin
  """
      SouthAfricaEx <: HolidayCalendar
      
  Public holidays for the Republic of South Africa.
  The focus is on holidays observed in the financial sector.
  """
  struct SouthAfricaEx <: HolidayCalendar end
  
  """
      ZA
  
  Constant, short code for the `SouthAfricaEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const ZA = SouthAfricaEx()
  
  function BusinessDays.isholiday(::SouthAfricaEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day
    (==(m,1) && ==(d,1)) ||
    # Human Rights Day
    (==(m,3) && ==(d,21)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter
    isequal(dt, es) ||
    # Family Day
    isequal(dt, es+Day(1)) ||
    # Freedom Day
    (==(m,4) && ==(d,27)) ||
    # Worker's Day
    (==(m,5) && ==(d,1)) ||
    # Youth Day
    (==(m,6) && ==(d,16)) ||
    # National Wormen's Day
    (==(m,8) && ==(d,9)) ||
    # Heritage Day
    (==(m,9) && ==(d,24)) ||
    # Day Of Reconciliation
    (==(m,12) && ==(d,16)) ||
    # Christmas Day
    (==(m,12) && ==(d,25)) ||
    # Day of Goodwill
    (==(m,12) && ==(d,26))
  end
end


begin
  """
      SouthKoreaEx <: HolidayCalendar
      
  Public holidays for the Republic of South Korea.
  The focus is on holidays observed in the financial sector.
  """
  struct SouthKoreaEx <: HolidayCalendar end
  
  """
      KR
  
  Constant, short code for the `SouthKoreaEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const KR = SouthKoreaEx()
  
  function BusinessDays.isholiday(::SouthKoreaEx, dt::Date)
    y, m, d = year(dt), month(dt), day(dt)
    # New Year's Day
    (==(m,1) && ==(d,1)) ||
    # Independence Movement Day
    (==(m,3) && ==(d,1)) ||
    # Childran's Day
    (==(m,5) && ==(d,5)) ||
    # Memorial Day
    (==(m,6) && ==(d,6)) ||
    # Labor Day
    (==(m,5) && ==(d,1)) ||
    # Memorial Day
    (==(m,6) && ==(d,6)) ||
    # Liberation Day
    (==(m,8) && ==(d,15)) ||
    # National Foundation Day
    (==(m,10) && ==(d,3)) ||
    # Hangul Day
    (==(m,10) && ==(d,9)) ||
    # Christmas
    (==(m,12) && ==(d,25))
  end
end


begin
  """
      SpainEx <: HolidayCalendar
      
  Public holidays for the Kingdom of Spain.
  The focus is on holidays observed in the financial sector.
  """
  struct SpainEx <: HolidayCalendar end
  
  """
      ES
  
  Constant, short code for the `SpainEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const ES = SpainEx()
  
  function BusinessDays.isholiday(::SpainEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day
    (==(m,1) && ==(d,1)) ||
    # Epiphany Three King's Day
    (==(m,1) && ==(d,6)) ||
    # Good Friday Eve
    isequal(dt, es-Day(3)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Day
    isequal(dt, es) ||
    # Labor Day
    (==(m,5) && ==(d,5)) ||
    # Assumption
    (==(m,8) && ==(d,15)) ||
    # National Day
    (==(m,10) && ==(d,12)) ||
    # All Saints' Day
    (==(m,11) && ==(d,1)) ||
    # Constitution Day
    (==(m,12) && ==(d,6)) ||
    # Immaculate Day
    (==(m,12) && ==(d, 8)) ||
    # Christmas Day
    (==(m,12) && ==(d,25))
  end
end


begin
  """
      SwedenEx <: HolidayCalendar
      
  Public holidays for the Kingdom of Sweden.
  The focus is on holidays observed in the financial sector.
  """
  struct SwedenEx <: HolidayCalendar end
  
  """
      SE
  
  Constant, short code for the `SwedenEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const SE = SwedenEx()
  
  function BusinessDays.isholiday(::SwedenEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day
    (==(m,1) && ==(d,1)) ||
    # Epiphany
    (==(m,1) && ==(d,6)) ||
    # Good Friday
    isequal(dt, es-Day(2)) ||
    # Easter Day
    isequal(dt, es) ||
    # Easter Monday
    isequal(dt, es+Day(1)) ||
    # International Workers' Day
    (==(m,5) && ==(d,1)) ||
    # Ascension Day
    isequal(dt, es+Day(39)) ||
    # Pentecost
    isequal(dt, es+Day(49)) ||
    # National Day of Sweden
    (==(m,6) && ==(d,6)) ||
    # Midsummer's Eve
    isequal(dt,tonext(Date(y,6,18), Fri)) ||
    # Midsummer's Day
    isequal(dt, tonext(Date(y,6,19), Sat)) ||
    # All Saints' Day
    isequal(dt, tonext(Date(y,10,30), Sat)) ||
    # Christmas Day
    (==(m,12) && ==(d,25)) ||
    # Second Day of Christmas
    (==(m,12) && ==(d,25)) ||
    # New Year's Eve
    (==(m,12) && ==(d,31))
  end
end


begin
  """
      SwitzerlandEx <: HolidayCalendar
      
  Public holidays for the Swiss Confederation.
  The focus is on holidays observed in the financial sector.
  """
  struct SwitzerlandEx <: HolidayCalendar end
  
  """
      CH
  
  Constant, short code for the `SwitzerlandEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const CH = SwitzerlandEx()
  
  function BusinessDays.isholiday(::SwitzerlandEx, dt::Date)
    y, m, d, es = year(dt), month(dt), day(dt), easter_date(Year(dt))
    # New Year's Day
    (==(m,1) && ==(d,1)) ||
    # Berchtholdstag
    (==(m,1) && ==(d,2)) ||
    # Good Friday
    ==(dt, es - Day(2)) ||
    # Easter Sunday
    ==(dt, es) ||
    # Eastar Monday
    ==(dt, es + Day(1)) ||
    # Labour Day
    (==(m,5) && ==(d,1)) ||
    # Ascension Day
    isequal(dt, es + Day(39)) ||
    # Whit Monday
    isequal(dt, es + Day(50)) ||
    # National Day
    (==(m,8) && ==(d,1)) ||
    # Christmas Eve
    (==(m,12) && ==(d,24)) ||
    # Christmas Day
    (==(m,12) && ==(d,25)) ||
    # St. Stephen's Day
    (==(m,12) && ==(d,26)) ||
    # New Year's Eve
    (==(m,12) && ==(d,31))
  end
end


begin
  """
      UnitedKingdomEx <: HolidayCalendar
      
  Public holidays for the United Kingdom.
  The focus is on holidays observed in the financial sector.
  """
  struct UnitedKingdomEx <: HolidayCalendar end
  
  function BusinessDays.isholiday(::UnitedKingdomEx, dt::Date)
    isholiday(UnitedKingdom(),dt)
  end

  """
      UK
  
  Constant, short code for the `UnitedKingdomEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const GB = UnitedKingdomEx()
end


begin
  """
      UnitedStatedEx <: HolidayCalendar
      
  Public holidays for the United States of America.
  The focus is on holidays observed in the financial sector.
  """
  struct UnitedStatesEx <: HolidayCalendar end
  
  BusinessDays.isholiday(::UnitedStatesEx, dt::Date) = isholiday(USNYSE(),dt)
  
  """
      US
  
  Constant, short code for the `UnitedStatesEx` calendar,
  based on the ISO 3166-1 alpha-2 standard.
  """
  const US = UnitedStatesEx()
end