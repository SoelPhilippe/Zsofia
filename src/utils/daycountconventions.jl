# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      OneOne
  
  `1/1` day count convention, a singleton type.
  
  Definition 4.16(a) in 2006 ISDA Definitions.
  
  # Constructors
      OneOne()
  """
  struct OneOne <: DayCountConventions end
  
  Base.show(io::IO, ::MIME"text/plain", ::OneOne) = print(io, "1/1")
  
  """
      Thirty360
  
  `30/360` day count convention, a singleton type.
  
  Definition 4.16(f) in 2006 ISDA Definitions.
  
  # Constructors
      Thirty360()
  """
  struct Thirty360 <: DayCountConventions end
  
  Base.show(io::IO, ::MIME"text/plain", ::Thirty360) = print(io, "30/360")
  
  
  """
      ThirtyE360

  `30E/360` day count convention, a singleton type.
  
  Definition 4.16(g) in 2006 ISDA Definitions.
  
  # Constructors
      ThirtyE360()
  """
  struct ThirtyE360 <: DayCountConventions end

  Base.show(io::IO, ::MIME"text/plain", ::ThirtyE360) = print(io, "30E/360")
  
  """
      ActAct
  
  `ACT/ACT` day count convention, a singleton type.
  
  Definition 4.16(b) in 2006 ISDA Definitions.
  
  # Constructors
      ActAct()
  """
  struct ActAct <: DayCountConventions end

  Base.show(io::IO, ::MIME"text/plain", ::ActAct) = print(io, "ACT/ACT")
  
  """
      Act360
  
  `ACT/360` day count convention, singleton type.
  
  Definition 4.16(e) in 2006 ISA Definitions.
  
  # Constructors
      Act360()
  """
  struct Act360 <: DayCountConventions end

  Base.show(io::IO, ::MIME"text/plain", ::Act360) = print(io, "ACT/360")
  
  """
      Act365
  
  `ACT/365` day count convention, a singleton type.
  
  Definition 4.16(d) in 2006 ISDA Definitions.
  
  # Constructor
      Act365()
  """
  struct Act365 <: DayCountConventions end

  Base.show(io::IO, ::MIME"text/plain", ::Act365) = print(io, "ACT/365")
  
  """
      yearfraction(::OneOne,    d1::Date, d2::Date)
      yearfraction(::Thirty360, d1::Date, d2::Date)
      yearfraction(::ThirtyE360,d1::Date, d2::Date)
      yearfraction(::ActAct,    d1::Date, d2::Date)
      yearfraction(::Act360,    d1::Date, d2::Date)
      yearfraction(::Act365,    d1::Date, d2::Date)
  
  Return the difference between `d1` and `d2` in years using the
  provided day count convention.
  
  # Notes
  The computation is not commutative `d2-d1` is computed, therefore the
  result can be negative.
  """
  function yearfraction(::OneOne, d1::A, d2::B) where {A<:TimeType,B<:TimeType}
    return one(Float64)
  end
  
  function yearfraction(
                        ::Thirty360,
                        d1::A,
                        d2::B
           )::Real where {A<:TimeType,B<:TimeType}
    u = 360 * (year(d2) - year(d1)) + 30 * (month(d2) - month(d1))
    v, w = min(day(d1),30), >(29)(day(d1)) ? min(day(d2),30) : day(d2)
    (u + w - v) / 360
  end
  
  function yearfraction(
                        ::ThirtyE360,
                        d1::A,
                        d2::B
           ) where {A<:TimeType,B<:TimeType}
    u = 360 * (year(d2) - year(d1)) + 30 * (month(d2) - month(d1))
    v, w = min(day(d1), 30), min(day(d2), 30)
    (u + w - v) / 360
  end
  
  function yearfraction(::ActAct, d1::A, d2::B) where {A<:TimeType,B<:TimeType}
    a = dayofyear(d2)/daysinyear(d2) - dayofyear(d1)/daysinyear(d1)
    a + year(d2) - year(d1)
  end
  
  function yearfraction(::Act360, d1::A, d2::B) where {A<:TimeType,B<:TimeType}
    (d2 - d1).value / 360
  end
  
  function yearfraction(::Act365, d1::A, d2::B) where {A<:TimeType,B<:TimeType}
    getfield(d2 - d1, :value) / 365
  end
  
  function yearfraction(
                        ::DayCountConventions,
                        d1::Q,
                        d2::R
           ) where {Q<:Real,R<:Real}
    -(d2,d1)
  end
  
  """
      diff(::DayCountConventions, ::AbstractVector{TimePoint})
      diff(::DayCountConventions, ::Matrix{TimePoint}; dims)
  """
  function Base.diff(
                     dcc::DayCountConventions,
                     v::AbstractVector{T}
                ) where T<:TimePoint
    n = length(v)
    @assert isless(1,n) return T[]
    map(
        (x,y) -> yearfraction(dcc,x,y),
        view(v, Colon()(1,n-1)),
        view(v, Colon()(2,n))
    )
  end
  
  function Base.diff(
                     dcc::DayCountConventions,
                     v::Matrix{T};
                     dims::Int=1
                ) where T<:TimePoint
    mapslices(x -> yearfraction(dcc, x), v, dims=dims)
  end
end