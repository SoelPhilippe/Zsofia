# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      dirac(x::Any, s::Set)
      dirac(x::Any, s::Vector)
      dirac(x::T, lo::T, hi::T)
  
  Return `1` (`true`) if `x` is inside the
  set `s` and `0` (`false`) otherwise.
  
  # Arguments
  - `x::Any`: the element we intend to test if they belong to `s`.
  - `s::Set`: the set we want to test belongness to.
  
  # Notes
  For the third method, `dirac(x::T, lo::T, hi::T)`, return `1` (`true`)
  whenever `lo <= x < hi` evaluate to `true`, or `0` (`false`) otherwise.
  
  # Examples
  ```julia
  julia> x, s = 7, Set(["A",7,Int64[],'B']);
  
  julia> dirac(x, s)
  true
  
  julia> dirac("B", s)
  false
  ```
  """
  dirac(x, s::Set) = in(x,s)
  
  dirac(x, s::Vector) = in(x,s)
  
  function dirac(x, lo::A, hi::B) where {A<:U,B<:U} where U
    lo <= x < hi
  end
end