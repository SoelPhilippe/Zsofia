# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      pnl(x::Real, y::Real)
      pnl(x::Real, y::AbstractVector{Real})
      pnl(x::Real, y::AbstractVector{Real})
      pnl(x::AbstractVector{Real})
  
  # Methods
  
  ## First Method
  Return the relative increment from `x` to `y`, in percentage (the result
  is multiplied by 100).
  
  ### Examples
  `̀ `julia-repl
  julia> x, y = 30.0, 45.0;
  
  julia> pnl(x,y)
  50.0
  ```
  
  ## Second Method
  
  Return `pnl`s of tuples in `zip(x,y)`.
  
  ### Examples
  ```julia-repl
  julia> x, y = [30, 40], [45, 60];
  
  julia> pnl(x,y)
  2-element Vector{Float64}:
   50.0
   50.0
  ```
  
  ## Third Method
  
  Return an `AbstractVector{Real}` of `pnl(x,v)` for `v` in `y`.
  
  ### Examples
  ```julia-repl
  julia> x, y = 30, [45,60];
  
  julia> pnl(x,y)
  2-element Vector{Float64}:
    50.0
   100.0
  ```
  
  ## Fourth Method
  
  Return the `pnl`s pnl(x[i-1], x[i]) ... for i running through x's indices.
  
  ### Examples
  ```julia-repl
  julia> x = [30,45,90];
  
  julia> pnl(x)
  3-element Vector{Float64}:
     0.0
    50.0
   100.0
  ```
  """
  function pnl(x::P, y::Q) where {P<:Real, Q<:Real}
    *(100, /(y,x) - 1)
  end
  
  function pnl(
               x::AbstractVector{P},
               y::AbstractVector{Q}
           ) where {P<:Real,Q<:Real}
    map(pnl, x, y)
  end
  
  function pnl(x::Q, y::AbstractVector{R}) where {Q<:Real, R<:Real}
    map(v -> pnl(x,v), y)
  end
  
  function pnl(x::AbstractVector{P}) where P<:Real
    n = length(x)
    if iszero(n)
      P[]
    elseif isone(n)
      zeros(1)
    else
      vcat(0, map(pnl,view(x,1:(n-1)), view(x,2:n)))
    end
  end
  
  """
      indcz(x::Real, y::Real)
      indcz(x::AbstractVector{Real}, y::AbstractVector{Real})
  
  Return the so-called *indecision* between `x` and `y`.
  
  # Notes
  Given a time frame, the corresponding closing price `c`,
  opening price `o`, the highest price `h` and the lowest price `l`,
  we define the indecision `ind` as following:
  
  ``ind=100\\cdot (1 - \\frac{|\\text{pnl}(o,c)|}{\\text{pnl}(l,h)}.
  """
  function indcz(x::P, y::Q) where {P<:Real,Q<:Real}
    ifelse(iszero(y), 0, *(100, 1 - /(abs(x), y)))
  end
  
  function indcz(
                 x::AbstractVector{P},
                 y::AbstractVector{Q}
           ) where {P<:Real,Q<:Real}
    map(indcz, x, y)
  end
  
  """
      gap(x::AbstractVector{Real}, y::AbstractVector{Real})
  
  Return the `pnl` between nth slot of x and (n+1)th slot of y.
  """
  function gap(
               x::AbstractVector{R},
               y::AbstractVector{S}
           ) where {R<:Real,S<:Real}
    n = length(x)
    if ~isequal(n,length(y))
      throw(DimensionMismatch("$(n) = dim(x) ≠ = dim(y) = $(length(y))"))
    elseif isone(n)
      zeros(1)
    else
      vcat(0, pnl(view(x,1:(n-1)),view(y,2:n)))
    end
  end
  
  """
      gauge(actual::Real, high::Real, low:::Real)
      gauge(actual::AbstractVector, high::AbstractVector, low::AbstractVector)
  
  Return the (relative, in%) level of `actual` with the `low-high` range. 
  """
  function gauge(actual::P, high::Q, low::R) where {P<:Real,Q<:Real,R<:Real}
    ifelse(isequal(low,high), 100, *(100, /(-(actual,low), -(high,low))))
  end
  
  function gauge(
                 actual::AbstractVector{P},
                 high::AbstractVector{Q},
                 low::AbstractVector{R}
           ) where {P<:Real,Q<:Real,R<:Real}
    map(gauge, actual, high, low)
  end
  
  """
      ticksize(x::AbstractVector{Real})
  """
  function ticksize(x::AbstractVector{R}) where R<:Real
    a = gcd(rationalize.(10000 .* x))
    ifelse(iszero(a), 1, /(a, 10_000//1))
  end

  cummax(x::R) where R<:Real = x
  
  # weakness in type stability when inline, wonder why. Look at this asap.
  function cummax(x::AbstractVector{R})::Vector{R} where R<:Real
    accumulate(max, x)
  end
  
  cummin(x::R) where R<:Real = x
  
  # weakness in type stability when inline, wonder why. look at this asap.
  function cummin(x::AbstractVector{R})::Vector{R} where R<:Real
    accumulate(min, x)
  end
  
  function Base.muladd(ζ::R) where R<:Real
    _f(x::A,y::B) where {A<:Real,B<:Real} = muladd(x,ζ,y)
    function _f(
                x::AbstractVector{A},
                y::AbstractVector{B}
             ) where {A<:Real,B<:Real}
      map(_f, x, y)
    end
  end
  
  """
      _r∞(r::Real, cmpf::Real)
  
  # Internal Facility
  Return the equivalent continous rate associated to the `cmpf`-times per year
  compounding rate `r`.
  
  ## Arguments
  - `r::Real`: rate level.
  - `cmpff::Real`: compounding frequency (per year)
  """
  _r∞(r::A,cmpf::B) where {A<:Real,B<:Real} = *(cmpf,log(+(1,/(r,cmpf))))
end

begin
  """
      luvut(x::Real)
  
  Return a `String` representing `x` in a human-friendly manner in a style
  common in the finance industry; i.e. bringing the number to the closer
  thousands units:
  
  # Notes
  |Scale|Converts to|
  |---:|:---|
  |'000s|K|
  |'000'000s|M|
  |'000'000'000s|B|
  |'000'000'000'000s|T|
  
  # Examples
  ```julia
  julia> luvut(125_786)
  "125.79K"
  ```
  """
  function luvut(x::R) where {R<:Real}
    avx = abs(x)
    szx = 3div(floor(Integer, log10(avx)), 3)
    rsx = *(sign(x), round(/(avx, exp10(szx)), digits=2))
    utx = begin
      if iszero(szx)
        ""
      elseif ==(szx,03)
        "K"
      elseif ==(szx,06)
        "M"
      elseif ==(szx,09)
        "B"
      elseif ==(szx,12)
        "T"
      elseif ==(szx,15)
        "Q"
      elseif ==(szx,18)
        "E"
      else
        "Z"
      end
    end
    string(rsx) * utx
  end
  
  """
      typpyst(msg; t::Real=1/10, bud::Bool=false, kol::Int=102)
  
  # Internal Facility
  Write msg on `stdout` in a typist-like manner.
  
  # Arguments
  - `msg::AbstractString`: the message to be printed.
  - `t::Real=1/10`: kwarg, a parameter regulating the average speed.
  - `bud::Bool=false`: kwarg, if `true`, bold font.
  - `kol::Int=102`: kwarg, text color.
  
  # Notes
  The parameter `t` fixes the average speed. The speed for writing
  each character is `u` milliseconds, where `u∈[0,1000t)` ranging
  uniformly into the range.
  """
  function typpyst(
                   msg::AbstractString;
                   t::R=1/10,
                   bud::Bool=false,
                   kol::Int=102
           ) where R<:Real
    z = zip(msg, rand(length(msg)))
    for (s,q) in z
      printstyled(s, color=kol, bold=bud)
      sleep(*(q,t))
    end
    println()
  end
end