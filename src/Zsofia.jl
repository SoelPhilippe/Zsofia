# This script is part of Zsofia. Soel Philippe © 2025

"""
# Zsofia.jl
Welcome to Zsofia.jl!

Zsofia is a package accompanying quants in their daily tasks. It has
been made by a quant for quants, taking advantage of Julia's fast computation,
multiple dispatch and excellent automatic differentiation tools highly relevant
in quantitative fields for computing sensivities of any kind.

## REPL help
`?` followed by a keyword name (`?Swap`), prints help to the terminal.
"""
module Zsofia

using Reexport

# dependencies
@reexport import Base.Threads: @spawn, @threads

@reexport import BusinessDays: BusinessDays,
                               advancebdays, bdayscount, firstbdayofmonth,
                               easter_date, isbday, isholiday, isweekday,
                               lastbdayofmonth, listbdays, listholidays,
                               tobday
import BusinessDays: AustraliaASX, BrazilBMF, CanadaTSX, Germany,
                     UnitedKingdom, UnitedStates, USNYSE

@reexport import BusinessDays: HolidayCalendar, NullHolidayCalendar,
                               WeekendsOnly

@reexport import CSV: File as CSVFile
@reexport import CSV: read as read_csv, write as write_csv

@reexport using Dates
@reexport import Dates: datetime2unix, unix2datetime

@reexport import DataFrames: rename as renamedf, rename! as renamedf!
@reexport using DataFrames

import Distributions: Normal, Poisson,
                      cdf, mean, pdf, quantile

import FredData: Fred, get_data

using HTTP

using LinearAlgebra

import Logging: @debug, @error, @info, @logmsg, @warn,
                AbstractLogger, ConsoleLogger, Debug, Error,
                Info, LogLevel, SimpleLogger, Warn,
                global_logger, with_logger

@reexport import Optim: Brent, optimize

@reexport using Polynomials

using Serde

@reexport using StatsBase

@reexport import QuadGK: quadgk as integrate

import TOML: parsefile, print as printtoml

## ----------------------EXPORTS------------------------------
#> abstract types
export AbstractModel, AbstractCurves, Actions, Commodities, Contracts,
       CompoundFrequencies, Conventions, CreditEvents, Curves,
       DayCountConventions, Debt, Derivatives, DummyModel, Equity,
       EquityModels, Events, GeneralModels, Indices, Marketables,
       MarketIndicators, Observables, NonMarketables, Options,
       RatesModels, Rates, VolatilityModels, Securities,
       StochasticProcesses, Swaps, TimePoint, ZsofiaDB, ZsofiaDBContext

#> calendars
export AustraliaEx, BelgiumEx, CanadaEx, ChinaEx, DenmarkEx,
       EstoniaEx, FinlandEx, FranceEx, GermanyEx, HongKongEx,
       IcelandEx, IndiaEx, IrelandEx, IsraelEx, ItalyEx, JapanEx,
       LatviaEx, LithuaniaEx, NetherlandsEx, NorwayEx, PortugalEx,
       SaudiArabiaEx, SouthAfricaEx, SouthKoreaEx, SwedenEx,
       SwitzerlandEx, UnitedKingdomEx, UnitedStatesEx
       
#> concrete types
export ActAct, Act360, Act365, AmericanStyle, AmericanOption,
       Annually, ArithmeticAverage, AsianOption, AsianOptionType, AverageType,
       BarrierOption, BarrierType, Basket, BernoulliProcess,BermudanOption,
       BermudanStyle, BesselProcess, BGM, BJORN, Bjorn, BinaryOption, Black76,
       BlackKarasinski, BlackScholes, Bond, BrownianMotion, BRYNHILD, Brynhild,
       Call, Cap, Cashflow, CauchyProcess, CIR, CIRpp, Commodity, CommodityType,
       CompoundFrequency, Continuous, CryptoCurrency, Currency, Daily, Deposit,
       DiscountCurve, DownIn, DownOut, EconoIndicator, ETF, EuropeanOption,
       EuropeanStyle, ExerciseStyle, ExponentialVasicek, FixedLeg, FixedStrike,
       FloatingLeg, FloatingStrike, Floor, Forward, FRA, FXRate, Futures,
       GeometricAverage, HardCommodity, HarmonicAverage, HJM, HJORDIS,
       Hjordis, HoLee, HRAFNHILDUR, Hrafnhildur, HullWhite1F, HullWhite2F,
       InterestRate, Linearly, LongstaffSchwartz, Margrabe, MonteCarlo, Monthly,
       NoArbitrage, OneOne, OrnsteinUhlenbeckProcess, OptionColor,
       ParisianOption, PoissonProcess, Put, Quarterly, Quote, SemiAnnually,
       SoftCommodity, SpreadOption, Stock, Swap, Swaption, Tenor, TermStructure,
       Thirty360, ThirtyE360, UpIn, UpOut, VALTYR, Valtyr, Vasicek, Weekly,
       WienerProcess, Yearly, YieldCurve, ZeroBond

#> functions
export Ξ, ξ,
       annuity, buildfeatures!, cashflows, collect, convertrate, cummax, cummin,
       commongreeks, delta, dirac, discount, evaluate, evaluate!, forwardrate,
       fotografi, fotografi!, gamma, getbunch, getcurve, getoutdated, getparams,
       getparams!, getfotografi, gettenors, hasparams, insertprices!, ivol,
       luvut, omega, payoff, rebuildfeatures!, resetprices!, resettimes!,
       resimulate, resimulate!, S, setparams!, setprices!, settimes!, rho,
       simulate, simulate!, theta, toyearfraction, update, vanna, vega,
       yearfraction

#> public names (types, functions etc.)
public DummyObs,
       dummyinception, dummyticker, averagelevel, gap, gauge, indcz,
       knockedin, ispayoffable, luvut, pnl, ticksize, typpyst
### ------------------------------------------------------------

"""
    _Zsofyles::Dict{AbstractString,Tuple{Vararg{AbstractString}}}

# Internal Facility
Required file paths holder.
"""
const _Zsofyles::Dict{AbstractString,Tuple{Vararg{AbstractString}}} = begin
  Dict{AbstractString,Tuple{Vararg{AbstractString}}}(
    joinpath(@__DIR__, "utils") => (
                                    "types.jl",
                                    "calendars.jl",
                                    "currency.jl",
                                    "daycountconventions.jl",
                                    "dirac.jl",
                                    "olennainen.jl",
                                    "compoundings.jl",
                                    "tenors.jl",
                                    "creditrating.jl",
                                    "cashflows.jl"
                                   ),
    joinpath(@__DIR__, "instruments") => (
                                          "types.jl",
                                          "observables.jl",
                                          "dummies.jl",
                                          "interestrates.jl",
                                          "tools_options.jl",
                                          "americans.jl",
                                          "asians.jl",
                                          "barriers.jl",
                                          "baskets.jl",
                                          "bermudans.jl",
                                          "binaries.jl",
                                          "bonds.jl",
                                          "caps.jl",
                                          "choosers.jl",
                                          "commodities.jl",
                                          "cryptocurrencies.jl",
                                          "econoindicators.jl",
                                          "etfs.jl",
                                          "europeans.jl",
                                          "floors.jl",
                                          "forwards.jl",
                                          "fras.jl",
                                          "futures.jl",
                                          "fxrates.jl", 
                                          "parisians.jl",
                                          "spreads.jl",
                                          "stocks.jl",
                                          "swaps.jl",
                                          "swaptions.jl",
                                          "bankruptcy.jl", #<-- breaks order
                                          "failuretopay.jl",
                                          "moratorium.jl",
                                          "obligationacceleration.jl",
                                          "obligationdefault.jl",
                                          "restructuring.jl",
                                          "deposits.jl",
                                          "zerobonds.jl",
                                          "cashflows.jl", #<--- to pre-tail
                                          "quotes.jl" #<-- to tail the list.
                                         ),
    joinpath(@__DIR__, "measures", "processes") => (
                                                    "types.jl",
                                                    "generics.jl",
                                                    "bernoulli.jl",
                                                    "poisson.jl",
                                                    "wiener.jl",
                                                    "bessel.jl",
                                                    "cauchy.jl",
                                                    "galtonwatson.jl",
                                                    "hawkes.jl",
                                                    "galtonwatson.jl",
                                                    "ornsteinuhlenbeck.jl",
                                                   ),
    joinpath(@__DIR__, "measures", "curves") => (
                                                 "types.jl",
                                                 "yieldcurves.jl",
                                                 "termstructures.jl",
                                                 "discountcurves.jl"
                                                ),
    joinpath(@__DIR__, "models") => (
                                     "types.jl",
                                     "noarbitrage.jl",
                                     "bgm.jl",
                                     "black76.jl",
                                     "blackscholes.jl",
                                     "cir.jl",
                                     "cirpp.jl",
                                     "hjm.jl",
                                     "holee.jl",
                                     "hw1f.jl",
                                     "hw2f.jl",
                                     "vasicek.jl",
                                     "xvasicek.jl",
                                     "blackkarasinski.jl",
                                     "montecarlo.jl",
                                     "longstaffschwartz.jl",
                                     "margrabe.jl"
                                    ),
    joinpath(@__DIR__, "measures", "pricing") => (
                                                  "pricers_tools.jl",
                                                  "ivols.jl",
                                                  "americans_pricers.jl",
                                                  "asians_pricers.jl",
                                                  "barriers_pricers.jl",
                                                  "binaries_pricers.jl",
                                                  "europeans_pricers.jl",
                                                  "forwards_pricers.jl",
                                                  "fras_pricers.jl",
                                                  "swaps_pricers.jl",
                                                  "spreads_pricers.jl",
                                                  "zerobonds_pricers.jl",
                                                  "swaptions_pricers.jl"
                                                 ),
    joinpath(@__DIR__, "freyja") => (
                                     "types.jl", 
                                     "initzers.jl",
                                     "fotografi.jl",
                                     "unnur.jl",
                                     "globs.jl",
                                     "tools_fetchers.jl",
                                     "dfstruckturs.jl",
                                     "fetchers.jl",
                                     "updaters.jl"
                                    )
  )
end

"""
    RADIX::String

Global constant holding the package directory path.
"""
const RADIX::String = dirname(@__DIR__)

"""
    BUNCH::NTuple{9,AbstractString}

Global constant holding (required) main bunches of `ZsofiaDB`s.

|**Bunch**|**Description**|
|:---:|:---:|
|Artemis|`Stock`s|
|Euterpe|`NonMarketables`|
|Mercure|`EconoIndicator`|
|Nemesis|`Futures{Equity}`, `Futures{Indices{Equity}}`|
|Ophelya|`ETF`|
|Origami|`Derivatives`|
|Phorcys|`FXRate`|
|Satoshi|`CryptoCurrency`|
"""
const BUNCH::NTuple{9,AbstractString} = begin
  (
   "Artemis", "Demeter", "Euterpe", "Mercure",
   "Nemesis", "Ophelya", "Origami", "Phorcys",
   "Satoshi"
  )
end

for p in (
          joinpath(@__DIR__, "utils"),
          joinpath(@__DIR__, "instruments"),
          joinpath(@__DIR__, "measures", "processes"),
          joinpath(@__DIR__, "measures", "curves"),
          joinpath(@__DIR__, "models"),
          joinpath(@__DIR__, "measures", "pricing"),
          joinpath(@__DIR__, "freyja")
         )
  for f in _Zsofyles[p]
    include(joinpath(p,f))
  end
end
end ##--> End Of Module.