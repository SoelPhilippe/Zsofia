# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Forward{U} <: Derivatives{U}
  
  Forward contract (assumed cash-settled).
  
  # Fields
  - `strike::Real`: strike price. 
  - `expiry::TimePoint`: expiry time of the contract.
  - `underlying::Observables`: asset underlying the contract.
  - `islong::Bool`: whether or not the point of view is from the buy-side.
  - `inception::TimePoint`: inception time point.
  - `currency::Currency`: underlying currency.
  - `prices::Vector{Ream}`: prices container.
  - `times::AbstractVector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      Forward(strike::Real,expiry::TimePoint,underlying::Observables,<kwargs>)
  
  ## Arguments
  - `strike::Real`: strike price; buying/selling price we agree upon.
  - `expiry::TimePoint`: expiry date.
  - `underlying::Observables`: underlying financial instrument/asset.
  - `islong::Bool=true`: kwarg, buyer (`true`) or seller (`false`) side.
  - `inception::TimePoint=dummyinception(expiry)`: inception date/time point.
  - `currency::Currency=Currency()`: payment currency.
  
  # Notes
  Forward contracts are contracts initiating delayed (pre-agreed) transactions
  between parties to buy/sell an underlying commodity/asset/financial instrument
  to each other, at the pre-agreed (contractual) price.
  
  # Examples
  ```julia
  julia> begin
           pltr, ccy = Stock("PLTR"), Currency("USD")
           inc, expiry = Date(2024,12,21), Date(2025,3,21)
           fwd = Forward(105.50, expiry, pltr)
         end;
  
  julia> isa(fwd, Forward)
  true
  
  julia> isa(fwd, Forward{Stock})
  true
  ```
  """
  struct Forward{U} <: Derivatives{U}
    strike::A where A<:Real
    expiry::TimePoint
    underlying::U where U<:Observables
    islong::Bool
    inception::TimePoint
    currency::Currency
    prices::AbstractVector{C} where C<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function Forward(
                     strike::A,
                     expiry::B,
                     underlying::C;
                     islong::Bool=true,
                     inception::B=dummyinception(expiry),
                     currency::Currency=Currency()
             ) where {A<:Real,B<:TimePoint,C<:Observables}
      @assert isless(inception,expiry)
      new{C}(
             strike, expiry, underlying, islong, inception,
             currency, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", fwd::Forward)
    print(
          io, "Forward[", getfield(fwd,:strike), ", ",
          getfield(fwd,:expiry),"](",getparams(fwd,"ticker"),")"
    )
  end
  
  Base.string(::Forward) = "Forward"
  
  """
      payoff(fwd::Forward, [p::Real, t::TimePoint])
  
  Return the payoff of the `Forward` contract `fwd` given the risk factor
  is at level `p` at the time point `t`.
  
  # Notes
  When the method `payoff(fwd::Forward)` is used, the risk factor is inferred
  from the underlying risk factor's last observed level (and therefore last
  observation time).
  """
  function payoff(fwd::Forward, p::R, t::T) where {R<:Real,T<:TimePoint}
    *(isequal(t,fwd.expiry), p - fwd.strike, ifelse(fwd.islong,1,-1))
  end
  
  function payoff(fwd::Forward)
    payoff(fwd,S(fwd.underlying)(),last(fwd.underlying.times))
  end
end