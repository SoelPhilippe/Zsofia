# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      hasparams(obs::Observables, tag::AbstractString)
      
  Return `true` is `obs` parameters containers has such a key as `tag`.
  """
  function hasparams(obs::Observables, tag::AbstractString)
    if isequal(lowercase(tag),"mu")
      haskey(getfield(obs,:params),"μ") || haskey(obs.params,tag)
    elseif isequal(lowercase(tag),"sigma")
      haskey(getfield(obs,:params),"σ") || haskey(obs.params,tag)
    else
      haskey(getfield(obs,:params),tag)
    end
  end
  
  """
      settimes!(obs::Observables, [times::Vector{TimePoint}])
  
  # Internal Facility
  Set `times` field/property for the `Observables` `obs`.
  
  # Arguments
  - `obs::Observables`: the `Observables` to set times of.
  - `times::Vector{TimePoint}`: vector of time points to set.
  
  # Notes
  The set `times` are returned, they should be unique and sorted, otherwise
  an `ArgumentError` is thrown.
  When the argument `times` is not provided, the `times` property/field is
  emptied.
  
  # Examples
  ```julia-repl
  julia> pltr = Stock("PLTR");
  
  julia> isempty(pltr)
  true
  
  julia> setprices!(pltr, [77.77,71.71,69.69], [0.10,0.15,0.20])
  ([0.1, 0.15, 0.2], [77.77, 71.71, 69.69])
  
  julia> setttimes!(pltr, [0.20,0.21,0.22])
  3-element Vector{Float64}:
  0.2
  0.21
  0.22
  ```
  """
  function settimes!(
                     obs::Observables,
                     times::AbstractVector{Q}
           ) where Q<:TimePoint
    @assert allunique(times)
    @assert issorted(times)
    resize!(getfield(obs,:times),length(times))
    broadcast!(identity,getfield(obs,:times),times)
  end
  
  settimes!(obs::Observables) = settimes!(obs,TimePoint[])
  
  """
      resettimes!(obs::Observables, [times::Vector{TimePoint}])
  
  # Internal Facility
  re`settimes!` of `obs` by `times`.
  
  See also [`settimes!`](@ref).
  """
  function resettimes!(
                       obs::Observables,
                       times::AbstractVector{Q}
           ) where Q<:TimePoint
    settimes!(obs,times)
  end
  
  resettimes!(obs::Observables) = resettimes!(obs::Observables,TimePoint[])

  """
      setprices!(obs::Observables, prices, times)
  
  Set `obs`' prices and times containers by (resp.) `prices` and `times`.
  
  # Arguments
  - `obs::Observables`: the `Observables` to set times of.
  - `prices::Vector{Real}`: vector of prices setting the field.
  - `times::Vector{TimePoint}`: vector of time points to set.
  
  See also [`settimes!`](@ref).
  
  # Notes
  The tuple `(times,prices)` is returned. The `times` vector should be made of
  unique and sorted values. When `prices` is not provided, `times` shouldn't
  be provided; in such a case containers are emptied.
  
  # Examples
  ```julia-repl
  julia> usd = Currency("USD");
  
  julia> ir = InterestRate(Tenor("3m"), currency=usd);
  
  julia> setprices!(ir,[0.04356,0.04371],[Date(2024,12,17),Date(2024,12,19)])
  ([0.04356, 0.04371], [Date("2024-12-17"), Date("2024-12-19")])
  ```
  """
  function setprices!(
                      obs::Observables,
                      prices::AbstractVector{R},
                      times::AbstractVector{Q}
           ) where {R<:Real,Q<:TimePoint}
    @assert isequal(length(prices),length(times)) || isone(length(prices))
    settimes!(obs,times)
    resize!(getfield(obs,:prices),length(prices))
    broadcast!(identity,getfield(obs,:prices),prices)
    times,prices
  end
  
  setprices!(obs::Observables) = setprices!(obs,Real[],TimePoint[])
  
  
  """
      resetprices!(obs::Observables, prices::Vector{R}, times::Vector{T})
  
  Re`setprices!` of `obs`.
  
  See also [`setprices!`](@ref), [`settimes!`](@ref).
  """
  function resetprices!(
                        obs::Observables,
                        prices::AbstractVector{R},
                        times::AbstractVector{Q}
           ) where R<:Real where Q<:TimePoint
    setprices!(obs, prices, times)
  end
  
  resetprices!(obs::Observables) = setprices!(obs)
  
  """
      setparams!(obs::Observables, tag::AbstractString, value; [force])
      setparams!(obs::Observables, dict::Dict{AbstractString,Any}; [force])
  
  Set `value` as a parameter (uniquely) identified by `tag` into `obs`'
  parameters container.
  
  # Arguments
  - `obs::Observables`: the underlying observable to set parameter of.
  - `tag::AbstractString`: the parameter tag.
  - `value`: parameter value to be stored under the identifier `tag`.
  - `dict::Dict{AbstractString,Any}`: a dictionary of parameters to set.
  - `force::Bool=false`: kwarg, if `true` existing value at `tag` is replaced.
  
  # Notes
  The parameter's value `value` identified by `tag` can be of any type.
  The function returns the pair `tag => value`.
  Some `tag`s are treated particulary: "mu", "sigma", "μ", "σ" as they are
  widespread in the financial realm.
  
  # Examples
  ```julia-repl
  julia> ir = InterestRate(Tenor("1m"), currency=Currency("USD"));
  
  julia> for (k,v) in zip(("n","f","t"),("A",cos,today()))
           setparams!(ir, k, v);
         end
  
  julia> setparams!(ir, "μ", 0.05)
  "μ" => 0.05
  ```
  """
  function setparams!(
                      obs::Observables,
                      tag::AbstractString,
                      value::Any;
                      force::Bool=false
           )
    if hasparams(obs,tag) && ~force
      throw(ArgumentError("$(tag) exists! Use `force=true` to modify."))
    end
    if lowercase(tag) == "mu" || (tag == "μ")
      ~isa(value, Real) ? throw(ArgumentError("$(tag) should be a number")) : 0
      obs.params["μ"] = value
    elseif (lowercase(tag) == "sigma") || (tag == "σ")
      ~isa(value, Real) ? throw(ArgumentError("$(tag) should be a number")) : 0
      <(0)(value) ? throw(ArgumentError("$(tag) should be non-negative")) : 0
      obs.params["σ"] = value
    else
      obs.params[tag] = value
    end
    tag => value
  end
  
  function setparams!(
                      obs::Observables,
                      dict::Dict{AbstractString,Any};
                      force::Bool=false
           )
    collect(setparams!(obs, k, v, force=force) for (k,v) in dict)
  end
  
  """
      getparams(obs::Observables, tag::AbstractString)
      getparams(obs::Observables, tag::AbstractString, default)
  
  Return the value stored at key `tag`.
  """
  function getparams(obs::Observables, tag::AbstractString)
    if lowercase(tag) == "mu"
      getfield(obs,:params)["μ"]
    elseif lowercase(tag) == "sigma"
      getfield(obs,:params)["σ"]
    else
      getfield(obs,:params)[tag]
    end
  end
  
  function getparams(obs::Observables, tag::AbstractString, default::Any)
    hasparams(obs,tag) ? getparams(obs,tag) : default
  end
  
  """
      getparams!(obs::Observables, tag::AbstractString, default::Any)
  
  Return the value stored into `obs`' parameters container for the given
  identifier `tag`, or if no mapping for the identifier is present,
  store `default` at that identifier and return `default`.
  
  # Arguments
  - `obs::Observables`: underlying `Observables` to process.
  - `tag::AbstractString`: parameter's identifier.
  - `default::Any`: default value to store if no value is present at `tag`.
  
  # Examples
  ```julia-repl
  julia> goog = Stock("GOOG");
  
  julia> isempty(goog.params)
  true
  
  julia> googaddress = "1600 Amphitheatre Parkway, Mountain View, CA 94043";
  
  julia> getparams!(goog, "address", googaddress)
  "1600 Amphitheatre Parkway, Mountain View, CA 94043"
  ```
  """
  function getparams!(obs::Observables,tag::AbstractString,default)
    try
      getparams(obs,tag)
    catch er
      default
    finally # non-intuitive behavior
      setparams!(obs,tag,default,force=true)
    end
  end
  
  """
      insertprices!(obs::Observables, price::Real, time::TimePoint)
  
  Insert the price level `p` into the prices container of `obs` at the
  time point `t`.
  
  See also [`insert!`](@ref).
  
  # Arguments
  - `obs::Observables`: underlying observable to modify.
  - `p::Real`: the price level to insert.
  - `t::R`: time at which to insert.
  
  # Notes
  Any existing value is overwritten without any notice.
  
  The implementation is built on top of `insert!` we chose not to overload
  `insert!`, but we have been tempted to very recently. This may happen
  in a future release.
  """
  function insertprices!(
                         obs::Observables,
                         price::R,
                         t::T
           ) where {R<:Real,T<:TimePoint}
    i = searchsortedfirst(getfield(obs,:times),t)
    insert!(getfield(obs,:times),i,t)
    insert!(getfield(obs,:prices),i,price)
    t, price
  end
  
  function insertprices!(
                         obs::Observables,
                         prices::AbstractVector{R},
                         times::AbstractVector{T}
           ) where {R<:Real,T<:TimePoint}
    @assert isequal(length(prices),length(times)) throw(DimensionMismatch("<>"))
    # to be bettered
    collect(insertprices!(obs,p,t) for (p,t) in zip(prices,times))
  end
  
  function Base.isempty(obs::Observables)
    isempty(getfield(obs,:times)) || isempty(getfield(obs,:prices))
  end
  
  function Base.isempty(obs::Observables, what::Symbol)
    isempty(getfield(obs,what))
  end
  
  function Base.popat!(
                       obs::Observables,
                       t::T,
                       default::Tuple{R,T}
                ) where {T<:TimePoint,R<:Real}
    i = searchsortedfirst(getfield(obs,:times),t)
    popat!(getfield(obs,:times),i,default),
    popat!(getfield(obs,:prices),i,default)
  end
  
  function Base.popat!(obs::Observables, t::T) where T<:TimePoint
    i = searchsortedfirst(getfield(obs,:times),t)
    popat!(getfiedl(obs,:times),i),
    popat!(getfield(obs,:prices),i)
  end
  
  function Base.deleteat!(obs::Observables, t::T) where T<:TimePoint
    i = searchsortedfirst(getfield(obs,:times),t)
    deleteat!(getfield(obs,:times),i)
    deleteat!(getfield(obs,:prices),i)
    obs
  end
  
  function Base.deleteat!(
                          obs::Observables,
                          t::AbstractVector{T}
                ) where T<:TimePoint
    for s in t
      deleteat!(obs, s)
    end
  end
  
  function Base.deleteat!(
                          obs::Observables,
                          t::Tuple{Vararg{T}}
                ) where T<:TimePoint
    for u in t
      deleteat!(obs,u)
    end
  end
  
  """
      S(obs::Observables)
  
  Return a step function mapping times to correspondin `obs`' prices.
  
  # Notes
  Domain subdivision is made up of left-opened intervals i.e. `[t1, t2)`.
  """
  function S(obs::Observables)
    if isempty(obs)
	    throw(ArgumentError("empty prices container"))
	  end
	  n::Int = length(getfield(obs, :times))
	  if ~isequal(n, length(getfield(obs,:prices)))
      throw(DimensionMismatch("times($(n)), prices($(length(obs.prices)))"))
	  end
	  function _f(t::TimePoint)
		    if isone(n)
		      *(
		        only(getfield(obs,:prices)),
		        dirac(t,only(getfield(obs,:times)), typemax(last(obs.times)))
		      )
		    else
		      +(
		        sum(
		            p * dirac(t,l,u)
		            for (p,l,u) in zip(
		                               view(getfield(obs,:prices), Colon()(1,n-1)),
		                               view(getfield(obs,:times), Colon()(1,n-1)),
		                               view(getfield(obs,:times), Colon()(2,n))
		                           )
		        ),
		        last(getfield(obs,:prices)) *
		        dirac(
		              t,
		              last(getfield(obs,:times)),
		              typemax(last(getfield(obs,:times)))
		        )
		      )
		    end
	  end
	  _f() = last(getfield(obs, :prices))
	  return _f
  end
end