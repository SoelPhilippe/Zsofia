# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      OptionColor::Enum
  
  An enemration type who instances are `Call` and `Put` denoting the color
  of a financial contract having an optionality feature.
  """
  @enum OptionColor begin
    Call = 1
    Put
  end
  
  """
      ExersiseStyle::Enum
  
  An enumeration type whose instances are:
  
  |**Instances**|**Short Descr.**|
  |:---:|:---:|
  |`AmericanStyle`|american exercize style|
  |`BermudanStyle`|bermudan exercize style|
  |`EuropeanStyle`|european exercize style|
  """
  @enum ExerciseStyle begin
    AmericanStyle = 1
    EuropeanStyle
    BermudanStyle
  end
end
