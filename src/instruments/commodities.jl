# This script is part of Zsofia. Soel Philippe © 2025

begin
  @enum CommodityType begin
    HardCommodity = 1
    SoftCommodity
  end
  
  """
      Commodity <: Commodities
  
  # Fields
  - `commoditytype::CommodityType`: either `SoftCommodity` or `HardCommodity`.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      Commodity([ticker::AbstractString];[commoditytype::CommodityType])
  
  ## Arguments
  - `ticker::AbstractString=DummyTicker()`: commodity's ticker symbol.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: observations time container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Notes
  This type `Commodity` is intended to represent commodities traded on spot.  
  By default the `params` field/property which is a
  `Dict{AbstractString,Any}` dictionary contains the following keys:
  - `ticker::AbstractString`: the ticker symbol defaulting to `DummyTicker()`.
  - `μ::Real`: the drift parameter under the Black-Scholes framework.
  - `σ::Real`: the volatility parameter under the Black-Scholes framework.
  - `carryrate::Real`: the cost of carrying the commodity.
  """
  struct Commodity <: Commodities
    commoditytype::CommodityType
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function Commodity(
                       ticker::AbstractString=dummyticker();
                       commoditytype::CommodityType=HardCommodity
              )
      new(
          commoditytype, Real[], TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => ticker,
                                   "μ" => 0, 
                                   "σ" => 0,
                                   "carryrate" => 0
          )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", c::Commodity)
    print(io, getfield(c,:commoditytype),"(",getparams(c,"ticker"),")")
  end
end