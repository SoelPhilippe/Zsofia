# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      InterestRate <: Rates
  
  The interest rates data structure representation facility.
  
  # Fields
  - `tenor::Tenor`: tenor, holding the compounding frequency.
  - `isfixed::Bool`: when `true`, the interest is fixed.
  - `currency::Currency`: underlying economy's currency.
  - `prices::Vector{Real}`: prices container.
  - `times::Vector{<:TimePoint}`: observation times container.
  - `params::Dict{String, Any}`: casual parameters container.
  
  # Constructors
      InterestRate(tenor::Tenor, <kwargs>)
      InterestRate(tenor::Tenor, fixedrate::Real, [currency=Currency()])
      InterestRate(fixedrate::R; [tenor::Tenor, currency=Currency()])

  ## Arguments
  - `tenor::Tenor`: the interest rate tenor/the compounding period.
  - `fixedrate::Real`: fixed interest rate quote (quoted on annual basis).
  - `currency::Currency=Currency()`: kwarg, currency of the related economy.
  - `isfixed::Bool=false`: kwarg, if `true` fixed interest rate.
  
  # Notes
  If you require a continuously compounded interest rate, use a zero-length
  tenor (`Tenor("0d")`, `Tenor("0m")`, `Tenor(0)` ...).  
  Whenever the `isfixed` is `true` it prevails in any facility.  
  When it's a fixed interest rate, you can change it's value using the
  `setparams!` facility with the key/tag "rate". 
  """
  struct InterestRate <: Rates
    tenor::Tenor
    isfixed::Bool
    currency::Currency
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function InterestRate(
                          tenor::Tenor;
                          currency::Currency=Currency(),
                          isfixed::Bool=false,
                          fixedrate::R=0
             ) where R<:Real
      new(
          tenor, isfixed, currency, Real[], TimePoint[],
          Dict{AbstractString, Any}(
                                    "ticker" => dummyticker(),
                                    "μ" => 0,
                                    "σ" => 0,
                                    "carryrate" => 0,
                                    "_rate" => fixedrate
          )
      )
    end
    
    function InterestRate(
                          tenor::Tenor,
                          fixedrate::R;
                          currency::Currency=Currency()
             ) where R<:Real
      InterestRate(tenor, currency=currency, isfixed=true, fixedrate=fixedrate)
    end
    
    function InterestRate(
                          fixedrate::R;
                          tenor::Tenor=Tenor("0y"),
                          currency::Currency=Currency()
             ) where R<:Real
      InterestRate(tenor, fixedrate, currency=currency)
    end
  end

  function hasparams(ir::InterestRate, tag::AbstractString)
    if isequal(lowercase(tag),"mu")
      haskey(getfield(ir,:params),"μ") || haskey(ir.params,tag)
    elseif isequal(lowercase(tag),"sigma")
      haskey(getfield(ir,:params),"σ") || haskey(ir.params,tag)
    elseif isequal(lowercase(tag),"rate")
      kaskey(getfield(ir,:params),"_rate") || haskey(ir.params,tag)
    else
      haskey(getfield(ir,:params),tag)
    end
  end

  function setparams!(
                      ir::InterestRate,
                      tag::AbstractString,
                      value;
                      force::Bool=false
           )
    if hasparams(ir, tag) && ~force
      throw(ArgumentError("$(tag) exists! Use `force=true` to modify."))
    end
    if (lowercase(tag) == "mu") || (tag == "μ")
      ~isa(value, Real) ? throw(ArgumentError("$(tag) should be a number")) : 0
      ir.params["μ"] = value
    elseif (lowercase(tag) == "sigma") || (tag == "σ")
      ~isa(value, Real) ? throw(ArgumentError("$(tag) should be a number")) : 0
      <(0)(value) ? throw(ArgumentError("$(tag) should be non-negative")) : 0
      ir.params["σ"] = value
    elseif lowercase(tag) == "rate"
      isa(value,Real) ? throw(ArgumentError("should be a real number")) : 0
      ir.params["_rate"] = value
    else
      ir.params[tag] = value
    end
    tag => value
  end
  
  function getparams(ir::InterestRate, tag::AbstractString)
    if (lowercase(tag) == "mu") || (tag == "μ")
      ir.params["μ"]
    elseif (lowercase(tag) == "sigma") || (tag == "σ")
      ir.params["σ"]
    elseif lowercase(tag) == "rate"
      ir.params["_rate"]
    else
      ir.params[tag]
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ir::InterestRate)
    if getfield(ir, :isfixed)
      print(
            io, "InterestRate[Ticker: ",getparams(ir, "ticker"),
            "; Tenor: ", getfield(ir, :tenor).p, "; Rate: ",
            getparams(ir, "_rate"), "; ", getfield(ir, :currency),"]"
      )
    else
      print(
            io, "InterestRate[Ticker: ",getparams(ir, "ticker"),
            "; Tenor: ", getfield(ir, :tenor).p,"; ",
            getfield(ir, :currency), "]"
      )
    end
  end

  """
      S(ir::InterestRate)::Function
  
  Return a step function mapping `times` field of `ir` to `prices` field.

  # Notes
  Domain subdivision is made up of right-opened intervals i.e. `[t1, t2)`.
  """
  function S(ir::InterestRate)::Function
    if ~getfield(ir, :isfixed) && isempty(ir)
	    throw(ArgumentError("empty prices container"))
	  end
	  n::Int = length(getfield(ir, :times))
	  if ~isequal(n, length(getfield(ir,:prices)))
	    throw(DimensionMismatch("times($(n)), prices($(length(ir.prices)))"))
	  end
	  
	  function _f(t::TimePoint)
	    if getfield(ir, :isfixed)
	      getparams(ir, "_rate")
	    else
		    if isone(n)
		      *(
		        only(getfield(ir, :prices)),
		        dirac(t, only(getfield(ir, :times)), typemax(last(ir.times)))
		      )
		    else
		      +(
		        sum(
		            p * dirac(t,l,u)
		            for (p,l,u) in zip(
		                               view(getfield(ir,:prices), Colon()(1,n-1)),
		                               view(getfield(ir,:times), Colon()(1,n-1)),
		                               view(getfield(ir,:times), Colon()(2,n))
		                           )
		        ),
		        last(getfield(ir,:prices)) *
		        dirac(t, last(ir.times), typemax(last(ir.times)))
		      )
		    end
	    end
	  end
	  _f() = getfield(ir, :isfixed) ? getparams(ir, "_rate") : last(ir.prices)
	  return _f
  end

  """
      convert(tenor::Tenor, ir::InterestRate; dcc::DayCountConvention=ActAct())
  
  Create and return an `InterestRate` by adapting `ir`'s period-related
  properties into their equivalent `tenor`-related ones.

  The return value is an `InterestRate` having `tenor` as tenor, and
  their `prices`, if any, converted to reflect the new period `tenor`.

  # Arguments
  - `tenor::Tenor`: 
  - `ir::InterestRate`: interest rate to convert.
  - `dcc::DayCountConventions`: day count convention.

  # Notes
  This function does not modify the input `InterestRate` but creates
  a new one. Any tenor-depending property is transformed/adapted
  into their equivalent to fit the new tenor given.
  """
  function Base.convert(
                        tenor::Tenor,
                        ir::InterestRate;
                        dcc::DayCountConventions=ActAct()
                )
    if ~isa(getfield(ir,:tenor), Period)
      throw(ArgumentError("ir's tenor is not convertible [different types]"))
    end
    tn, to = yearfraction(dcc,tenor), yearfraction(dcc,getfield(ir,:tenor))
    _v = Vector{Real}(map(x -> _convert_rates(tn,to,x), getfield(ir,:prices)))
    _ir = InterestRate(
                       tenor,
                       currency=getfield(ir,:currency),
                       isfixed=getfield(ir,:isfixed),
                       fixedrate=_convert_rates(tn,to,getparams(ir,"_rate"))
          )
    setprices!(_ir,_v,getfield(ir,:times))
    for k in ("ticker","μ","σ","carryrate")
      setparams!(_ir,k,getparams(ir,k),force=true)
    end
    _ir
  end
end