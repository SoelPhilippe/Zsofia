# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      BinaryOption <: Options

  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry date of the binary option.
  - `option::OptionColor`: `Call` or `Put`, the option color.
  - `underlying::Observables`: underlying risk factor.
  - `inception::TimePoint`: inception time point of the contract.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{<:TimePoint}`: observation times container.
  - `params::Dict{AsbtractString, Any}`: casual parameters container.
  
  # Constructors
      BinaryOption(strike, expiry, [option], [underlying], [inception])
  
  ## Arguments
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry date of the contract.
  - `option::OptionColor`: kwarg, `Call`/`Put` (default: `Call`).
  - `underlying::Observables`: kwarg, the underlying rsik factor.
  - `inception::TimePoint`: kwarg, the `inception` time point of the contract.

  # Notes
  In the current implementation, the exercise style for BinaryOption is
  by default `EuropeanStyle`.
  """
  struct BinaryOption{U} <: Options{U}
    strike::R where R<:Real
    expiry::T where T<:TimePoint
    option::OptionColor
    underlying::U where U<:Observables
    inception::T where T<:TimePoint
    prices::AbstractVector{Q} where Q<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function BinaryOption(
                          strike::R,
                          expiry::Q;
                          option::OptionColor=Call,
                          underlying::U=DummyObs(),
                          inception::Q=dummyinception(expiry)
             ) where {R<:Real,Q<:TimePoint,U<:Observables}
      if isless(expiry, inception)
        throw(ArgumentError("expiry($(expiry)) < inception($(inception))"))
      end
      new{U}(
             strike, expiry, option, underlying,
             inception, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
  end

  function Base.show(io::IO, ::MIME"text/plain", op::BinaryOption)
    print(
          io, "BinaryOption[",getfield(op,:strike),", ",
          getfield(op,:expiry),"](", getparams(op,"ticker"),")"
    )
  end
  
  """
      payoff(op::BinaryOption, [s::Real], [t::TimePoint])
  
  Return the payoff of the binary option `op` with regard to the
  underlying risk factor level `s` and the observation time `t`.

  # Arguments
  - `op::BinaryOption`: binary option.
  - `p::Real`: underlying risk factor price/level.
  - `t::TimePoint=op.inception`: observation time.
  """
  function payoff(
                  op::BinaryOption,
                  s::R = S(op.underlying)(),
                  t::T=last(getfield(op.underlying,:times))
           ) where {R<:Real,T<:TimePoint}
    *(
      isequal(t, getfield(op,:expiry)),
      isless(0, (2*isequal(op.option,Call)-1) * (s - getfield(op,:strike))),
      1
    )
  end
end