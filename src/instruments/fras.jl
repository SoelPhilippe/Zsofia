# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      FRA <: Derivatives{InterestRate}
  
  Forward Rate Agreement, aka FRA.
  
  # Fields
  - `strike::Real`: pre-agreed fixed rate, the strike rate level.
  - `fixing::TimePoint`: expiry date of the underlying risk randomness.
  - `underlying::InterestRate`: underlying interest rate/risk factor.
  - `maturity::TimePoint`: settlement, payment date.
  - `inception::TimePoint`: inception date of the contract.
  - `ispayer::Bool`: whether or not paying the fixed rate.
  - `notional::Real`: notional/principal of the contract.
  - `currency::Currency`: settlement/payment currency.
  - `dcc::DayCountConventions`: day count convention.
  - `prices::Vector{Real}`: prices container.
  - `times::Vector{<:TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      FRA(strike, fixing::TimePoint, maturity::TimePoint, underlying, <kwargs>)
  
  ## Arguments
  - `strike::Real`: agreed-upon rate to be exchanged at `maturity`.
  - `fixing::TimePoint`: fixing/observation date of the risk factor level.
  - `underlying::InterestRate`: underlying risk factor determiner.
  - `maturity::TimePoint=+(fixing,underlying.tenor)`: kwarg, settlement date.
  - `inception::TimePoint=dummyinception(fixing)`: kwarg, inception date.
  - `ispayer::Bool=true`: kwarg, payer or receiver point of view.
  - `notional::Real=1`: kwarg, contract's notional.
  - `currency::Currency=Currency()`: settlement/payment currency.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  
  # Notes
  A forward rate agreement consist of three main dates: the inception at which
  the contract starts to have value, the fixing date, the time at which any
  contingency inherent to the contract collapse since the risk factor
  (interest rate) is fixed and then the maturity, the payment/settlement date
  which is the date at which all interest is accrued and paid.
  
  # Examples
  ```julia
  julia> ccy, dcc = Currency("USD"), Thirty360();
  
  julia> r = InterestRate(Tenor("3m"),currency=ccy);
  
  julia> fra = begin
           FRA(
               0.05, Date(2025,3,15), r, maturity=Date(2025,6,15),
               inception=Date(2025,1,15), ispayer=true, currency=ccy, dcc=dcc
           );
         end
  
  julia> isa(fra, FRA)
  true
  ```
  """
  struct FRA <: Derivatives{InterestRate}
    strike::A where A<:Real
    fixing::TimePoint
    underlying::InterestRate
    maturity::TimePoint
    inception::TimePoint
    ispayer::Bool
    notional::B where B<:Real
    currency::Currency
    dcc::DayCountConventions
    prices::AbstractVector{C} where C<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function FRA(
                 strike::A,
                 fixing::B,
                 underlying::U;
                 maturity::B=+(fixing,underlying.tenor),
                 inception::B=dummyinception(fixing),
                 ispayer::Bool=true,
                 notional::Real=one(Real),
                 currency::Currency=Currency(),
                 dcc::DayCountConventions=ActAct()
             ) where {A<:Real,B<:TimePoint,U<:InterestRate}
      @assert inception <= fixing <= maturity
      new(
          strike, fixing, underlying, maturity, inception,
          ispayer, notional, currency, dcc, Real[], TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => dummyticker(),
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0
          )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", fra::FRA)
    print(
          io, "ForwardRateAgreement[", fra.strike, ", ", fra.expiry,
          ", ", ifelse(fra.ispayer,"payer","receiver"),", ",
          fra.maturity,"](",getparams(fra,"ticker"),")"
    )
  end
  
  """
      payoff(fra::FRA, [p::Real, t::TimePoint])
  
  Return the payoff of the Forward Rate Agreement `fra` given
  the risk factor level `p` at time `t`.
  """
  function payoff(fra::FRA, p::R, t::T) where {R<:Real,T<:TimePoint}
    *(
      isequal(t,fra.maturity),
      ifelse(fra.ispayer,1,-1),
      -(p, fra.strike),
      yearfraction(fra.dcc,fra.fixing,fra.maturity),
      fra.notional
    )
  end
end