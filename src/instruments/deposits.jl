# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Deposit <: Rates
      
  Financial instrument widely known as deposit (rate).
  
  # Fields
  - `tenor::Tenor`: tenor object to model maturity.
  - `isfixed::Bool`: if `true`, fixed rate.
  - `currency::Currency`: underlying currency.
  - `prices::Vector{Real}`: prices container.
  - `times::Vector{TimePoint}`: time stamps container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      Deposit(tenor::Tenor; currency::Currency=Currency())
      Deposit(fixedrate::Real, tenor::Tenor; currency::Currency=Currency())
  
  ## Arguments
  - `tenor::Tenor`: underlying tenor.
  - `currency::Currency=Currency()`: deposit's Currency.
  - `fixedrate::Real`: fixed rate level.
  
  # Notes
  This instrument get quite the same properties as `InterestRate`s.
  """
  struct Deposit <: Rates
    tenor::Tenor
    isfixed::Bool
    currency::Currency
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function Deposit(tenor::Tenor;currency::Currency=Currency())
      new(
          tenor,false,currency,Real[],TimePoint[],
          Dict{AbstractString,Any}(
            "ticker" => dummyticker(),
            "μ" => 0,
            "σ" => 0,
            "carryrate" => 0
          )
      )
    end
    
    function Deposit(
                     fixedrate::R,
                     tenor::Tenor;
                     currency::Currency=Currency()
             ) where R<:Real
      new(
          tenor,true,currency,Real[],TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => dummyticker(),
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0,
                                   "_rate" => fixedrate
          )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", dep::Deposit)
    if getfield(dep, :isfixed)
      print(
            io, "Deposit[", getparam(dep,"ticker"),"; ",
            "Tenor: ", getfield(dep,:tenor),"; Rate: ",
            getparams(dep, "_rate"), ";  ", getfield(dep, :currency),"]"
      )
    else
      print(
            io, "Deposit[", getparams(dep,"ticker"),"; Tenor: ",
            getfield(dep, :tenor),"; ", getfield(dep, :currency),"]"
      )
    end
  end

  """
      S(dep::Deposit)::Function
  
  Return a step function mapping `times` field of `dep` to `prices` field.
  
  # Notes
  Domain subdivision is made up of right-opened intervals i.e. `[t1, t2)`.
  """
  function S(dep::Deposit)::Function
    if ~getfield(dep,:isfixed) && isempty(dep)
	    throw(ArgumentError("empty prices container"))
	  end
	  n::Int = length(getfield(dep,:times))
	  if ~isequal(n,length(getfield(dep,:prices)))
	    throw(DimensionMismatch("times($(n)), prices($(length(dep.prices)))"))
	  end
	  
	  function _f(t::TimePoint)
	    if getfield(dep,:isfixed)
	      getparams(dep,"_rate")
	    else
		    if isone(n)
		      *(
		        only(getfield(dep,:prices)),
		        dirac(t,only(getfield(dep,:times)), typemax(last(dep.times)))
		      )
		    else
		      +(
		        sum(
		            p * dirac(t,l,u)
		            for (p,l,u) in zip(
		                               view(getfield(dep,:prices), Colon()(1,n-1)),
		                               view(getfield(dep,:times), Colon()(1,n-1)),
		                               view(getfield(dep,:times), Colon()(2,n))
		                           )
		        ),
		        last(getfield(dep,:prices)) *
		        dirac(t,last(dep.times), typemax(last(dep.times)))
		      )
		    end
	    end
	  end
	  _f() = getfield(dep,:isfixed) ? getparams(dep,"_rate") : last(dep.prices)
	  return _f
  end

  """
      convert(tenor::Tenor, dep::Deposit; [dcc::DayCountConventions=ActAct()])
  
  Create and return an `Deposit` by adapting `dep`'s period-related
  properties into their equivalent `tenor`-related ones.

  The return value is an `Deposit` having `tenor` as tenor, and
  their `prices`, if any, converted to reflect the new period `tenor`.

  # Arguments
  - `tenor::Tenor`: 
  - `dep::Deposit`: interest rate to convert.
  - `dcc::DayCountConventions`: day count convention.

  # Notes
  This function does not modify the input `Deposit` but creates
  a new one. Any tenor-depending property is transformed/adapted
  into their equivalent to fit the new tenor given.
  """
  function Base.convert(
                        tenor::Tenor,
                        dep::Deposit;
                        dcc::DayCountConventions=ActAct()
                )
    if ~isa(getfield(dep,:tenor), Period)
      throw(ArgumentError("dep's tenor is not convertible [different types]"))
    end
    tn, to = yearfraction(dcc,tenor), yearfraction(dcc,getfield(dep,:tenor))
    _v = Vector{Real}(map(x -> _convert_rates(tn,to,x), getfield(dep,:prices)))
    if getfield(dep,:isfixed)
      _dep = Deposit(
                     _convert_rates(tn,to,getparams(dep,"_rate")),
                     tenor,
                     currency=getfield(dep,:currency)
             )
    else
      _dep = Deposit(
                     tenor,
                     currency=getfield(dep,:currency)
             )
    end
    setprices!(_dep,_v,getfield(dep,:times))
    for k in ("ticker","μ","σ","carryrate")
      setparams!(_dep,k,getparams(dep,k),force=true)
    end
    _dep
  end
end