# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      FixedLeg <: Conventions
  
  A singleton type representing legs of financial instruments having
  a fixed leg and a floating leg.

  See also [`FloatingLeg`](@ref).

  # Notes
  This type has been specially built for interest rates swaps having
  a fixed and a floating leg.

  This is intended to be used in facilities such as `cashflows`,
  `payoff` etc.
  """
  struct FixedLeg <: Conventions end

  Base.show(io::IO, ::MIME"text/plain", ::FixedLeg) = print(io,"FixedLeg")
  Base.string(::FixedLeg) = "FixedLeg"

  """
      FloatingLeg <: Conventions
  
  A singleton type representing legs of financial instruments having
  a fixed leg and a floating leg.

  See also [`FixedLeg`](@ref).

  # Notes
  This type has been specially built for interest rates swaps having
  a fixed and a floating leg.
  
  This is intended to be used in facilities such as `cashflows`, `payoff` etc.
  """
  struct FloatingLeg <: Conventions end
  
  function Base.show(io::IO, ::MIME"text/plain", ::FloatingLeg)
    print(io,"FloatingLeg")
  end

  Base.string(::FloatingLeg) = "FloatingLeg"

  """
      Swap <: Swaps{InterestRate}
  
  Vanilla swap, fixed vs floating interest rate swap.
  
  # Fields
  - `swaprate::Real`: swap rate, pre-agreed fixed leg rate.
  - `expiry::TimePoint`: expiry/maturity of the swap.
  - `underlying::InterestRate`: underlying risk factor/benchmark.
  - `inception::TimePoint`: should be of same subtype as `expiry`.
  - `fixingsflt::NTuple{Vararg{TimePoint}}`: fixing dates for the floating leg.
  - `fixingsfix::NTuple{Vararg{TimePoint}}`: fixing dates for the fixed leg.
  - `ispayer::Bool`: if false, a receiver swap otherwise a payer swap.
  - `notional::Real`: notional amount of the contract.
  - `currencyflt::Currency`: floating leg currency.
  - `currencyfix::Currency`: fixed leg currency.
  - `dccflt::DayCountConventions`: day count convention, floating leg.
  - `dccfix::DayCountConventions`: day count convention, fixed leg.
  - `prices::Vector{Real}`: observed prices container.
  - `times::Vector{TimePoint}`: observation times container.
  - `params::Dict{String,Any}`: casual parameters container.
  
  # Constructors
      Swap(swaprate::Real,expiry::TimePoint,underlying::InterestRate, <kwargs>)
      Swap(swaprate::Real,tenor::Tenor,underlying::InterestRate, <kwargs>)
  
  ## Arguments
  -  `swaprate::Real`: swap rate.
  -  `expiry::TimePoint`: contract's maturity/expiry.
  -  `tenor::Tenor`: swap tenor, to specify maturity.
  -  `underlying::InterestRate`: underlying risk factor.
  -  `inception::TimePoint`: kwarg, contract's inception date.
  -  `fixingsflt::AbstractVector{TimePoint}}`: kwarg, fix-leg fixings times.
  -  `fixingsfix::AbstractVector{TimePoint}}`: kwarg, float-leg fixings times.
  -  `ispayer::Bool=true`: kwarg, whether it's a payer or receiver swap.
  -  `notional::Real=1.0`: kwarg, notional amount.
  -  `currencyflt::Currency=Currency()`: kwarg, float leg currency.
  -  `currencyfix::Currency=Currency()`: kwarg, fixed leg currency.
  -  `dccflt::DayCountConventions=ActAct()`: kwarg, floating leg dcc.
  -  `dccfix::DayCountConventions=ActAct()`: kwarg, fixed leg dcc.
  
  # Notes
  This implementation is intended for vanilla's fixed vs floating rate.
  Provided the existence of a given `YieldCurve` one can use the `cashflows`
  function to get (estimated) cashflows binded to the swap
  (see [`cashflows`](@ref)).

  # Examples
  ```julia
  julia> r = InterestRate(Tenor("3m"), currency=Currency("USD"));
  
  julia> inc, xp = Date(2024,1,3), Date(2027,1,3);
  
  julia> swap1 = Swap(0.05, xp, r,inception=inc);
  
  julia> swap2 = Swap(0.05, Tenor("3y"), r, inc);
  
  julia> isequal(swap1.fixingsfix, swap2.fixingsfix)
  true
  
  julia> isequal(swap1.fixingsflt, swap2.fixingsflt)
  true
  ```
  """
  struct Swap <: Swaps{InterestRate}
    swaprate::R where R<:Real
    expiry::T where T<:TimePoint
    underlying::InterestRate
    inception::T where T<:TimePoint
    fixingsflt::AbstractVector{<:TimePoint}
    fixingsfix::AbstractVector{<:TimePoint}
    ispayer::Bool
    notional::V where V<:Real
    currencyflt::Currency
    currencyfix::Currency
    dccflt::DayCountConventions
    dccfix::DayCountConventions
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function Swap(
                  swaprate::R,
                  expiry::S,
                  underlying::InterestRate;
                  inception::S=dummyinception(expiry),
                  fixingsflt::AbstractVector{S}=begin
                    Colon()(
                            +(inception,underlying.tenor),
                            underlying.tenor.p,
                            expiry
                    )
                  end,
                  fixingsfix::AbstractVector{S}=fixingsflt,
                  ispayer::Bool=true,
                  notional::V=1.0,
                  currencyflt::Currency=getfield(underlying,:currency),
                  currencyfix::Currency=currencyflt,
                  dccflt::DayCountConventions=ActAct(),
                  dccfix::DayCountConventions=dccflt
             ) where {R<:Real,S<:TimePoint,V<:Real}
      if isless(expiry,inception)
        throw(ArgumentError("expiry[$(expiry)] < inception[$(inception)]"))
      end
      @assert inception <= minimum(fixingsflt) <= maximum(fixingsflt) <= expiry
      @assert inception <= minimum(fixingsfix) <= maximum(fixingsfix) <= expiry
      new(
          swaprate, expiry, underlying, inception, fixingsflt, fixingsfix,
          ispayer, notional, currencyflt, currencyfix, dccflt,dccfix,Real[],
          TimePoint[], Dict{AbstractString,Any}(
                                                "ticker" => dummyticker(),
                                                "μ" => 0,
                                                "σ" => 0,
                                                "carryrate" => 0
                       )
      )
    end
    
    function Swap(
                  swaprate::R,
                  tenor::Tenor,
                  underlying::InterestRate,
                  inception::S;
                  fixingsflt::AbstractVector{S}=begin
                    Colon()(
                            +(inception,underlying.tenor),
                            underlying.tenor.p,
                            +(inception,tenor)
                    )
                  end,
                  fixingsfix::AbstractVector{S}=fixingsflt,
                  ispayer::Bool=true,
                  notional::V=1.0,
                  currencyflt::Currency=underlying.currency,
                  currencyfix::Currency=currencyflt,
                  dccflt::DayCountConventions=ActAct(),
                  dccfix::DayCountConventions=dccflt
             ) where {R<:Real,S<:TimePoint,V<:Real}
      Swap(
           swaprate, +(inception,tenor), underlying, inception=inception,
           fixingsflt=fixingsflt, fixingsfix=fixingsfix,
           ispayer=ispayer, notional=notional, currencyflt=currencyflt,
           currencyfix=currencyfix, dccflt=dccflt, dccfix=dccfix
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", swp::Swap)
    print(
          io, "Swap[",swp.swaprate,", ",swp.expiry,
          "](",swp.params["ticker"],")"
    )
  end

  function payoff(swap::Swap, p::R, t::TimePoint) where R<:Real
    i = first(indexin([t],swap.fixingsfix))
    j = first(indexin([t],swap.fixingsflt))
    τflt = if isnothing(j)
      0
    else
      yearfraction(
                   swap.dccflt,
                   isone(j) ? swap.inception : getindex(swap.fixingsflt,j-1),
                   t
      )
    end
    τfix = if isnothing(i)
      0
    else
      yearfraction(
                   swap.dccfix,
                   isone(i) ? swap.inception : getindex(swap.fixingsflt,i-1),
                   t
      )
    end
    -(τflt*p, τfix*swap.swaprate) * swap.notional * (swap.ispayer ? 1 : -1)
  end
  
  function payoff(swap::Swap, ::R, t::TimePoint, ::FixedLeg) where R<:Real
    i = first(indexin([t],swap.fixingsfix))
    τfix = if isnothing(i)
      0
    else
      yearfraction(
                   swap.dccfix,
                   isone(i) ? swap.inception : getindex(swap.fixingsfix,i-1),
                   t
      )
    end
    -τfix * swap.swaprate * swap.notional * (swap.ispayer ? 1 : -1)
  end
  
  function payoff(swap::Swap, ::FixedLeg)
    payoff(swap,0,last(swap.underlying.times),FixedLeg())
  end
  
  function payoff(swap::Swap, p::R, t::TimePoint, ::FloatingLeg) where R<:Real
    i = first(indexin([t],swap.fixingsflt))
    τflt = if isnothing(i)
      0
    else
      yearfraction(
                   swap.dccflt,
                   isone(i) ? swap.inception : getindex(swap.fixingsflt,i-1),
                   t
      )
    end
    -τflt * p * swap.notional * (swap.ispayer ? 1 : -1)
  end
  
  function payoff(swap::Swap, ::FloatingLeg)
    payoff(swap,S(swap.underlying)(),last(swap.underlying.times),FloatingLeg())
  end
  
  function payoff(swap::Swap)
    payoff(swap, S(swap.underlying)(), last(swap.underlying.times))
  end
end