# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Bond <: Debt
  
  An implementation of bonds.
  
  # Fields
  - `maturity::TimePoint`: bond's maturity.
  - `coupon::InterestRate`: bond's coupon (interest) rate.
  - `notional::Real`: notional tied to the bond.
  - `inception::TimePoint`: inception date/time point of the bond.
  - `currency::Currency`: bond's currency.
  - `dcc::DayCountConventions`: the day count convention.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      Bond(maturity, coupon::InterestRate; <kwargs>)
      Bond(maturity, coupon::Real, freq; <kwargs>)
  
  See also [`ZeroBond`](@ref).
  
  ## Arguments
  - `maturity::TimePoint`: bond's maturity.
  - `coupon::Union{Real,InterestRate}`: coupon rate of the bond.
  - `freq::Union{Period,Int}`: coupon frequency (per year, when `Real`).
  - `notional::Real=one(Real)`: kwarg, bond's notional.
  - `inception::TimePoint=dummyinception(maturity)`: kwarg, bond inception.
  - `currency::Currency=Currency()`: kwarg, underlying currency.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  `Bond` instances have into their *casual parameters container* a field
  `"coupon_dates"` intended to hold coupon payment dates.
  """
  struct Bond <: Debt
    maturity::T where T<:TimePoint
    coupon::InterestRate
    notional::R where R<:Real
    inception::T where T<:TimePoint
    currency::Currency
    dcc::DayCountConventions
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}

    function Bond(
                  maturity::Q,
                  coupon::InterestRate;
                  inception::Q=dummyinception(maturity),
                  notional::R=one(Real),
                  currency::Currency=Currency(),
                  dcc::DayCountConventions=ActAct()
             ) where {Q<:TimePoint,R<:Real}
      @assert <=(inception,maturity)
      new(
          maturity, coupon, notional,
          inception, currency, dcc,
          Real[], TimePoint[],
          Dict{AbstractString,Any}(
                                   "μ" => 0,
                                   "σ" => 0,
                                   "ticker" => dummyticker(),
                                   "coupon_dates" => StepRange(
                                                     +(inception,coupon.tenor),
                                                     coupon.tenor.p,
                                                     maturity
                                                     )
          )
      )
    end
    
    function Bond(
                  maturity::Q,
                  coupon::R,
                  freq::Period;
                  inception::Q=dummyinception(maturity),
                  notional::P=1,
                  currency::Currency=Currency(),
                  dcc::DayCountConventions=ActAct()
             ) where {Q<:TimePoint,R<:Real,P<:Real}
      Bond(
           maturity,
           InterestRate(Tenor(freq), coupon, currency=currency),
           inception=inception,
           notional=notional,
           currency=currency,
           dcc=dcc
      )
    end
    
    function Bond(
                  maturity::Q,
                  coupon::R,
                  freq::Int;
                  inception::Q=dummyinception(maturity),
                  notional::P=one(Real),
                  currency::Currency=Currency(),
                  dcc::DayCountConventions=ActAct()
             ) where {Q<:TimePoint,R<:Real,P<:Real}
      if freq < 1
        throw(ArgumentError("coupon frequency should be a positive integer"))
      end
      _p = if in(freq, 1:6) || in(freq, 12:12)
        Month(12/freq)
      elseif in(freq, (13,26,52))
        Week(52/freq)
      else
        throw(ArgumentError("Unusual frequency: construct it outside..."))
      end
      Bond(
           maturity, coupon, _p, inception=inception,
           notional=notional,currency=currency,dcc=dcc
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", bnd::Bond)
    print(
          io,
          "Bond[", getfield(bnd, :maturity), ", ", getfield(bnd, :coupon),
          ", ", getfield(bnd, :notional), ", ", getfield(bnd, :currency),")"
    )
  end
  

  """
      payoff(bond::Bond, t::TimePoint)
  
  Return the payoff of `bond` at `t`.
  
  # Arguments
  - `bond::Bond`: `Bond`'s instance.
  - `t::TimePoint`: payoff at `t`.
  """
  function payoff(bond::Bond, t::T) where T<:TimePoint
    *(
      in(t, getparams(bond,"coupon_dates")),
      S(getfield(bond,:coupon))(t),
      getfield(bond, :notional),
      yearfraction(getfield(bond,:dcc), t-getfield(bond,:coupon).tenor, t)
    ) + *(isequal(t,getfield(bond,:maturity)),getfield(bond,:notional))
  end
end