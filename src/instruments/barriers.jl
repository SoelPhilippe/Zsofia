# This script is part of Zsofia. Soel Philippe © 2025

#> tools
begin
  """
      BarrierType
  
  An `Enum` type representing the kind of `BarrierOption` flavor.  
  
  # Flags
  |**flag**|**description**|
  |:---:|:---:|
  |`UpIn`|activated when barrier is hit from below, wrt to inception levels|
  |`UpOut`|deactivated when barrier is hit from below, wrt to inception levels|
  |`DownIn`|activated when barrier is hit from above, wrt to inception levels|
  |`DownOut`|deactivated when barrier is hit from above, wrt to inception levels|
  """
  @enum BarrierType begin
    UpIn = 1
    UpOut
    DownIn
    DownOut
  end
  
  """
      dummybarrier(bt::BarrierType)
  
  Return a tuple `(a, b)` where `a` is the corresponding dummy barrier level
  and `b` the correponding barrier type. This provide a facility to use
  smoothly the double barrier interface for building single barriers options.
  
  # Arguments
  - `barriertype::BarrierType`: either `UpIn`, `UpOut`, `DownIn` or `DownOut`.
  """
  function dummybarrier(barriertype::BarrierType)::Tuple{Real,BarrierType}
    if isequal(barriertype, UpIn)
      (-Inf, UpIn)
    elseif isequal(barriertype, UpOut)
      (Inf, UpOut)
    elseif isequal(barriertype, DownIn)
      (Inf, DownIn)
    else
      (-Inf, DownOut)
    end
  end
end


begin
  """
      BarrierOption <: Options
  
  # Fields
  - `strike::Real`: strike price. 
  - `expiry::TimePoint`: expiry date.
  - `barrier::Real`: level of the barrier.
  - `barriertype::BarrierType`: type of the barrier (tied to `barrier`).
  - `option::OptionColor`: the `OptionColor` (`Call`/`Put`).
  - `underlying::Observables`: underlying financial instrument.
  - `inception::TimePoint`: inception of the contract.
  - `barrier2::Union{Nothing, Real}`: level of the second barrier.
  - `barrier2type::Union{Nothing, BarrierType}`:(tied to `barrier2̀ ). 
  - `exestyle::ExerciseStyle`: exercise style.
  - `prices::AbstractVector{Real}`: observed prices container.
  - `times::AbstractVector{<:TimePoint}`: observation times.
  - `params::Dict{AbstractString, Any}`: casual parameters container.
  
  # Constructors
      BarrierOption(strike, expiry, barrier; <kwarg>)
  
  ## Arguments
  - `strike::Real`: strike price. 
  - `expiry::TimePoint`: expiry date.
  - `barrier::Real`: level of the barrier.
  - `barriertype::BarrierType=UpIn`: kwarg, tied to `barrier`.
  - `option::OptionColor=Call`: kwarg, the `OptionColor` (`Call`/`Put`).
  - `underlying::Observables=DummyObs()`: underlying financial instrument.
  - `inception::TimePoint=dummyinception(expiry)`: contract inception.
  - `barrier2::Real=dummybarrier(barriertype)`: second barrier.
  - `barrier2type::BarrierType=dummybarrier(barriertype)[2]`: `barrier2` type. 
  - `exestyle::ExerciseStyle=EuropeanStyle`: exercise style.
  """
  struct BarrierOption{U} <: Options{U}
    strike::Q where Q<:Real
    expiry::T where T<:TimePoint
    barrier::R where R<:Real
    barriertype::BarrierType
    option::OptionColor
    underlying::U where U<:Observables
    inception::T where T<:TimePoint
    barrier2::S where S<:Real
    barrier2type::BarrierType
    exestyle::ExerciseStyle
    prices::AbstractVector{V} where V<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString, Any}
    
    function BarrierOption(
                        strike::Q,
                        expiry::W,
                        barrier::R;
                        barriertype::BarrierType=UpIn,
                        option::OptionColor=Call,
                        underlying::U=DummyObs(),
                        inception::W=dummyinception(expiry),
                        barrier2::Y=dummybarrier(barriertype)[1],
                        barrier2type::BarrierType=dummybarrier(barriertype)[2],
                        exestyle::ExerciseStyle=EuropeanStyle
      ) where Q<:Real where W<:TimePoint where R<:Real where Y<:Real where U
      if expiry < inception
        throw(ArgumentError("inception > expiry"))
      end
      new{U}(
             strike, expiry, barrier, barriertype, option, underlying,
             inception, barrier2, barrier2type, exestyle, Real[],
             TimePoint[], Dict{AbstractString,Any}(
                                           "ticker" => dummyticker(),
                                           "μ" => 0,
                                           "σ" => 0,
                                           "carryrate" => 0
                          )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", op::BarrierOption)
    print(
          io, "BarrierOption[",
          op.strike,", ", (op.barrier, op.barrier2), ", ",
          (op.barriertype, op.barrier2type), "]"
    )
  end
  
  """
      knockedin(barriertype, barrier, lvls)
  
  Return `true` if the barrier product knocked-in wrt the levels (`lvls`).
  
  # Arguments
  - `barriertype::BarrierType`: the type of the barrier.
  - `barrier::Real`: barrier level.
  - `lvls::AbstractVector{Real}`: observed levels.
  """
  function knockedin(
                     barriertype::BarrierType,
                     barrier::R,
                     lvls::AbstractVector{S}
           )::Bool where R<:Real where S<:Real
    if isequal(barriertype, DownOut)
      minimum(lvls) > barrier
    elseif isequal(barriertype, DownIn)
      minimum(lvls) <= barrier
    elseif isequal(barriertype, UpIn)
      maximum(lvls) >= barrier
    elseif isequal(barriertype, UpOut)
      maximum(lvls) < barrier
    end
  end
  
  """
      ispayoffable(op::BarrierOption, lvls::AbstractVector{Real})

  Return `true` if the option knocked-in w.r.t. levels, `false` otherwise.
  """
  function ispayoffable(
	                      op::BarrierOption,
	                      lvls::AbstractVector{R}
           ) where {R<:Real}
    knockedin(op.barriertype, op.barrier, lvls) &&
	  knockedin(op.barrier2type, op.barrier2, lvls)
  end

  function ispayoffable(op::BarrierOption, lvls::R) where R<:Real
    ispayoffable(op, [lvls])
  end
  
  function payoff(
	                op::BarrierOption,
	                p::R=S(op.underlying)(),
	                t::TimePoint=last(op.underlying.times),
	                prices::AbstractVector{S}=op.underlying.prices,
	                times::AbstractVector{T}=op.underlying.times
           ) where {R<:Real,S<:Real,T<:TimePoint}
    if ~isequal(length(times), length(prices))
      n, m = length(times), length(prices)
      throw(DimensionMismatch("$(n)=dim(prices) ≠ dim(times)=$(m)"))
    end
    @assert issorted(times)
    i = searchsortedfirst(times, getfield(op, :inception))
    j = searchsortedlast(times, getfield(op, :expiry))
    if (
        isequal(getfield(op, :exestyle), EuropeanStyle) &&
        ~isequal(getfield(op, :expiry), t)
       ) ||
       (
        isequal(getfield(op, :exestyle), AmericanStyle) &&
        ~(getfield(op, :inception) <= t <= getfield(op, :expiry))
       ) ||
       isempty(i:j)
      return zero(Real)
    end
    max(
        *(
          ifelse(ispayoffable(op,view(prices,i:j)), one(Real), zero(Real)),
          ifelse(isequal(getfield(op,:option),Put), -one(Real), one(Real)),
          p - getfield(op, :strike)
        ),
        zero(Real)
    )
  end
end