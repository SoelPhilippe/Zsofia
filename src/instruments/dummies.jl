# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      DummyObs <: Observables
  
  Singleton type serving as dummy observables, intended to serve as placeholder
  whenever an `Observable` is specified.
  
  # Examples
  ```julia
  julia> DummyObs() isa Observables
  true
  ```
  """
  struct DummyObs <: Observables end
  
  function Base.show(io::IO, ::MIME"text/plain", ::DummyObs)
    print(io,"DummyObs")
  end
  
  """
      dummyticker()
  
  Return a `String` value hexadecimal characters inferred from the current
  UNIX time.
  
  # Notes
  This function is used to generate a dummy ticker symbol whenever needed.
  """
  dummyticker() = string(hash(time()), base=16)
  
  """
      payoff(der::Derivatives{DummyObs},...)
  
  Invariably return `0`.
  
  # Notes
  We implemented a dummy observable to play the role of place holder
  whenever/wherever required.
  
  See also [`DummyObs`](@ref).
  """
  payoff(::U,xs...) where U<:Derivatives{DummyObs} = 0
  
  """
      dummyinception(t::TimePoint)
  
  Return a dummy date given the `t`.
  
  As a rule of thumb:
  + `t::Date => firstdayofyear(t-Day(1))`
  + `t::Real => 0`
  + `t::DateTime => t-Day(7)`
  """
  dummyinception(t::A) where A<:Real = zero(A)
  
  dummyinception(t::Date) = firstdayofyear(t - Day(1))  
  
  dummyinception(t::DateTime) = -(t, Week(1))
  
  dummyinception(t::Time) = Time(0)
  
  dummyinception(t::TimeType) = -(t, firstdayofyear(t-Day(1)))
  
  hasparams(::DummyObs, ::AbstractString) = false
  
  settimes!(::DummyObs, ::AbstractVector{Q}) where Q<:TimePoint = nothing
  
  resettimes!(::DummyObs, ::AbstractVector{Q}) where Q<:TimePoint = nothing
  
  function setprices!(
                      ::DummyObs,
                      ::AbstractVector{R},
                      ::AbstractVector{Q}
           ) where {R<:Real,Q<:TimePoint}
    nothing
  end
  
  function resetprices!(
                        ::DummyObs,
                        ::AbstractVector{R},
                        ::AbstractVector{Q}
           ) where {R<:Real,Q<:TimePoint}
    nothing
  end
  
  function setparams!(
                      ::DummyObs,
                      ::AbstractString,
                      ::Any;
                      force::Bool=false
           )
    nothing
  end
  
  function setparams!(
                      ::DummyObs,
                      ::Dict{AbstractString,Any};
                      force::Bool=false
           )
    nothing
  end
  
  function getparams(::DummyObs, tag::AbstractString)
    lowercase(tag) == "ticker" ? "" : nothing
  end
  
  getparams(::DummyObs, ::AbstractString, default::Any) = default
  
  function getparams!(::DummyObs, ::AbstractString, default::Any)
    @warn "not applicable for DummyObs"
    default
  end
  
  function insertprices!(
                         ::DummyObs,
                         price::R,
                         t::T
           ) where {R<:Real,T<:TimePoint}
    @warn "not applicable for DummyObs"
    (t, price)
  end
  
  function insertprices!(
                         ::DummyObs,
                         prices::AbstractVector{R},
                         times::AbstractVector{T}
           ) where {R<:Real,T<:TimePoint}
    @warn "not applicable for DummyObs"
    times, prices
  end
  
  Base.isempty(::DummyObs) = true
  
  Base.isempty(::DummyObs, ::Symbol) = true
  
  function Base.popat!(
                       ::DummyObs,
                       ::T,
                       default::Tuple{R,T}
                ) where {R<:Real,T<:TimePoint}
    @warn "not applicable to DummyObs"
    default
  end
  
  function Base.popat!(::DummyObs, ::T) where T<:TimePoint
    @warn "not applicable to DummyObs"
    nothing
  end
  
  function Base.deleteat!(::DummyObs, ::T) where T<:TimePoint
    @warn "not applicable to DummyObs"
    nothing
  end
  
  function Base.deleteat!(::DummyObs, ::AbstractVector{T}) where T<:TimePoint
    @warn "not applicable to DummyObs"
    nothing
  end
  
  function Base.deleteat!(::DummyObs, ::Tuple{Vararg{T}}) where T<:TimePoint
    @warn "not applicable to DummyObs"
    nothing
  end
  
  function S(::DummyObs)
    _f(::TimePoint) = 0
  end
end