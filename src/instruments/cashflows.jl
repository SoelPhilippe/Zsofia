# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      cashflows(obs::Observables)
  
  Return the stream of `Cashflow`s inherent to `obs`.
  
  # Arguments
  - `obs::Observables`: the observable cashflows are required of.
  
  # Notes
  An `ErrorException` is thrown whenever the method is not implemented for
  required `Observables`.
  
  # Examples
  ```julia
  julia> cflw = Cashflow(10_000, Date(2025,2,16), Currency("USD"));
  
  julia> cashflows(cflw)
  1-element Vector{Cashflow}:
   Cashflow(10000,2025-02-16,USD)
  ```
  """
  function cashflows(obs::Observables,y...)
    throw("Type $(typeof(obs)) is not supported.")
  end
  
  cashflows(cflw::Cashflow) = [cflw]
  
  cashflows(cflws::AbstractVector{Cashflow}) = collect(cflws)
  
  function cashflows(bond::Bond)
    collect(
            Cashflow(
                     payoff(bond,t),
                     t,
                     getfield(bond,:currency)
            ) for t in getparams(bond,"coupon_dates")
    )
  end
  
  function cashflows(zcb::ZeroBond)
     cashflows(
               Cashflow(
                        getfield(zcb,:notional),
                        getfield(zcb,:maturity),
                        getfield(zcb,:currency)
               )
     )         
  end
end