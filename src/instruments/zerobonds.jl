# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      ZeroBond <: Debt
  
  An implementation of zero(-coupon-)bond financial instruments.
  
  # Fields
  - `maturity::TimePoint`: zero bond's maturity.
  - `notional::`: notional tied to the contract.
  - `inception::TimePoint`: inception date of the contract.
  - `currency::Currency`: underlying currency.
  - `dcc::DayCountConventions`: the day count convention.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      ZeroBond(maturity::TimePoint; <kwargs>)
      ZeroBond(tenor::Tenor, inception::TimePoint; <kwargs>)
  
  See also [`Bond`](@ref).
  
  ## Arguments
  - `maturity::TimePoint`: maturity of the zero-coupon-bond
  - `tenor::Tenor`: representing the lenght of the contract.
  - `inception::TimePoint`: inception date (mandatory when using method 2).
  - `notional::Real=1`: kwarg, notional/principal amount.
  - `currency::Currency=Currency()`: kwarg, contract's underlying currency.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  When using the second method, `inception` argument should be provided. When
  the first method is used, the default inception is `dummyinception(maturity)`.
  """
  struct ZeroBond <: Debt
    maturity::T where T<:TimePoint
    notional::R where R<:Real
    inception::T where T<:TimePoint
    currency::Currency
    dcc::DayCountConventions
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function ZeroBond(
                      maturity::A;
                      inception::A=dummyinception(maturity),
                      notional::B=1,
                      currency::Currency=Currency(),
                      dcc::DayCountConventions=ActAct()
             ) where {A<:TimePoint,B<:Real}
      @assert <=(inception,maturity)
      new(
          maturity, notional, inception, currency, dcc,
          Real[], TimePoint[],
          Dict{AbstractString,Any}(
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0,
                                   "ticker" => dummyticker()
           )
      )
    end
    
    function ZeroBond(
                      tenor::Tenor,
                      inception::A;
                      notional::B=1,
                      currency::Currency=Currency(),
                      dcc::DayCountConventions=ActAct()
             ) where {A<:TimePoint,B<:Real}
      ZeroBond(
               +(inception,tenor),
               inception=inception,
               notional=notional,
               currency=currency,
               dcc=dcc
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", zcb::ZeroBond)
    print(
          io, "ZeroBond[",getfield(zcb,:inception),", ",
          getfield(zcb,:maturity),"](",getparams(zcb,"ticker"),")"
    )
  end
  
  function payoff(zcb::ZeroBond, t::TimePoint)
    isequal(t,getfield(zcb,:maturity)) * getfield(zcb,:notional)
  end
end