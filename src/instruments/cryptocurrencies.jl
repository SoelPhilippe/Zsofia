# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      CryptoCurrency <: Commodities
  
  Crypto currency instrument type.

  # Fields
  - `quoteccy::Currency`: quote currency.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.

  See also [`Stock`](@ref), [`Commodity`](@ref).

  # Constructors
      CryptoCurrency([ticker::String],[quoteccy::Currency])
  
  ## Arguments
  - `ticker::AbstractString=dummyticker()`: ticker symbol.
  - `quoteccy::Currency=Currency()`: quote currency.
  """
  struct CryptoCurrency <: Commodities
    quoteccy::Union{CryptoCurrency,Currency}
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function CryptoCurrency(
                            ticker::AbstractString=dummyticker(),
                            quoteccy::Union{Currency,CryptoCurrency}=Currency()
             )
      new(
          quoteccy,Real[],TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => ticker,
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0
          )
      )
    end
  end

  function Base.show(io::IO, ::MIME"text/plain", c::CryptoCurrency)
    print(
          io, "CryptoCurrency[", getparams(c,"μ"),", ",getparams(c,"σ"),
         "](",getparams(c,"ticker"),")"
    )
  end
end