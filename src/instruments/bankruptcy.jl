# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Bankruptcy <: CreditEvents
  
  A structure modeling the *bankruptcy* credit event.
  
  See also [`FailureToPay`](@ref), [`ObligationDefault`](@ref).
  
  # Fields
  - `prices::Vector{Bool}`: event observations container, `true` is occurrence.
  - `times::Vector{TimePoint}` : observation times container.
  - `params::Dict{AbstractString,Any}` parameters container.
  
  # Constructors
      Bankruptcy()
      
  ## Arguments
  None
  
  # Notes
  **Bankruptcy** is a credit event used in all credit default swaps
  where the reference entity is a corporate (bankruptcy does not apply
  to Sovereign names). Bankruptcy events include the reference entity
  dissolved, becoming insolvent, making an arrangement for the benefit of
  its creditors, being wound up or having a judgment of insolvency made against
  it.  
  Source: [ISDA](https://www.isda.org/1985/01/01/glossary/#b)
  
  # Examples
  ``̀`julia
  julia> bnk = Bankruptcy();
  
  julia> bnk isa Bankruptcy
  true
  
  julia> isa(bnk,Bankruptcy) && ( Bankruptcy <: CreditEvents )
  true
  ```
  """
  struct Bankruptcy <: CreditEvents
    prices::AbstractVector{Bool}
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    Bankruptcy() = new(
                       Bool[],
                       TimePoint[],
                       Dict{AbstractString,Any}()
                   )
  end
  
  function show(io::IO, ::MIME"text/plain", bankruptcy::Bankruptcy)
    print(
          io, "BankruptcyEvent[",
          ifelse(any(bankruptcy.prices),Char(0x2714),Char(0x2718)),"]"
    )
  end
end