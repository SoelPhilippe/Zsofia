# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      SpreadOption <: Options
  
  Spread options implementation.
  
  # Fields
  - `expiry::TimePoint`: option's expiry.
  - `option::OptionColor`: either `Call` or `Put`.
  - `underlying1::Observables`: first underlying.
  - `underlying2::Ovservables`: second underlying.
  - `inception::TimePoint`: inception date of the contract.
  - `prices::Vector{Real}`: prices container.
  - `times::Vector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      SpreadOption(underlying1, underlying2, expiry; <kwargs>)
  
  See also [`AmericanOption`](@ref), [`BarrierOption`](@ref).
  
  ## Arguments
  - `underlying1::Observables`: first underlying.
  - `underlying2::Observables`: second underlying.
  - `expiry::TimePoint`: the expiry time point of the contract.
  - `option::OptionColor=Call`: kwarg, either ̀ Put` or `Call`.
  - `inception::TimePoint=dummyinception(expiry)`: kwarg, inception time point.
  
  # Notes
  Spread options are type of options having payoffs based on the difference
  in price between two underlying risk factors.
  """
  struct SpreadOption{U,V} <: Options{Union{U,V}}
    expiry::B where B<:TimePoint
    option::OptionColor
    underlying1::U where U<:Observables
    underlying2::V where V<:Observables
    inception::C where C<:TimePoint
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function SpreadOption(
                          expiry::Q,
                          underlying1::U,
                          underlying2::V;
                          option::OptionColor=Call,
                          inception::Q=dummyinception(expiry)
             ) where {Q<:TimePoint,U<:Observables,V<:Observables}
      @assert isless(inception,expiry)
      new{U,V}(
               expiry, option, underlying1, underlying2,
               inception, Real[], TimePoint[],
               Dict{AbstractString,Any}(
                                        "ticker" => dummyticker(),
                                        "μ" => 0,
                                        "σ" => 0,
                                        "carryrate" => 0
               )
      )
    end
  end
  
  function Base.show(
                     io::IO,
                     ::MIME"text/plain",
                     op::SpreadOption{U,V}
                ) where {U<:Observables,V<:Observables}
    print(
          io, "SpreadOption{",U,",",V,"}[",getfield(op,:expiry),", ",
          ifelse(getfield(op,:option) == Call,"Call","Put"),"](",
          getparams(op,"ticker"),")"
    )
  end
  
  """
      payoff(op::SpreadOption, p::Real, q::Real, t::TimePoint)
      payoff(op::SpreadOption)
  
  Return the payoff of the spread option `op` given the risk factors being
  respectively at levels `p` and `q`, observed at time `t`.
  
  # Arguments
  - `op::SpreadOption`: option the payoff is required of.
  - `p::Real`: underlying first risk factor level.
  - `q::Real`: underlying second risk factor level.
  - `t::TimePoint`: observation time.
  
  # Notes
  A method `payoff(op::SpreadOption)` is provided. In that case, the latest
  observations are assumed.
  """
  function payoff(
                  op::SpreadOption,
                  p::P,
                  q::Q,
                  t::T
           ) where {P<:Real,Q<:Real,T<:TimePoint}
    isequal(t, getfield(op,:expiry)) *
    max(
        *(
          -(2isequal(getfield(op, :option),Call), 1),
          -(p, q)
        ),
        0
    )
  end
  
  function payoff(op::SpreadOption, t::TimePoint)
    payoff(
           op,
           S(getfield(op,:underlying1))(t),
           S(getfield(op,:underlying2))(t),
           t
    )
  end
  
  function payoff(op::SpreadOption)
    t = min(
            last(getfield(getfield(op,:underlying1),:times)),
            last(getfield(getfield(op,:underlying2),:times))
        )
    payoff(
           op,
           S(getfield(op,:underlying1))(t),
           S(getfield(op,:underlying2))(t),
           t
    )
  end
end