# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      ChooserOption <: Options
  
  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry of the to-be-chosen option.
  - `decisiontime::TimePoint`: decision time.
  - `underlying::Observables`: underlying observable.
  - `inception::TimePoint`: inception date of the contract.
  - `prices::AbstractVector{Real}`: observed prices container.
  - `times::AbstractVector{<:TimePoint}`: observation times container.
  - `params::Dict{AsbtractString, Any}`: casual parameters container.
  
  # Constructors
      ChooserOption(strike, expiry, decisiontime::TimePoint; <kwargs>)
  
  ## Arguments
  - `strike::Real`: strike price of the to-be-chosen option's underlying.
  - `expiry::TimePoint`: expiry of the to-be-chosen option.
  - `decisiontime::TimePoint`: decision time.
  - `underlying::Observables`: underlying instrument of the to-be-chosen option.
  - `inception::TimePoint`: kwarg, inception time point of the contract.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: observations times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.

  # Notes
  A **chooser option** is a special type of option contract. It gives the
  purchaser a fixed period to decide whether the derivative will be a
  european option call or put.

  For stocks without dividend in particular, the chooser can be replicated
  using one call option with strike price `strike` and expiration time
  `expiry` and one put option with strike price
  `strike * exp(-r * (expiry - decisiontime))` and expiration time
  `decisiontime`.
  
  # Lil Tip
  The current implementation expects the user to store the color decision,
  whether the choice has been a `Call` or a `Put` at `decisiontime` into
  the casual parameter container using the `setparams!` facility.
  """
  struct ChooserOption{U} <: Options{U}
    strike::A where A<:Real
    expiry::B where B<:TimePoint
    decisiontime::B where B<:TimePoint
    underlying::U where U<:Observables
    inception::B where B<:TimePoint
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}

    function ChooserOption(
                           strike::A,
                           expiry::B,
                           decisiontime::B,
                           underlying::U;
                           inception::B=dummyinception(expiry)
             ) where {A<:Real,B<:TimePoint,U<:Observables}
      if isless(expiry, inception)
        throw(ArgumentError("expiry($(expiry)) < inception($(inception))"))
      end
      new{U}(
             strike, expiry, decisiontime, underlying,
             inception, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
  end

  function Base.show(io::IO, ::MIME"text/plain", op::ChooserOption)
    print(
          io,
          "BinaryOption[",op.strike,", ",op.expiry,"](",
          getparams(op,"ticker"),")"
    )
  end

  """
      payoff(op::ChooserOption,[s::Real],[t::TimePoint],[decision::OptionColor])
  
  Return the payoff of the chooser option `op`.

  # Arguments
  - `op::ChooserOption`: the underlying option.
  - `s::Real`: price level.
  - `t::TimePoint`: time at which payoff is required.
  - `decision::OptionColor`: `Call` or `Put`.
  """
  function payoff(
                  op::ChooserOption,
                  s::R,
                  t::TimePoint;
                  decision::OptionColor=Call
           ) where R<:Real
    max(
        *(
          isequal(t,op.expiry),
          ifelse(isequal(decision,Call),1,-1),
          s - getfield(op,:strike)
        ),
        0
    )
  end
end