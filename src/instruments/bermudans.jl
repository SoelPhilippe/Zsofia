# This script is part of Zsofia. Soel Philippe © 2025

begin
   """
       BermudanOption <: Options
   
   Bermuda option, an option exercizable at specified time, points.

   # Fields
   - `strike::Real`: strike price.
   - `expiry::TimePoint`: expiry date.
   - `exetimes::Vector{TimePoint}`: times at which option is exercizable.
   - `option::OptionColor`: either `Call` or `Put`.
   - `underlying::Observables`: underlying option's risk factor.
   - `inception::TimePoint`: time point at which the option starts.
   - `prices:Vector{Real}`: option's prices container.
   - `times::Vector{TimePoint}`: option's times container.
   - `params::Dict{AbstractString,Any}`: casual parameters container.
   
   # Constructors
       BermudanOption(strike, expiry, exetimes; <kwargs>)
   
   See also [`AmericanOption`](@ref), [`EuropeanOption`](@ref).
   
   ## Arguments
   - `strike::Real`: option's strike (price) level.
   - `expiry::TimePoint`: contract's expiry time point.
   - `exetimes::Vector{TimePoint}`: times at which option is exercizable.
   - `option::OptionColor=Call`: either `Call` or `Put`.
   - `underlying::Observables=DummyObs()`: kwarg, underlying of the contract.
   - `inception::TimePoint=dummyinception(expiry)`: kwarg, starting point.

   # Notes
   The uniqueness of `exetimes` is not checked anymore. It's left to the
   user's discretion to insure it's uniqueness.
   """
   struct BermudanOption{U} <: Options{U}
     strike::R where R<:Real
     expiry::T where T<:TimePoint
     exetimes::AbstractVector{<:TimePoint}
     option::OptionColor
     underlying::U where U<:Observables
     inception::T where T<:TimePoint
     prices::AbstractVector{Q} where Q<:Real
     times::AbstractVector{<:TimePoint}
     params::Dict{AbstractString,Any}
     
     function BermudanOption(
                             strike::R,
                             expiry::Q,
                             exetimes::Union{AbstractVector{Q},Q};
                             option::OptionColor=Call,
                             underlying::U=DummyObs(),
                             inception::Q=dummyinception(expiry)
              ) where {Q<:TimePoint,R<:Real,U<:Observables}
       @assert <=(inception,expiry) "inception($inception) > expiry($(expiry))"
       @assert inception <= minimum(exetimes) <= maximum(exetimes) <= expiry
       new{U}(
              strike, expiry, exetimes, option, underlying, inception,
              Real[], TimePoint[], Dict{AbstractString,Any}(
                                                    "ticker" => dummyticker(),
                                                    "μ" => 0,
                                                    "σ" => 0,
                                                    "carryrate" => 0
                                   )
       )
     end
     
     function BermudanOption(
                             strike::R,
                             expiry::Q,
                             exetime::Q;
                             option::OptionColor=Call,
                             underlying::U=DummyObs(),
                             inception::Q=dummyinception(expiry)
              ) where {Q<:TimePoint,R<:Real,U<:Observables}
       BermudanOption(
                      strike, expiry, [exetime], option=option,
                      underlying=underlying, inception=inception
       )
     end
   end
 
   function Base.show(io::IO, ::MIME"text/plain", op::BermudanOption)
     print(
           io, "BermudanOption[",getfield(op,:strike),", ",
           getfield(op,:expiry),"](", getparams(op,"ticker"),")"
     )
   end
   
   function payoff(
                    op::BermudanOption{U},
                    p::R=S(op.underlying)(),
                    t::TimePoint=getfield(op,:inception)
            ) where {U<:Observables,R<:Real}
     max(
         *(
           ifelse(isequal(getfield(op,:option),Put),-1,1),
           p - getfield(op,:strike),
           in(t,getfield(op,:exetimes))
        ),
        0
     )
   end
 end