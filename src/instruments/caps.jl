# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Cap <: Options{InterestRates}
  
  A `Cap` is a financial instrument paying the positive exposure of a
  payer interest rate `Swap` at specified fixings time points.
  
  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry date.
  - `underlying::InterestRate`: underlying risk factor (an interest rate).
  - `inception::TimePoint`: starting point (in time) of the contract.
  - `fixings::AbstractVector{<:TimePoint}`: dates of fixings.
  - `notional::Real`: notional tied to the contract.
  - `currency::Currency`: payments are made in this currency.
  - `prices::AbstractVector{Real}`: observed prices container.
  - `dcc::DayCountConventions=ActAct()`: day count convention.
  - `times::AbstractVector{<:TimePoint}`: observation times container.
  - `params::Dict{String, Any}`: casual parameters container.
  
  See also [`Floor`](@ref).
  
  # Constructors
        Cap(strike::Real, expiry::TimePoint, underlying; <kwargs>)
    
  # Arguments
  - `strike::Real`: strike price/level.
  - `expiry::TimePoint`: expiry date.
  - `tenor::Tenor`: tenor (duration in time) of the contract.
  - `underlying::InterestRate`: underlying interest rate.
  - `inception::TimePoint=dummyinception(expiry)`: inception time point.
  - `fixings::AbstractVector{<:TimePoint}`: kwarg, fixings dates.
  - `notional::Real=one(Float64)`: kwarg, contract notional.
  - `currency::Currency=underlying.currency`: kwarg, contract's currency.
  - `dcc::DayCountConventions=ActAct()`: kwarg, day count convention.
  
  # Notes
  A `Cap` is a contract that can be viewed as a portfolio of *caplets*:
  
      ``\\text{Payoff} = \\sum_{i}\\{D(t,T_i)\\tau_i (r(T_{i-1},T_i)-K)^{+}\\}``
  
  In the current implementation, we embedded the definition of a *caplet* into
  the `Cap` framework. Indeed a caplet is just a `Cap` with only one
  fixing date.
  
  When the contruction is made by a `tenor`, this `tenor` represents the global
  time duration of the contract. For a contract incepted at `inception`,
  the expiry is then `inception+tenor`.
  """
  struct Cap <: Options{InterestRate}
    strike::R where R<:Real
    expiry::T where T<:TimePoint
    underlying::InterestRate
    inception::T where T<:TimePoint
    fixings::AbstractVector{<:TimePoint}
    notional::S where S<:Real
    currency::Currency
    dcc::DayCountConventions
    prices::AbstractVector{S} where S<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function Cap(
                 strike::R,
                 expiry::Q,
                 underlying::InterestRate,
                 inception::Q=dummyinception(expiry);
                 fixings::AbstractVector{V} where V<:Q = begin
                    Colon()(
                            +(inception,underlying.tenor),
                            underlying.tenor.p,
                            +(inception,tenor)
                    )
                 end,
                 notional::S=one(Real),
                 currency::Currency=underlying.currency,
                 dcc::DayCountConventions=ActAct()
             ) where {R<:Real,Q<:TimePoint,S<:Real}
      @assert isless(expiry,inception)
      @assert inception <= minimum(fixings) <= maximum(fixings) <= expiry
      new(
          strike, expiry, underlying, fixings, notional, inception,
          currency, dcc, Real[], TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => dummyticker(),
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0
          )
      )
    end
    
    function Cap(
                 strike::R,
                 tenor::Tenor,
                 underlying::InterestRate,
                 inception::TimePoint;
                 fixings::AbstractVector{V} where V<:Q = begin
                    Colon()(
                            +(inception,underlying.tenor),
                            underlying.tenor.p,
                            +(inception,tenor)
                    )
                 end,
                 notional::S=one(Real),
                 currency::Currency=underlying.currency,
                 dcc::DayCountConventions=ActAct()
             ) where {R<:Real,Q<:TimePoint,S<:Real}
      Cap(
          strike, +(inception, tenor), underlying, inception,
          fixings=fixings, notional=notional, currency=currency, dcc=dcc
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", cap::Cap)
    print(
          io, "Cap[", getfield(cap,:strike),", ", getparams(cap,:expiry),"](",
          getparams(cap,"ticker"),")"
    )
  end
  
  """
      payoff(cap::Cap, p::Real, t::TimePoint)
      payoff(cap::Cap)
  
  Return the payoff of the `Cap` contract `cap` given the risk factor being
  at level `p` for the time point `t`.
  
  # Arguments
  - `cap::Cap`: contract of which payoff is required.
  - `p::Real`: underlying risk factor level.
  - `t::TimePoint` the observation of the level `p` has been made at `t`.
  """
  function payoff(cap::Cap, p::R, t::T) where {R<:Real,T<:TimePoint}
    i = first(indexin([t],getfield(cap,:fixings)))
    τ = if isnothing(i)
      0
    else
      yearfraction(
                   cap.dcc,
                   isone(i) ? cap.inception : getindex(cap.fixings,i-1),
                   t
      )
    end
    max(p - cap.strike, 0) * τ * cap.notional
  end
  
  payoff(cap::Cap) = payoff(cap,S(cap.underlying)(),last(cap.underlying.times))
end