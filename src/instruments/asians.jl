# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      AsianOptionType
  
  An enumeration type for asian option types specification.
  
  # Values
  |Instance|Descr.|
  |:---:|:---:|
  |`FixedStrike`|the strike is known from inception|
  |`FloatingStrike`|struck at the average level|
  
  The `Fixedstrike` type instance represents options where the payoff
  at `expiry` is computed as the spread between a fixed strike and the
  averaged risk factor.
  
  The `FloatingStrike` type instance represents options where the payoff
  at `expiry` is computed as the spread between the terminal value of
  the option's underlying and the averaged price level during the
  life of the contract.
  
  # Notes
  An asian option pays off the same payoff as a vanilla european option;
  the difference being the risk factor which is, in the case of an asian
  option, either the averaged price levels of the underlying risk factor
  within the lifespan of the option or the terminal value.
  
  """
  @enum AsianOptionType begin
    FixedStrike = 1
    FloatingStrike
  end
  
  """
      AverageType
  
  An enumeration type for the average type.
  
  # Values
  |Instance|Descr.|
  |:---:|:---:|
  |`ArithmeticAverage`|arithmetic mean|
  |`GeometricAverage`|geometric mean|
  |`HarmonicAverage`|harmonic mean|
  
  # Examples
  ```julia
  julia> x = collect(1:3);
  
  julia> average(x, ArithmeticAverage) == 2
  true
  
  julia> average(x, GeometricAverage) == cbrt(6)
  true
  
  julia> average(x, HarmonicAverage) == 18//11
  true
  ```
  """
  @enum AverageType begin
    ArithmeticAverage = 1
    GeometricAverage
    HarmonicAverage
  end
  
  """
      average(s::Vector{Real}, [avgtype::AverageType], [w::AbstractWeights])
  
  Return the (weighted) average of `s`, given the `avgtype`.
  
  # Arguments
  - `s::Vector{Real}`: a vector of numbers.
  - `atype::AverageType`: (`Arithmetic`|`Geometric`|`Harmonic`)Average.
  - `w::AbstractWeights`: weights, created with the specification `weights`
  
  See also [`AvgerageType`](@ref).
  """
  function average(
                   s::AbstractVector{R},
                   avgtype::AverageType=ArithmeticAverage,
                   w::AbstractWeights=weights(ones(length(s)))
           ) where R<:Real
    if isequal(avgtype, ArithmericAverage)
      mean(s, w)
    elseif isequal(avgtype, GeometricAverage)
      exp(mean(log.(s), w))
    elseif isequal(avgtype, HarmonicAverage)
      inv(mean(inv.(s), w))
    end
  end
  
  function average(s::R, ::AverageType, ::AbstractWeights) where R<:Real
    average([s])
  end
  
  """
      AsianOption <: Options
  
  Asian optons.
  
  # Fields
  - `strike::Real`: when relevant, the strike price of the option.
  - `expiry::TimePoint`: contract's expiry date.
  - `option::OptionColor`: either `Call` or `Put`.
  - `underlying::Observables`: underlying financial instrument.
  - `inception::TimePoint`: inception date of the option.
  - `striketype::AsianOptionType`: either `FloatingStrike` or `FixedStrike`
  - `avgtype::AverageType`: `ArithmeticAverage`, `GeometricAverage`...
  - `prices::Vector{Real}`: prices container.
  - `times::Vector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      AsianOption(strike::Real, expiry::TimePoint; <kwargs>)
      AsianOption(expiry::Real; <kwargs>)
  
  See also [`AmericanOption`](@ref).
  
  # Arguments
  - `strike::Real`: when relevant, option's strike price.
  - `expiry::TimePoint`: contract's expiry date.
  - `option::OptionColor=Call`: kwarg, either `Call` or `Put`.
  - `underlying::Observables=DummyObs()`: kwarg, underlying risk factor.
  - `inception::TimePoint=DymmyInception(expiry)`: kwarg, inception date.
  - `avgtype::AverageType=ArithmeticAverage`: averaging type.  
  
  # Notes
  In the second constructor, the `FloatingStrike` is assumed.
  """
  struct AsianOption{U} <: Options{U}
    strike::Q where Q<:Real
    expiry::T where T<:TimePoint
    option::OptionColor
    underlying::U where U<:Observables
    inception::T where T<:TimePoint
    striketype::AsianOptionType
    avgtype::AverageType
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function AsianOption(
                         strike::R,
                         expiry::Q;
                         option::OptionColor=Call,
                         underlying::U=DummyObs(),
                         inception::Q=dummyinception(expiry),
                         avgtype::AverageType=ArithmeticAverage
             ) where {R<:Real,Q<:TimePoint,U<:Observables}
      @assert isless(inception,expiry)
      new{U}(
             strike, expiry, option, underlying, inception, FixedStrike,
             avgtype, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
    
    function AsianOption(
                         expiry::Q;
                         option::OptionColor=Call,
                         underlying::U=DummyObs(),
                         inception::Q=dummyinception(expiry),
                         avgtype::AverageType=ArithmeticAverage
             ) where {U<:Observables,Q<:TimePoint}
      @assert isless(inception,expiry)
      new{U}(
             zero(Real), expiry, option, underlying, inception,
             FloatingStrike, avgtype, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
            )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", op::AsianOption)
    a = isequal(op.striketype,FixedStrike) ? op.strike : op.striketype
    print(
          io, "AsianOption[", a, ", ", op.expiry, ", ", op.avgtype,"](",
          getparams(op,"ticker"),")"
    )
  end
  
  """
      payoff(op::AsianOption, p::Real, t::TimePoint prices, times], weight)
  
  Return the payoff of the asian option `op` at time `t` given the underlying's
  price level is `p` (when relevant) when price path is `prices` observed at
  `times`.
  
  # Arguments
  - `op::AsianOption`: asian option.
  - `p::Real=S(op.underlying)()`: current underlying's price.
  - `t::TimePoint=last(op.underlying.times)`: current time point.
  - `prices::Vector{Real}=op.underlying.prices`: price path.
  - `times::Vector{TimePoint}=op.underlying.times`: observation times.
  - `weight::AbstractWeights=ones(length(prices))`: weights.
  
  # Notes
  It is **mandatory** that `times` is sorted.
  """
  function payoff(
                  op::AsianOption,
                  p::R,
                  t::T,
                  prices::AbstractVector{R},
                  times::AbstractVector{<:TimePoint},
                  weight::AbstractWeights
           ) where {R<:Real,T<:TimePoint}
    @assert isequal(length(prices),length(times))
    @assert isequal(length(weight),length(prices))
    if isequal(op.striketype,FixedStrike)
      throw("when arg `p` is given, the option should be FloatingStrike")
    end
    i = Colon()(
                searchsortedfirst(times,op.inception),
                searchsortedlast(times,t)
        )
    ω = (2isequal(op.option,Call)-1) * (op.inception <= t <= op.expiry)
    max(-(p,average(view(prices,i),op.avgtype,view(weight,i))) * ω, 0)
  end
  
  function payoff(
                  op::AsianOption,
                  p::R,
                  t::T,
                  prices::AbstractVector{B},
                  times::AbstractVector{<:TimePoint},
                  weight::AbstractVector{A}
           ) where {R<:Real,T<:TimePoint,B<:Real,A<:Real}
    payoff(op, p, t, prices, times, weigths(weight))
  end

  function payoff(
                  op::AsianOption,
                  p::R,
                  t::T,
                  prices::AbstractVector{A},
                  times::AbstractVector{<:TimePoint}
           ) where {R<:Real,T<:TimePoint,A<:Real}
    payoff(op,p,t,prices,times,ones(length(prices)))
  end
  
  function payoff(
                  op::AsianOption,
                  t::T,
                  prices::AbstractVector{R},
                  times::AbstractVector{<:TimePoint},
                  weight::AbstractWeights
           ) where {T<:TimePoint,R<:Real}
    @assert isequal(length(prices),length(times))
    @assert isequal(length(weight),length(prices))
    if isequal(op.striketype,FloatingStrike)
      throw("when arg `p` is not given, the option should be FixedStrike")
    end
    i = Colon()(
                searchsortedfirst(times,op.inception),
                searchsortedlast(times,t)
        )
    ω = (2isequal(op.option,Call)-1) * (op.inception <= t <= op.expiry)
    max(-(average(view(prices,i),op.avgtype,view(weight,i)),op.strike) * ω, 0)
  end

  function payoff(
                  op::AsianOption,
                  t::T,
                  prices::AbstractVector{R},
                  times::AbstractVector{<:TimePoint},
                  weight::AbstractVector{A}
           ) where {R<:Real,T<:TimePoint,A<:Real}
    payoff(op, t, prices, times, weigths(weight))
  end

  function payoff(
                  op::AsianOption,
                  t::T,
                  prices::AbstractVector{R},
                  times::AbstractVector{<:TimePoint}
           ) where {R<:Real,T<:TimePoint}
    payoff(op, t, prices, times, ones(length(prices)))
  end
end