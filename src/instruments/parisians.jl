# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      ParisianOption <: Options
  
  An implementation of a "Parisian" option.
  
  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry date.
  - `barrier::Real`: level of the barrier.
  - `barriertype::BarrierType`: type of the barrier (tied to `barrier`).
  - `triggerclock::Union{Period,Real}`: duration trigger of the option.
  - `option::OptionColor`: the `OptionColor` (`Call`/`Put`).
  - `underlying::Observables`: underlying financial instrument.
  - `inception::TimePoint`: inception of the contract.
  - `barrier2::Union{Nothing, Real}`: level of the second barrier.
  - `barrier2type::Union{Nothing, BarrierType}`:(tied to `barrier2̀ ). 
  - `exestyle::ExerciseStyle`: exercise style.
  - `prices::AbstractVector{Real}`: observed prices container.
  - `times::AbstractVector{<:TimePoint}`: observation times.
  - `params::Dict{AbstractString, Any}`: casual parameters container.
  
  See also [`BarrierOption`](@ref).
  
  # Constructors
      ParisianOption(strike, expiry, barrier, triggerclock; <kwarg>)
  
  ## Arguments
  - `strike::Real`: strike price. 
  - `expiry::TimePoint`: expiry date.
  - `barrier::Real`: level of the barrier.
  - `barriertype::BarrierType=UpIn`: kwarg, tied to `barrier`.
  - `triggerclock::Union{Period,Real}`: duration trigger of the option.
  - `option::OptionColor=Call`: kwarg, the `OptionColor` (`Call`/`Put`).
  - `underlying::Observables=DummyObs()`: kwarg, underlying instrument.
  - `inception::TimePoint=DummyInception(expiry)`: kwarg, contract inception.
  - `barrier2::Real=dummybarrier(barriertype)`: kwarg, second barrier.
  - `barrier2type::BarrierType=dummybarrier(barriertype)[2]`: kwarg. 
  - `exestyle::ExerciseStyle=EuropeanStyle`: kwarg, exercise style.
  
  # Notes
  **Parisian options** are barrier options for which the barrier feature
  (knock-in or knock-out) is only triggered after the underlying has spent
  a certain prescribed time beyond the barrier.  
  Note that when `expiry` and (and then `inception`) are of (sub)type
  `TimeType`, the `triggerclock` field is required to be of `Period` type
  while when having `expiry` (and then `inception`) of `Real` (sub)type,
  the `triggerclock` field is required to also be of type a `Real` sub(type). 
  """
  struct ParisianOption{U} <: Options{U}
    strike::Q where Q<:Real
    expiry::T where T<:TimePoint
    barrier::R where R<:Real
    barriertype::BarrierType
    triggerclock::Union{Period,Real}
    option::OptionColor
    underlying::U
    inception::T where T<:TimePoint
    barrier2::S where S<:Real
    barrier2type::BarrierType
    exestyle::ExerciseStyle
    prices::AbstractVector{V} where V<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString, Any}
    
    function ParisianOption(
                        strike::Q,
                        expiry::W,
                        barrier::R,
                        triggerclock::Period;
                        barriertype::BarrierType=UpIn,
                        option::OptionColor=Call,
                        underlying::U=DummyObs(),
                        inception::W=DummyInception(expiry),
                        barrier2::Y=dummybarrier(barriertype)[1],
                        barrier2type::BarrierType=dummybarrier(barriertype)[2],
                        exestyle::ExerciseStyle=EuropeanStyle
      ) where {Q<:Real,W<:TimeType,R<:Real,Y<:Real,U<:Observables}
      if expiry < inception
        throw(ArgumentError("expiry($(expiry)) < inception($(inception))"))
      end
      new{U}(
             strike, expiry, barrier, barriertype, triggerclock, option,
             underlying, inception, barrier2, barrier2type, exestyle, Real[],
             TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => zero(Real),
                                      "σ" => zero(Real),
                                      "carryrate" => zero(Real)
             )
      )
    end
    
    function ParisianOption(
                        strike::Q,
                        expiry::W,
                        barrier::R,
                        triggerclock::Z;
                        barriertype::BarrierType=UpIn,
                        option::OptionColor=Call,
                        underlying::U=DummyObs(),
                        inception::W=DummyInception(expiry),
                        barrier2::Y=dummybarrier(barriertype)[1],
                        barrier2type::BarrierType=dummybarrier(barriertype)[2],
                        exestyle::ExerciseStyle=EuropeanStyle
      ) where {Q<:Real,W<:Real,R<:Real,Y<:Real,U<:Observables,Z<:Real}
      if expiry < inception
        throw(ArgumentError("expiry($(expiry)) < inception($(inception))"))
      end
      new{U}(
             strike, expiry, barrier, barriertype, option, underlying,
             triggerclock, inception, barrier2, barrier2type, exestyle,
             Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => zero(Real),
                                      "σ" => zero(Real),
                                      "carryrate" => zero(Real)
             )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", op::ParisianOption)
    print(
          io, "ParisianOption[", getfield(op, :strike), ", ",
          (getfield(op, :barrier), getfield(op, :barrier2)), ", ",
          (getfield(op, :barriertype), getfield(op, :barrier2type)),"]"
    )
  end

  """
      ispayoffable(op::ParisianOption, lvls::AbstractVector{Real})
  
  Return `true` if the option knocked-in w.r.t. levels, `false` otherwise.
  """
  function ispayoffable(
	                      op::ParisianOption,
	                      lvls::AbstractVector{R}
           ) where {R<:Real}
    knockedin(op.barriertype, op.barrier, lvls) &&
	  knockedin(op.barrier2type, op.barrier2, lvls)
  end

  function ispayoffable(op::ParisianOption, lvls::R) where R<:Real
    ispayoffable(op, [lvls])
  end
  
  """
      payoff(op, p, t, prices::Vector{Real}, times::Vector{Real})
  
  Return the payoff of `op` knowing `prices`, `times` and price level `p`.

  # Arguments
  - `op::ParisianOption`: the underlying option contract.
  - `p::Real`: price level.
  - `t::TimePoint`: time at which the payoff is required.
  - `prices::Vector{Real}`: vector of `prices` (default underlying prices)
  - `times::Vector{Real}`: vector of observation times.
  - `ktime::Union{Period,Real}`: maximum time spent within barriers levels.
  """
  function payoff(
                  op::ParisianOption,
                  p::R,
                  t::T,
                  prices::AbstractVector{A},
                  times::AbstractVector{B},
                  ktime::C
        ) where {R<:Real,T<:TimePoint,A<:Real,B<:Real,C<:Union{Period,<:Real}}
    if ~isequal(length(prices),length(times))
      n, m = length(prices), length(times)
      throw(DimensionMismatch("$(n)=dim(prices) ≠ dim(times)=$(m)"))
    end
    i = searchsortedfirst(times, getfield(op, :inception))
    j = searchsortedlast(times, getfield(op, :expiry))
    k = begin
        (
         isequal(op.exestyle,EuropeanStyle) &&
         ~isequal(op.expiry, t)
        ) ||
        (
         isequal(op.exestyle,AmericanStyle) &&
         ~(op.inception <= t <= op.expiry)
        ) ||
        isempty(Colon()(i,j))
    end
    max(
        *(
          ~k,
          ispayoffable(op,view(prices,i:j)),
          isless(getfield(op,:triggerclock),ktime),
          ifelse(isequal(getfield(op,:option),Put), -one(Real), one(Real)),
          p - getfield(op, :strike)
        ),
        zero(Real)
    )
  end
end