# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Quote{U} <: NonMarketables
  
  Parametric type for market quotes representation.
  
  # Fields
  - `class::Type{Observables}`: `Observables`' category the quote is of.
  - `quotation::Real`: quote level.
  - `prices`:  quote levels/prices container.
  - `times::Vector{TimePoint}`: observations' times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      Quote(class::Observables, quote::Real)
  
  ## Arguments
  - `class::Type{<:Observables}`: instrument's type (subtype of `Observables`).
  - `quotation::Real`: quote level/price.
  
  # Notes
  This type is intended to be a lightweight version of usual `Observables`,
  to be used for displaying purposes, calculations needs such as yield curve
  bootstrapping, as a facility to feed usual `Observables`...
  
  *An interesting name for the `quotation` property/field would've been
  `quote` but this already refers to something specific in the Julia
  `Base` module.*
  
  # Examples
  ```julia
  julia> q = Quote(Swap,0.025);
  
  julia> q isa Quote
  true
  
  julia> q isa Quote{Swap}
  true
  ```
  """
  mutable struct Quote{U} <: NonMarketables
    class::Type{U} where U<:Observables
    quotation::R where R<:Real
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function Quote(class::Type{U}, quotation::R) where {U<:Observables,R<:Real}
      new{U}(
             class, quotation, Real[], TimePoint[],
             Dict{AbstractString,Any}("ticker" => dummyticker())
      )
    end
    
    function Quote(
                   class::Type{U},
                   quotation::R,
                   tenor::Tenor
             ) where {U<:Union{A,Derivatives{A}} where A<:Rates,R<:Real}
      new{U}(
             class, quotation, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "tenor" => tenor
             )
      )
    end
  end
  
  function Base.show(
                     io::IO,
                     ::MIME"text/plain",
                     q::Quote{U}
                ) where U<:Observables
    if haparams(q, "tenor")
      print(io, string(U),"(",q.quotation," ", getparams(q,"tenor"),")")
    else
      print(io, string(U),"(",q.quotation,")")
    end
  end
end