# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Moratorium <: CreditEvents
  
  A structure modeling a *moratorium/repudiation* credit event.
  
  See also [`FailureToPay`](@ref), [`ObligationDefault`](@ref).
  
  # Fields
  - `prices::Vector{Bool}`: event observations container, `true` is occurrence.
  - `times::Vector{TimePoint}` : observation times container.
  - `params::Dict{AbstractString,Any}` parameters container.
  
  # Constructors
      Moratorium()
      
  ## Arguments
  None
  
  # Notes
  A **moratorium/repudiation** is a credit event applicable only to Sovereign
  reference entities whereby the relevant Sovereign refuses to acknowledge or
  honor its debts.  
  Source: [ISDA](https://www.isda.org/1985/01/01/glossary/#m)
  
  # Examples
  ``̀`julia
  julia> mrt = Moratorium();
  
  julia> mrt isa Moratorium
  true
  
  julia> isa(mrt,Moratorium) && ( Moratorium <: CreditEvents )
  true
  ```
  """
  struct Moratorium <: CreditEvents
    prices::AbstractVector{Bool}
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    Moratorium() = new(
                       Bool[],
                       TimePoint[],
                       Dict{AbstractString,Any}(
                         "ticker" => dummyticker()
                       )
                   )
  end
  
  function show(io::IO, ::MIME"text/plain", moratorium::Moratorium)
    print(
          io, "BankruptcyEvent[",
          ifelse(any(moratorium.prices),Char(0x2714),Char(0x2718)),"]"
    )
  end
  
  const Repudiation = Moratorium
end