# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      EconoIndicator <: MarketIndicators
  
  A facility to manage Economical Data/Series/Indicators.
  
  # Fields
  - `prices::AbstractVector{Real}`: observed prices container.
  - `times::AbstractVector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      EconoIndicator([ticker::String])
      
  ## Arguments
  - `ticker::AbstractString=dummyticker()`: ticker symbol.
  """
  struct EconoIndicator <: Indices{Rates}
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function EconoIndicator(ticker::AbstractString=dummyticker())
      new(
          Real[],
          TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => ticker,
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0
          )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", ecn::EconoIndicator)
    print(
          io, "EconoIndicator[", getparams(ecn,"μ"),", ", getparams(ecn,"σ"),
          getparams(ecn, "ticker"), ")"
    )
  end
end