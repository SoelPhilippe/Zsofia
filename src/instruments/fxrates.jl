# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
    FXRate <: Rates
  
  Model cross currency exchange rates.
  
  # Fields
  - `domestic::Currency`: quote currency.
  - `foreign::Currency`: base currency.
  - `prices::Vector{Real}`: observed prices container.
  - `times::Vector{<:TimePoint}`: observation times.
  - `params::Dict{String, Any}`: casual parameters container.
  
  # Constructor
  
      FXRate([foreign], [domestic]; [prices], [times], [params])
  
  ## Arguments
  - `foreign::Currency`: base currency.
  - `domestic::Currency`: quote currency.
  - `prices::Vector{Real}`: observed prices container.
  - `times::Vector{<:TimePoint}`: observation times.
  """
  struct FXRate <: Rates
    foreign::Currency
    domestic::Currency
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{String, Any}
    
    function FXRate(
                    foreign::Currency=Currency(),
                    domestic::Currency=Currency();
                    prices::AbstractVector{R}=Real[],
                    times::AbstractVector{<:TimePoint}=TimePoint[]
             ) where R<:Real
      new(foreign, domestic, prices, times,Dict{String,Any}(
                                             "ticker" => dummyticker(),
                                             "μ" => 0,
                                             "σ" => 0,
                                             "carryrate" => 0
                                           )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", fx::FXRate)
    print(io, "FX[",fx.foreign,"/",fx.domestic,"]")
  end
end