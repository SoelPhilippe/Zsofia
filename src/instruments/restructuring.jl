# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Restructuring <: CreditEvents
  
  A structure modeling the *Restructuring* credit event.
  
  See also [`FailureToPay`](@ref), [`ObligationDefault`](@ref).
  
  # Fields
  - `prices::Vector{Bool}`: event observations container, `true` is occurrence.
  - `times::Vector{TimePoint}` : observation times container.
  - `params::Dict{AbstractString,Any}` parameters container.
  
  # Constructors
      Restructuring()
      
  ## Arguments
  None
  
  # Notes
  **Restructuring** is a credit event triggered when a reference entity
  restructures its debt in agreement with its creditors.  
  Source: [ISDA](https://www.isda.org/1985/01/01/glossary/#r)
  
  # Examples
  ``̀`julia
  julia> rstr = Restructuring();
  
  julia> rstr isa Restructuring
  true
  
  julia> isa(rstr,Restructuring) && ( Restructuring <: CreditEvents )
  true
  ```
  """
  struct Restructuring <: CreditEvents
    prices::AbstractVector{Bool}
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    Restructuring() = new(Bool[],TimePoint[],Dict{AbstractString,Any}())
  end
  
  function show(io::IO, ::MIME"text/plain", restructuring::Restructuring)
    print(
          io, "RestructuringEvent[",
          ifelse(any(restructuring.prices),Char(0x2714),Char(0x2718)),"]"
    )
  end
end