# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      ETF <: Equity
  
  Exchange Traded Funds.
  
  # Fields
  - `basket::Basket{Marketables,Int64}`: the underlying basket. 
  - `prices::AbstractVector{Real}`: observed prices container.
  - `times::AbstractVector{TimePoint}`: observations times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      ETF([ticker::AbstractString=dummyticker()], [basket::Basket=Basket()])
  
  ## Arguments
  - `ticker::AbstractString=dummyticker()`: etf's ticker symbol.
  - `basket::Basket`: basket of ETF components.
  """
  struct ETF{U,R} <: Equity
    basket::Basket{U,R} where {U<:Marketables,R<:Integer}
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function ETF{U,R}(
                      ticker::AbstractString,
                      basket::Basket{U,R}
             ) where {U<:Marketables,R<:Integer}
      new{U,R}(
               basket, Real[], TimePoint[],
               Dict{AbstractString,Any}(
                                "ticker" => ticker,
                                "μ" => 0,
                                "σ" => 0,
                                "carryrate" => 0
               )
      )
    end
  end
  
  
  function ETF{U,R}(
                    ticker::AbstractString=dummyticker()
           ) where {U<:Marketables,R<:Integer}
    ETF{U,R}(ticker,Basket{U,R}())
  end
  
  function ETF{U}(
                  ticker::AbstractString=dummyticker(),
                  basket::Basket{U,R}=Basket{U,Integer}()
           ) where U<:Marketables where R<:Integer
    ETF{U,R}(ticker,basket)
  end
  
  function ETF{R}(
                  ticker::AbstractString=dummyticker(),
                  basket::Basket{U,R}=Basket{Marketables,R}()
           ) where R<:Integer where U<:Marketables
    ETF{U,R}(ticker,basket)
  end
  
  function ETF(
               ticker::String=dummyticker(),
               basket::Basket{U,R}=Basket{Marketables,Int128}()
           ) where U<:Marketables where R<:Integer
    ETF{U,R}(ticker, basket)
  end
  
  function ETF(basket::Basket{U,R}) where {U<:Marketables,R<:Integer}
    ETF(dummyticker(), basket)
  end
  
  function Base.length(etf::ETF)
    length(etf.basket)
  end

  function Base.show(io::IO, ::MIME"text/plain", etf::ETF)
    print(io, "ETF[$(length(etf.basket))]($(etf.params["ticker"]))")
  end
  
  # Operations on ETFs, deriving mostly from from src/instruments/baskets.jl
  """
      size(etf::ETF)
      
  Return the `size` of `etf`'s basket.
  """
  function Base.size(etf::ETF)
    size(etf.basket)
  end
  
  """
      isempty(etf::ETF)
  
  Return `true` if `etf.basket` is empty.
  """
  function Base.isempty(etf::ETF)
    isempty(etf.basket)
  end
  
  """
      in(obj::Marketables, etf::ETF)
  
  Return `true` if `obj` is in `etf`'s basket, `false` otherwise.
  """
  function Base.in(obj::Marketables, etf::ETF)
    in(obj, etf.basket)
  end

  """
      in(ticker::String, etf::ETF)
  
  Return `true` if any of the `Marketables` object in `etf.basket` has `ticker`
  as ticker.
  """
  function Base.in(ticker::AbstractString, etf::ETF)
    in(ticker, etf.basket)
  end
  
  """
      push!(etf::ETF, obj::Marketables, [units::Integer=0])
  """
  function Base.push!(
                     etf::ETF{U},
                     obj::U,
                     units::Integer=zero(Integer)
               ) where U<:Marketables
    push!(etf.basket, obj, units)
  end
  
  """
      push!(etf::ETF, obj::Vector{Marketables}, [units::Integer=0])
  """
  function Base.push!(
              etf::ETF{U},
              obj::AbstractVector{U},
              units::Union{
                           AbstractVector{Integer},
                           Integer
                     }=zeros(Integer,length(obj))
          ) where U<:Marketables
    push!(etf.basket, obj, units)
  end
  
  """
      put!(etf::ETF, obj::Marketables, [units=0])
  """
  function Base.put!(
                     etf::ETF{U},
                     obj::U,
                     units::Integer=0
           ) where U<:Marketables
    put!(etf.basket, obj, units)
  end
  
  function Base.put!(
                      etf::ETF{U},
                      obj::AbstractVector{U},
                      units::AbstractVector{Integer}
                 ) where U<:Marketables
    put!(etf.basket, obj, units)
  end
  
  """
      put!(etf::ETF, obj::Marketables, [units=0])
  """
   function Base.put!(
              etf::ETF{U},
              obj::AbstractVector{U},
              units::Union{
                           AbstractVector{Integer},
                           Integer
                     }=zeros(Integer,length(obj))
          ) where U<:Marketables
    put!(etf.basket, obj, units)
  end
  
  """
      deleteat!(etf::ETF, inds)
  
  Remove the items at the indices given by `inds` and return the modified `etf`.
  """
  function Base.deleteat!(etf::ETF, i::Integer)
    (deleteat!(etf.basket, i); etf)
  end
  
  function Base.deleteat!(etf::ETF, r::AbstractUnitRange{<:Integer})
    (deleteat!(etf.basket, r); etf)
  end
  
  function Base.deleteat!(etf::ETF, inds::AbstractVector{Bool})
    (deleteat!(etf.basket, inds); etf)
  end
  
  function Base.deleteat!(etf::ETF, inds::AbstractVector)
    (deleteat!(etf.basket, inds); etf)
  end
  
  """
      keepat!(etf::ETF, inds)
  
  Remove the items at all indices which are not given by `inds`, and return
  the modified `etf`.
  """
  function Base.keepat!(etf::ETF, inds::Integer)
    (keepat!(etf.basket, inds); etf)
  end
  
  function Base.keepat!(etf::ETF, inds::AbstractUnitRange{<:Integer})
    (keepat!(etf.basket, inds); etf)
  end
  
  function Base.keepat!(etf::ETF, inds::AbstractVector{Bool})
    (keepat!(etf.basket, inds); etf)
  end
  
  function Base.keepat!(etf::ETF, inds::AbstractVector)
    (keepat!(etf, inds); etf)
  end
  
  """
      get(a::ETF, what::Symbol)
      
  Return `what` of `etf::ETF` where valids arguments of `what` are:
  - `:components`: components (`Vector{Marketables}`) of `etf`.
  - `:units`: number of shares (`Vector{Integer}`) of each `etf`'s component.
  - `:value`: the last value computed from `etf`'s components.
  - `:nav`: net asset value of `etf`.
  
  # Notes
  The current implementation gives the same result for `:nav` and `:value`.
  The current implementation throws an `ArgumentError` if wrong `what`.
  """
  function Base.get(etf::ETF, what::Symbol)
    if what in (:components, :units)
      get(etf.basket, what)
    elseif what in (:value, :nav)
      get(etf.basket, :value)
    else
      throw(
            ArgumentError(
            "'what' should be amongst :components, :nav, :units or :value"
            )
      )
    end
  end
  
  function Base.get(etf::ETF, what::Symbol, t::TimePoint)
    if what in (:value,:nav)
      get(etf.basket, :value, t)
    else
      throw(ArgumentError("valid args should be amongs (:nav, :value)"))
    end
  end
  
  """
      get(etf::ETF, obj::Marketables)
      
  Return the tuple `(component, unit)` if `in(obj, etf)` evaluates to `true` and
  an error otherwise.
  """
  function Base.get(etf::ETF, obj::Marketables)
    get(etf.basket, obj)
  end
  
  """
      empty!(etf::ETF)
  """
  function Base.empty!(etf::ETF)
    empty!(etf.basket)
  end
  
  """
      count(f, etf::ETF, [what::Symbol=:units])
  
  Count the number of elements in `etf`'s basket into the `what` silo for
  which `f` evaluates to `true`.
  """
  function Base.count(f::Function, etf::ETF, what::Symbol=:units)
    count(f, get(etf, what))
  end
  
  """
      count(etf::ETF)
      
  Return the size of `etf`'s underlying basket.
  """
  function Base.count(etf::ETF)
    size(etf.basket)
  end
  
   function Base.pop!(etf::ETF)
    pop!(etf.basket)
  end
  
  function Base.popat!(etf::ETF, i::Integer)
    popat!(etf.basket, i)
  end
  
  function Base.popat!(etf::ETF, i::Integer, default::Marketables)
    popat!(etf.basket, i, default)
  end
  
  function Base.popat!(
                       etf::ETF,
                       i::Integer,
                       default::Tuple{Marketables,Integer}
                   )
    popat!(etf.basket, i, default)
  end
  
  function Base.popfirst!(etf::ETF)
    popfirst!(etf.basket)
  end
end