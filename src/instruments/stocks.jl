# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Stock <: Equity
  
  # Fields
  - `prices::Vector{Real}`: observed prices container.
  - `times::Vector{TimePoint}`: observation times container.
  - `params::Dict{String, Any}`: casual parameters container.
  
  By default `params` field/property which is a `Dict{String, Any}` contains
  the following keys:
  - `ticker::String`: the ticker symbol defaulting to `dummyticker()`.
  - `μ::Real`: the drift parameter under the Black-Scholes framework.
  - `σ::Real`: the volatility parameter under the Black-Scholes framework.
  - `carryrate::Real`: the carry rate parameter under Black-Scholes.
  
  # Constructor
  
      Stock([ticker], [prices], [times]; [mu], [sigma])
  
  ## Arguments
  - `ticker::String`: stock's ticker symbol.
  - `prices::Vector{Real}`: vector of prices.
  - `times::Vector{TimePoint}`: vector of times.
  - `mu::Real`: kwarg, the drift parameter in the Black-Scholes model.
  - `sigma::Real`: kwarg, the volatility parameter in the Black-Scholes model.
  - `carryrate::Real`: kwarg, the carryrate (dividend continuous rate).
  """
  struct Stock <: Equity
    prices::AbstractVector{R} where R<:Real
    times::AbstractVector{Q} where Q<:TimePoint
    params::Dict{String,Any}
    
    function Stock(
                   ticker::String=dummyticker(),
                   prices::AbstractVector{M}=Float64[],
                   times::AbstractVector{<:TimePoint}=TimePoint[];
                   mu::N=zero(Float64),
                   sigma::Q=zero(Float64),
                   carryrate::R=zero(Float64)
             ) where M<:Real where N<:Real where Q<:Real where R<:Real
      if ~isequal(size(prices,1), size(times,1))
        throw(DimensionMismatch("`prices` and `times` are not consistent!"))
      end
      new(prices, times, Dict{String,Any}(
                                          "ticker" => ticker,
                                          "μ" => mu,
                                          "σ" => sigma,
                                          "carryrate" => carryrate
                                         )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", s::Stock)
    print(
          io, "Stock[",s.params["μ"],", ",s.params["σ"],
          "](",s.params["ticker"],")"
    )
  end
end