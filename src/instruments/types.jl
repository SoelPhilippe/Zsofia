# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Observables
  
  Abstract sypertype for all observables, (e.g. `Stock`, `Swaps`, ...) this
  type is at the top of the type graph.
  
  # Examples
  `̀``julia
  julia> Stock("PLTR") isa Observables
  true
  
  julia> Swaps <: Observables
  true
  
  julia> Swap <: Observables
  true
  ```
  """
  abstract type Observables end
  
  """
      Marketables <: Observables
  
  Abstract supertype for all `Observables` that are marketables.
  
  See also [`NonMarketables`](@ref).
  
  # Examples
  ```julia
  julia> Stock <: Marketables
  true
  
  julia> InterestRate <: Marketables
  false
  ```
  """
  abstract type Marketables <: Observables end
  
  """
      NonMarketables <: Observables
  
  Abstract supertype for all `Observables` that are not marketables.
  
  See also [`Marketables`](@ref).
  
  # Examples
  ``̀`julia
  julia> InterestRate <: NonMarketables
  true
  
  julia> Indices{Stock} <: NonMarketables
  true
  
  julia> Stock <: NonMarketables
  false
  ```
  """
  abstract type NonMarketables <: Observables end
  
  """
      Contracts <: Contracts
  
  Abstract supertypes for all `Marketables` that are contracts but not assets.
  
  See also [`Commodities`](@ref), [`Securities`](@ref).
  
  # Examples
  `̀``julia
  julia> InterestRate <: Contracts
  false
  
  julia> Futures <: Contracts
  true
  ```
  """
  abstract type Contracts <: Marketables end
  
  """
      Commodities
  
  Abstract supertypes for all `Marketables` belonging to the commodity
  asset class.
  
  See also [`Contracts`](@ref), [`Securities`](@ref).
  
  # Examples
  ```julia
  julia> Commodity <: Commodities
  true
  
  julia> Securities <: Commodities
  false
  ```
  """
  abstract type Commodities <: Marketables end

  """
      Securities <: Marketables
  
  Abstract supertype gathering securities.
  
  See also [`Contracts`](@ref), [`Commodities`](@ref).
  
  # Examples
  ```julia
  julia> Commodity <: Securities
  false
  
  julia> Stock <: Securities
  true
  ```
  """
  abstract type Securities <: Marketables end
  
  """
      Indices{<:Observables} <: NonMarketables
  
  Abstract supertype for all `NonMarketables` that are market indices.
  
  # Examples
  ```julia
  julia> Basket{Stock} <: Indices{Stock}
  true
  ```
  """
  abstract type Indices{T<:Observables} <: NonMarketables end
  
  """
      Rates <: NonMarketables
  
  Abstract supertype for all `Observables` representing rates in a broad sense.
  
  # Examples
  ```julia
  julia> InterestRate <: Rates
  true
  
  julia> Stock <: Rates
  false
  ```
  """
  abstract type Rates <: NonMarketables end
  
  """
      Events <: NonMarketables
  
  Abstract supertype representing events.
  """
  abstract type Events <: NonMarketables end
  
  """
      CreditEvents <: Events
  
  Abstract supertype representing 
  """
  abstract type CreditEvents <: Events end
  
  """
      Derivatives{<:Observables} <: Contracts
  
  Abstract supertype representing derivatives contracts.
  """
  abstract type Derivatives{T<:Observables} <: Contracts end
  
  """
      Debt <: Securities
  
  Abstract supertype for debt securities.
  
  # Examples
  ``̀`julia
  julia> Bond <: Securities
  true
  
  julia> Bond <: Debt
  true
  """
  abstract type Debt <: Securities end
  
  """
      Equity <: Securities
  
  Asbtract supertype representing equity type `Observables`.
  
  # Examples
  ```julia
  julia> Stock <: Equity
  true
  
  julia> Bond <: Equity
  false
  """
  abstract type Equity <: Securities end
  
  """
      Options{<:Observables}
  
  Abstract parametric supertype representing options derivatives type.
  """
  abstract type Options{T} <: Derivatives{T} end
  
  """
      Swaps{<:Observables}
  
  Abstract parametric supertype representing swaps. 
  """
  abstract type Swaps{T} <: Derivatives{T} end
  
  """
      MarketIndicators
  
  Global constant representing indices on non-marketables observables.
  """
  const MarketIndicators = Indices{T} where T <: NonMarketables
end