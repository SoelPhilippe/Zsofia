# This script is part of Zsofia. Soel Philippe © 2025
begin
  """
      ObligationAcceleration <: CreditEvents
  
  A structure modeling the *obligation acceleration* credit event.
  
  See also [`FailureToPay`](@ref), [`ObligationDefault`](@ref).
  
  # Fields
  - `prices::Vector{Bool}`: event observations container, `true` is occurrence.
  - `times::Vector{TimePoint}` : observation times container.
  - `params::Dict{AbstractString,Any}` parameters container.
  
  # Constructors
      ObligationAcceleration()
      
  ## Arguments
  None
  
  # Notes
  **Obligation Acceleration** is a credit event term used in a credit derivative
  when one or more of the obligations of the reference entity have become
  declared due and payable before they would otherwise have been, due to an
  event of default of the reference entity (other than failure to pay).  
  Source: [ISDA](https://www.isda.org/1985/01/01/glossary/#o)
  
  # Examples
  ``̀`julia
  julia> oacc = ObligationAcceleration();
  
  julia> oacc isa ObligationAcceleration
  true
  
  julia> isa(oacc,ObligationAcceleration) && ( ObligationAcceleration <: CreditEvents )
  true
  ```
  """
  struct ObligationAcceleration <: CreditEvents
    prices::AbstractVector{Bool}
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    ObligationAcceleration() = new(Bool[],TimePoint[],Dict{AbstractString,Any}())
  end
  
  function show(io::IO, ::MIME"text/plain", oacc::ObligationAcceleration)
    print(
          io, "ObligationAccelerationEvent[",
          ifelse(any(oacc.prices),Char(0x2714),Char(0x2718)),"]"
    )
  end
end