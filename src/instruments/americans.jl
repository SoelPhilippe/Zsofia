# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      AmericanOption <: Options
  
  American options.
  
  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: option's expiry.
  - `option::OptionColor`: either `Call` or `Put`.
  - `underlying::Observables`: option's underlying risk factor's holder.
  - `inception::TimePoint`: contract's inception.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{<:TimePoint}`: observation times container.
  - `params::Dict{AbstractString, Any}`: casual parameters container.
  
  # Constructors
      AmericanOption(strike, expiry; [option], [underlying], [inception])
  
  See also [`EuropeanOption`](@ref).
  
  ## Arguments
  - `strike::Real`: strike price of the option.
  - `expiry::TimePoint`: expiry date.
  - `option::OptionColor`: kwarg, either `Call` or `Put`.
  - `underlying::Observables=DummyObs()`: kwarg, underlying.
  - `inception::TimePoint=dummyinception(expiry)`: kwarg, inception time point.
  
  # Examples
  ```julia
  julia> op = AmericanOption(321.25,Date(2025,1,31));
  
  julia> isa(op,AmericanOption)
  true
  
  julia> isa(op,AmericanOption{Stock})
  true
  ```
  """
  struct AmericanOption{U} <: Options{U}
    strike::R where R<:Real
    expiry::A where A<:TimePoint
    option::OptionColor
    underlying::U where U<:Observables
    inception::A where A<:TimePoint
    prices::AbstractVector{B} where B<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function AmericanOption(
                            strike::A,
                            expiry::B;
                            option::OptionColor=Call,
                            underlying::U=DummyObs(),
                            inception::B=dummyinception(expiry)
             ) where {A<:Real,B<:TimePoint,U<:Observables}
      @assert isless(inception,expiry)
      new{U}(
             strike, expiry, option, underlying,
             inception, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", op::AmericanOption)
    print(
          io, "AmericanOption[", getfield(op,:strike), ", ",
          getfield(op, :expiry),", ",
          ifelse(isequal(getfield(op,:option),Call),"Call","Put"),
          "](",getparams(op,"ticker"), ")"
    )
  end
  
  
  function payoff(op::AmericanOption, p::R, t::T) where {R<:Real,T<:TimePoint}
    *(
      getfield(op,:inception) <= t <= getfield(op,:expiry),
      max(
          -(2isequal(getfield(op,:option),Call),-1) * -(p,getfield(op,:strike)),
          0
      )
    )
  end
  
  function payoff(op::AmericanOption)
    payoff(
           op, S(getfield(op,:underlying))(),
           last(getfield(getfield(op,:underlying),:times))
    )
  end
end