# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Futures{U} <: Derivatives{U}
  
  Futures contract (yes, with an 's').
  
  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: contract expiry date.
  - `underlying::Observables`: underlying observable.
  - `islong::Bool`: long or short, that's the question.
  - `inception::TimePoint`: inception date of the contract.
  - `currency::Currency`: settlement currency.
  - `prices::Vector{Real}`: observed prices container.
  - `times::Vector{<:TimePoint}`: observation times.
  - `params::Dict{AbstractString, Any}`: casual parameters container.
  
  # Constructors
      Futures(strike, expiry, underlying, <kwargs>)
  
  ## Arguments
  - `strike::Real`: futures' strike price
  - `expiry::TimePoint`: maturity date.
  - `underlying::Observables`: underlying risk factor.
  - `islong::Bool`: kwarg, whether short or long.
  - `inception::TimePoint`: kwarg, inception of the contract.
  - `currency::Currency`: kwarg, settlement currency.
  
  # Examples
  ```julia
  julia> pltr = Stock("PLTR");
  
  julia> fut = Future(105.75, 0.75, pltr, islong=false, inception=0.0);
  
  julia> isa(fut, Future)
  true
  
  julia> isa(fut, Future{Stock})
  true
  ```
  """
  struct Futures{U} <: Derivatives{U}
    strike::A where A<:Real
    expiry::T where T<:TimePoint
    underlying::U where U<:Observables
    islong::Bool
    inception::T where T<:TimePoint
    currency::Currency
    prices::AbstractVector{Q} where Q<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function Futures(
                     strike::A,
                     expiry::B,
                     underlying::C=DummyObs();
                     islong::Bool=true,
                     inception::B=dummyinception(expiry),
                     currency::Currency=Currency()
             ) where {A<:Real,B<:TimePoint,C<:Observables}
      @assert isless(inception,expiry)
      new{C}(
             strike, expiry, underlying, islong, inception,
             currency, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", fut::Futures)
    print(
          io,"Futures[",fut.strike,", ",fut.expiry,
          "](",getparams(fut,"ticker"),")"
    )
  end
  
  function Base.show(io::IO, fut::Futures)
    print(
          io,"Futures[",getfield(fut,:strike),", ",getfield(fut,:expiry),"](",
          getparams(fut,"ticker"),")"
    )
  end
  
  """
      payoff(fut::Futures, [p::Real, [t::TimePoint]])
  
  Return the payoff of the `Futures` contract `fut` assuming
  the underlying risk factor level to be at `p` at time `t`.
  
  # Arguments
  - `fut::Futures`: the contract payoff is required of.
  - `p::Real`: underlying risk factor level.
  - `t::TimePoint`: the level `p` is observed at `t`.
  
  # Notes
  When `p` (and therefore `t`) is not specified, the latest underlying's
  price level (and time) is assumed.
  
  # Examples
  ```julia
  julia> ccy = Currency("USD");
  
  julia> pltr = Stock("PLTR");
  
  julia> fut = Futures(5.0,0.5,pltr,islong=true,inception=0,currency=ccy);
  
  julia> setprices!(pltr,[4.95,5.05,5.01,4.99,4.75],[0.05,0.1,0.15,0.45,0.55]);
  
  julia> payoff(fut)
  0
  ```
  """
  function payoff(fut::Futures, p::R, t::T) where {R<:Real,T<:TimePoint}
    *(
      fut.inception <= t <= fut.expiry,
      ifelse(fut.islong,1,-1),
      -(p, fut.strike)
    )
  end
end