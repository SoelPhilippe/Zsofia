# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      Swaption <: Options{Swap}
  
  Vanilla swaption, a european option to enter into a fixed vs floating swap.
  
  # Fields
  - `strike::Real`: pre-agreed swap rate.
  - `expiry::TimePoint`: expiry of the right to enter into the specified swap.
  - `underlying::Swap`: underlying swap.
  - `inception::TimePoint`: inception time point of the contract.
  - `prices::Vector{Real}`: prices container.
  - `times::Vector{TimePoint}`: times container.
  - `params::Dict{AbstractString,Any}`: parameters container.
  
  # Constructors
      Swaption(strike, expiry, underlying; inception=dummyinception(expiry))
  
  See also [`Swap`](@ref).
  
  # Arguments
  - `strike::Real`: strike of the swaption.
  - `expiry::TimePoint`: expiry of the option contract.
  - `underlying::Swap`: the underlying swap contract.
  - `inception::TimePoint`: inception of the contract.
  
  # Notes
  Whether it is a payer/receiver swaption is determined by the nature of
  the `underlying` swap contract.  
  This contract is a plain vanilla european-style swaption.  
  In the current implementation, a whole swap is required to be passed as
  argument to fully determine the swaption.
  
  # Examples
  ```julia
  julia> nothing
  ```
  """
  struct Swaption <: Options{Swap}
    strike::R where R<:Real
    expiry::T where T<:TimePoint
    underlying::Swap
    inception::A where A<:TimePoint
    prices::AbstractVector{Q} where Q<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function Swaption(
                      strike::A,
                      expiry::B,
                      underlying::Swap;
                      inception::B=dummyinception(expiry)
             ) where {A<:Real,B<:TimePoint}
      @assert isless(inception,expiry)
      new(
          strike, expiry, underlying, inception,
          Real[], TimePoint[],
          Dict{AbstractString,Any}(
                                   "ticker" => dummyticker(),
                                   "μ" => 0,
                                   "σ" => 0,
                                   "carryrate" => 0
           )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", op::Swaption)
    print(
          io, "Swaption[", getfield(op,:strike),", ",getfield(op,:expiry),", ",
          getfield(getfield(op,:underlying),:ispayer) ? "Payer]" : "Receiver]"
    )
  end
  
  """
      payoff(swaption::Swaption, p::Real, t::TimePoint, d::Real)
  
  Return the payoff of the `swaption` at time `t` given a swaprate at level
  `p` and the *annuity* (present value of the fixed leg) level at `d`.
  
  # Arguments
  - `swaption::Swaption`: the swaption contract to get payoff of.
  - `p::Real`: level of the underlying risk factor (underlying swap rate).
  - `t::TimePoint`: valuation time point.
  - `d::Real`: annuity level, aka present value of the underlying fixed leg.
  """
  function payoff(
                  swaption::Swaption,
                  p::A,
                  t::T,
                  d::B
           ) where {A<:Real,T<:TimePoint,B<:Real}
    ω = 2getfield(getfield(swaption,:underlying),:ispayer) - 1
    max(ω * (p - getfield(swaption,:strike)), 0) * d
  end
end