# This script is part of Zsofia. Soel Philippe © 2025

begin
   """
       Basket <: Indices
   
   A facility for building baskets of `Observables`.
   
   A data structure allowing to put, value, process, monitor together
   financial `Observables`. This is intended to build, for example an
   underlying instrument for baskets' options or exchange traded funds
   (ETFs).

   # Fields
   - `grupp::NamedTuple{(:components,:units)}`: constituting `Observables`.
   - `prices::Vector{Real}`: prices container.
   - `times::Vector{TimePoint}`: times container.
   - `params::Dict{AbstractString,Any}`: parameters container.
   
   # Constructors
       Basket{U,R}(components::Vector{U},[units::Vector{R}])
       Basket{U}([components::Vector{Observables}])
       Basket(components::Vector{Observables},[units::Vector{Real}])
   
   ## Arguments
   - `components::AbstractVector{Observables}`: `Basket`'s components.
   - `units::Vector{Real}`: units/share/prevalence of each component.
   
   # Notes
   The field `grupp` is a `NamedTuple{(:components,:units)}` gathering
   components and their shares/weight in the `Basket`.
   """
   struct Basket{U,R} <: Indices{U}
     grupp::NamedTuple{
                       (:components,:units),
                       Tuple{
                             AbstractVector{U} where U<:Observables,
                             AbstractVector{R} where R<:Real
                       }
            }
     prices::AbstractVector{Q} where Q<:Real
     times::AbstractVector{T} where T<:TimePoint
     params::Dict{AbstractString,Any}
     
     function Basket{U,R}(
                          comps::AbstractVector{U},
                          units::AbstractVector{R}
              ) where {U<:Observables,R<:Real}
       if ~isequal(length(comps), length(units))
         n, m = length(comps), length(units)
         throw(DimensionMismatch("components -> $n\nunits -> $m"))
       end
       @assert allunique(comps)
       new{U,R}(
                NamedTuple{(:components,:units)}((comps,units)),
                Real[], TimePoint[],
                Dict{AbstractString,Any}(
                                         "ticker" => dummyticker(),
                                         "μ" => 0,
                                         "σ" => 0,
                                         "carryrate" => 0
                )
       )
     end
     
     function Basket{U,R}(
                          comps::AbstractVector{U}
              ) where {U<:Observables,R<:Real}
       Basket{U,R}(comps,zeros(R,length(comps)))
     end
     
     Basket{U,R}() where {U<:Observables,R<:Real} = Basket{U,R}(U[],R[])
     
     function Basket{U}(
                        comps::AbstractVector{U},
                        units::AbstractVector{R}=zeros(Real,length(comps))
              ) where {R<:Real,U<:Observables}
       Basket{U,Real}(comps, units)
     end
     
     Basket{U}() where U<:Observables = Basket{U}(U[])
     
     function Basket{R}(
                        comps::AbstractVector{U},
                        units::AbstractVector{R}=zeros(R,length(comps))
              ) where {U<:Observables,R<:Real}
       Basket{Observables,R}(comps,zeros(R,length(comps)))
     end
     
     Basket{R}() where R<:Real = Basket{R}(Observables[])
     
     function Basket(
                     comps::AbstractVector{U},
                     units::AbstractVector{R}
              ) where {U<:Observables, R<:Real}
       Basket{U,R}(comps, units)
     end
     
     function Basket(comps::AbstractVector{U}) where U<:Observables
       Basket(comps, zeros(Real,length(comps)))
     end
     
     Basket() = Basket(Observables[], Real[])
     
     function Basket(dict::Dict{<:U,<:R}) where {U<:Observables,R<:Real}
       Basket{U,R}(collect(keys(dict)), collect(d[k] for k in keys(dict)))
     end
   end
   
   const Portfolio = Basket{<:Marketables,<:Integer}
   
   function Base.show(io::IO,::MIME"text/plain",b::Basket)
     print(
           io,
           "Basket[$(length(b.grupp.components))]($(b.params["ticker"]))"
     )
   end
   
   function Base.show(io::IO, ::MIME"text/plain", p::Portfolio)
     print(
           io,
           "Portfolio[$(length(p.grupp.components))]($(p.params["ticker"]))"
     )
   end
 end
 
 
 #> operations on Baskets
 begin
   Base.length(a::Basket) = length(getfield(getfield(a,:grupp),:components))
   
   Base.size(a::Basket) = size(getfield(getfield(a,:grupp),:components))
   
   Base.isempty(a::Basket) = isempty(getfield(getfield(a,:grupp),:components))
   
   Base.in(elmt,b::Basket) = in(elmt,getfield(getfield(a,:grupp),:components))
   
   function Base.in(
                    tckr::AbstractString,
                    b::Basket
                 )
     in(tckr, getfield.(getfield(getfield(b,:grupp),:components),"ticker"))
   end
   
   function Base.push!(
                       basket::Basket{U,R},
                       obs::U,
                       units::R
                 ) where {U<:Observables,R<:Real}
     i = only(indexin(obs,getfield(getfield(basket,:grupp),:components)))
     if isnothing(i)
       push!(getfield(getfield(basket,:grupp),:units), units)
       push!(getfield(getfield(basket,:grupp),:components), obs)
     else
       setindex!(getfield(getfield(basket,:grupp),:units),units,i)
       setindex!(getfield(getfield(basket,:grupp),:components),obs,i)
     end
     basket
   end
   
   function Base.push!(
                       basket::Basket{U,R},
                       obs::AbstractVector{U},
                       units::AbstractVector{R}
                 ) where {U<:Observables, R<:Real}
     @assert allunique(obs)
     for (i, j) in zip(obs, units)
       push!(basket, i, j)
     end
   end
   
   function Base.push!(
                       basket::Basket{U,R},
                       obs::AbstractVector{U},
                       units::R
                 ) where {U<:Observables, R<:Real}
     push!(basket, obs, units * ones(R, length(obs)))
   end
   
   function Base.push!(
                       basket::Basket{U},
                       obs::AbstractVector{U}
                 ) where U<:Observables
     push!(basket, obs, 0)
   end
   
   """
       put!(basket::Basket, obs::Observables, [units::Real])
       put!(basket::Basket, obs::Vector{Observables}, [units::Vector{Real}])
   
   Put the `Observables` `obs` into the basket `basket`. Return the updated
   basket.

   See also [`ETF`](@ref).

   # Arguments
   - `basket::Basket`: the basket/portfolio we want to update.
   - `obs::Observables`: the `Observables` to add-on into `basket`.
   - `units::Real`: corresponding units.

   # Notes
   Add the given `Observables` into the basket `basket` and return the 
   updated `Basket`.
   """
   function Base.put!(
                      basket::Basket{U,R},
                      obs::Union{U,AbstractVector{U}},
                      units::Union{R,AbstractVector{R}}=0
                 ) where {U<:Observables,R<:Real}
     push!(basket, obs, units)
   end
   
   function Base.deleteat!(basket::Basket,i::Integer)
     deleteat!(getfield(getfield(basket,:grupp),:units), i)
     deleteat!(getfield(getfield(basket,:grupp),:components), i)
     basket
   end
   
   function Base.deleteat!(basket::Basket, r::AbstractUnitRange{<:Integer})
     deleteat!(getfield(getfield(basket,:grupp),:units), r)
     deleteat!(getfield(getfield(basket,:grupp),:components), r)
     basket
   end
   
   function Base.deleteat!(basket::Basket, inds::AbstractVector{Bool})
     deleteat!(getfield(getfield(basket,:grupp),:units), inds)
     deleteat!(getfield(getfield(basket,:grupp),:components), inds) 
     basket
   end
   
   function Base.deleteat!(basket::Basket, inds::AbstractVector)
     deleteat!(getfield(getfield(basket,:grupp),:units), inds)
     deleteat!(getfield(getfield(basket,:grupp),:components), inds)
     basket
   end
   
   function Base.deleteat!(
                           basket::Basket{U,<:Real},
                           obs::Union{U,AbstractVector{U}}
                 ) where U<:Observables
     i = indexin(obs,getfield(getfield(basket,:grupp),:components))
     isnothing(i) ? deleteat!(basket, i) : nothing
   end

   function Base.keepat!(basket::Basket, inds::Integer)
     keepat!(getfield(getfield(basket,:grupp),:units), inds)
     keepat!(getfield(getfield(basket,:grupp),:components), inds)
     basket
   end
   
   function Base.keepat!(basket::Basket, inds::AbstractUnitRange{<:Integer})
     keepat!(getfield(getfield(basket,:grupp),:units), inds)
     keepat!(getfield(getfield(basket,:grupp),:components), inds)
     basket
   end
   
   function Base.keepat!(basket::Basket, inds::AbstractVector)
     keepat!(getfield(getfield(basket,:grupp),:units), inds)
     keepat!(getfield(getfield(basket,:grupp),:components), inds)
     basket
   end
   
   """
       get(a::Basket, what::Symbol, [t::TimePoint])
       get(a::Basket, obs::Observables)
   
   Return `what` of `Basket` for the time point `t`.

   # Arguments
   - `a::Basket`: basket we want `what` from.
   - `what::Symbol`: either `:components`, `:units`, `:value`.

   # Notes
   |`what` valid values|**description**|
   |:---:|:---:|
   |`:components`|components of the basket|
   |`:units`|units of basket's components|
   |`:value`|the value of the `basket at specified time|

   When the time point `t` is not specified, the last observed value
   is returned.

   # Examples
   ```julia
   julia> basket = Basket();

   julia> s1, s2 = Stock("PLTR"), Stock("CFLT");

   julia> setprices!(s1, [100.25, 100.75], [1,2]);

   julia> setprices!(s2, [200.50, 210.50], [1,2]);

   julia> isa(s1,Stock) && isa(s2,Stock)
   true

   julia> push!(basket, [s1,s2], [99,250]);

   julia> isapprox(get(a, :value), 99*100.75 + 250*210.50)
   true
   ```
   """
   function Base.get(a::Basket, what::Symbol)
     if in(what,(:components,:units))
        getfield(getfield(basket, :grupp), what)
     elseif in(what, (:value,))
      sum(
          .*(
             S.(getfield(getfield(basket,:grupp),:components))(),
             getfield(getfield(basket,:grupp),:units)
          )
      )
     else
      throw(ArgumentError("valid `what` -> :units, :value or :components"))
     end
   end
   
   function Base.get(basket::Basket, what::Symbol, t::TimePoint)
     if in(what, (:value,))
       sum(
           .*(
              S.(getfield(getfield(basket,:grupp),:components))(t),
              getfield(getfield(basket,:grupp),:units)
            )
       )
     else
       throw(ArgumentError("only `:value` is allowed when `t` is specified"))
     end
   end
   
   function Base.get(basket::Basket, obs::Observables)
     if ~in(obs, basket)
       throw(DomainError("$(obs) is not in $(basket)"))
     end
     i = only(indexin(obs,getfield(getfield(basket,:grupp),:components)))
     (
      getindex(getfield(getfield(basket,:grupp),:components), i),
      getindex(getfield(getfield(basket,:grupp),:units), i)
     )
   end
   
   function Base.empty!(basket::Basket)
     empty!(getfield(getfield(basket,:grupp),:units))
     empty!(getfield(getfield(basket,:grupp),:components))
   end
   
   """
       count(f, basket::Basket, [what::Symbol=:units])
   
   # Arguments
   - `f::Function`: 
   - `basket::Basket`: 
   - `what::Symbol=:units`: 
   Count the number of elements in `basket::Basket` into `what` for which
   `f` evaluates to `true`.
   """
   function Base.count(f::Function, basket::Basket, what::Symbol=:units)
     if what in propertynames(getfield(basket, :grupp))
       count(f, get(a, what))
     else
       throw(ArgumentError("`what` is one of $(propertynames(basket.grupp))"))
     end
   end
   
   Base.count(basket::Basket) = length(basket)
   
   function Base.pop!(basket::Basket)
     (
      pop!(getfield(getfield(basket,:grupp),:components)),
      pop!(getfield(getfield(basket,:grupp),:units))
     )
   end
   
   function Base.popat!(basket::Basket, i::Int)
     (
      popat!(getfield(getfield(basket,:grupp),:components),i),
      popat!(getfield(getfield(basket,:grupp),:units),i)
     )
   end
   
   function Base.popat!(a::Basket,i::Int,default::Tuple{Observables,Int})
     (
      popat!(getfield(getfield(basket,:grupp),:components),i,default[1]),
      popat!(getfield(getfield(basket,:grupp),:units),i,default[2])
     )
   end
   
   function Base.popfirst!(basket::Basket)
     (
      popfirst!(getfield(getfield(basket,:grupp),:components)),
      popfirst!(getfield(getfield(basket,:grupp),:units))
     )
   end
 end