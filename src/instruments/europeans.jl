# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      EuropeanOption <: Options
  
  A european option.
  
  # Fields
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry date.
  - `option::OptionColor`: either `Call` or ̀`Put`.
  - `underlying::Observables`: undrlying instrument holding the risk factor.
  - `inception::TimePoint`: inception date of the contract.
  - `prices::AbstractVector{Real}`: prices container.
  - `times::AbstractVector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: casual parameters container.
  
  # Constructors
      EuropeanOption(strike, expiry, [option], [underlying], [inception])
  
  ## Arguments
  - `strike::Real`: strike price.
  - `expiry::TimePoint`: expiry date of the contract.
  - `options::OptionColor=Call`: kwarg, `Call`/`Put` (default: `Call`).
  - `underlying::Observables=DummyObs()`: kwarg, underlying instrument.
  - `inception::TimePoint`: kwarg, inception date of the contract.
  
  See also  [`AmericanOption`](@ref).
  
  # Notes
  A european option gives the right, but not the obligation to its
  holder, to buy or sell the underlying financial instrument at the
  pre-agreed price called the strike price at a pre-agreed time point.
  
  # Examples
  ```julia
  julia> op = EuropeanOption(10,1,option=Put);
  
  julia> isa(op,Option)
  true
  
  julia> isequal(op.option,Call)
  false
  
  julia> isequal(op.option,Put)
  true
  ```
  """
  struct EuropeanOption{U} <: Options{U}
    strike::R where R<:Real
    expiry::T where T<:TimePoint
    option::OptionColor
    underlying::U where U<:Observables
    inception::C where C<:TimePoint
    prices::AbstractVector{Q} where Q<:Real
    times::AbstractVector{<:TimePoint}
    params::Dict{AbstractString,Any}
    
    function EuropeanOption(
                            strike::A,
                            expiry::B;
                            option::OptionColor=Call,
                            underlying::U=DummyObs(),
                            inception::B=dummyinception(expiry)
             ) where {A<:Real,B<:TimePoint,U<:Observables}
      @assert isless(inception,expiry)
      new{U}(
             strike, expiry, option, underlying,
             inception, Real[], TimePoint[],
             Dict{AbstractString,Any}(
                                      "ticker" => dummyticker(),
                                      "μ" => 0,
                                      "σ" => 0,
                                      "carryrate" => 0
             )
      )
    end
  end
  
  function Base.show(io::IO, ::MIME"text/plain", op::EuropeanOption)
    print(
          io, "EuropeanOption[",getfield(op,:strike),", ", getfield(op,:expiry),
          ", ",ifelse(isequal(getfield(op,:option),Call),"Call","Put"),"](",
          getparams(op,"ticker"),")"
    )
  end
  
  """
      payoff(op::AmericanOption, [p::Real, t::TimePoint])
      payoff(op::BarrierOption, [p::Real, t::TimePoint, prices, times])
      payoff(op::BermudanOption, [p::Real, t::TimePoint])
      payoff(op::EuropeanOption, [p::Real, t::TimePoint])
      payoff(op::Swap, [p::Real, t::TimePoint])
      payoff(op::Swaption, [p::Real, t::TimePoint])
  
  Return the payoff of the derivative product `op` at time `t` given
  the underlying risk factor level being at `p`.
  
  # Arguments
  - `op::Derivatives`: option payoff is required of.
  - `p::Real`: underlying risk factor level.
  - `t::TimePoint`: observation time.
  - `prices::Vector{Real}`: when relevant, vector of prices.
  - `times::Vector{TimePoint}`: when relevant, observation time points.
  
  # Notes
  When the method `payoff(op::Derivative)` is used, the latest observed
  underlying factor levels are assumed.
  """
  function payoff(op::EuropeanOption, p::R, t::T) where {R<:Real,T<:TimePoint}
    isequal(t, getfield(op,:expiry)) *
    max(
        *(
          -(2isequal(getfield(op,:option),Call),1),
          -(p, getfield(op,:strike))
        ),
        0
    )
  end
  
  function payoff(op::EuropeanOption)
    payoff(
           op, S(getfield(op,:underlying))(),
           last(getfield(getfield(op,:underlying),:times))
    )
  end
end