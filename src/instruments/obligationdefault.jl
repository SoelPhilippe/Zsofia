# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      ObligationDefault <: CreditEvents
  
  A structure modeling the *obligation default* credit event.
  
  See also [`FailureToPay`](@ref), [`Moratorium`](@ref).
  
  # Fields
  - `prices::Vector{Bool}`: event observations container, `true` is occurrence.
  - `times::Vector{TimePoint}` : observation times container.
  - `params::Dict{AbstractString,Any}` parameters container.
  
  # Constructors
      ObligationDefault()
      
  ## Arguments
  None
  
  # Notes
  **Obligation Default** is a credit event term used in a credit derivative
  when one or more of th obligations of the reference entity have become
  of being declaredd due and payable before theu would otherwise been, due
  to an event of default of the reference entity (other than failure to pay).
  Normally caused when the obligation(s) under question are subject to
  cross-default provisions with defaulted obligation(s).  
  Source: [ISDA](https://www.isda.org/1985/01/01/glossary/#o)
  
  # Examples
  ``̀`julia
  julia> od = ObligationDefault();
  
  julia> od isa ObligationDefault
  true
  
  julia> isa(od,ObligationDefault) && ( ObligationDefault <: CreditEvents )
  true
  ```
  """
  struct ObligationDefault <: CreditEvents
    prices::AbstractVector{Bool}
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    ObligationDefault() = new(Bool[],TimePoint[],Dict{AbstractString,Any}())
  end
  
  function show(io::IO, ::MIME"text/plain", od::ObligationDefault)
    print(
          io, "ObligationDefaultEvent[",
          ifelse(any(od.prices),Char(0x2714),Char(0x2718)),"]"
    )
  end
end