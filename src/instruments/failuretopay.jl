# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      FailureToPay <: CreditEvents
  
  A structure modeling the *failure to pay* credit event.
  
  See also [`Bankruptcy`](@ref), [`ObligationDefault`](@ref).
  
  # Fields
  - `prices::Vector{Bool}`: event observations container, `true` is occurrence.
  - `times::Vector{TimePoint}`: observation times container.
  - `params::Dict{AbstractString,Any}`: parameters container.
  
  # Constructors
      FailureToPay()
  
  ## Arguments
  None
  
  # Notes
  **Failure to pay** is a credit event that is triggered if the reference
  entity fails to make interest or principal payments due under the terms
  of a reference obligation after a permitted *grace period*.  
  Source: [ISDA](https://www.isda.org/1985/01/01/glossary/#b)
  
  # Examples
  ``̀`julia
  julia> bnk = FailureToPay();
  
  julia> bnk isa FailureToPay
  true
  
  julia> isa(bnk,FailureToPay) && ( FailureToPay <: CreditEvents )
  true
  ```
  """
  struct FailureToPay <: CreditEvents
    prices::AbstractVector{Bool}
    times::AbstractVector{T} where T<:TimePoint
    params::Dict{AbstractString,Any}
    
    function FailureToPay()
      new(
          Bool[],TimePoint[],
          Dict{AbstractString,Any}("ticker" => dummyticker())
      )
    end
  end
  
  function show(io::IO, ::MIME"text/plain", failuretopay::FailureToPay)
    print(
          io, "FailureToPay[",ifelse(
                                     any(getfield(failuretopay,:prices)),
                                     Char(0x2714),
                                     Char(0x2718)
                              ), "]"
    )
  end
end