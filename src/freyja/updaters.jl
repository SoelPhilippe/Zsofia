# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      getoutdated(::Type{U},::ZsofiaDBContext,[::HolidayCalendar,::Date])
  
  Return either a `Dict`ionary (keyed by calendar's when called on `Stock`s
  and having as values `Vector{String}` container of the outdated ticker
  symbols), or a `Vector{String}` containing ticker symbols (not `Symbol`s but
  `String`s) of outdated `Observables` of argument-givent type.
  
  # Arguments
  - `::Type{<:Fetchables}`: set to lookup into (see [`Fetchables`](@ref)).
  - `::ZsofiaDBContext`: one of `BJORN`, `BRYNHILD`, `HJORDIS` or `VALTYR`.
  - `::HolidayCalendar`: a calendar, eg. `UnitedStatesEx()`, `GermanyEx()`...
  - `::Date`: date to lookup.
  
  # Notes
  For some `::ZsofiaDBContext`, giving a `::Date` is not relevant, for example
  `VALTYR` or `BRYNHILD`.  
  Note that the decision of being 'outdated' is based on the glob `FOTO`,
  therefore whenever the last `fotografi!` date precedes last business day
  according to the calendar `cal`, `fotografi!()` is run.  
  Methods should/will be added/adapted as package is being enhanced.
  
  # Examples
  Herebelow some examples of correct call for the method.
  ```
  julia> a=getoutdated(Stock, BRYNHILD, Zsofia.US);
  
  julia> b=getoutdated(Futures{Stock}, BRYNHILD);
  
  julia> c=getoutdated(Futures{Stock}, HJORDIS, Date(2025,2,28));
  ```
  """
  function getoutdated(u::Type{Stock},zc::ZsofiaDBContext,cal::HolidayCalendar)
    _dee = tobday(cal,-(today(),Day(1)), forward=false)
    if ~haskey(UNNUR,"LastFoto") || isless(UNNUR["LastFoto"], _dee)
      fotografi!()
    end
    _b = _to_bunch(u)
    if ~haskey(FOTO,_b)
      @warn "Fotografi can't find $(u)'s enclosing bunch $(_b)"
      return String[]
    end
    if ~haskey(FOTO[_b],zc)
      @warn "can't find $(zc) context in $(_b)'s Fotografi..."
      return String[]
    end
    _geo = _stock_silo(cal)
    if ~haskey(FOTO[_b][zc], _geo)
      _col = collect(HRAFNHILDUR, BRYNHILD, u)
      return get(_col, _geo, String[])
    end
    _fotos = FOTO[_b][zc][_geo]
    _out = collect(k for k in keys(_fotos) if isless(_fotos[k]["Date"],_dee))
    sort!(_out)
    _out
  end
  
  function getoutdated(
                       u::Type{U},
                       zc::ZsofiaDBContext,
                       cal::HolidayCalendar=US
           ) where U<:Fetchables
    _dee = tobday(cal, -(today(),Day(1)), forward=false)
    if ~haskey(UNNUR, "LastFoto") || isless(UNNUR["LastFoto"], _dee)
      fotografi!()
    end
    _b = _to_bunch(u)
    if ~haskey(FOTO, _b)
      @warn "Fotografi can't find $(u)'s enclosing bunch $(_b)"
      return String[]
    end
    if ~haskey(FOTO[_b], zc)
      @warn "can't find $(zc) context in $(_b)'s Fotografi..."
      return String[]
    end
    _fotos = FOTO[_b][zc]
    _out = collect(k for k in keys(_fotos) if isless(_fotos[k]["Date"],_dee))
    sort!(_out)
    _out
  end
  
  """
      update(obs::Observables, ::ZsofiaDBContext; <kwargs>)
  
  Update `obs` in the given `::ZsofiaDBContext`. Return the updated record.
  
  # Arguments
  - `obs::Observables`: the observable to update.
  - `::ZSofiaDBContext`: either `Brynhild`, `Bjorn`, `Hjordis` or `Valtyr`.
  - `force::Bool`: kwarg, force the update if `true`.
  - `localdb::ZsofiaDB=HRAFNHILDUR`: kwarg, the local data base.
  - `return_data::Bool`: kwarg, if `true` updated record is returned.
  - `timeout::Int64`: kwarg, number of seconds before *timeout*.
  
  See also [`fetch`](@ref).
  
  # Notes
  The facility updates `obs` into the specified context, and return the
  updated `DataFrame` object whenever `return_data=true`.
  
  In the current implementation the facility tries to guess the relevant
  `HolidayCalendar` to be applied, it defaults to `US` without any notice
  if nothing can be inferred.
  """
  function update(
                  obs::U,
                  ::Brynhild;
                  force::Bool=false,
                  localdb::ZsofiaDB=HRAFNHILDUR,
                  return_data::Bool=false,
                  timeout::Int=30
           ) where U<:Fetchables
    _foto, bunch = getfotografi(obs,BRYNHILD), _to_bunch(obs)
    cal, tckr = getparams(obs,"calendar",US), getparams(obs,"ticker")
    if isless(_foto["LastFoto"], _foto["Date"])
      throw("BRYNHILD[$(U)]> outdated FOTO detected in $(tckr)")
    end
    _nxtbd = tobday(cal, _foto["Date"]+Day(1))
    if _nxtbd >= today()
      @info "BRYNHILD[$(U)]> $(tckr) [up-to-date] " * Char(0x2714)
      return nothing
    end
    df_new = _fetch_yahoo(
                          BRYNHILD,_apitckr(obs),_nxtbd,today(),
                          interval="1d",version=8,timeout=timeout
             )
    transform!(df_new, :Open => ByRow(x -> tckr) => :Tckr)
    buildfeatures!(obs,BRYNHILD,df_new)
    df = read(localdb,obs,BRYNHILD)
    append!(df,df_new)
    rebuildfeatures!(obs,BRYNHILD,df)
    write(localdb,obs,df,BRYNHILD)
    @info "BRYNHILD[$(U)]> $(tckr) -got-> " * string(last(df.Date))
    _foto = fotografi(df)[tckr]
    return_data ? df : nothing
  end
  
  function update(
                  obs::AbstractVector{U},
                  ::Brynhild;
                  force::Bool=false,
                  localdb::ZsofiaDB=HRAFNHILDUR,
                  return_data::Bool=false,
                  timeout::Int=30
           ) where U<:Fetchables
    _taskies = map(obs) do elmt
      @task begin
        update(
               elmt,BRYNHILD,force=force,localdb=localdb,
               return_data=false,timeout=timeout
        )
      end
    end
    (schedule.(_taskies); wait.(_taskies))
    _f_tasks = filter(istaskfailed,_taskies)
    n, m = length(_taskies), length(_f_tasks)
    if isequal(n,m)
      @warn "BRYNHILD[$(U)], updation[$(n)] failed($(m)) [" * Char(0x2718) * "]" 
    elseif 0 < m < n
      @info "BRYNHILD[$(U)], updation[$(n)] failed($(m)) ..." failures=_f_tasks
    else
      @info "BRYNHILD[$(U)], updation[$(n)] fully succeeded " * Char(0x2714)
    end
  end
  
  function update(
                  obs::Set{U},
                  ::Brynhild;
                  force::Bool=false,
                  localdb::ZsofiaDB=HRAFNHILDUR,
                  return_data::Bool=false,
                  timeout::Int=30
          ) where U<:Fetchables
    update(
           collect(obs), BRYNHILD, force=force, localdb=localdb,
           return_data=return_data, timeout=timeout
    )
  end
  
  function update(
                  obs::U,
                  ::Hjordis;
                  force::Bool=false,
                  interval::AbstractString="1m",
                  localdb::ZsofiaDB=HRAFNHILDUR,
                  return_data::Bool=false,
                  timeout::Int=30
           ) where U<:Fetchables
    tckr = getparams(obs, "ticker")
    _foto, bunch = fotografi(HRAFNHILDUR,HJORDIS,obs)[tckr], _to_bunch(obs)
    cal, tckr    = getparams(obs,"calendar",US), getparams(obs,"ticker")
    if isless(_foto["LastFoto"], _foto["Date"])
      throw("BRYNHILD[$(U)]> outdated FOTO detected for $(tckr)")
    end
    _nxtbd = listbdays(cal,_foto["Date"]+Day(1),today())
    filter!(<(today()), _nxtbd)
    if isempty(_nxtbd)
      @info "BRYNHILD[$(U)]> $(tckr)[up-to-date] " * Char(0x2714)
      return nothing
    end
    dfs=fetch(
              obs, HJORDIS, _nxtbd, interval=interval, force=force,
              localdb=localdb, return_data=return_data, timeout=timeout
        )
    return_data ? dfs : nothing
  end
  
  function update(
                  obs::AbstractVector{U},
                  ::Hjordis;
                  force::Bool=false,
                  interval::AbstractString="1m",
                  localdb::ZsofiaDB=HRAFNHILDUR,
                  return_data::Bool=false,
                  timeout::Int=30
           ) where U<:YahooFetchables
    _taskies =  map(obs) do _obs
      @task update(
                   _obs, HJORDIS, force=force, interval=interval,
                   localdb=localdb, return_data=return_data, timeout=timeout
            )
    end
    (schedule.(_taskies); wait.(_taskies))
    _f_tasks = filter(istaskfailed, _taskies)
    n, m = length(_taskies), length(_f_tasks)
    filter!(istaskdone, _taskies)
    if isless(0,m)
      @info "HJORDIS[$(U)]> some failures occurrences " failures=_f_tasks
    elseif iszero(length(_taskies))
      @info "HJORDIS[$(U)]> updation wholly failed " * Char(0x2718)
    end
    if return_data
      dfs = Dict{AbstractString,AbstractDataFrame}()
      for _task in _taskies
        dfs[last(getfield(_task,:result).Tckr)] = getfield(_task,:result)
      end
      dfs
    else
      nothing
    end
  end

  #######################################################
  #                   DELETTION FACILITY                #
  #######################################################

  function Base.delete!(db::ZsofiaDB, stock::Stock, ::Bjorn, date::Date)
    _p = _path_to(db, stock, BJORN, date)
    if ~ispath(_p)
      @warn """
            No BJORN record in Stock[$(_stock_silo(stock))] for the $(date)
            trading session.\n
            NO DELETION PERFORMED.
            """
      return nothing
    end
    _em = collect(db,BJORN,date,stock)[_stock_silo(stock)]
    got = "n"
    msg = """
          You are trying to delete $(getparams(stock,"ticker"))
          from the $(string(db)) data base, within the BJORN context
          related to the $(date) trading session.\n
          This will cause $(length(_em)) tickers to be deleted.\n
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readuntil(stdin, "\n")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "BJORN's $(date) trading session deleted! " * Char(0x2714)
    else
      @info "No deletion performed, BJORN's $(date) trading session " *
            "kept back."
    end
  end
  
  function Base.delete!(db::ZsofiaDB, stock::Stock, ::Brynhild)
    _p = _path_to(db, stock, BRYNHILD)
    tckr = getparams(stock,"ticker")
    if ~ispath(_p)
      @info """
            No BRYNHILD record in Stock[$(_stock_silo(stock))] for the
            $(tckr) ticker symbol\nNO DELETION PERFORMED.
            """
      return nothing
    end
    got = "n"
    msg = """
          You are trying to delete $(tckr) from the $(string(db)) data base
          within the BRYNHILD context.
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readundil(stdin,"\n")
    if in(lowercase(got),("yes","y"))
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "BRYNHILD's $(tckr) ticker symbol deleted! "
    else
      @info "No deletion performed, BRYNHILD's $(tckr) is kept back!"
    end
  end

  function Base.delete!(db::ZsofiaDB, stock::Stock, ::Valtyr)
    _p = _path_to(db, stock, VALTYR)
    tckr = getparams(stock,"ticker")
    if ~ispath(_p)
      @info """
            No VALTYR record in Stock[$(_stock_silo(stock))] for the
            $(tckr) ticker symbol\nNO DELETION PERFORMED.
            """
      return nothing
    end
    got = "n"
    msg = """
          You are trying to delete $(tckr) from the $(string(db)) data base
          within the VALTYR context.
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readundil(stdin,"\n")
    if in(lowercase(got),("yes","y"))
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "VALTYR's $(tckr) ticker symbol deleted! "
    else
      @info "No deletion performed, VALTYR's $(tckr) is kept back!"
    end
  end

  function Base.delete!(db::ZsofiaDB, stock::Stock, ::Hjordis, date::Date)
    _p = _path_to(db, stock, HJORDIS, date)
    if ~ispath(_p)
      @info """
            No HJORDIS record in Stock[$(_stock_silo(stock))] for the
            $(date) trading session.\n
            NO DELETION PERFORMED.
            """
      return nothing
    end
    got = "n"
    msg = """
          You are trying to delete $(getparams(stock,"ticker"))
          from the $(string(db)) data base, within the HJORDIS context
          related to the $(date) trading session.
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readuntil(stdin, "\n")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "HJORDIS' $(date) trading session deleted! " * Char(0x2714)
    else
      @info "No deletion performed, BJORN's $(date) trading session " *
            "kept back."
    end
  end
  
  function Base.delete!(db::ZsofiaDB,obs::U,::Brynhild) where U<:Fetchables
    _p = _path_to(db, obs, BRYNHILD)
    tckr = getparams(obs,"ticker")
    if ~ispath(_p)
      @info """
            No BRYNHILD record for $(U)[$(tckr)] ticker symbol\n
            NO DELETION PERFORMED.
            """
      return nothing
    end
    got = "n"
    msg = """
          You are trying to delete $(U)[$(tckr)] from the $(string(db))
          data base within the BRYNHILD context.
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readundil(stdin,"\n")
    if in(lowercase(got),("yes","y"))
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "BRYNHILD's $(U)[$(tckr)] ticker symbol deleted! "
    else
      @info "No deletion performed, BRYNHILD's $(U)[$(tckr)] is kept back!"
    end
  end
  
  function Base.delete!(db::ZsofiaDB,obs::U,::Valtyr) where U<:Fetchables
    _p = _path_to(db, obs, VALTYR)
    tckr = getparams(obs,"ticker")
    if ~ispath(_p)
      @info """
            No VALTYR record for $(U)[$(tckr)] ticker symbol\n
            NO DELETION PERFORMED.
            """
      return nothing
    end
    got = "n"
    msg = """
          You are trying to delete $(U)[$(tckr)] from the $(string(db))
          data base within the VALTYR context.
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readundil(stdin,"\n")
    if in(lowercase(got),("yes","y"))
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "VALTYR's $(U)[$(tckr)] ticker symbol deleted! "
    else
      @info "No deletion performed, VALTYR's $(U)[$(tckr)] is kept back!"
    end
  end

  function Base.delete!(
                        db::ZsofiaDB,
                        obs::U,
                        ::Hjordis,
                        date::Date
                ) where U<:Fetchables
    _p = _path_to(db, HJORDIS, obs, date) 
    tckr = getparams(obs,"ticker")
    if ~ispath(_p)
      @warn """
            No HJORDIS record in $(U) for the $(date) trading session.\n
            NO DELETION PERFORMED.
            """
      return nothing
    end
    _em = collect(db,HJORDIS,date,obs) # it's crazy, fix `collect`'s order
    got = "n"
    msg = """
          You are trying to delete $(tckr) from the $(string(db)) data base
          within the HJORDIS context related to the $(date) trading session.\n
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readuntil(stdin, "\n")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "HJORDIS' $(U)[$(date)] trading session deleted! " * Char(0x2714)
    else
      @info "No deletion performed, HJORDIS' $(U)[$(date)] trading session " *
            "kept back."
    end
  end

  function Base.delete!(
                        db::ZsofiaDB,
                        obs::U,
                        ::Bjorn,
                        date::Date
                ) where U<:Fetchables
    _p = _path_to(db, obs, BJORN, date)
    tckr = getparams(obs,"ticker")
    if ~ispath(_p)
      @warn """
            No BJORN record in $(U) for the $(date) trading session.\n
            NO DELETION PERFORMED.
            """
      return nothing
    end
    _em = collect(db,BJORN,date,obs)
    got = "n"
    msg = """
          You are trying to delete $(tckr) from the $(string(db)) data base
          within the BJORN context related to the $(date) trading session.\n
          This will cause $(length(_em)) tickers to be deleted.\n
          
          Continue? [y/n]:  
          """
    typpyst(msg,kol=11)
    got = readuntil(stdin, "\n")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      rm(_p, force=true)
      print("\033[2J")
      @warn "BJORN's $(U)[$(date)] trading session deleted! " * Char(0x2714)
    else
      @info "No deletion performed, BJORN's $(U)[$(date)] trading session " *
            "kept back."
    end
  end

  function Base.delete!(
                        db::ZsofiaDB,
                        obs::Vector{U},
                        ::Bjorn,
                        date::Date
                ) where U<:Fetchables
    @assert ~isempty(obs)
    _paths = map(x -> _path_to(db,x,BJORN,date), stocks)
    _inds = findall(isfile, _paths)
    got = "n"
    msg = ""
    m = /(100length(_inds),length(obs))
    if ~isempty(_inds)
      msg *= """
             $(m)% of Observables given
             as argument are present in $(string(db))...\n
             """
    end
    filter!(isfile, _paths)
    if isempty(_paths)
      msg *= "exiting..."
      typpyst(msg,kol=11)
      return nothing
    end
    msg *= "The following Bjorn's Observables will be removed "*
           "from $(string(db)):\n"
    typpyst(msg,kol=11)
    _tckrs = getparams.(getindex(obs,_inds),"ticker")
    _tckrs .= _tckrs .* "<" .* string.(typeof.(getindex(obs,_inds))) .* ">, "
    _msg = reduce(*, _tckrs)
    printstyled(_msg,color=102)
    printstyled("\nContinue [y/n]?  ")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      print("\033[2J")
      for _path in _paths
        rm(_path, force=true)
      end
      @warn "all of $(m) stocks have been deleted!"
    else
      typpyst("everything is kept back...", color=11)
      return nothing
    end
  end

  function Base.delete!(
                        db::ZsofiaDB,
                        obs::Vector{U},
                        ::Brynhild
                ) where U<:Fetchables
    @assert ~isempty(obs)
    _paths = map(x -> _path_to(db,x,BRYNHILD), obs)
    _inds = findall(isfile, _paths)
    got = "n"
    msg = ""
    m = /(100length(_inds),length(obs))
    if ~isempty(_inds)
      msg *= """
             $(m)% of $(U) given as argument are present in $(string(db))...\n
             """
    end
    filter!(isfile, _paths)
    if isempty(_paths)
      msg *= "exiting..."
      typpyst(msg,kol=11)
      return nothing
    end
    msg *= "The following BRYNHILD's $(U) will be removed from $(string(db)):\n"
    typpyst(msg,kol=11)
    _tckrs = getparams.(getindex(obs,_inds),"ticker")
    _tckrs .= _tckrs .* "<" .* string.(typeof.(getindex(obs,_inds))) .* ">, "
    _msg = reduce(*, _tckrs)
    printstyled(_msg,color=102)
    printstyled("\nContinue [y/n]?  ")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      print("\033[2J")
      for _path in _paths
        rm(_path, force=true)
      end
      @warn "all of $(m) stocks have been deleted!"
    else
      typpyst("everything is kept back...", color=11)
      return nothing
    end
  end

  function Base.delete!(
                        db::ZsofiaDB,
                        obs::Vector{U},
                        ::Hjordis,
                        date::Date
                ) where U<:Fetchables
    @assert ~isempty(obs)
    _paths = map(x -> _path_to(db,x,HJORDIS,date), stocks)
    _inds = findall(isfile, _paths)
    got = "n"
    msg = ""
    m = /(100length(_inds),length(obs))
    if ~isempty(_inds)
      msg *= """
             $(m)% of Observables given
             as argument are present in $(string(db))...\n
             """
    end
    filter!(isfile, _paths)
    if isempty(_paths)
      msg *= "exiting..."
      typpyst(msg,kol=11)
      return nothing
    end
    msg *= "The following HJORDIS Observables will be removed "*
           "from $(string(db)):\n"
    typpyst(msg,kol=11)
    _tckrs = getparams.(getindex(obs,_inds),"ticker")
    _tckrs .= _tckrs .* "<" .* string.(typeof.(getindex(obs,_inds))) .* ">, "
    _msg = reduce(*, _tckrs)
    printstyled(_msg,color=102)
    printstyled("\nContinue [y/n]?  ")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      print("\033[2J")
      for _path in _paths
        rm(_path, force=true)
      end
      @warn "all of $(m) stocks have been deleted!"
    else
      typpyst("everything is kept back...", color=11)
      return nothing
    end
  end

  function Base.delete!(
                        db::ZsofiaDB,
                        obs::Vector{U},
                        ::Valtyr
                ) where U<:Fetchables
    @assert ~isempty(obs)
    _paths = map(x -> _path_to(db,x,VALTYR), obs)
    _inds = findall(isfile, _paths)
    got = "n"
    msg = ""
    m = /(100length(_inds),length(obs))
    if ~isempty(_inds)
      msg *= """
             $(m)% of $(U) given as argument are present in $(string(db))...\n
             """
    end
    filter!(isfile, _paths)
    if isempty(_paths)
      msg *= "exiting..."
      typpyst(msg,kol=11)
      return nothing
    end
    msg *= "The following VALTYR's $(U) will be removed from $(string(db)):\n"
    typpyst(msg,kol=11)
    _tckrs = getparams.(getindex(obs,_inds),"ticker")
    _tckrs .= _tckrs .* "<" .* string.(typeof.(getindex(obs,_inds))) .* ">, "
    _msg = reduce(*, _tckrs)
    printstyled(_msg,color=102)
    printstyled("\nContinue [y/n]?  ")
    if in(lowercase(got),("yes","y"))
      printstyled("OK,\n",color=11)
      printstyled("deleting ...", sleep=3, blink=true, color=11)
      print("\033[2J")
      for _path in _paths
        rm(_path, force=true)
      end
      @warn "all of $(m) stocks have been deleted!"
    else
      typpyst("everything is kept back...", color=11)
      return nothing
    end
  end
end