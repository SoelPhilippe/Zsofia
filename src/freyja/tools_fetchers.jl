# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _path_in_bjorn(db::ZsofiaDB, bunch::String, date::Date)
      
  # Internal Facility
  Return a function returning paths to BJORN records' having its provided
  argument as intermediate directory.
  
  # Arguments
  - `db::ZsofiaDB`: the required Zsofia data base.
  - `bunch::AbstractString`: a bunch, (an element of glob `BUNCH`).
  - `date::Date`: `BJORN` record's date.
  
  # Notes
  The returned function taskes `arg::AbstractString` and return the
  path to the corresponding `Bjorn` record having `arg` as intermediate
  directory. The facility is intended for `"Artemis"` element of `BUNCH`
  which has another middle layer for geographic zones.
  
  # Examples
  ```julia
  julia> bunch = "Artemis";
  
  julia> f = _path_to_bjorn(HRAFNHILDUR, bunch, today());
  
  julia> p1 = Zsofia.UNNUR["paths"]["Hrafnhildur"];
  
  julia> fyle = bunch * "-intradays-" * string(today()) * ".csv";
  
  julia> p = joinpath(p1,bunch,"Bjorn","ABC",fyle);
  
  julia> isequal(p, f("ABC"))
  true
  ```
  """
  function _path_in_bjorn(db::ZsofiaDB, bunch::AbstractString, date::Date) 
    function _loo(x::AbstractString)
      joinpath(
               UNNUR["paths"][string(db)],
               bunch,
               "Bjorn",
               x,
               bunch * "-intradays-" * string(date) * ".csv"
      )
    end
    return _loo
  end

  """
      _isempty_zc(db::ZsofiaDB, bunch::AbstractString, zc::ZsofiaDBContext)
  
  # Internal Facility
  Return `true` if `zc` is empty or does not exist in `bunch` (should be
  element of global constant `BUNCH`), into the data base `db`; return `false`
  otherwise. See [`BUNCH`](@ref).

  # Arguments
  - `db::ZsofiaDB`: Zsofia's data base instance (eg. `HRAFNHILDUR`).
  - `bunch::AbstractString`: an element of glob `BUNCH`.
  - `zc::ZsofiaDBContext`: a Zsofia data base context (`BRYNHILD`...).

  # Notes
  The non-emptiness is asserted whenever contained files (if any) is not
  zero-sized. If the size is null, the return value if `true`. If the
  data base does not exist, an `AssertionError` is thrown. 
  The checker is physical, the facility walks the directory to perform
  size checking of the files and assert the emptiness. The only high-level
  facility used is `UNNUR["paths"]`, to check if database is initialized.
  """
  function _isempty_zc(db::ZsofiaDB,bunch::AbstractString,zc::ZsofiaDBContext)
    @assert in(bunch, BUNCH)
    @assert haskey(UNNUR["paths"], string(db))
    _wd = walkdir(joinpath(UNNUR["paths"][string(db)],bunch,string(zc)))
    ~any(isless(25,lstat(joinpath(p[1],j)).size) for p in _wd for j in p[3])
  end
  
  """
      _collect(db::ZsofiaDB, [bunch::String, [::ZsofiaDBContext], [date::Date])
  
  # Internal Facility
  Return a `Vector{<:AbstractString}` or a dictionary having as keys some
  `Union{Date,AbstractString}` type objects and `Vector{<:AbstractString}` as
  values type. The values are meant to be vectors of ticker symbols physically
  found into the specified pit determined by `db`, `bunch`, `::ZsofiaDBContext`
  and `date`.
  
  # Arguments
  - `db::ZsofiaDB`: a Zsofia's data base object.
  - `bunch::String`: a bunch, element of `BUNCH`, see [`BUNCH`](@ref).
  - `::ZsofiaDBContext`: either `BJORN`, `BRYNHILD`, `HJORDIS` or `VALTYR`.
  - `date::Date`: when `ZsofiaDBContext` is `Bjorn` or `Hjordis`, relevant.
  
  # Notes
  This facility works at the physical level, the only thing inferred from
  high-level objects is the path, relying on `UNNUR["paths"]` values to
  get informations about what is actually in the data base.  
  For bunches like `"Artemis"` which has many layers related to their
  geographic zones, the return value will be a dictonary keyed by their
  relevant geographic identifiers. Another nesting layer may be/is added
  if required `::ZsofiaDBContext` is `Bjorn` or `Hjordis` wich are fetched
  trading sessions wise identified by the very trading session dates.
  """
  function _collect(db::ZsofiaDB,bunch::AbstractString,::Brynhild)
    @assert ~_isempty_zc(db,bunch,BRYNHILD)
    p = joinpath(UNNUR["paths"][string(db)],bunch,"Brynhild")
    ispath(p) || throw(SystemError("$(bunch)'s Brynhild",2))
    if isequal(bunch,"Artemis")
      _g = begin
        (
         s => readdir(
                      joinpath(p,s),
                      join=false
              ) for s=readdir(p,join=false) if isdir(joinpath(p,s))
        )
      end
      dict = Dict{AbstractString,Vector{AbstractString}}(_g)
      for k in keys(dict)
        map!(x -> replace(x,".csv" => ""), dict[k], dict[k])
      end
      return dict
    else
      map(readdir(p,join=false)) do x
        isfile(joinpath(p,x)) ? x : nothing
      end |> (x -> filter(!isnothing,x)) |> (x -> replace.(x,".csv" => ""))
    end
  end
  
  function _collect(db::ZsofiaDB,bunch::AbstractString,::Bjorn,date::Date)
    @assert in(bunch, BUNCH)
    p = _path_in_bjorn(db,bunch,date)
    if ~ispath(dirname(p("")))
      @warn "$(bunch)'s Bjorn does not exist for trading session $(date)"
      if isequal(bunch,"Artemis")
        return Dict{AbstractString,Vector{AbstractString}}()
      else
        return String[]
      end
    end
    if isequal(bunch,"Artemis")
      _silos = readdir(dirname(p("")), join=false)
      filter!(x -> isdir(dirname(p(x))), _silos)
      dict = begin
        Dict{
             AbstractString,
             Vector{AbstractString}
        }(
          s => convert(
                       Vector{AbstractString},
                       CSVFile(
                               p(s),
                               select=["Tckr"],
                               stringtype=String,
                               types=Dict(:Tckr => String)
                       ).Tckr
               ) for s in _silos if isfile(p(s))
        )
      end
      map!(unique,values(dict))
      return dict
    else
      unique(
             convert(
                     Vector{AbstractString},
                     CSVFile(
                             p(""),
                             select=["Tckr"],
                             stringtype=String,
                             types=Dict(:Tckr => String)
                     ).Tckr
             )
      )
    end
  end
  
  function _collect(db::ZsofiaDB,bunch::AbstractString,::Bjorn)
    @assert ~_isempty_zc(db,bunch,BJORN)
    p = joinpath(UNNUR["paths"][string(db)],bunch,"Bjorn")
    if isequal(bunch,"Artemis")
      _silos = readdir(p,join=false)
      filter!(x -> isdir(joinpath(p,x)), _silos)
      dict = begin
        Dict{AbstractString,Vector{AbstractString}}(
          s => map(
                   x -> match(r"\d{4}-\d{2}-\d{2}",x).match,
                   readdir(joinpath(p,s),join=false)
               ) for s in _silos
        )
      end
      filter!(x -> ~isempty(x.second), dict)
      dee = Dict{AbstractString,Dict{Date,Vector{AbstractString}}}()
      @threads for s in collect(keys(dict))
        get!(dee,s,Dict{Date,Vector{AbstractString}}())
        for ts in dict[s]
          dee[s][Date(ts)] = begin
            convert(
                    Vector{AbstractString},
                    CSVFile(
                            joinpath(p, s, bunch * "-intradays-" * ts * ".csv"),
                            select=["Tckr"],
                            stringtype=String,
                            types=Dict(:Tckr => String)
                    ).Tckr
            ) |> unique
          end
        end
      end
    else
      dee = Dict{Date,Vector{AbstractString}}()
      _dates = readdir(p,join=false)
      map!(fm -> match(r"\d{4}-\d{2}-\d{2}",fm).match, _dates, _dates)
      filter!(!isnothing, fm)
      @threads for ts in _dates
        dee[Date(ts)] = _collect(db, bunch, BJORN, Date(ts))
      end
    end
    dee
  end
  
  function _collect(db::ZsofiaDB, bunch::AbstractString, ::Valtyr)
    @assert ~_isempty_zc(db,bunch,VALTYR)
    p = joinpath(UNNUR["paths"][string(db)],bunch,"Valtyr")
    _silos = readdir(p)
    if isequal(bunch, "Artemis")
      Dict{AbstractString,Vector{AbstractString}}(
        _silo => map(
                     x -> replace(x, ".toml" => ""),
                     readdir(joinpath(p, _silo), join=false)
                ) for _silo=_silos
      )
    else
      filter!(endswith(".toml"), _silos)
      map!(x -> replace(x,".toml" => ""), _silos, _silos)
      _silos
    end
  end
  
  function _collect(db::ZsofiaDB,bunch::AbstractString,::Hjordis,date::Date)
    @assert in(bunch,BUNCH)
    if isequal(bunch,"Artemis")
      p = joinpath(
                   UNNUR["paths"][string(db)],
                   bunch, "Hjordis", "Hjordis_" * string(date)
          )
      _silos = readdir(p,join=false)
      filter!(x -> isdir(joinpath(p,x)), _silos)
      Dict{AbstractString,Any}(
        s => map(
                 x -> replace(x,".csv" => ""),
                 filter(endswith(".csv"),readdir(joinpath(p,s),join=false))
             ) for s in _silos
      )
    else
      p = joinpath(
                   UNNUR["paths"][string(db)],
                   bunch,
                   "Hjordis",
                   "Hjordis_" * string(date)
          )
      map(
          x -> replace(x, ".csv" => ""),
          filter(endswith(".csv"),readdir(p,join=false))
      )
    end
  end
  
  function _collect(db::ZsofiaDB, bunch::AbstractString, ::Hjordis)
    @assert in(bunch,BUNCH)
    if isequal(bunch,"Artemis")
      p = joinpath(UNNUR["paths"][string(db)],bunch,"Hjordis")
      _silos = readdir(p,join=false)
      filter!(x -> isdir(joinpath(p,x)),_silos)
      _silos_dt = begin
        Dict{AbstractString,Vector}(
          s => map(
                   x -> match(r"\d{4}-\d{2}-\d{2}",x),
                   readdir(joinpath(p,s),join=false)
               ) for s in _silos
        )
      end
      filter!(v -> ~isnothing(v.second), _silos_dt)
      filter!(v -> ~isempty(v.second), _silos_dt)
      _dts = Date.(union(_silos_dt[silo] for silo in _silos))
      dee = Dict{Date,Any}()
      @threads for dt in _dts
        dee[dt] = _collect(db,bunch,HJORDIS,dt)
      end
    else
      p = joinpath(UNNUR["paths"][string(db)],bunch,"Hjordis")
      _dates = readdir(p, join=false)
      filter!(x -> isdir(joinpath(p,x)),_dates)
      map!(x -> match(r"\d{4}-\d{2}-\d{2}",x).match,_dates)
      filter!(!isnothing, _dates)
      _dts = Vector{Date}(undef,length(_dates))
      map!(Date,_dts,_dates)
      dee = Dict{Date,Any}()
      @threads for dt in _dts
        dee[dt] = _collet(db,bunch,HJORDIS,dt)
      end
    end
    dee
  end
  
  function _collect(db::ZsofiaDB,zc::ZsofiaDBContext)
    Dict{AbstractString,Any}(
       bunch => _collect(db,bunch,zc)
       for bunch=BUNCH if ~_isempty_zc(db,bunch,zc)
    )
  end
  
  function _collect(db::ZsofiaDB,::Bjorn,date::Date)
    Dict{AbstractString,Any}(
      bunch => _collect(db,bunch,BJORN,date) for bunch in BUNCH
    )
  end
  
  function _collect(db::ZsofiaDB,::Hjordis,date::Date)
    Dict{AbstractString,Any}(
      bunch => _collect(db,bunch,HJORDIS,date) for bunch in BUNCH
    )
  end
  
  _collect(zc::ZsofiaDBContext) = _collect(HRAFNHILDUR,zc)
end


# [_to_bunch] facility
begin
  """
      _to_bunch(obs::Observables)
      _to_bunch(obs::Type{<:Observables})
  
  # Internal facility
  Return the ad-hoc bunch (element of `BUNCH`) associated to `obs`.
  
  # Notes
  The association with *bunches* etc is the product of author's own
  conventions, author's own design and may change without notice. 
  In the current implementation, the convention is the following:
  
  |**Bunch**|**Description**|
  |:---:|:---:|
  |`Artemis`|in general, `Stock`s|
  |`Demeter`|non-`Equity` `Futures`|
  |`Euterpe`|`NonMarketables` observables, `MarketIndicators`|
  |`Mercure`|`EconoIndicator`s|
  |`Nemesis`|`Futures` on `Equity`, `Indices{Equity}`|
  |`Ophelya`|`ETF`s|
  |`Origami`|`Derivatives`, any `Contracts` no-elsewhere silo'ed|
  |`Phorcys`|`FXRate`|
  |`Satoshi`|`CryptoCurrency`|
  """
  _to_bunch(::Observables) = "Origami"
  _to_bunch(::Type{Observables}) = "Origami"
  
  _to_bunch(::Stock) = "Artemis"
  _to_bunch(::Type{Stock}) = "Artemis"
  
  _to_bunch(::Futures{U}) where U<:Equity = "Nemesis"
  _to_bunch(::Type{Futures{U}}) where U<:Equity = "Nemesis"
  
  _to_bunch(::Futures{U}) where U<:Indices{V} where V<:Equity = "Nemesis"
  _to_bunch(::Type{Futures{U}}) where U<:Indices{V} where V<:Equity = "Nemesis"
  
  _to_bunch(::Futures) = "Demeter"
  _to_bunch(::Type{Futures}) = "Demeter"
  
  _to_bunch(::ETF) = "Ophelya"
  _to_bunch(::Type{ETF}) = "Ophelya"
  
  _to_bunch(::FXRate) = "Phorcys"
  _to_bunch(::Type{FXRate}) = "Phorcys"
  
  _to_bunch(::CryptoCurrency) = "Satoshi"
  _to_bunch(::Type{CryptoCurrency}) = "Satoshi"
  
  _to_bunch(::Indices) = "Euterpe"
  _to_bunch(::Type{Indices}) = "Euterpe"
  
  _to_bunch(::EconoIndicator) = "Mercure"
  _to_bunch(::Type{EconoIndicator}) = "Mercure"
  
  _to_bunch(::T) where T<:Union{Options,Swaps} = "Origami"
  _to_bunch(::Type{T}) where T<:Union{Options,Swaps} = "Origami"
end

# [_apitckr] facility
begin
  """
      _apitckr(obs::Observables)
  
  # Internal Facility
  Return the appropriate api-friendly ticker symbol. 
  
  # Notes
  This facility is built with the `fetch`ing facility in mind and is
  purposely design to fit the Yahoo Finance API.
  
  In the current implementation, the whole Zsofia's fetching facility relies
  on Yahoo Finance and therefore is very sensitive to changes of any kind.
  That said, without any notice and at any time, the software segment
  depending on this implementation may break. Another robust implementation
  is needed here for fetching market data.
  """
  function _apitckr(stock::Stock)
    @assert isa(getparams(stock,"calendar",US),HolidayCalendar)
    getparams(stock,"ticker") * APIstocks[getparams(stock,"calendar",US)]
  end
  
  function _apitckr(sym::AbstractString,::Type{Stock},cal::HolidayCalendar)
    sym * APIstocks[cal]
  end

  """
      _stock_silo(stock::Stock)
  
  # Internal Facility
  For `Stock`s, return the appropriate silo to which `stock` belongs to.
  
  # Examples
  ```
  julia> bnp = Stock("BNP");
  
  julia> setparams!(bnp, "calendar", FranceEx());
  
  julia> _stock_silo(bnp)
  "PA"
  
  julia> pltr = Stock("PLTR");
  
  julia> setparams!(pltr, "calendar", UnitedStatesEx());
  
  julia> _stock_silo(pltr)
  "US"
  ```
  """
  function _stock_silo(stock::Stock)
    i = match(r"\.(.*)",_apitckr(stock))
    isnothing(i) ? "US" : replace(getfield(i,:match),"." => "")
  end
  
  function _stock_silo(cal::HolidayCalendar)
    isempty(APIstocks[cal]) ? "US" : replace(APIstocks[cal], "." => "")
  end
  
  _apitckr(obs::Observables) = getparams(obs,"ticker")
  
  function _apitckr(fut::Futures{U}) where U<:Equity
    getparams(fut,"ticker") * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}},
                    ::HolidayCalendar
           ) where U<:Equity
    sym * "=F"
  end
  
  function _apitckr(sym::AbstractString,::Type{Futures{U}}) where U<:Equity
    sym * "=F"
  end
  
  function _apitckr(fut::Futures{U}) where U<:Indices{V} where V<:Equity
    getparams(fut,"ticker") * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}},
                    ::HolidayCalendar
           ) where U<:Indices{V} where V<:Equity
    sym * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}}
           ) where U<:Indices{V} where V<:Equity
    sym * "=F"
  end
  
  function _apitckr(fut::Futures{U}) where U<:Indices{V} where V<:Commodity
    getparams(fut,"ticker") * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}},
                    ::HolidayCalendar
           ) where U<:Indices{V} where V<:Commodity
    sym * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}}
           ) where U<:Indices{V} where V<:Commodity
    sym * "=F"
  end
  
  function _apitckr(fut::Futures{U}) where U<:Commodity
    getparams(fut,"ticker") * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}},
                    ::HolidayCalendar
           ) where U<:Commodity
    sym * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}}
           ) where U<:Commodity
    sym * "=F"
  end
  
  function _apitckr(fut::Futures{U}) where U<:Securities
    getparams(fut,"ticker") * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}},
                    ::HolidayCalendar
           ) where U<:Securities
    sym * "=F"
  end
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{Futures{U}}
           ) where U<:Securities
    sym * "=F"
  end
  
  function _apitckr(obs::Futures{CryptoCurrency})
    getparams(obs,"ticker") * "=F"
  end
  
  _apitckr(sym::AbstractString,::Type{Futures{CryptoCurrency}}) = sym * "=F"
  
  _apitckr(etf::ETF) = getparams(etf,"ticker")
  
  _apitckr(sym::AbstractString,::Type{ETF},::HolidayCalendar) = sym
  
  _apictkr(sym::AbstractString,::Type{ETF}) = sym
  
  function _apitckr(op::Options{U}) where U<:Union{Stock,ETF}
    @assert isa(getfield(op,:expiry),Date)
    i, d = divrem(round(getfield(op,:strike),digits=3),1)
    i, d = floor(Int,i), floor(Int, 1000 * trunc(d,digits=3))
    *(
      getparams(getfield(op, :underlying),"ticker"),
      lpad(rem(year(getfield(op,:expiry)),100),2,"0"),
      lpad(month(getfield(op,:expiry)),2,"0"),
      lpad(day(getfield(op,:expiry)),2,"0"),
      ifelse(isequal(getfield(op,:option),Call),"C","P"),
      lpad(i,5,"0"),
      rpad(d,3,"0")
    )
  end
  
  _apitckr(fx::FXRate) = fx.foreign.ticker * fx.domestic.ticker * "=X"
  
  function _apitckr(sym::AbstractString, ::Type{FXRate}, ::HolidayCalendar)
    sym * "=X"
  end
  
  function _apictckr(sym::AbstractString, ::Type{FXRate})
    sym * "=X"
  end
  
  function _apitckr(crypto::CryptoCurrency)
    if isa(crypto.quoteccy,Currency)
      getparams(crypto,"ticker") * "-" * crypto.quoteccy.ticker
    elseif isa(getfield(crypto,:quoteccy),CryptoCurrency)
      getparams(crypto,"ticker") * "-" *
      getparams(crypto.quoteccy,"ticker")
    else
      throw("INCOMPLETE IMPLEMENTATION, RAISE TO MAINTAINERS")
    end
  end
  
  _apitckr(eco::EconoIndicator) = getparams(eco,"ticker")
  
  function _apitckr(
                    sym::AbstractString,
                    ::Type{EconoIndicator},
                    ::HolidayCalendar
           )
    sym
  end
  
  _apitckr(sym::AbstractString, ::Type{EconoIndicator}) = sym
  
  _apitckr(mktindex::Indices) =  "^" * getparams(mktindex, "ticker")
  
  function _apitckr(sym::AbstractString,::Type{Indices},::HolidayCalendar)
    "^" * sym
  end
  
  _apitckr(sym::AbstractString, ::Type{Indices}) = "^" * sym
end


# [_is_built] facility
begin
  """
      _is_built(db::ZsofiaDB)
  
  # Internal Facility
  Return `true` if `db` is empty, `false` if not empty and throw an
  `AssertionError` if `db` is not configured.
  """
  function _is_built(db::ZsofiaDB)
    if ~haskey(UNNUR["local_dbs"],string(db)) ||
       ~ispath(UNNUR["paths"][string(db)])
      throw(string(db) * " not configured, cannot path it!")
    end
    isempty(readdir(UNNUR["paths"][string(db)]))
  end
end

# [_path_to] facility
begin
  """
      _path_to([::ZsofiaDB,], obs::Observables, [::ZsofiaDBContext], [::Date])
      _path_to([::ZsofiaDB,], obs::Type{Observables}, [::ZsofiaDBContext])
  
  # Internal Facility
  Return the path to `obs` in `db`, under the specified `::ZsofiaDBContext`.
  
  # Notes
  In general, the path is built without any existence checking. When `::Date`
  is not specified, when it is relevant, the latest file is sought out. When
  `::ZsofiaD` is not specified, `HRAFNHILDUR` is assumed.
  
  In general, when the context ( `Bjorn`, `Hjordis`, `Brynhild`, `Valtyr`)
  is not specified, `Brynhild()` is assumed.
  """
  function _path_to(db::ZsofiaDB, stock::Stock, ::Bjorn, date::Date)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)],
             _to_bunch(stock), "Bjorn",
             _stock_silo(stock) * "-intradays-" * string(date) * ".csv"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    cal::HolidayCalendar,
                    ::Bjorn,
                    date::Date
           )
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(Stock), "Bjorn",
             _stock_silo(cal) * "-intradays-" * string(date) * ".csv"
    )
  end
  
  """
      _path_to(db::ZsofiaDB, stock::Stock, ::Bjorn)
      _path_to(db::ZsofiaDB, type{Stock}, cal::HolidayCalendar, ::Bjorn)
  
  # Internal Facility
  Return the latest record available in `db` for `Observables` sharing the same
  characteristics as the `stock`.
  
  # Notes
  Note that in the second method, the type is provided and the `cal`endar
  must be provided.
  """
  function _path_to(db::ZsofiaDB, stock::Stock, ::Bjorn)
    last(readdir(dirname(_path_to(db,stock,BJORN,Date(0,1,1))),join=true))
  end
  
  function _path_to(db::ZsofiaDB, ::Type{Stock}, cal::HolidayCalendar, ::Bjorn)
    last(readdir(dirname(_path_to(db,Stock,cal,BJORN,Date(0,1,1))),join=true))
  end
  
  """
      _path_to(db, stock::Stock, ::Hjordis, date::Date)
      _path_to(db, ::Type{Stock}, ticker , cal, ::Hjordis, date::Date)
  
  # Internal Facility
  Return the path to the record in `db`, identified by `ticker` for the
  within the `ZsofiaDBContext` `HJORDIS` for the `date` trading session.
  """
  function _path_to(db::ZsofiaDB, stock::Stock, ::Hjordis, date::Date)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(stock), "Hjordis",
             "Hjordis_" * string(date), _stock_silo(stock),
             getparams(stock,"ticker") * ".csv"
    )
  end

  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Hjordis,
                    date::Date
           )
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(Stock), "Hjordis",
             "Hjordis_" * string(date), _stock_silo(cal), sym * ".csv"
    )
  end
 
  """
      _path_to(db::ZsofiaDB, obs::U, ::Hjordis) where U<:Observables
      _path_to(db::ZsofiaDB, ::Type{U}, ::Hjordis) where U<:Observables
  
  # Internal Facility
  Return the latest records container in `db`, for `U`-typed `Observables`
  in the `HJORDIS`' `ZsofiaDBContext`.
  
  # Notes
  If none of them contains the record, return an empty `String`.
  """
  function _path_to(db::ZsofiaDB, stock::Stock, ::Hjordis)
    p = _path_to(db, stock, HJORDIS, Date(0,1,1))
    _t = getparams(stock, "ticker")
    _r = readdir((dirname ∘ dirname ∘ dirname)(p),join=true)
    _s = _stock_silo(stock)
    filter!(contains(r"\d{4}-\d{2}-\d{2}"),_r)
    filter!(isdir,_r)
    filter!(v -> in(_s,readdir(v)),_r)
    filter!(v -> in(_t * ".csv", readdir(joinpath(v,_s))),_r)
    if isempty(_r)
      ""
    else
      joinpath(last(_r),_s,_t * ".csv")
    end
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Hjordis
           )
    p = _path_to(db,Stock,sym,cal,HJORDIS,Date(0,1,1))
    _r = readdir((dirname ∘ dirname ∘ dirname)(p),join=true)
    _s = _stock_silo(cal)
    filter!(contains(r"\d{4}-\d{2}-\d{2}"),_r)
    filter!(isdir,_r)
    filter!(v -> in(_s,readdir(v)),_r)
    filter!(v -> in(sym * ".csv", readdir(joinpath(v,_s))),_r)
    if isempty(_r)
      ""
    else
      joinpath(last(_r), _s, sym * ".csv")
    end
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    cal::HolidayCalendar,
                    ::Hjordis
           )
   p = _path_to(db,Stock,"dummy",cal,HJORDIS,Date(0,1,1))
   _r = readdir((dirname ∘ dirname ∘ dirname)(p),join=true)
   filter!(v -> in(_stock_silo(cal),readdir(v)),_r)
   isempty(_r) ? "" : last(_r)
 end
  
  function _path_to(db::ZsofiaDB, stock::Stock, ::Brynhild)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(stock), "Brynhild",
             _stock_silo(stock), getparams(stock,"ticker") * ".csv"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Brynhild
           )
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(Stock), "Brynhild",
             _stock_silo(cal), sym * ".csv"
    )
  end                  
  
  _path_to(db::ZsofiaDB, stock::Stock) = _path_to(db, stock, BRYNHILD)
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar
           )
    _path_to(db,Stock,sym,cal,BRYNHILD)
  end
  
  function _path_to(db::ZsofiaDB, stock::Stock, ::Valtyr)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(stock), "Valtyr",
             _stock_silo(stock), getparams(stock, "ticker") * ".toml"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Valtyr
           )
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(Stock), "Valtyr",
             _stock_silo(cal), sym * ".toml"
    )
  end
  
  _path_to(stock::Stock, ::Brynhild) = _path_to(HRAFNHILDUR, stock, BRYNHILD)
  
  function _path_to(
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Brynhild
           )
    _path_to(HRAFNHILDUR,Stock,sym,cal,BRYNHILD)
  end
  
  _path_to(stock::Stock) = _path_to(stock, BRYNHILD)
  
  function _path_to(::Type{Stock},sym::AbstractString,cal::HolidayCalendar)
    _path_to(Stock,sym,cal,BRYNHILD)
  end
  
  function _path_to(stock::Stock, ::Bjorn, date::Date)
    _path_to(HRAFNHILDUR, stock, BJORN, date)
  end
  
  function _path_to(::Type{Stock}, cal::HolidayCalendar, ::Bjorn, date::Date)
    _path_to(HRAFNHILDUR,Stock,cal,BJORN,date)
  end
  
  _path_to(stock::Stock, ::Bjorn) = _path_to(HRAFNHILDUR, stock, BJORN)
  
  function _path_to(::Type{Stock}, cal::HolidayCalendar, ::Bjorn)
    _path_to(HRAFNHILDUR, Stock, cal, BJORN)
  end
  
  _path_to(stock, ::Hjordis) = _path_to(HRAFNHILDUR, stock, HJORDIS)
  
  function _path_to(
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Hjordis
           )
    _path_to(HRAFNHILDUR, Stock, sym, cal, HJORDIS)
  end
  
  _path_to(stock, ::Valtyr) = _path_to(HRAFNHILDUR, stock, VALTYR)
  
  function _path_to(
                    ::Type{Stock},
                    sym::AbstractString,
                    cal::HolidayCalendar,
                    ::Valtyr
           )
    _path_to(HRAFNHILDUR, Stock, sym, cal, VALTYR)
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    obs::U,
                    ::Bjorn,
                    date::Date
           ) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(obs), "Bjorn",
             _to_bunch(obs) * "-intradays-" * string(date) * ".csv"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{U},
                    ::Bjorn,
                    date::Date
           ) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(U), "Bjorn",
             _to_bunch(U) * "-intradays-" * string(date) * ".csv"
    )
  end
  
  function _path_to(db::ZsofiaDB,obs::U,::Bjorn) where U<:YahooFetchables
    last(readdir(dirname(_path_to(db,obs,BJORN,Date(0,1,1))),join=true))
  end
  
  function _path_to(db::ZsofiaDB,::Type{U},::Bjorn) where U<:YahooFetchables
    last(readdir(dirname(_path_to(db,U,BJORN,Date(0,1,1))),join=true))
  end
  
  function _path_to(db::ZsofiaDB,obs::YahooFetchables,::Hjordis,date::Date)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(obs), "Hjordis",
             "Hjordis_" * string(date), getparams(obs, "ticker") * ".csv"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{U},
                    sym::AbstractString,
                    ::Hjordis,
                    date::Date
           ) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(U), "Hjordis",
             "Hjordis_" * string(date), sym * ".csv"
    )
  end
  
  function _path_to(db::ZsofiaDB,obs::U,::Hjordis) where U<:YahooFetchables
    p = _path_to(db,obs,HJORDIS,Date(0,1,1))
    _r = readdir((dirname ∘ dirname)(p),join=true)
    filter!(contains(r"\d{4}-\d{2}-\d{2}"),_r)
    filter!(isdir,_r)
    filter!(v -> in(sym * ".csv", readdir(v)), _r)
    if isempty(_r)
      ""
    else
      joinpath(last(_r), sym * ".csv")
    end
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{U},
                    sym::AbstractString,
                    ::Hjordis
           ) where U<:YahooFetchables
    p = _path_to(db,U,sym,HJORDIS,Date(0,1,1))
    _r = readdir((dirname ∘ dirname)(p),join=true)
    filter!(contains(r"\d{4}-\d{2}-\d{2}"),_r)
    filter!(isdir,_r)
    filter!(v -> in(sym * ".csv", readdir(v)), _r)
    last(readdir((dirname ∘ dirname)(p),join=true))
    if isempty(_r)
      ""
    else
      joinpath(last(_r), sym * ".csv")
    end
  end
  
  function _path_to(db::ZsofiaDB,obs::U,::Brynhild) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(obs), "Brynhild",
             getparams(obs, "ticker") * ".csv"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{U},
                    sym::AbstractString,
                    ::Brynhild
           ) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)],
             _to_bunch(U), "Brynhild", sym * ".csv"
    )
  end
  
  function _path_to(db::ZsofiaDB,obs::U,::Valtyr) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(obs),
             "Valtyr", getparams(obs, "ticker") * ".toml"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{U},
                    sym::AbstractString,
                    ::Valtyr
           ) where U<:YahooFetchables
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(U),
             "Valtyr", sym * ".toml"
    )
  end
  
  function _path_to(obs::U,::Brynhild) where U<:YahooFetchables
    _path_to(HRAFNHILDUR, obs, BRYNHILD)
  end
  
  function _path_to(
                    ::Type{U},
                    sym::AbstractString,
                    ::Brynhild
           ) where U<:YahooFetchables
    _path_to(HRAFNHILDUR, U, sym, BRYNHILD)
  end
  
  function _path_to(obs::U) where U<:YahooFetchables
    _path(obs, BRYNHILD)
  end
  
  function _path_to(::Type{U}, sym::AbstractString) where U<:YahooFetchables
    _path_to(U, sym, BRYNHILD)
  end
  
  function _path_to(obs::U,::Bjorn) where U<:YahooFetchables
    _path_to(HRAFNHILDUR,obs,BJORN)
  end
  
  function _path_to(
                    ::Type{U},
                    sym::AbstractString,
                    ::Bjorn
           ) where U<:YahooFetchables
    _path_to(HRAFNHILDUR, U, sym, BJORN)
  end
  
  function _path_to(obs::U,::Hjordis) where U<:YahooFetchables
    _path_to(HRAFNHILDUR, obs, HJORDIS)
  end
  
  function _path_to(
                    ::Type{U},
                    sym::AbstractString,
                    ::Hjordis
           ) where U<:YahooFetchables
    _path_to(HRAFNHILDUR, U, sym, HJORDIS)
  end
  
  function _path_to(obs::U,::Valtyr) where U<:YahooFetchables
    _path_to(HRAFNHILDUR,obs,VALTYR)
  end
  
  function _path_to(
                    ::Type{U},
                    sym::AbstractString,
                    ::Valtyr
           ) where U<:YahooFetchables
    _path_to(HRAFNHILDUR, U, sym, VALTYR)
  end
  
  function _path_to(db::ZsofiaDB,obs::EconoIndicator,::Brynhild)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(obs), "Brynhild",
             getparams(obs, "ticker") * ".csv"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{EconoIndicator},
                    sym::AbstractString,
                    ::Brynhild
           )
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(EconoIndicator),
             "Brynhild", sym * ".csv"
    )
  end
  
  _path_to(obs::EconoIndicator,::Brynhild) = _path_to(HRAFNHILDUR,obs,BRYNHILD)
  
  function _path_to(::Type{EconoIndicator}, sym::AbstractString, ::Brynhild)
    _path_to(HRAFNHILDUR, EconoIndicator, sym, BRYNHILD)
  end
  
  _path_to(obs::EconoIndicator) = _path_to(obs,BRYNHILD)
  
  function _path_to(::Type{EconoIndicator}, sym::AbstractString)
    _path_to(EconoIndicator, sym, BRYNHILD)
  end
  
  function _path_to(db::ZsofiaDB,obs::EconoIndicator,::Valtyr)
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(obs), "Valtyr",
             getparams(obs,"ticker") * ".toml"
    )
  end
  
  function _path_to(
                    db::ZsofiaDB,
                    ::Type{EconoIndicator},
                    sym::AbstractString,
                    ::Valtyr
            )
    _is_built(db)
    joinpath(
             UNNUR["paths"][string(db)], _to_bunch(EconoIndicator),
             "Valtyr", sym * ".toml"
    )
  end
  
  _path_to(obs::EconoIndicator,::Valtyr) = _path_to(HRAFNHILDUR,obs,VALTYR)
  
  function _path_to(
                    ::Type{EconoIndicator},
                    sym::AbstractString,
                    ::Valtyr
           )
    _path_to(EconoIndicator, sym, VALTYR)
  end
end

#> [in] methods for observables
begin
  function Base.in(
                   obs::U,
                   db::ZsofiaDB;
                   silo::ZsofiaDBContext=BRYNHILD,
                   date::Date=today()
                ) where U<:YahooFetchables
    if isa(silo, Brynhild) || isa(silo, Valtyr)
      isless(25,getfield(stat(_path_to(db,obs,silo)),:size))
    elseif isa(silo, Bjorn) || isa(silo, Hjordis)
      isless(25,getfield(stat(_path_to(db,obs,silo,date)),:size))
    else
      throw("[incomplete implementation for $(silo)], contact the maintainer")
    end
  end
  
  function Base.in(
                   obs::EconoIndicator,
                   db::ZsofiaDB;
                   silo::ZsofiaDBContext=Brynhild
                )
    isless(25,stat(_path_to(db,obs,silo)).size)
  end
  
  function Base.in(
                   sym::AbstractString,
                   ::Type{Stock},
                   cal::HolidayCalendar,
                   ::Brynhild;
                   db::ZsofiaDB=HRAFNHILDUR
               )
    p = _path_to(db,Stock,sym,cal,BRYNHILD)
    ispath(p) ? isless(25,stat(p).size) : false
  end
  
  function Base.in(
                   date::Date,
                   ::Type{Stock},
                   cal::HolidayCalendar,
                   ::Bjorn;
                   db::ZsofiaDB=HRAFNHILDUR
               )
    p = _path_to(db,Stock,cal,BJORN,date)
    ispath(p) ? isless(25,stat(p).size) : false
  end
  
  function Base.in(
                   date::Date,
                   ::Type{Stock},
                   cal::HolidayCalendar,
                   ::Hjordis;
                   db::ZsofiaDB=HRAFNHILDUR
               )
    p = (dirname ∘ dirname ∘ dirname)(_path_to(db,Stock,"_",cal,HJORDIS,date))
    if ~isdir(p)
      return false
    end
    any(contains(string(date)),readdir(p))
  end
  
  function Base.in(
                   sym::AbstractString,
                   ::Type{Stock},
                   cal::HolidayCalendar,
                   ::Hjordis,
                   date::Date;
                   db::ZsofiaDB=HRAFNHILDUR
                )
    p = _path_to(db,Stock,sym,cal,HJORDIS,date)
    isfile(p) && isless(25,stat(p).size)
  end
  
  function Base.in(
                   sym::AbstractString,
                   ::Type{Stock},
                   cal::HolidayCalendar,
                   ::Valtyr;
                   db::ZsofiaDB=HRAFNHILDUR
                )
    p = _path_to(db,Stock,sym,cal,VALTYR)
    isfile(p) && isless(25,stat(p).size)
  end
  
  function Base.in(
                   sym::AbstractString,
                   ::Type{U},
                   ::Brynhild;
                   db::ZsofiaDB=HRAFNHILDUR
               ) where U<:Fetchables
    p = _path_to(db,U,sym,BRYNHILD)
    ispath(p) ? isless(25,stat(p).size) : false
  end
  
  function Base.in(
                   date::Date,
                   ::Type{U},
                   ::Bjorn;
                   db::ZsofiaDB=HRAFNHILDUR
               ) where U<:Fetchables
    p = _path_to(db,U,BJORN,date)
    ispath(p) ? isless(25,stat(p).size) : false
  end
  
  function Base.in(
                   date::Date,
                   ::Type{U},
                   ::Hjordis;
                   db::ZsofiaDB=HRAFNHILDUR
               ) where U<:Fetchables
    p = (dirname ∘ dirname)(_path_to(db,U,"_",cal,HJORDIS,date))
    if ~isdir(p)
      return false
    end
    any(contains(string(date)),readdir(p))
  end
  
  function Base.in(
                   sym::AbstractString,
                   ::Type{U},
                   ::Hjordis,
                   date::Date;
                   db::ZsofiaDB=HRAFNHILDUR
                ) where U<:Fetchables
    p = _path_to(db,U,sym,cal,HJORDIS,date)
    isfile(p) && isless(25,stat(p).size)
  end
  
  function Base.in(
                   sym::AbstractString,
                   ::Type{U},
                   ::Valtyr;
                   db::ZsofiaDB=HRAFNHILDUR
                ) where U<:Fetchables
    p = _path_to(db,U,sym,VALTYR)
    isfile(p) && isless(25,stat(p).size)
  end
end

#> fetchers...
begin
  """
      _query_url(::Brynhild, sym::AbstractString, from, to; <kwargs>)
      _query_url(::Hjordis, sym::AbstractString, from, to; <kwargs>)
      _query_url(::Bjorn, sym::AbstractString, from, to; <kwargs>)
      _query_url(::Valtyr, sym::AbstractString, from, to; <kwargs>)
  
  # Internal Facility
  Return the http(s) url query for the given ticker symbol `sym` as
  an `AbstractString`, under the given `ZsofiaDBContext` for the provided
  api version, `version`.
  
  # Arguments
  - `::ZsofiaDBContext`: either `Brynhild`, `Hjordis`, `Bjorn` or `Valyr`.
  - `sym::AbstractString`: the api-friendly ticker symbol.
  - `from::Union{Date,DateTime,Int}`: starting time point.
  - `to::Union{Date,DateTime,Int}`: ending time point.
  - `interval::AbstractString`: "1m","2m",...,"30m","1d","1w","1mo"...
  - `events::AsbtractString`: kwarg, one of ("history",...) 
  - `version::Int`: kwarg, either 7 or 8.

  # Notes
  Herebelow valid kwargs values when the context is `Brynhild`:
  |**Kwargs**|**Values**|
  |:---:|:---:|
  |`version::Int`|7, 8|
  |`events::AbstractString`|"history",|
  |`interval::AbstractString`|"1d","5d","1w","1mo"|
  
  Herebelow valid kwargs values when `Hjordis` or `Bjorn`:  
  |**kwargs**|**values**|
  |:---:|:---:|
  |`version::Int`|7, 8|
  |`events::AbstractString`|"history",|
  |`interval::AbstractString`|"1m","3m","5m","30m","90m"|
  """
  function _query_url(
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1d",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Int,B<:Int}
    "https://query2.finance.yahoo.com" *
    "/v" * string(version) * "/finance/chart/" *
    sym * "?period1=" * string(from) * "&period2=" *
    string(to) * "&interval=" * interval * "&events=" * events
  end
  
  function _query_url(
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1d",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Union{Date,DateTime},B<:Union{Date,DateTime}}
    _query_url(
               sym,
               floor(Int,datetime2unix(DateTime(from))),
               floor(Int,datetime2unix(DateTime(to))),
               interval=interval,
               events=events,
               version=version
    )
  end
  
  function _query_url(
                      ::Brynhild,
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1d",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Int,B<:Int}
    @assert in(interval, ("1d","5d","1w","1mo"))
    _query_url(sym,from,to,interval=interval,events=events,version=version)
  end
  
  function _query_url(
                      ::Brynhild,
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1d",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Union{Date,DateTime},B<:Union{Date,DateTime}}
    @assert in(interval, ("1d","5d","1w","1mo"))
    _query_url(sym,from,to,interval=interval,events=events,version=version)
  end

  function _query_url(
                      ::Hjordis,
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1m",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Int,B<:Int}
    @assert in(interval, ("1m","3m","5m","30m","90m"))
    _query_url(sym,from,to,interval=interval,events=events,version=version)
  end
  
  function _query_url(
                      ::Hjordis,
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1m",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Union{Date,DateTime},B<:Union{Date,DateTime}}
    @assert in(interval, ("1m","3m","5m","30m","90m"))
    _query_url(sym,from,to,interval=interval,events=events,version=version)
  end

  function _query_url(
                      ::Bjorn,
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1m",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Int,B<:Int}
    _query_url(
               HJORDIS,sym,from,to,
               interval=interval,events=events,version=version
    )
  end

  function _query_url(
                      ::Bjorn,
                      sym::AbstractString,
                      from::A,
                      to::B;
                      interval::AbstractString="1m",
                      events::AbstractString="history",
                      version::Int=8
           ) where {A<:Union{Date,DateTime},B<:Union{Date,DateTime}}
    _query_url(
               HJORDIS,sym,from,to,
               interval=interval,events=events,version=version
    )
  end
  
  function _query_url(
                      ::Valtyr,
                      sym::AbstractString;
                      events::AbstractString="history",
                      version::Int=8
           )
    nothing
  end

  """
      _fetch_yahoo(::Brynhild, sym::String, from::TimePoint, to::TimePoint)
      _fetch_yahoo(::Hjordis, sym::String, from::TimePoint, to::TimePoint)
      _fetch_yahoo(::Valtyr, sym::String, from::TimePoint, to::TimePoint)
  
  # Internal Facility
  Return a `DataFrame` holding the proceed from fetching data from
  [yahoo finance](finance.yahoo.com)

  # Notes
  The data is fetched under json file format has kinda the following structure
  when parsed into a `Dict` from raw json.
  
  Let's say `json::Dict{String,Any}` is the parsed file,
  1. `json::Dict{String,Any}`
     |**Keys**|**Values**|
     |:---:|:---:|
     |"chart"|`Dict{String,Any}`|
  1. `json["chart"]::Dict{String,Any}`
     |**Keys**|**Values**|
     |:---:|:---:|
     |"error"|`nothing` if `HTTP.get` succeeded|
     |"result"|`Dict{String,Any}`|
  1. `json["chart"]["result"]::Vector{Dict{String,Any}}`
     1-element `Vector{Dict{String,Any}}`
  1. `only(json["chart"]["result"])::Dict{String,Any}`
     |**Keys**|**Values**|
     |:---:|:---:|
     |"meta"|`Dict{String,Any}`|
     |"indicators"|`Dict{String,Any}`|
     |"timestamp"|`Vector{Any}`, unix times, parsable into DateTime|
  
  At this level, we can say that the field containing relevant informations
  is `only(json["chart"]["result"])::Dict{String,Any}`, let's say we've done
  `dict = only(json["chart"]["result"])::Dict{String,Any}`.
  This `Dict{String,Any}` object shall have the following features:  
     |**Keys**|**Values**|
     |:---:|:---:|
     |"meta"|`Dict{String,Any}`|
     |"indicators"|`Dict{String,Any}`|
     |"timestamp"|`Vector{Any}`, unix times, parsable into DateTime|
  
  Let's dive into every single element:  
  1. `dict["meta"]` is `Dict{String,Any}` having the following properties:
     |**Keys**|**Values**|
     |"chartPreviousClose"| |
     |"currency"||
     |"currentTradingPeriod"| |
     |"dataGranularity"||
     |"exchangeName"||
     |"exchangeTimezoneName"||
     |"fiftyTwoWeekHigh"||
     |"fiftyTwoWeekLow"| |
     |"firstTradeDate"| |
     |"fullExchangeName"||
     |"gmtoffset"||
     |"hasPrePostMarketData"||
     |"instrumentType"||
     |"longName"||
     |"previousClose"||
     |"priceHint"||
     |"range"||
     |"regularMarketDayHigh"||
     |"regularMarketDayLow"||
     |"regularMarketPrice"||
     |"regularMarketTime"||
     |"regularMarketVolume"||
     |"scale"||
     |"shortName"||
     |"symbol"||
     |"timezone"||
     |"tradingPeriods"||
     |"validRanges"||
  2. `dict["indicators"]::Dict{String,Any}` this is a 1-entry dict pointing to
     a 1-element vector. `only(dict["indicators"]["quote"])::Dict{String,Any}`:
     `only(dict["indicators"]["quote"])::Dict{String,Any}` is the dictionary
     gathering the features of interest. Observed features are summarized into
     the following table:
     |**ZsofiaDBContext**|**Values**|
     |---:|:---|
     |`BRYNHILD`|"high","volume", "open", "low", "close", "adjclose"|
     |`HJORDIS`|"high","volume", "open", "low", "close"|
     |`BJORN`|"high","volume", "open", "low", "close"|
  3. `dict["timestamp"]::Vector{Any}` gathering actual time stamps. In general
     the vector is made of unix time (seconds since unix epoch) to be parsed
     into `Vector{DateTime}` object if human readable object needed. The
     convertion can be done using the `Dates.unix2datetime` function.
  """
  function _fetch_yahoo(
                        zc::Brynhild,
                        sym::AbstractString,
                        from::A,
                        to::B;
                        interval::AbstractString="1d",
                        version::Int=8,
                        timeout::Int=30
    ) where {A<:Union{Int,Date,DateTime},B<:Union{Int,Date,DateTime}}
    got = HTTP.get(
                   _query_url(
                              zc, sym, from, to, interval=interval,
                               events="history", version=version
                    ),
                    connect_timeout=timeout
               )
    @assert isequal(getfield(got,:status),200)
    jz = Serde.parse_json(getfield(got,:body))
    @assert haskey(jz, "chart") &&
            haskey(jz["chart"], "result") &&
            haskey(only(jz["chart"]["result"]),"indicators") &&
            haskey(only(jz["chart"]["result"]),"timestamp")
    res = only(jz["chart"]["result"])
    res["timestamp"] = .+(
                          res["timestamp"],
                          get(res["meta"],"gmtoffset",0)
                       ) |> (x -> unix2datetime.(x))
    df = DataFrame(only(res["indicators"]["quote"]))
    insertcols!(df,1,:timestamp => res["timestamp"])
    insertcols!(
                df,
                :adjclose => only(
                                  res["indicators"]["adjclose"]
                             )["adjclose"]
    )
    renamedf!(titlecase, df)
  end

  function _fetch_yahoo(
                        zc::Hjordis,
                        sym::AbstractString,
                        from::A,
                        to::B;
                        interval="1m",
                        version::Int=8,
                        timeout::Int=30
    ) where {A<:Union{Int,Date,DateTime},B<:Union{Int,Date,DateTime}}
    got = HTTP.get(
                   _query_url(
                              zc, sym, from, to, interval=interval,
                              events="history", version=version
                   ),
                   connect_timeout=timeout
               )
    @assert isequal(getfield(got, :status), 200)
    jz = Serde.parse_json(getfield(got, :body))
    @assert haskey(jz, "chart") &&
            haskey(jz["chart"], "result") &&
            haskey(only(jz["chart"]["result"]), "indicators") &&
            haskey(only(jz["chart"]["result"]), "timestamp")
    res = only(jz["chart"]["result"])
    res["timestamp"] = .+(
                          res["timestamp"],
                          get(res["meta"],"gmtoffset",0)
                       ) |> (x -> unix2datetime.(x))
    df = DataFrame(only(res["indicators"]["quote"]))
    insertcols!(df,1,:timestamp => res["timestamp"])
    renamedf!(titlecase, df)
  end
  
  function _fetch_yahoo(
                        zc::Bjorn,
                        sym::AbstractString,
                        from::A,
                        to::B;
                        interval="1m",
                        version::Int=8,
                        timeout::Int=30
    ) where {A<:Union{Int,Date,DateTime},B<:Union{Int,Date,DateTime}}
    _fetch_yahoo(
                 HJORDIS, sym, from, to, interval=interval,
                 version=version, timeout=timeout
    )
  end

  function _fetch_yahoo(zc::Valtyr, sym::AbstractString, version::Int=8)
    nothing
  end
end