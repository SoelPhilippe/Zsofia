#######################
##### Csilla::add #####
#######################
begin
  """
      csi_add(fi::FiZ,[rfyl])
  
  Add `fi` into Hrafnhildur within the Csilla Frmk.
  The function adds `fi` into Hrafnhildur, provided it's already been
  added under the Mercedesz silo.
  """
  function csi_add(fi::FiZ;rfyl::Union{String,IO}=Rfyl)::Union{Nothing,FiZ}
    #globs>
    begin
      t0 = Dates.now()
      d0 = tobday(Vala[fi.vala],Date(t0),forward=false)
      c0 = Unnur[string("CsillaDB_",fi.land)]
      l0 = Dict{String,Any}()
    end
    
    #main>
    rf = let p = joinpath(Silk,fi.land,string(fi.tckr,".csv"))
      if isfile(p) && in(fi.tckr,Unnur[fi.land])
        nothing
      else
        dfetch([fi],string(d0),rfyl=rfyl)
      end
    end
    
    if isnothing(rf)
      let
        d = dfetch([fi],string(d0),rfyl=rfyl)
        hludana(d,rfyl=rfyl)
      end
      ##----------------------------->>
      merge!(l0,parsefile(c0))
      ##----------------------------->>
      if in(fi.tckr,l0[string(d0)])
        printstyled(
                    "Looks like adding ",
                    fi.tckr," into ",fi.land,
                    " within the Csilla framework succeeded!\n\n",
                    "Now marking Hrafnhildur...\n",
                    color=102
        )
        begin
          push!(Unnur[fi.deer],fi.tckr)
          unique!(Unnur[fi.deer])
          sort!(Unnur[fi.deer])
        end
        open(Unnur["iPath"],"w") do io
          toml_print(io,Unnur)
        end
        printstyled("Hrafnhildur updated!",Char(0x2714),"\n",color=102)
        return nothing
      else
        msg_i = string(
                       "Addition of ",
                       fi.tckr,
                       " into ",
                       fi.land,
                       " within the Csilla framework failed!\n",
                       "See Valkyrja for more details.\n",
                       "Exiting...\n"
                )
        typpyst(msg_i,bud=true)
        return fi
      end
    else
      msg_ii = string(
                      Char(0x276c),
                      fi.land,"|",fi.tckr,
                      Char(0x276d),
                      " failed!\n"
               )
      printstyled(msg_ii,color=13)
      return fi
    end
  end
  """
      csi_add(fi::FiZ,[rfyl])
    
  The vectorized version of `csi_add`.
  """
  function csi_add(fi::Vector{FiZ};rfyl::Union{String,IO}=Rfyl)::Nothing
    #globs>
    if isempty(fi)
      throw(MissingException("fi::Vector{FiZ} is empty\n"))
    end
    
    begin
      t0  = Dates.now()
      d0  = Date(t0)
      vfi = Vector{Union{FiZ,Nothing}}(undef,length(fi))
    end
    
    for i in range(1,length(fi))
      heil("is being processed...",fi[i],iam="csilla_add")
      vfi[i] = csi_add(fi[i],rfyl=rfyl)
    end
    
    filter!(!isnothing,vfi)

    if isempty(vfi)
      msg_i = "All FiZs gotten, here are tickers processed:\n"
      typpyst(msg_i)
      for i in fi
        printstyled(Char(0x276c),fi.land,"|",fi.tckr,Char(0x276d),color=102)
        print(" ")
      end
    else
      msg_ii = string(
                      "The following instruments failed: \n",
                      join(string(i.tckr," ") for i in vfi)
               )
      typpyst(msg_ii)
    end
    msg_iii = string("\nTime elapsed: ",round(Dates.now()-t0,Second,"\n"))
    typpyst(msg_iii)
  end
end
#########################
##### Csilla::check #####
#########################
begin
  """
      csi_check(fi::FiZ,[rfyl])
  
  Does check wether `fi` does exist in Hrafnhlidur,
  It's relative age (how long it's been into CsillaDB),
  from which trading session it's not been present
  and many more...
  
  The function returns `nothing`.
  """
  function csi_check(fi::FiZ;rfyl::Union{String,IO}=Rfyl)::Nothing
    #globs>
    begin
      t0 = Dates.now()
      c0 = Unnur[string("CsillaDB_",fi.land)]
      u0 = ~isfile(c0)
      s0 = in(fi.tckr,Unnur[fi.deer])
    end
    
    #valids>
    if u0
      msg_qs = string(
                      "CsillaDB of ",
                      fi.land,
                      "(",
                      fi.deer,
                      ")\n",
                      "does not exist in Hrafnhildur!\n",
                      lpad("Passing...",15)
               )
      typpyst(msg_qs,kol=11)
      return nothing
    end
    
    #main>
    printstyled("Loading CsillaDB of ",fi.land," ...",color=102)
    csilla_db = parsefile(c0)
    if isempty(csilla_db)
      msg_qsi = string("CsillaDB of ",fi.land," is empty!\n","Passing...\n")
      typpyst(msg_qsi,kol=11)
      return nothing
    end
    printstyled(" done!\n",color=102)
    ##
    ## Gathering information...
    ##
    begin
      v0 = collect(Date(i) for i in collect(keys(csilla_db)))
      v1 = collect(Date(i) for i in v0 if in(fi.tckr,csilla_db[string(i)]))
      n0 = size(v0,1)
      n1 = size(v1,1)
    end
    
    msg_prt = string(
                     lpad("Land: ",25),fi.land,"\n",
                     lpad(string("Trading sessions: ",n0),25),"\n",
                     Char(0x276c),fi.land,"|",fi.tckr,Char(0x276d),"\n"
              )
    typpyst(msg_prt,bud=true)

    if iszero(n1)
      printstyled("This ticker is not Csilla'ed into Hrafnhildur!\n",color=102)
    else
      v2 = listbdays(Vala[fi.vala],firstdayofyear(t0),maximum(v1))
      md = strip(join(string(d," ") for d in v2 if ~in(d,v1)))
      msg_prt0 = string(
                        "CsillaDB of ",fi.land," range: ",
                        minimum(v0)," - ",maximum(v0),"\n",
                        "Range of ",fi.tckr,": ",
                        minimum(v1)," - ",maximum(v1),"\n",
                        "Coverage: ",round(*(100.0,/(n1,n0)),digits=2),"%\n",
                        "Missing sessions (",
                        fi.tckr,") this year: ",count(!in(v2),v1),
                        md
                 )
      typpyst(msg_prt0,bud=true)
      printstyled("\nPress [ENTER] to continue...",color=102)
      readuntil(stdin,"\n")
      printstyled("OK!\n",color=10)
    end
  end
  """
      csi_check(fi::FiZ,[rfyl])

  Vectorized version of `csi_check`. This function basically shows
  coverage statistics of the given `FiZ`s within CsillaDB.
  """
  function csi_check(fi::Vector{FiZ},rfyl::Union{String,IO}=Rfyl)::Nothing
    #globs>
    begin
      t0 = Dates.now()
      fr = rand(fi)
      c0 = Unnur[string("CsillaDB_",fr.land)]
      u0 = ~isfile(c0)
    end
    
    #valids>
    if u0
      msg_out1 = string(
                        "CsillaDB of ",fr.land," (",fr.deer,")\n",
                        " does not exist in Hrafnhildur...\n",
                        "Passing...\n"
                 )
      typpyst(msg_out1)
      return nothing
    end
    
    begin
      csi_db = parsefile(c0)
      n0     = getfield(csi_db,:count)
      d0     = collect(Date(k) for k in keys(csi_db))
      v1     = filter(!in(Unnur[fr.deer]),(getfield(i,:tckr) for i in fi))
    end

    if iszero(size(v1,1))
      printstyled("OK!\n",color=102)
    elseif isequal(size(v1,1),size(fi,1))
      msg_out2 = string("None of the input FiZs is Unnured!\nExiting...\n")
      typpyst(msg_out2)
      return nothing
    elseif isless(size(v1,1),size(fi,1))
      msg_out3 = string(
                        "The following tickers are not Unnured within",
                        " Csilla:\n",
                        strip(join(string(tckr," ") for tckr in v1)),
                        "\n"
                 )
      typpyst(msg_out3)
    end
    
    #main>
    printstyled("Collecting information...",color=102,blink=true)
    rakna = let
      a=(i.tckr => sum(in(i.tckr,csi_db[k]) for k in keys(csi_db)) for i in fi)
      b=(k => round(*(100,/(v,n0)),digits=2) for (k,v) in a)
      Dict{String,Float64}(b)
    end
    
    tb = permutedims(DataFrame(rakna),[:Tckr,:Freq])
    sort!(tb,:Freq)
    printstyled("collected!\n",color=102)
    sleep(0.75)
    printstyled("\033[2J","\033[H",color=102)

    msg_i = string(
                   lpad("Trading sessions: ",30),n0,"\n",
                   lpad("Dates range: ",30),minimum(v0),"~",maximum(v0),"\n",
                   "\nLeast (5) frequent tickers:\n"
            )
    typpyst(msg_i)
    show(
         first(tb,5,view=true),
         summary=false,
         eltypes=false,
         show_row_number=false
    )
    typpyst("\nPress [ENTER] to continue...")
    readuntil(stdin,"\n")
    
    typpyst("Most (5) frequent Tickers: \n")
    show(
         last(tb,5,view=true),
         summary=false,
         eltypes=false,
         show_row_number=false
    )
    typpyst("\nPress [ENTER] to continue...")
    readuntil(stdin,"\n")
    
    ex::Union{Nothing,Int64} = 1
    
    begin
      while ~isnothing(ex) && isone(ex)
        printstyled("[0] EXIT\t[1] REQUEST\n\n",bold=true)
        printstyled("Choose a number:  ",color=102)
        ex = tryparse(Int64,strip(readline(stdin)))
        println()
        if isone(ex)
          printstyled("Enter the Ticker:  ",color=102)
          tckr = strip(readline(stdin))
          if in(tckr,tb.Tckr)
            show(
                 filter(:Tckr => in((tckr,)),tb),
                 summary=false,
                 eltypes=false,
                 show_row_number=false
            )
            println()
           else
             msg_out4 = string(
                               tckr, " is not a valid ticker in ",fr.land,
                               lpad("\nQuitting...\n",20)
                        )
             typpyst(msg_out4)
             break
           end
        elseif iszero(ex)
          msg_out5 = string(
                            "Exiting...\n",
                            "Time elapsed: ",
                            round(Dates.now()-t0,Second),"\n"
                     )
          typpyst(msg_out5)
          sleep(1)
          break
        else
          printstyled("Invalid choice!\n",color=11)
          printstyled("Quitting...\n",color=102)
          break
        end
      end
    end
    printstyled(lpad("\ndone!\n",20),color=102)
  end
  """
      csi_check(b::String,[rfyl])
  
  `csi_check`s every element of b, where b is a Bunch.
  """
  function csi_check(b::String,rfyl::Union{String,IO}=Rfyl)::Nothing
    #globs>
    banner = string("Csilla [Checker] of [",b,"]\n")
    

    #main>
    if in(b,Bunch)
      typpyst(string(b," is not a valid Bunch!\nExiting... \n"))
      printstyled(b," is not a valid Bunch!\nExiting...\n",color=102)
    else
      if isempty(Unnur[Bmap[b]])
        msg = string("Unnur of ",b," seems empty! [Csilla]\n")
        prinstyled(msg,color=102,bold=true)
        return nothing
      end
      printstyled("Collecting FiZs...",color=102)
      ##---------------------------------------------
      fi = collect(FiZ(b,i) for i in Unnur[Bmap[b]])
      ##---------------------------------------------
      printstyled(" done!\n\n",color=102)
      typpyst(banner,bud=true,kol=12)
      #########################
      csi_check(fi,rfyl=rfyl) #
      #########################
    end
  end
  """
      csi_check([rfyl])
  
  Silly applying `csi_check` to the whole Hrafnhildur!
  """
  function csi_check(;rfyl::Union{String,IO})::Nothing
    typpyst("\nCsi_Checking the whole Hrafnhildur...\n")
    for b in Bunch
      csi_check(b,rfyl=rfyl)
    end
  end
end
###############################
##### Csilla::consolidate #####
###############################
begin
  """
      csi_consolidate(b::String,[rfyl])
  
  This function consolidates Hrafnhlidur under the Csilla umbrella.
  """
  function csi_consolidate(b::String;rfyl::Union{String,IO}=Rfyl)::Nothing
    
    if ~in(b,Bunch)
      msg_out1 = string(
                        lpad("Consolidate [Csilla]",40),"\n",
                        b," is not a valid Bunch!\n",
                        "Exiting...\n"
                 )
      typpyst(msg_out1)

      return nothing
    end
    
    #globs>
    begin
      p0 = joinpath(Silk,b)
      v1 = setdiff(Unnur[Bmap[b]],Unnur[b])
    end
    
    #main>
    if isempty(v1)
      msg_c = string(b," is consistently Unnured: ",Char(0x2714),"\n")
      printstyled(msg_c,color=10)
    else
      msg_inconsist = string(
                             "Some inconsistencies detected into ",
                             b,
                             " [[CSILLA::Consolidate]]\n"
                      )
      printstyled(msg_inconsist,color=11)
      msg_inconsist1 = string(
                              "The following instruments are ",
                              "Unnured with respect to the Csilla silo ",
                              "but not actually Unnured wrt Mercedesz!\n"
                       )
      typpyst(msg_inconsist1)
      for i in v1
        printstyled(Char(0x276c),b,"|",i,Char(0x276d)," ",color=13)
      end
      printstyled("\nCleanly that's: \n",color=102)
      typpyst(strip(join(string(i," ") for i in v1)))
      printstyled("\nPress [ENTER] to continue...",color=102)
      readuntil(stdin,"\n")
    end
    
    typpyst("\nBuilding-up a facility to clean-up Csilla")
    typpyst(uppercase(first(b,3))," temp. dir:\n\n")
    typpyst("[1] QUIT\t","[2] PROCEED\n\n→ ",bud=true)
    
    q_o_n = tryparse(Int64,strip(readline(stdin)))
    
    if isa(q_o_n,Int64) && isone(q_o_n-1)
      d1 = let
        a = readdir(p0,join=true)
        filter!(contains(string("Csilla",uppercase(first(b,3)))),a)
        filter!(isdir,a)
      end
      
      msg_temp_d = string("Some temp dirs have been detected in ",b,"!\n")

      if ~isempty(d1)
        typpyst(msg_temp_d)
        for p in d1
          i = only(findlast("/",p))+1
          printstyled(p[i:end]," ",color=13)
        end
        typpyst("\nThey'll be deleted...\n")
        print("[1] CANCEL\t[2] PROCEED\n\n→ ")
        q_o_n_1 = tryparse(Int64,strip(readline(stdin)))
        if isa(q_o_n_1,Int64) && isone(-(q_o_n_1,1))
          for p in d1
            i = +(only(findlast("/",p)),1)
            typpyst(string("Removing ",p[i:end],"... "),kol=11)
            rm(p,force=true,recursive=true)
            typpyst(string("done. ",Char(0x2714),"\n"),kol=11)
          end
          printstyled(string("\n",length(d1)," "))
          msg_done = >=(length(d1),2) ? "directories have" : "directory has"
          printstyled(msg_done," been removed!\n",color=102)
          prinstyled("Press [ENTER] to continue...\n",color=102)
          return nothing
        else
          typpyst("\nExiting...\n")
        end
      else
        typpyst(string(b," has its Csilla clean: ",Char(0x2714),"\n"),kol=10)
      end
    else
      typpyst("\nExiting...\n")
      return nothing
    end
  end
end
##########################
##### Csilla::remove #####
##########################
begin
  """
      csi_remove(fi::FiZ)
  
  This function facilitates the removal of `fi` from  Hrafnhildur within
  Csilla silos.
  
  The function does modify the Glob `Unnur` on the fly. The modification
  is only made within the Csilla framework. It dereferences `fi`
  given as input and returns nothing.
  """
  function csi_remove(
                      fi::FiZ,
                      force::Bool=false;
                      rfyl::Union{String,IO}=Rfyl
           )::Nothing
    #valids> 
    if in(fi.land,Bunch)
      msg_out1=string(
                     "The Land of ", fi.tckr, ": → ",fi.land,"\n",
                     " is not a valid land.\nQuitting... \n"
               )
      typpyst(msg_out1,kol=11)
      return nothing
    elseif ~in(fi.tckr,Unnur[fi.deer])
      msg_out2 = string(
                        fi.tckr," is not an element of ",fi.land,
                        " (Csilla framework)\nQuitting...\n"
                 )
      typpyst(msg_out2)
      return nothing
    end
    
    #main>
    p0 = Unnur["iPath"]
    
    if force
      msg_out3 = string(
                        fi.tckr," will be removed from Unnur of ",fi.land,
                        " within the Csilla framework!\n",
                        "\nRemoving ",
                        Char(0x276c),fi.land,"|",fi.tckr,Char(0x276d),
                        "... "
                 )
      typpyst(msg_out3,kol=11)
      filter!(!in((fi.tckr,)),Unnur[fi.deer])
      typpyst(lpad("done!",20))
      printstyled("Removal: ",Char(0x2714),"\n",color=11)
      return nothing
    else
      printstyled("Continue ? [Y/n]:  ",color=102)
      
      yn::String = strip(readline(stdin))
      
      if in(yn,("Yes","yes","YES","Y","y"))
        msg_out4 = string(
                      "\nRemoving ",
                      Char(0x276c),fi.land,"|",fi.tckr,Char(0x276d)," ..."
                   )
        typpyst(msg_out4,kol=11)
        filter!(!in((fi.tckr,)),Unnur[fi.deer])
        typpyst("done!\n",kol=11)
        printstyled("Removal: ",Char(0x2714),"\n",color=11)
      else
        msg_out5 = string(
                          "\nQuitting without processing ",
                          Char(0x276c),fi.land,"|",fi.tckr,Char(0x276d),
                          " (probably due to invalid input)...\n"
                   )
        typpyst(msg_out5,kol=9)
      end
      return nothing
    end
  end
  """
      `csi_remove(fi::Vector{FiZ},[rfyl=Rfyl])`
  """
  function csilla_remove(
                         fi::Vector{FiZ},
                         force::Bool=false;
                         rfyl::Union{String,IO}=Rfyl
           )::Nothing
    msg_1 = string(
                   "This is the vectorized version of ",
                   "the deletion facility within Csilla!\n",
                   "Do you want to process it interactively ? [Y/n]:   "
            )
    typpyst(msg_1,kol=11)
    
    yn = strip(readline(stdin))
    
    if in(yn,("Yes","yes","YES","Y","y")) || force
      for i in fi
        csilla_remove(i,true,rfyl=rfyl)
      end
    else
      typpyst("Continue or Exit the deletion facility?\n")
      printstyled(
                  "[1] CANCEL\t [2] CONTINUE\n\n",
                  Char(0x2192),
                  "  ",
                  color=102
      )
      yn_1 = tryparse(Int64,strip(readline(stdin)))
      if isa(yn_1,Int64) && isone(-(yn_1,1))
        for i in fi
          csilla_remove(i,rfyl=rfyl)
        end
      else
        typpyst("Exiting the facility...\n",color=11)
      end
    end
    return nothing
  end
end
##########################
##### Csilla::report #####
##########################
begin
  """
    `csi_report(fi::FiZ,[rfyl=Rfyl])`
  
  `csi_report` generates reports about Csilla (or CsillaDB) into a text or
  a tex file. The output may also be printed only on the stdout!
  """
  function csi_report(fi::FiZ;rfyl::Union{String,IO}=Rfyl)::Nothing
    printstyled("Sorry!\ncsi_report has not been implemented yet!\n",color=11)
    sleep(0.5)
    return nothing
  end
end
##########################
##### Csilla::update #####
##########################
begin
  """
      csi_update(fi::FiZ,[rfyl])
  
  `dfetches` every `fi` in `b` (`b` ∈ `Bunch`).
  See `dfetch` documentation for more details.
  """
  function csi_update(b::String;rfyl::Union{String,IO}=Rfyl)
    if ~in(b,Bunch)
      msg_out1 = string(b," is not a valid Bunch\nQuitting...")
      typpyst(msg_out1)
      return nothing
    else
      try
        hludana(b,rfyl=rfyl)
      catch err
        msg_e = string(
                       "Some errors occured in [[Csilla Update]] ",
                       "while updating ",
                       b,"...\n"
                )
        typpyst(ms_e,kol=11)
        msg_e1=string("\n",Dates.now(),"\n",err,"\n")
        typpyst(msg_e1,kol=13)
      finally
        msg_e2 = string("Update of ",b," (Csilla) exited!\n")
        typpyst(msg_e2)
      end
    end
  end
  """
      `csi_update([rfyl])`
  
  Update wathever can be updated!
  
  ## Note
  
  Some bugs have been identified when pre-testing, some
  stuffs need to be tweaked around the taskying stage.
  
  + This function should be optimized and made safe!
  + We recommand not to use this facility.
  """
  function csi_update(;rfyl::Union{String,IO}=Rfyl)::Nothing
    msg  = string("Updating the whole Hrafnhildur (Csilla)...\n")
    msg1 = string(
                  "Updation of the whole Hrafnhildur terminated...\n",
                  "Press [ENTER] to continue..."
           )
    typpyst(msg)
    for b in Bunch
      csi_update(b,rfyl=rfyl)
    end
    typpyst(msg1)
    readline(stdin)
    println("OK!")
  end
end
###############################
##### Csilla::Management  #####
###############################
begin
  """
      csi_mgmt(command,[rfyl])
    
  This function manages the **Csilla** part of Zsofia.
  It does take `command::NamedTuple{(:args,:bunch,:frmk,:scheme,:verb)}`
  as input which is a data stucture aimed to instruct how to perform
  the management.
  
  ## Details
  
  The `command` arg is a data structure spitted by the `gefjon` function
  (see `kielioppi` in the `ZsREPL` module).
  
  The `command` argument is a named tuple having the following fields:
  + `args` which gathers the vector of arguments, typically instrument
     to be processed (usually as a vector of string representing tickers).
  + `bunch` which gathers the related `Bunch`.
  + `frmk` is the framework, in this case `csilla`.
  + `scheme` is typically the ID of the correspondding `Scheme`
     (1,2,3 or nothing -- typically.)
  + `verb` is a field gathering the action to be performed.
    
  The following actions/verbs are available:
  
  + **add**          :: Adds some `fi`s into the contextual bunch.
  + **check**        :: Checks whether the given instruments are up-to-date.
  + **consolidate**  :: Cleans up swap dirs/files...
  + **remove**       :: Ditches the given instruments from selected `Bunch`.
  + **update**       :: Updates the given bunch.
  + **report**       :: Generate report (CSV, or TEX file) about Csilla DB.
  """
  function csi_mgmt(
                    command::NamedTuple{(:args,:bunch,:frmk,:scheme,:verb)};
                    rfyl::Union{String,IO}=Rfyl
           )
    #globs>
    begin
      n____ = length(getfield(command,:args))
      
      frmk_ = getfield(command,:frmk)
      verb_ = getfield(command,:verb)
      bunch = getfield(command,:bunch)
      
      # Wether there's args or not.
      solo_ = iszero(n____)
    end
    
    #valids>
    if ~in(frmk_,("csilla",))
      printstyled("<csilla> → Inconsistent Framework!\n",color=9,bold=true)
      printstyled("Exiting <Csilla> ...\n",color=102)
      return nothing
    end
    
    if iszero(n____)
      nothing
    elseif isone(n____)
      fi_ = FiZ(bunch,only(getfield(command,:args)))
    else
      fi_ = FiZ.(bunch,getfield(command,:args))
    end
    
    #main>
    try
      if ==(verb_,"add")
        csi_add(fi_,rfyl=rfyl)
      elseif ==(verb_,"check")
        solo_ ? csi_check(bunch,rfyl=rfyl) : csi_check(fi,rfyl=rfyl)
      elseif ==(verb_,"consolidate")
        csi_consolidate(bunch,rfyl=rfyl)
      elseif ==(verb_,"remove")
        csi_remove(fi_,rfyl=rfyl)
      elseif ==(verb_,"update")
        solo_ ? csi_update(bunch,rfyl=rfyl) : csi_update(fi,rfyl=rfyl)
      elseif ==(verb_,"report")
        solo_ ? csi_report(bunch,rfyl=rfyl) : csi_update(fi,rfyl=rfyl)
      end
    catch err
      typpyst("\nCsilla Management caught some errors:\n\n")
      typpyst(string(err,"\n"),kol=11)
      typpyst("\nPress ENTER to continue...")
      readuntil(stdin,"\n")
    end
  end
end