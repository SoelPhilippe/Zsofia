# Interfaces of Tiraw
# These are layers/prompters showing off informations
# about a given `FiZ`.
# It is intended to perform the getting and showing parts.
# We divided `FiZ` informations in four categories:
# - Fundamentals (show_fundamentals) providing fundamental info about the FiZ.
# - Overview (show_overview) providing a glance about the FiZ.
# - Metrics (show_metrics) providing usual metrics about the FiZ.
# - Summary (show_summary) providing the summary about the actual FiZ.
"""
**Name**: `þqmetrics`

**Summary**: Nyyrikki facility for QMetrics,
It should be compatible with Nyyrikki implementation.
"""
function þqmetrics(fi::FiZ)
  grub=("Fundamentals", "Overview", "QMetrics", "Summary", "Quit")
  đ=Dict()
  if fi.junk in ("ECN",)
    printstyled(
                "NOT IMPLEMENTED YET FOR $(fi.land)\nQuitting...\n",
                color=96,
                bold=true
    )
    return 4
  end
  try
    đ=YAML.load_file(Silk*fi.land*"/Tyyni/"*fi.tckr*".yaml")
  catch err
    printstyled(
                "Some errors did occur while reading"*
                " Tyyni of "*Char(0x3008)*fi.land*"|"*
                fi.tckr*Char(0x3009)*"\n",
                color=216
    )
    return 5
  end
  isempty(đ) ? printstyled("KO!\n", color=9) : printstyled("OK!\n", color=10)
  d=Dict(
   "Artemis" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Demeter" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Euterpe" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Mercure" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Nemesis" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Ophelya" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Origami" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Phorcys" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))])),
   "Satoshi" => Tuple(sort([k for k in keys(µtyyniz) if !in(k,
                                                              ("annikki",))]))
  )
  c=nothing
  while isnothing(c)
    run(`clear`)
    printstyled(
                Date(now()),
                " ",
                Time(round(now(),Second)), "\n",
                Char(0x3008) * fi.land* "|" * fi.tckr * Char(0x3009) * "\n\n",
                bold=true
    )
    if in(fi.land, Bunch)
      tb, t = Dict(), ()
      kv=Dict{String, Vector{Union{Float64, Int, Tuple{Float64,Float64}}}}()
      psorunn=Dict{String, Union{Vector{Float64}, Vector{Date}}}()
      xyz=CSV.read(
                   Silk*fi.land*"/"*fi.tckr*".csv",
                   DataFrame,
                   stringtype=String,
                   truestrings=nothing,
                   falsestrings=nothing
      )
      if haskey(đ, "modelY") && haskey(đ["modelY"], "t")
        t=get(
              đ["modelY"],
              "t",
              [
                i for i=firstdayofmonth(today()):lastdayofmonth(today()) if
                               !in(i, holidayz.holl[fi.land])
              ]
        )
        t=Tuple(x for x in t)
        filter!(
                x -> in(
                        x.Date,
                        union(
                              [ t[begin]-Day(k) for k in 10:-1:1 ],
                              t
                        )
                    ),
               xyz
              )
      end
      roo=[k for k in keys(µtyyniz) if !in(k, ("annikki",))]
      coo=map(x -> x[(collect(findfirst(r"[0-9]", x))[1]):end], roo)
      ι(x)=prevind(x, collect(findfirst(r"[0-9]", x))[1])
      roo=map(x -> x[begin:ι(x)], roo)
      coo=sort(coo)
      roo=sort(roo)
      tb["metric"]=[i for i in roo]
      for k in coo
        tb[k]=[round(get(đ, i*k, missing), digits=2) for i in roo]
      end
      tb=DataFrame(tb)
      roo, coo = nothing, nothing
      η(x)=select(x,[k for (k, v) in pairs(eachcol(x)) if !all(ismissing, v)]) 
      df=Tuple(η(filter(x -> x.metric in (k,), tb)) for k in unique(tb.metric))
      tb=DataFrame()
      for ζ in df
        append!(tb, unique(ζ))
      end
      select!(tb, :metric, ["3d","5d","10d","30d","360d"])
      ζ=("ω", "γ", "β", "α", "ν", "µ")
      if haskey(đ, "modelY") && haskey(đ["modelY"], "t")
        printstyled("Hz (1mo) → ", t[end], "\n", bold=true)
      end
      printstyled("Model → ", bold=true)
      if haskey(đ, "modelY")
        for l in ζ
          print(l, ": ", round(get(đ["modelY"], l, NaN), digits=3), "  ")
          isequal(l,"µ") ? print("\n") : nothing
        end
      end
      optio=Dict{String,Any}()
      if haskey(đ, "modelY")
        ir=0.035
        carry_r=0.0
        if isfile(Silk*"Mercure/SOFR.csv")
          ir=CSV.read(
                      Silk*"Mercure/SOFR.csv",
                      DataFrame,
                      stringtype=String,
                      truestrings=nothing,
                      falsestrings=nothing
                 )
          filter!(x -> isless(x.Date, đ["modelY"]["t"][begin]), ir)
          filter!(x -> !isnan(x.Price), ir)
          ir=0.01*ir.Price[end]
        elseif isfile(Silk*"Mercure/FF.csv")
          ir=CSV.read(
                      Silk*"Mercure/FF.csv",
                      DataFrame,
                      stringtype=String,
                      truestrings=nothing,
                      falsestrings=nothing
                 )
          filter!(x -> isless(x.Date, đ["modelY"]["t"][begin]), ir)
          filter!(x -> !isnan(x.Price), ir)
          ir=0.01*ir.Price[end]
        else
          ir=0.035
        end
        for l in (1,5,10,15,85,90,95,99)
          ŋ=round(get(đ["modelY"], "Kq"*string(l), NaN), digits=2)
          θ=round.(get(đ["modelY"], "vola_Kq"*string(l), NaN), digits=2)
          ψ=0.0
          if isless(
                    xyz.Date[end],
                    get(đ, "exDate", firstdayofyear(xyz.Date[end]))
                   ) && isless(
                               get(đ, "exDate", firstdayofyear(xyz.Date[end])),
                               t[end]
                        )
            carry_r=log(1 + get(đ, "DividendYield", carry_r)*0.01)
          end
          gbmm=GeomBMModel(
                           t[begin],
                           get(đ["modelY"], "S0", NaN),
                           ir,
                           carry_r,
                           sqrt(360) .* std(log.(1.0 .+ 0.01 .* xyz.Co))
               )
          if l in (1,5,10,15)
            ψ=(
               EuropeanPut(t[end], SingleStock(), ŋ),
               ifelse(
                      iszero(
                             get(đ["modelY"], "put_Kq"*string(l), 1.0)
                      ),
                      1.0,
                      get(đ["modelY"], "put_Kq"*string(l), 1.0)
               )
              )
            optio["put_Kq"*string(l)]=(
                                       round(value(gbmm,ψ[1]), digits=3),
                                       round(
                                             100*((value(gbmm,ψ[1])/ψ[2])-1.0),
                                             digits=2
                                       )
                                      )
          else
            ψ=(
               EuropeanCall(t[end], SingleStock(), ŋ),
               ifelse(
                      iszero(
                             get(đ["modelY"], "call_Kq"*string(l), 1.0)
                      ),
                      1.0,
                      get(đ["modelY"], "call_Kq"*string(l), 1.0)
               )
              )
            optio["call_Kq"*string(l)]=(
                                       round(value(gbmm,ψ[1]), digits=3),
                                       round(
                                             100*((value(gbmm,ψ[1])/ψ[2])-1.0),
                                             digits=2
                                       )
                                      )
          end
          kv["Kσ"*string(l)]=[ŋ, (θ[1],θ[2])]
          psorunn["σ"*string(l)] = round.(
                                          get(
                                              đ["modelY"],
                                              "vola_q"*string(l),
                                              [NaN for k in 1:length(t)]
                                          ),
                                          digits=2
                                  )
          isequal(l,99) ? print("\n") : nothing
        end
      end
      psorunn["t"]=[i for i in t]
      kv=DataFrame(kv)
      psorunn=DataFrame(psorunn)
      select!(kv, ["Kσ"*string(l) for l in (1,5,10,15,85,90,95,99)])
      select!(psorunn, :t, Not(:t))
      eni=NaN
      haskey(đ, "modelY") ? eni=get(đ["modelY"], "S0", NaN) : nothing
      isnan(eni) ? eni=7 : eni=ifelse(eni > xyz.Price[end], 9, 4)
      print(xyz.Date[end], " → ")
      sørensen=xyz.Price[end]/get(đ["modelY"], "S0", NaN)
      sørensen=100*(sørensen-1.0)
      printstyled(
                  round(xyz.Price[end], digits=2),
                  " (",
                  ifelse(
                         sørensen > 0,
                         "+"*string(round(sørensen, digits=2))*"%",
                         string(round(sørensen, digits=2))*"%"
                  ),
                  ")\n\n",
                  bold=true,
                  reverse=true,
                  color=eni
      )
      show(
           stdout,
           kv[:,1:4],
           show_row_number=false,
           summary=false,
           eltypes=false
      )
      print("\n\n")
      show(
           stdout,
           kv[:,5:end],
           show_row_number=false,
           summary=false,
           eltypes=false
      )
      print("\n\n")
      if haskey(đ, "modelY")
        printstyled("Lots: ", đ["modelY"]["olots"], "\n")
      end
      for l in (1,85,5,90,10,95,15,99)
        if l in (1,5,10,15)
          printstyled(
                      "\tput"*string(l)*": ",
                      optio["put_Kq"*string(l)][1],
                      " (",
                      optio["put_Kq"*string(l)][2],
                      "%)",
                      color=13,
                      bold=true,
                      reverse=true
          )
          print("\t")
        else
          printstyled(
                      "call"*string(l)*": ",
                      optio["call_Kq"*string(l)][1],
                      " (",
                      optio["call_Kq"*string(l)][2],
                      "%)",
                      color=106,
                      bold=true,
                      reverse=true
          )
          print("\n")
        end
      end
      print("\n\n")
      show(
           stdout,
           tb,
           show_row_number=false,
           summary=false,
           eltypes=false
      )
    else
      printstyled("Unusual LAND: $(fi.land)\nExiting...", color=11)
      sleep(0.625)
      println("")
      c=5
    end
    println("\n")
    for o in 1:length(grub)
      printstyled("[",o,"] ", grub[o], " ", color=237)
    end
    begin
      try
        c=parse(
                Int,
                readuntil(stdin, "\n")
        )
        in(c, 1:5) ? nothing : c=nothing
      catch err
        printstyled("Invalid Input.\n")
        c=nothing
      end
    end
  end
  return c
end
"""
**Name**: `þoverview`

**Summary**: Nyyrikki facility for Overview,
It should be compatible with Nyyrikki implementation.
"""
function þoverview(fi::FiZ)
  grub=("Fundamentals", "Overview", "QMetrics", "Summary", "Quit")
  đ=Dict()
  try
    đ=YAML.load_file(Silk*fi.land*"/Tyyni/"*fi.tckr*".yaml")
  catch err
    printstyled(
                "Some errors did occur while reading"*
                " Tyyni of "*Char(0x3008)*fi.land*"|"*
                fi.tckr*Char(0x3009)*"\n",
                color=216
    )
    return 5
  end
  isempty(đ) ? printstyled("KO!\n", color=9) : printstyled("OK!\n", color=10)
  d=Dict(
         "Artemis" => (
                        "Name", "Symbol", "Sector", "Industry",
                        "Country", "City", "State", "Address",
                        "Isin", "Zip", "Exchange", "Cap",
                        "OutstandingShares", "FloatingShares",
                        "InsidersPctHeld", "InstitutionsPctHeld", 
                        "Website", "Officers"
                      ),
         "Demeter" => (
                       "Symbol", "Name", "Underlying",
                       "Exchange", "Expiration", "OpenInterest",
                       "FirstTrade"
                      ),
         "Euterpe" => (
                       "Name", "TimeZone", "Exchange",
                       "QuoteType", "Currency"
                      ),
         "Mercure" => (
                       "Id", "LastUpdated", "SeasAdj", "Frequency",
                       "LastUpdated"
                      ),
         "Nemesis" => (
                       "Symbol", "Name", "Underlying",
                       "Exchange", "Expiration", "OpenInterest",
                       "FirstTrade"
                      ),
         "Ophelya" => (
                       "Name", "Symbol", "Category",
                       "FundFamily", "TotalAssets", "Underlying",
                       "Yield", "Exchange", "Beta3Yr",
                       "LegalType", "FirstTrade", "TimeZone"
                      ),
         "Origami" => (
                        "Name", "Symbol", "Sector", "Industry",
                        "Country", "City", "Address",
                        "Zip", "Exchange", "Cap",
                        "OutstandingShares", "FloatingShares",
                        "InsidersPctHeld", "InstitutionsPctHeld", 
                        "Website", "Officers"
                      ),
         "Phorcys" => (
                       "Name", "Symbol", "Currency", "Exchange", "QuoteType"
                      ),
         "Satoshi" => (
                       "Name", "Symbol", "CirculatingSupply", "Exchange",
                       "TimeZone"
                      )
  )
  shwr=[]
  for v in values(d[fi.land])
    if haskey(đ, v) && isa(đ[v], Number)
      if occursin("Pct", v)
        push!(shwr, (þs[v], string(round(đ[v], digits=2))*"%"))
      elseif in(
                þs[v],
                (
                 "EBITDA","DEBT",
                 "Out.Sh", "Mkt.Cap", "OpenInterest",
                 "Floating","SharesShort", "Cash",
                )
               ) && !isempty(đ[v])
        push!(shwr, (þs[v], convert(Int, đ[v])))
      elseif !isempty(đ[v])
        push!(shwr, (þs[v], round(đ[v], digits=2)))
      end
    elseif haskey(đ, v) && !isa(đ[v], Number)
      push!(shwr, (þs[v], đ[v]))
    end
  end
  shwr=Tuple(v for v in shwr)
  c=nothing
  while isnothing(c)
    run(`clear`)
    print(
          Date(now()),
          " ",
          Time(round(now(),Second)),
          Char(0x3008)*fi.land*"|"*fi.tckr*Char(0x3009)*"\n\n"
    )
    if in(fi.land, Bunch)
      for v in 1:length(shwr)
        if isequal(mod(v, 3), 0)
          if shwr[v][1] in ("Officers",)
            println("")
            for k in keys(shwr[v][2])
              printstyled(k, underline=true)
              temp=Tuple((l,r) for (l,r) in shwr[v][2][k])
              println("")
              for (l,r) in temp
                !isequal(l, "Maxage") ? println(l, ": ", r) : nothing
              end
              println("")
            end
          else
            printstyled(shwr[v][1], bold=true)
            print(": ", shwr[v][2], "  \n")
          end
        else
          if shwr[v][1] in ("Officers",)
            println("")
            for k in keys(shwr[v][2])
              printstyled(k, underline=true)
              temp=Tuple((l,r) for (l,r) in shwr[v][2][k])
              println("")
              for (l,r) in temp
                !isequal(l, "Maxage") ? println(l, ": ", r) : nothing
              end
              println("")
            end
          else
            printstyled(shwr[v][1], bold=true)
            print(": ", shwr[v][2], "   ")
          end
        end
      end
    else
      printstyled("Unusual LAND: $(fi.land)\nExiting...", color=11)
      sleep(0.625)
      println("")
      c=5
    end
    println("\n")
    for o in 1:length(grub)
      printstyled("[",o,"] ", grub[o], " ", color=237)
    end
    begin
      try
        c=parse(
                Int,
                readuntil(stdin, "\n")
        )
        in(c, 1:5) ? nothing : c=nothing
      catch err
        printstyled("Invalid Input.\n")
        c=nothing
      end
    end
  end
  return c
end
"""
**Name**: `þsummary`

**Summary**: Nyyrikki facility for Summary,
It should be compatible with Nyyrikki implementation.
"""
function þsummary(fi::FiZ)
  grub=("Fundamentals", "Overview", "QMetrics", "Summary", "Quit")
  đ=Dict()
  try
    đ=YAML.load_file(Silk*fi.land*"/Tyyni/"*fi.tckr*".yaml")
  catch err
    printstyled(
                "Some errors did occur while reading"*
                " Tyyni of "*Char(0x3008)*fi.land*"|"*
                fi.tckr*Char(0x3009)*"\n",
                color=216
    )
    return 5
  end
  isempty(đ) ? printstyled("KO!\n", color=9) : printstyled("OK!\n", color=10)
  d=Dict(
         "Artemis" => ("BusinessSummary",),
         "Demeter" => (
                       "Symbol", "Name", "Underlying",
                       "Exchange", "Expiration", "OpenInterest",
                       "FirstTrade"
                      ),
         "Euterpe" => (
                       "Name", "TimeZone", "Exchange",
                       "QuoteType", "Currency"
                      ),
         "Mercure" => ("Notes",),
         "Nemesis" => (
                       "Symbol", "Name", "Underlying", "Exchange",
                       "Expiration", "OpenInterest", "FirstTrade"
                      ),
         "Ophelya" => (
                       "Name", "Symbol", "Category",
                       "FundFamily", "TotalAssets", "Underlying",
                       "Yield", "Exchange", "Beta3Yr",
                       "LegalType", "FirstTrade", "TimeZone"
                      ),
         "Origami" => ("BusinessSummary",),
         "Phorcys" => (
                       "Name", "Symbol", "Currency", "Exchange", "QuoteType"
                      ),
         "Satoshi" => (
                       "Name", "Symbol", "CirculatingSupply", "Exchange",
                       "TimeZone"
                      )
  )
  shwr=[]
  for v in values(d[fi.land])
    if haskey(đ, v) && isa(đ[v], Number)
      if occursin("Pct", v)
        push!(shwr, (þs[v], string(round(đ[v], digits=2))*"%"))
      elseif in(
                þs[v],
                (
                 "EBITDA","DEBT", "OpenInterest",
                 "Out.Sh", "Mkt.Cap", "Cash",
                 "Floating","SharesShort"
                )
               ) && !isempty(đ[v])
        push!(shwr, (þs[v], convert(Int, đ[v])))
      else
        push!(shwr, (þs[v], round(đ[v], digits=2)))
      end
    elseif haskey(đ, v) && !isa(đ[v], Number)
      push!(shwr, (þs[v], đ[v]))
    end
  end
  shwr=Tuple(v for v in shwr)
  c=nothing
  while isnothing(c)
    run(`clear`)
    print(
          Date(now()),
          " ",
          Time(round(now(),Second)),
          Char(0x3008)*fi.land*"|"*fi.tckr*Char(0x3009)*"\n\n"
    )
    if in(fi.land, Bunch)
      for v in 1:length(shwr)
        if isequal(mod(v, 3), 0)
          printstyled(shwr[v][1], bold=true)
          print(": ", shwr[v][2], "  \n")
        else
          printstyled(shwr[v][1], bold=true)
          print(": ", shwr[v][2], "   ")
        end
      end
    else
      printstyled("Unusual LAND: $(fi.land)\nExiting...", color=11)
      sleep(0.625)
      println("")
      c=5
    end
    println("\n")
    for o in 1:length(grub)
      printstyled("[",o,"] ", grub[o], " ", color=237)
    end
    begin
      try
        c=parse(
                Int,
                readuntil(stdin, "\n")
        )
        in(c, 1:5) ? nothing : c=nothing
      catch err
        printstyled("Invalid Input.\n")
        c=nothing
      end
    end
  end
  return c
end
"""
**Name**: `nyyrikki`.

**NArgs**: (1,0)

**Summary**: `nyyrikki` is the function displaying informations
about instruments into `tirprofile`. It takes one (01) positional
argument: `bunchee`, the asset class to be processed.
"""
function nyyrikki(fi::FiZ)
  ## spec. purpose heil
  messnyyr(msg)=heil(    
                          msg::AbstractString;    
                          rfyl=stdout,     
                          fi=fi,     
                          iam="nyyrikki",
                          kol=102,
                          bud=true 
                )
  grub=("Fundamentals", "Overview", "QMetrics", "Summary", "Quit")
  c=1
  while true
    if isequal(grub[c], "Fundamentals")
      c=þfundamentals(fi)
    elseif isequal(grub[c], "Overview")
      c=þoverview(fi)
    elseif isequal(grub[c], "QMetrics")
      c=þqmetrics(fi)
    elseif isequal(grub[c], "Summary")
      c=þsummary(fi)
    elseif isequal(grub[c], "Quit")
      printstyled(
                  "\nExiting [nyyrikki] of "*
                  Char(0x3008)*fi.land*"|"*
                  fi.tckr*Char(0x3009)*"...\n\n",
                  color=102
      )
      sleep(1)
      break
    end
  end
end
"""
**Name**: `tirprofile`. It does print the profile
of the instruments entered!
"""
function tirprofile(
                    bunchee::String;
                    rfyl=Rfyl
         )
  db=YAML.load_file("Unnur.yaml")[bunchee]
  td=map(x -> x[begin:(end-5)], readdir(Silk*bunchee*"/Tyyni/"))
  printstyled("\nEnter the ticker for $bunchee:  ", color=7, bold=true)
  them=string.(split(readuntil(stdin, "\n")))
  if isempty(them)
    printstyled("No Entry.\nQuitting...\n\n", color=95, bold=true)
    sleep(0.5)
    return nothing
  elseif isequal(length(them), 1)
    fi=FiZ(bunchee, them[1], Junkme[bunchee])
    if !in(fi.tckr, td)
      printstyled(
                  Char(0x3008)*fi.land*"|"*fi.tckr*Char(0x3009)*
                  " is not Tyynied!\nExiting...\n\n", color=216
      )
      sleep(0.625)
      return nothing
    elseif !in(fi.tckr, db)
      printstyled(
                  Char(0x3008)*fi.land*"|"*fi.tckr*Char(0x3009)*
                  " is not Unnured!\nExiting...\n\n", color=216
      )
      sleep(0.625)
      return nothing
    else
      ########## NYYRRIKKI ############
      nyyrikki(fi::FiZ)        #
      #################################
    end
  elseif isless(1, length(them))
    printstyled("Not Implemented Yet!\n", color=216, bold=true)
  end
end
"""
**Name**: `tirmodels`.
"""
function tirmodels(
                   bunchee::String;
                   rfyl=Rfyl
         )
  nothing
end
"""
**Name**: `tir5upper`.
"""
function tir5upper(
                   bunchee::String;
                   rfyl=Rfyl
         )
  nothing
end
"""
**Name**: `tirclean`.
"""
function tirclean(
                  bunchee::String;
                  rfyl=Rfyl
         )
  jakko=("Csillaz","Tyyniz")
  while true
    cj=chooseme(jakko, "TIRAW::CLEAN::["*bunchee*"]")
    if isnothing(cj)
      printstyled(
                  "\tExiting TIRAW::CLEAN::["*bunchee*"]...\n",
                  color=102,
                  bold=true
      )
      sleep(0.5)
      break
    elseif !iszero(cj)
      if isequal(jakko[cj], "Csillaz")
        pee=filter(
                   x -> isdir(x),
                   readdir(Silk*bunchee*"/",
                   join=true)
        )
        filter!(
                x -> occursin("Csilla", x),
                pee
        )
        msg="THIS TASK IS INTENDED TO CLEAN THE ZSOFIA's DB.\n"*
            "LET ME ANALYZE THE TREE STRUCTURE TO DETERMINE \n"*
            "WHICH DIRECTORY DO I NEED TO REMOVE ..."
        printstyled(
                    "                  [MESSAGE]                    \n",
                    color=13,
                    bold=true,
                    blink=true
        )
        for f in msg
          printstyled(f, color=138, bold=true)
          sleep(0.005*rand())
        end
        readuntil(stdin, "\n")
        if isempty(pee)
          printstyled(
                      "\n\tEVERYTHING IS CLEAN!\n\tExiting...\n",
                      color=10,
                      bold=true
          )
          sleep(1)
          return nothing
        else
          for f in pee
            for g in f
              printstyled(g, color=9, bold=true)
              sleep(0.005*rand())
            end
            println("  will be removed!")
          end
        end
        printstyled("Continue ? [Y/N]: ")
        yn=string.(split(readuntil(stdin, "\n")))
        if isempty(yn)
          printstyled("\nNOTHING READ!\nExiting...\n\n", color=13, bold=true)
          return nothing
        end
        if yn[1] in ("YES", "Yes", "yes", "Y", "y")
          for f in pee
            rm(f, force=true, recursive=true)
            printstyled(f, " has been removed!\n", color=3, bold=true)
          end
        else
          printstyled(
                      "\nStepping back...\nReturning to TIRAW::CLEAN...\n",
                      color=138
          )
          sleep(0.5)    
        end
      elseif isequal(jakko[cj], "Tyyniz")
        þ=filter(
                 x -> occursin(".yaml", x),
                 readdir(Silk*bunchee*"/Tyyni/", join=true)
        )
        trub=Set{Any}()
        for p in þ
          try
            i=collect(findlast("/", p))[1] + 1
            đ=YAML.load_file(p)
            s=setdiff(keys(đ), þµŋ[bunchee])
            if !isempty(s)
              for k in s
                delete!(đ, k)
                if "Symbol" in keys(đ)
                  printstyled(
                              k, " has been cleared from "*
                              Char(0x3008)*bunchee*"|"*
                              đ["Symbol"]*Char(0x3009)*"\n",
                              color=102,
                              bold=true
                  )
                else
                  printstyled(
                              k,
                              " has been cleared from ",
                              Char(0x3008)*bunchee*"|"*
                              p[i:(end-5)]*Char(0x3009)*
                              "\n",
                              color=102,
                              bold=true
                  )
                end
              end
              YAML.write_file(p, đ, "---\n")
              printstyled(
                          Char(0x3008)*bunchee*"|"*
                          p[i:(end-5)]*Char(0x3009),
                          " processed and written out!\n",
                          color=11,
                          bold=true
              )
            else
              printstyled(
                          Char(0x3008)*bunchee*"|"*
                          p[i:(end-5)]*Char(0x3009),
                          " is clean!\n",
                          color=10,
                          bold=true
              )
            end
          catch err
            printstyled(
                        "Some troubles came out!\n"*
                        string(err)*"\n",
                        color=11,
                        bold=true
            )
            trub=union(trub, Set([(p, string(err))]))
            continue
          end
        end
        print("Continue...")
        readuntil(stdin, "\n")
        if !isempty(trub)
          printstyled("Some errors ($( length(trub) )) occured:\n", color=3)
          for elem in trub
            print(elem[1], " ")
            printstyled(Char(0x261b)," ", color=4)
            printstyled(elem[2], "\n", color=96)
          end
          print("Continue...")
          readuntil(stdin, "\n")
          printstyled("Troubling items in $bunchee :\n", color=3)
          for elem in trub
            i=collect(findlast("/", elem[1]))[1] + 1
            printstyled(elem[1][i:(end-5)], " ", color=13)
          end
          print("\nContinue...")
          readuntil(stdin, "\n")
        else
          nothing
        end
      end
    else
      nothing
    end
  end
end
"""
**Name**: `tirtyyniz`. It updates TIRAW's TYYNI(Z) of
Zsofia DB.
"""
function tirtyyniz(
                    bunchee::String;
                    rfyl=Rfyl
         )
  tc=("SOME","TUTTI", "SUMMARY")
  while true
    ctc=chooseme(tc, "TIRAW::TYYNIZ::["*bunchee*"]")
    if isnothing(ctc)
      printstyled("\tExiting TYYNIZ...\n", color=102, bold=true)
      sleep(0.5)
      break
    elseif !iszero(ctc)
      if isequal(tc[ctc], "SOME")
        print("Enter tickers from $bunchee: ")
        them=string.(split(readuntil(stdin, "\n")))
        if isempty(them)
          printstyled("\n\tNone read!\n\tExiting...\n\n",color=13,bold=true)
          sleep(0.5)
        else
          n=length(them)
          db=(l for l=YAML.load_file("Unnur.yaml")[bunchee])
          fi=Vector{FiZ}(undef, n)
          taskies=Vector{Task}(undef, n)
          inds=Vector{Bool}(undef, n)
          for i in 1:n
            if in(them[i], db)
              printstyled("Initializing $(them[i]) ... ", color=102)
              fi[i]=FiZ(bunchee, them[i], Junkme[bunchee])
              taskies[i] = @task tyynizupdate(fi[i], rfyl=rfyl)
              inds[i]=false
              printstyled(Char(0x2714)*"\n", color=102)
            else
              fi[i]=FiZ("", "", "")
              taskies[i] = @task cos(i)
              inds[i]=true
              printstyled("$(them[i]) is not into $bunchee Zsofia DB",color=13)
              sleep(0.5)
            end
          end
          (deleteat!(fi, inds); deleteat!(taskies, inds))
          #---------- TASKYING TYYNIZUPDATE___SOME ---------#
          (schedule.(taskies); wait.(taskies))              #
          #-------------------------------------------------#
          printstyled("\nTYYNIZ UPDATE of $bunchee: done!", color=10)
          printstyled("\nPress ENTER...", color=102)
          readuntil(stdin, "\n")
          fi, taskies, n = nothing, nothing, nothing
        end
      elseif isequal(tc[ctc], "TUTTI")
        (them=YAML.load_file("Unnur.yaml")[bunchee]; n=length(them))
        if iszero(n)
          printstyled("\nNo Entry Read from $bunchee (Unnur)\n\n", color=13)
          sleep(0.5)
        else
          fi=Vector{FiZ}(undef, n)
          taskies=Vector{Task}(undef, n)
          for i in 1:n
            fi[i]=FiZ(bunchee, them[i], Junkme[bunchee])
            taskies[i] = @task tyynizupdate(fi[i], rfyl=rfyl)
          end
          #---------- TASKYING TYYNIZUPDATE___TUTTI ---------#
          (schedule.(taskies); wait.(taskies))               #
          #--------------------------------------------------#
          printstyled("\nTYYNIZ UPDATE of $bunchee "*Char(0x2714), color=10)
          printstyled("\nPress ENTER...", color=102)
          readuntil(stdin, "\n")
          fi, taskies = nothing, nothing
        end
        n=nothing
      elseif isequal(tc[ctc], "SUMMARY")
        tyynied=readdir(Silk*bunchee*"/Tyyni/")
        n=Vector{Int}(undef, length(tyynied))
        p=joinpath.(Silk*bunchee*"/Tyyni/", tyynied)
        printstyled("Extracting information...", color=3, blink=true)
        rookies=Vector{Any}(undef, length(p))
        for i in range(1,length(n))
          try
            n[i] = length(YAML.load_file(p[i]))
            rookies[i]=(i, false)
          catch err
            heil(
                      "[FST]"*string(err),
                      fi=FiZ(bunchee, tyynied[i], Junkme[bunchee]),
                      iam="tirtyyniz"
            )
            rookies[i]=(i, true)
           continue
          end
        end
        v=filter(x -> x[2], rookies)
        printstyled(" done!\n", color=3)
        tyynied=map(x -> x[begin:(end-5)], tyynied)
        sleep(0.5)
        run(`clear`)
        if !isempty(v)
          printstyled(
                      "\n[FST] Problems within Tyynis of:\n",
                      color=13,
                      bold=true
          )
          for i in getindex(tyynied, map(x -> x[1], v))
            printstyled(i, " ", color=167, bold=true)
          end
          print("\n\nPress [ENTER]")
          readuntil(stdin, "\n")
          println("")
          deleteat!(n, getfield.(v, 1))
          deleteat!(tyynied, getfield.(v, 1))
        end
        v=[]
        for i in zip(tyynied, n)
          i[2] < quantile(n,0.05) ? push!(v, i) : nothing
        end
        unnured=YAML.load_file("Unnur.yaml")[bunchee]
        ntd=length(tyynied)
        nud=length(unnured)
        ntot= µtyyniz.count + getfield(Mξp[bunchee], :count)
        tb1=DataFrame(
                     :Unnured => nud,
                     :Tyynied => ntd,
                     :Δ => nud - ntd
        )
        tb2=DataFrame(
                      :theo_n => string(ntot),
                      :min => string(
                                     round(
                                              100*minimum(n)/ntot,
                                              digits=2
                                     )
                              )*"%",
                      :q5 => string(
                                    round(
                                          100*
                                          quantile(n, .05)/ntot,
                                          digits=2
                                    )
                             )*"%",
                     :q25 => string(
                                    round(
                                          100*
                                          quantile(n, .25)/ntot,
                                          digits=2
                                    )
                             )*"%",
                     :mean => string(
                                     round(
                                           100*
                                           mean(n)/ntot,
                                           digits=2
                                     )
                              )*"%",
                     :q50 => string(
                                    round(
                                          100*
                                          quantile(n, .50)/ntot,
                                          digits=2
                                    )
                             )*"%",
                     :q75 => string(
                                    round(
                                          100*
                                          quantile(n, .75)/ntot,
                                          digits=2
                                    )
                             )*"%",
                     :q95 => string(
                                    round(
                                          100*
                                          quantile(n, .95)/ntot,
                                          digits=2
                                    )
                             )*"%",
                     :max => string(
                                    round(
                                          100*maximum(n)/ntot,
                                          digits=2
                                    )
                             )*"%"
        )
        printstyled(
                    "\nUnnured or not Unnured, that's the question:\n",
                    color=13
        )
        show(tb1, summary=false, eltypes=false, show_row_number=false)
        print("\n\nPress [ENTER]")
        readuntil(stdin, "\n")
        println("")
        printstyled("\nCoverage summary of Tyynied:\n", color=13)
        show(tb2, summary=false, eltypes=false, show_row_number=false)
        print("\n\nPress [ENTER]")
        readuntil(stdin, "\n")
        println("")
        if !isempty(v)
          printstyled(
                      "The following tickers are under q5 ((tckr, size)):\n",
                      color=13,
                      bold=true
          )
          for i in range(1, length(v))
            printstyled(v[i], " ", color=13, bold=true)
          end
          readuntil(stdin, "\n")
          printstyled("\nCleanly that's:\n", color=13, bold=true)
          for i in getfield.(v, 1)
            printstyled(i, " ", color=167)
          end
          readuntil(stdin, "\n")
          print("\n")
        end
        println("")
        if isless(tb1.Δ[1], 0)
          v=setdiff(tyynied, unnured)
          printstyled(
                "\nThe following tickers are Tyynied but not Unnured:\n",
                color=13,
                bold=true
          )
          for i in range(1, length(v))
            if iszero(mod(i,4))
              printstyled("\t"*string(v[i])*"\n", color=13, bold=true)
            else
              printstyled("\t"*string(v[i]), color=13, bold=true)
            end
          end
          print("\n\nPress [ENTER]")
          readuntil(stdin, "\n")
          println("")
        elseif isless(0, tb1.Δ[1])
          v=setdiff(unnured, tyynied)
          printstyled(
                "\nThe following tickers are Unnured but not Tyynied:\n",
                color=13,
                bold=true
          )
          for i in range(1, length(v))
            if iszero(mod(i,4))
              printstyled("\t"*string(v[i])*"\n", color=13, bold=true)
            else
              printstyled("\t"*string(v[i]), color=13, bold=true)
            end
          end
          print("\n\nPress [ENTER]")
          readuntil(stdin, "\n")
          println("")
        else
          printstyled("\nDone!", color=102)
          readuntil(stdin, "\n")
        end
      end
    else
      nothing
    end
  end
end
const mtrm = Dict(
                  "ADD" => tiradd,
                  "UPDATE" => tirupdate,
                  "PROFILE" => tirprofile,
                  "MODELS" => tirmodels,
                  "5UPPER" => tir5upper,
                  "CLEAN" => tirclean,
                  "TYYNIZ" => tirtyyniz
             )
"""
**Name**: `maintiraw`  

**NArgs**: (0, 0)  

**Summary**: is a zero-argument function
purposely built to to perform the "TIRAW"
task from Zsofia MainMenu.
"""
function maintiraw(; rfyl=Rfyl)
  tir=(
       "PROFILE","ADD",
       "UPDATE","MODELS",
       "5UPPER","CLEAN",
       "TYYNIZ"
      )
  while true
    cmtrm=chooseme(tir, "TIRAW")
    if isnothing(cmtrm)
      printstyled("\tExiting TIRAW...\n", color=102, bold=true)
      sleep(1)
      break
    elseif !isequal(cmtrm, 0)
      while true
        cbunch=chooseme(Bunch, "TIRAW::"*tir[cmtrm])
        if isnothing(cbunch)
          break
        elseif !isequal(cbunch, 0)
          mtrm[tir[cmtrm]](
                           Bunch[cbunch],
                           rfyl=rfyl
          )
        elseif isequal(cmtrm, 0)
          nothing
        end
      end
    end
  end
end
