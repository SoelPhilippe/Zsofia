# This script is part of Zsofia. Soel Philippe © 2025
begin
  """
      alkukirjain(::Nothing)
  
  Check, validate and consolidate files at the very start of Zsofia.
  """
  function alkukirjain()::Nothing
    for c in "\nALKUKIRJAIN init'zer.\nChecking silos...\n"
      printstyled(c,color=12,bold=true)
      sleep(0.01 * rand())
    end
    
    descr = (
      "Artemis" => "US Stocks and ADRs.",
      "Demeter" => "Non equity, indexes Futures (commodities, bonds...).",
      "Euterpe" => "Spot indexes (equity, commodities, yields...).",
      "Mercure" => "Economical data.",
      "Nemesis" => "Equity indexes Futures.",
      "Ophelya" => "Exchange Traded Funds, Trackers.",
      "Origami" => "Japanese stocks, mainly NIKKEI 225.",
      "Phorcys" => "FX Currencies, denominated in USD.",
      "Satoshi" => "Cryptocurrencies, denominated in USD."
    )
    
    let
      msg = "\n"
      for b in descr
        msg *= getfield(b, :first) * " « " * getfield(b, :second) * " »\n"
      end
      @info msg
      sleep(2)
    end
    
    #0> intro, mandatory files
    begin
      if @isdefined Radix
        nothing
      else
        @error "Radix: " * Char(0x2718) *
               "\nRadix is not available, exiting...\n"
        sleep(1)
        exit(46)
      end
      
      lagen = (
                assets = joinpath(Radix, "assets"),
                hrafnh = joinpath(dirname(Radix), ".Hrafnhildur"),
                models = joinpath(Radix, "models")
              )
      
      vagen = (
               Beat     = joinpath(Radix, "assets", "Beat.toml"),
               Foto     = joinpath(Radix, "assets", "Fotografi.toml"),
               Unnur    = joinpath(Radix, "assets", "Unnur.toml"),
               Valkyrja = joinpath(Radix, "assets", "Valkyrja.toml")
              )
      
      pits  = ("Dailies", "Tyyni", ".Beat")
    end
    
    #1> lagen -> assets and relevant vagen
    let assets = getfield(lagen, :assets)
      if ~isdir(assets)
        @info "assetizing..."
      end
      mkpath(assets)
      ## vagen --> Radix/src/assets/ (because lagen -> assets)
      for path in vagen
        l = chop(path, head=first(findlast("/", path)), tail=4)
        if in(l, ("Valkyrja",))
          mkpath(joinpath(Radix, "assets", l), mode=0o755)
        else
          touch(path)
        end
      end
    end
    
    #2> lagen -> hrafnh
    let hrafnh = getfield(lagen, :hrafnh)
      if ~isdir(hrafnh)
        @info "Hrafnhildured!"
      end
      mkpath(hrafnh)
      ## Silowing Hrafnhildur...
      msg = "\n"
      for path in map(mkpath, joinpath.(hrafnh, getfield.(descr, :first)))
        for pit in pits
          i = +(last(findlast("/", path)), 1)
          p = joinpath(path, pit)
          mkpath(p, mode=0o755)
        end
      end
    end
    
    #3> lagen -> models
    begin
      mkpath(getfield(lagen, :models))
    end
    
    #4> vagen -> Unnur
    Unnur = parsefile(getfield(vagen, :Unnur))
    
    let unnur = getfield(vagen, :Unnur)
      get!(Unnur, "Hrafnhildur", collect(getfield(b,:first) for b in descr))
      get!(Unnur, "Valkyrja", joinpath(Radix, "assets", "Valkyrja"))
      if ~isdir(Unnur["Valkyrja"])
        Unnur["Valkyrja"] = joinpath(Radix, "assets", "Valkyrja")
      end
      get!(Unnur, "Silk", getfield(lagen, :hrafnh))
      get!(Unnur, "FRED_API_KEY", "")
      if isempty(Unnur["FRED_API_KEY"])
        @info """
              
              No FRED api key detected!
              Required to leverage full features of Mercure bunch.
              Please, go to https://fred.stlouisfed.org/docs/api/api_key.html.
              
              """
      end
      get!(Unnur, "iPath", unnur)
      if Unnur["iPath"] != unnur
        Unnur["iPath"] = unnur
      end
      get!(Unnur, "Fotografi", getfield(vagen, :Foto))
      for bunch in getfield.(descr, :first)
        p = joinpath(Radix, "assets", "Csilla_db_" * bunch * ".toml")
        get!(Unnur, "Csilla_db_" * bunch, p)
        get!(Unnur, bunch, String[])
        if isempty(Unnur[bunch])
          btckrs = let
            r = readdir(joinpath(getfield(lagen, :hrafnh), bunch), join=true)
            filter!(endswith(".csv"), r)
            filter!(!isempty, r)
            filter!(x -> countlines(x) > 1, r)
            map(r) do tckr
              i = last(findlast("/", tckr))
              chop(tckr, head=i, tail=4)
            end
          end
          if ~isempty(btckrs)
            msg_b = "\nDetected " * string(size(btckrs, 1)) *
                    " element" * ifelse(size(btckrs, 1) > 1, "s ", "") *
                    "in " * bunch * "!\nAdding them...\n"
            Unnur[bunch] = ifelse(!isempty(btckrs), btckrs, String[])
            @info msg_b
          else
            @info "\nNo element detected in " * bunch * ".\n"
          end
        end
        get!(Unnur, bunch * "_i", String[])
        if in(bunch, ("Mercure",))
          nothing
        else
          touch(p)
          r_i = readdir(
                        joinpath(
                                 getfield(lagen, :hrafnh),
                                 bunch,
                                 "Dailies"
                                ),
                        join=true
                       )
          filter!(endswith(".csv"), r_i)
          filter!(!isempty, r_i)
          filter(x -> countlines(x) > 1, r_i)
          t_s = map(r_i) do p_dailies
            match(r"\d{4}-\d{2}-\d{2}", last(eachline(p_dailies))).match
          end
          unique!(t_s)
          if size(t_s, 1) > countlines(p)
            db = parsefile(p)
            for t_ in t_s
              db[t_] = read_csv(
                                p,
                                DataFrame,
                                stringtype=String,
                                truestrings=nothing,
                                falsestrings=nothing,
                                select=[1]
                               ).Tckr
              unique!(db[t_])
            end
            open(p, "w") do io
              print_toml(io, db)
            end
            @info "\nRefactored Csilla DB of " * bunch * ".\n" *
                  "Number of dates tweaked: " * length(db) * ".\n"
          end
        end
      end
    end
    
    #5> vagen -> Foto
    let path_foto = Unnur["Fotografi"]
      enn = 0
      for bunch in getfield.(descr, :first)
        if in(bunch, ("Mercure",))
          enn += *(6, size(Unnur[bunch], 1))
        else
          enn += *(16, size(Unnur[bunch], 1))
        end
      end
      if isempty(path_foto) || isless(countlines(path_foto), enn)
        open(path_foto, "w") do io
          print_toml(
                     io,
                     Dict{String,Any}(
                                      bunch => Dict{String,Any}()
                                     ) for bunch in getfield.(descr, :first)
                    )
        end
      end
    end
    open(getfield(vagen, :Unnur), "w") do io
      print_toml(io, Unnur)
    end
  end
end
##
## Processing alkukirjain
##
alkukirjain()