# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      ZsofiaDB
      
  An abstract type representing Zsofia's data base (dir tree).
  
  # Notes
  **To developers**, make sure to implement a `Base.print` method while
  adding a `ZsofiaDB` subtype. The implementation rely on it throw the
  `Base.string` function.
  
  I have left the way open to anyone in need of more sophisticated database
  management to built their facility by defining a subtype of this abstract
  type.
  
  Basically, what I had in my head when building this `asbtract type` is a
  directory tree stucture identifier associated to (possibly internals)
  methods/functions such as [`collect`](@ref), [`_collect`](@ref),
  [`_path_to`](@ref), [`read`](@ref), [`write`](@ref) etc... to interact with,
  for the purposes of fetching, reading, storing informations. Price actions
  data are stored as a comma-separated-value (csv) files, meta-information data
  are stored as TOML files.
  
  See also, [`BUNCH`](@ref), [`UNNUR`](@ref), [`ZsofiaDBContext`](@ref).
  
  ## TO DO
  **I will properly and more clearly describe the design of this sooner.**
  """
  abstract type ZsofiaDB end
  
  """
      ZsofiaDBContext
  
  An abstrat type representing **Zsofia**'s data base *silos*. Actually,
  each silo (subtypes of `ZsofiaDBContext`) is implemented as a subdirectory
  of each bunch in `BUNCH`. For more details, see `Hrafnhildur` doccumentation.
  
  # Subtypes
  The initial implementation is made of four (04) subtypes:
  - `Bjorn`: `Hjordis` bunched/recorded together under the same `BUNCH`.
  - `Brynhild`: lower frequency price action records, typically day-to-day.
  - `Hjordis`: higher frequency price action records, typically minute-to-minute
  - `Valtyr`: fundamentals, quantitative metrics, various meta informations.
  """
  abstract type ZsofiaDBContext end
  
  """
      Hrafnhildur <: ZsofiaDB
  
  A concrete (sub)type of `ZsofiaDB`, representing a dir-tree structure.
  
  # Description
  Every bunch (a bunch is an element of the `BUNCH` global constant) is
  associated to `Observables` representing sharing an underlying common theme.
  For example `"Artemis"` is a bunch gathering together `Stock`s having
  that underlying theme of being a stock. For equity `Futures` precizely
  `Futures{Equity}`, we associated the bunch `"Nemesis"`... use the internal
  facility `_to_bunch` to guess to which bunch the `Observables`' instance 
  belongs to.
  
  `Hrafnhildur`'s directory tree structure is organized in such a way that
  any element (bunch) of the global constant `BUNCH::NTuple{9,AsbtractString}`
  has every of the `ZsofiaDBContext`'s (direct) concrete subtypes as directory
  which we usually refer to as *silo*s.
  
  |**sub directory of `Hrafnhildur`**|**notes**|
  |:---:|:---:|
  |`Bjorn`|no subdirs for `Fetchables` (unless `Stock` by geo-zones)|
  |`Brynhild`|no subdirs for `Fetchables` (unless `Stock` by geo-zones)|
  |`Hjordis`|no subdirs for `Fetchables` (unless `Stock` by geo-zones)|
  |`Valtyr`|no subdirs for `Fetchables` (unless `Stock` by geo-zones)|
  
  > **`Hjordis`** has another layer of subdivision by trading session.
  
  See [`ZsofiaDBContext`](@ref) for more details.
  
  Visually, the tree-structure is as following:
  
  .Hrafnhildur
  ├── bunch
      ├── Bjorn
      │   ├── ...
      │   └── ...
      ├── Brynhild
      │   ├── ...
      │   └── ...
      ├── Hjordis
      │   ├── Hjordis_date_i
      │   │   ├── ...
      │   │   └── ...
      │   └── ...
      └── Valtyr
          ├── ...
          └── ...
  """
  struct Hrafnhildur <: ZsofiaDB end
  
  function Base.show(io::IO, ::MIME"text/plain", ::Hrafnhildur)
    print(io, "Hrafnhildur")
  end
  
  Base.string(::Hrafnhildur) = "Hrafnhildur"
  
  Base.print(io::IO, ::Hrafnhildur) = print(io, "Hrafnhildur")
  
  """
      Brynhild <: ZsofiaDBContext
  
  A `ZsofiaDBContext`'s singleton type intended to hold lower frequencies
  price actions data records, typically day-to-day.
  
  **This is usually records of CSV files.**
  
  See also [`Hjordis`](@ref), [`Bjorn`](@ref), [`Valtyr`](@ref).
  """
  struct Brynhild <: ZsofiaDBContext end
  
  Base.show(io::IO, ::MIME, ::Brynhild) = print(io, "Brynhild")
  
  Base.string(::Brynhild) = "Brynhild"
  
  Base.print(io::IO, ::Brynhild) = print(io, "Brynhild")
  
  
  """
      Hjordis <: ZsofiaDBContext
  
  A `ZsofiaDBContext`'s singleton type intended to hold/represent higher
  frequencies price actions data records, typically minute-to-minute.
  
  **This is usually records of CSV files.**
  
  See also [`Bjorn`](@ref), [`Brynhild`](@ref), [`Valtyr`](@ref).
  """
  struct Hjordis <: ZsofiaDBContext end
  
  Base.show(io::IO, ::MIME, ::Hjordis) = print(io, "Hjordis")
  
  Base.string(::Hjordis) = "Hjordis"
  
  Base.print(io::IO, ::Hjordis) = print(io, "Hjordis")
  
  """
      Bjorn <: ZsofiaDBContext
      
  A `ZsofiaDBContext`'s singleton type intended to hold/represent higher
  frequencies price actions data records, gathered together by trading
  sessions. The difference with `Hjordis` is that `Bjorn` bunch together
  `Hjordis` records into a single file by trading session.
  
  `Hjordis` gathers together several `Observables` (of the same theme) records
  individually while `Bjorn` gather the proceed of `Hjordis`es into the same
  record.
  
  **This is usually records of CSV files.**
  
  See also [`Brynhild`](@ref), [`Hjordis`](@ref), [`Valtyr`](@ref).
  """
  struct Bjorn <: ZsofiaDBContext end
  
  Base.show(io::IO, ::MIME, ::Bjorn) = print(io, "Bjorn")
  
  Base.string(::Bjorn) = "Bjorn"
  
  Base.print(io::IO, ::Bjorn) = print(io, "Bjorn")
  
  
  """
      Valtyr <: ZsofiaDBContext

  A `ZsofiaDBContext`'s singleton type intended to hold/represent meta data
  informations, quantitative metrics beside price action.
  
  **This is usually records of TOML files.**

  See also [`Bjorn`](@ref), [`Brynhild`](@ref), [`Valtyr`](@ref).
  """
  struct Valtyr <: ZsofiaDBContext end
  
  Base.show(io::IO, ::MIME, ::Valtyr) = print(io, "Valtyr")
  
  Base.string(::Valtyr) = "Valtyr"
  
  Base.print(io::IO, ::Valtyr) = print(io, "Valtyr")
  
  """
      YahooFetchables
      
  A `Union` type specifying which asset classes have at least one of
  their subtypes implemented to be fetched from the Yahoo Finance
  API and nothing else (but nothing).
  """
  const YahooFetchables = Union{
                                Stock, Futures, ETF, FXRate,
                                CryptoCurrency, Indices, AmericanOption
                          }
  
  """
      FredFetchables
  
  A `Union` type specifying which asset classes have at least one of
  their subtypes implemented to be fetched from the St-Louis Fed's FRED
  API and nothing else (but nothing).
  """
  const FredFetchables = Union{EconoIndicator,}
  
  """
      Fetchables
  
  This constant type is defined to be `Union{YahooFetchables,FredFetchables}`.
  
  See also [`YahooFetchables`](@ref), [`FredFetchables`](@ref).
  """
  const Fetchables = Union{FredFetchables,YahooFetchables}
end