"""
**Name**: `tiradd`

**NArgs**: (1,2)

**Aruments**: `bunchee` the asset class, `kromi` as first kwargs representing
the vector of instruments from `bunchee` to be added; and
`rfyl` the errors file dump.

**Description**: This function initializes the Tyyni container of
instruments given in argument (`kromi`) without any check of prior
belonging of "Unnured" Bunch...
"""
function tiradd(
                bunchee::String;
                kromi::Vector{String}=String[],
                rfyl=Rfyl
         )
  printstyled("[TIRAW :: $bunchee :: ADD]\n",color=10,bold=true)
  printstyled("Meant to add some folks into $bunchee...\n",color=102)
  them=kromi
  if !isempty(them)
    þ(i)=Silk*bunchee*"/Tyyni/"*i*".yaml"
    while !isempty(them)
      fi=FiZ(bunchee,pop!(them),Junkme[bunchee])
      touch(þ(getfield(fi,:tckr)))
      if iszero(lstat(þ(fi.tckr)).size)
        printstyled("Formatting ", fi.tckr, "...\n",color=102,bold=true)
        đ=Dict{String,Any}(k => nothing for k in þµŋ[bunchee])
        YAML.write_file(þ(getfield(fi,:tckr)), đ, "---\n")
        printstyled(
                    Char(0x3008),
                    fi.land,
                    "|",
                    fi.tckr,
                    Char(0x3009),
                    " created: ",
                    Char(0x2714),
                    "\n",
                    color=10,
                    bold=true
        )
      else
        printstyled("Better to leave ", fi.tckr, " as is!\n",color=11,bold=true)
      end
    end
  else
    printstyled("WARNING!\n",
                "This will wash out the current TYYNI of $bunchee ...\n",
                "Continue? [y/N]: ",
                color=11,
                bold=true
    )
    yN = readuntil(stdin,"\n") |> string
    if yN in ("yes","YES","y","Y","Yes")
      b=YAML.load_file("Unnur.yaml")[bunchee]
      while !isempty(b)
        i=pop!(b)
        touch(þ(i))
        if iszero(lstat(þ(i)).size)
          đ=Dict{String,Any}(k => nothing for k in þµŋ[bunchee])
          YAML.write_file(þ(i), đ, "---\n")
          printstyled(
                      Char(0x3008),
                      bunchee,
                      "|",
                      i,
                      Char(0x3009),
                      " created: ",
                      Char(0x2714),
                      "\n",
                      color=10,
                      bold=true
          )
        else
          printstyled("Better to leave ", i, " as is!\n",color=11,bold=true)
        end
      end
    else
      printstyled("$bunchee is kept safe as it is!\n",color=102,bold=true)
    end
  end
end
"""
**Name**: `tyynizupdate`

**Summary**: `tyynizupdate` computes TIRAW's metrics
from local database.
"""
function tyynizupdate(fi::FiZ; rfyl=Rfyl)
  messtyynizup(
            msg;
            fyle=stdout,
            kol=102
  )=heil(
              msg,
              rfyl=fyle,
              fi=fi,
              iam="tyynizupdate",
              kol=kol,
              bud=true
  )
  þ = Silk*fi.land*"/Tyyni/"*fi.tckr*".yaml"
  tb = CSV.read(
                Silk*fi.land*"/"*fi.tckr*".csv",
                DataFrame,
                stringtype=String,
                truestrings=nothing,
                falsestrings=nothing
  )
  if isfile(þ)
    try
      đ = YAML.load_file(þ)
      messtyynizup("table initialized!")
    catch err
      messtyynizup("[FST] ↓↓"*string(err), kol=3, fyle=rfyl)
      return nothing
    end
  else
    đ = Dict{String, Any}()
    messtyynizup("table initialized!")
  end
  for ĸ in keys(µtyyniz)
    if in(ĸ, ("annikki",)) && in("Vol", names(tb))
      try
        nuttin=get(đ, "modelY", Dict())
        a=length(keys(nuttin))
        b=get(nuttin, "UpdatedOn", firstdayofyear(today()))
        if isless(a,19) || isless(b, firstdayofmonth(today()))
          đ=merge(đ, µtyyniz[ĸ](tb))
          messtyynizup("$ĸ has been generated!")
        end
      catch err
        messtyynizup("[FST] while working $ĸ → :"*string(err), fyle=rfyl)
        đ=merge(đ, µtyyniz[ĸ](tb))
        continue
      end
    else
      try
        !in("Vol", names(tb)) ? continue : nothing
        messtyynizup("working $ĸ ...")
        đ[ĸ] = µtyyniz[ĸ](tb)
        messtyynizup("$ĸ has been generated!")
      catch err
        messtyynizup("[FST] while working $ĸ → :"*string(err), fyle=rfyl)
        continue
      end
    end
  end
  YAML.write_file(þ, đ, "---\n")
  messtyynizup("saved!", fyle=stdout, kol=13)
end
############ END OF METRICS ZONE #######
"""
**Name**: tirupdate

**NArgs**: (1,1)

**Args Decr.**: The function updates the TIRAW's Tyyni DB.
It takes one argument `bunchee::String` one of
`const Bunch`, applies the update to each Bunch instrument,
and return the list of non-updated instrument. And return nothing
if `none` of them has been processed.
"""
function tirupdate(
                   bunchee::String;
                   kromi::Vector{String}=String[],
                   rfyl=Rfyl,
                   smrz::Bool=false
         )
  tc=("SOME","TUTTI", "SUMMARY")
  pathee=Silk*bunchee*"/Tyyni/"
  mkpath(pathee)
  if isempty(kromi) && !all(smrz)
    ctc=findfirst(x -> isequal(x,"TUTTI"), tc) 
  elseif !isempty(kromi) && !all(smrz)
    ctc=findfirst(x -> isequal(x,"SOME"), tc)
  elseif isempty(kromi) && all(smrz)
    ctc=findfirst(x -> isequal(x,"SUMMARY"), tc)
  else
    ctc=nothing
    printstyled("This Combo-Command is not implemented!\n",color=11)
  end
  if isnothing(ctc)
    print("\tExiting TIRAW[UPDATE] "*Char(0x2234)*"\n")
    for u in [Char(0x24f2), Char(0x24f1), Char(0x24f0)]
      printstyled(u*" ")
      sleep(0.625)
    end
    sleep(0.75)
    return nothing
  elseif !iszero(ctc)
    if isequal(tc[ctc], "SOME")
      them=kromi
      if isless(length(them), 1) || isempty(them)
        printstyled("Nothing has been read!\nExiting...\n", color=13)
        for u in (Char(0x2778), Char(0x2777), Char(0x2776))
          printstyled(u*" ")
          sleep(0.625)
        end
        println("")
        sleep(0.75)
        return nothing
      end
      fi=Vector{PyZsofia.PyIz}(undef, length(them))
      for k in range(1, length(them))
        fi[k]=PyZsofia.PyIz(bunchee, them[k], Junkme[bunchee])
      end
      for i in fi
        try
          þ=Silk*bunchee*"/Tyyni/"*i.tckr*".yaml"
          isfile(þ) ? đ=YAML.load_file(þ) : đ=Dict{Any,Any}()
          dick=tyyni(i, rfyl=rfyl)
          if isempty(dick)
            printstyled(
                        Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" ",
                        "is skipped! (new dick is empty)\n",
                        color=95,
                        bold=true
            )
            sleep(0.625)
            continue
          else
            merge!(đ, dick)
            YAML.write_file(þ, đ, "---\n")
            printstyled(
                        Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" ",
                        color=102,
                        bold=true
                       )
            printstyled(
                        " has been updated successfully!\n",
                        color=102,
                        bold=true
                       )
          end
        catch err
          printstyled(
                      "\t[:TIRAW::Tyyni]: "*
                      Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" "*
                      "non-smoothly process!\n",
                      color=9,
                      bold=true
          )
          heil(
                    "[:TIRAW::Tyyni]: "*
                    Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" "*
                    "non smooth process:\n"*
                    string(err),
                    rfyl=rfyl
          )
          þ=Silk*bunchee*"/Tyyni/"*i.tckr*".yaml"
          đ=tyyni(i, rfyl=rfyl)
          isempty(đ) ? nothing : YAML.write_file(þ, đ, "---\n")
        end
      end
    elseif isequal(tc[ctc], "TUTTI")
      b=YAML.load_file("Unnur.yaml")[bunchee]
      if !isempty(b)
        printstyled(
                    "[:TIRAW::Tyyni] containerizing $bunchee ...",
                    color=102,
                    bold=true
        )
        fi=Vector{PyZsofia.PyIz}(undef, length(b))
        ioane=Vector{Task}() ## defining task vector.
        for k in range(1,length(b))
          fi[k]=PyZsofia.PyIz(bunchee, b[k], Junkme[bunchee])
        end
        printstyled(" done!\n", color=102, bold=true)
        for i in fi
          push!(
            ioane,
            @task begin
              đ=Dict{String,Any}()
              try
                if isfile(pathee*i.tckr*".yaml")
                  đ=YAML.load_file(pathee*i.tckr*".yaml")
                  dick=tyyni(i, rfyl=rfyl)
                  merge!(đ,dick)
                else
                  đ=tyyni(i, rfyl=rfyl)
                end
                if isempty(đ)
                  nothing
                else
                  YAML.write_file(pathee*i.tckr*".yaml", đ, "---\n")
                  printstyled(
                              Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" ",
                              color=102,
                              bold=true
                  )
                  printstyled(
                              " has been updated successfully!\n",
                              color=102,
                              bold=true
                  )
                end
              catch err
                  if isa(err, YAML.ScannerError)
                    đ=tyyni(i, rfyl=rfyl)
                  else
                    printstyled(
                                "[:TIRAW::Tyyni::Update]: "*
                                Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" "*
                                "Something went wrong!\n",
                                color=9,
                                bold=true
                    )
                    
                    heil(
                              "[:TIRAW::Tyyni]: "*
                              Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*" "*
                              " faced some troubles:\n"*
                              string(err),
                              rfyl=rfyl
                    )
                  end
              end
            end ## End of Begin block
          )
        end ## End of For loop.
        #-------------TASKYING----------
        (schedule.(ioane); wait.(ioane))
        #-------------------------------
      else
        printstyled(
                    "$bunchee (see Unnur.yaml) is empty!\n"*
                    "Nothing to be done.\n",
                    color=13,
                    bold=true
        )
        sleep(0.5)
      end ## End of if-else block.
      ioane, b, fi = nothing, nothing, nothing
    elseif isequal(tc[ctc], "SUMMARY")
      tyd = filter(x -> occursin(".yaml", x), readdir(pathee))
      tyd = map(x -> x[begin:(end-5)], tyd)
      uud = YAML.load_file("Unnur.yaml")[bunchee]
      tyd, uud = (tyd, length(tyd)), (uud, length(uud))
      tb=DataFrame(
                   :A => tyd[2],
                   :B => uud[2]
         )
      transform!(tb, [:B, :A] => ByRow((x,y) -> 100*(x\y)) => :C)
      DataFrames.rename!(
                         tb,
                         :B => :Unnured,
                         :A => :Tyynied,
                         :C => :Coverage
      )
      Ŧ=tb.Coverage[1]
      transform!(tb, :Coverage =>
                                 ByRow(
                                       (x -> string(
                                                    round(
                                                          x,
                                                          digits=2
                                                    )
                                             )*"%")
                                 ) => :Coverage
      )
      show(tb, summary=false, eltypes=false, show_row_number=false)
      readuntil(stdin, "\n")
      if Ŧ < 100
        printstyled("\nNON TYYNIed INSTRUMENTS...", color=3, bold=true)
        readuntil(stdin, "\n")
        println("")
        for j in setdiff(uud[1],tyd[1])
          printstyled(j*" ", color=13, bold=true)
        end
        readuntil(stdin, "\n")
      elseif Ŧ > 100
        printstyled(
                    "\nThe following instruments are TYYNIed but not"*
                    "UNNURed:\n", color=3, bold=true)
        for j in setdiff(tyd[1], uud[1])
          printstyled(j*" ", color=13, bold=true)
        end
        readuntil(stdin, "\n")
        printstyled("\nFix it? [y/N]:  ", color=95)
        (rεsp=readuntil(stdin, "\n"); rεsp=string(rεsp[1]))
        if rεsp in ("y", "Y", "yes", "Yes", "YES")
          Ð=setdiff(tyd[1], uud[1])
          for l in Ð
            printstyled(
                        "Remove ",
                        l,
                        " from "*
                        Char(0x3008)*bunchee*"|"*
                        Char(0x2022)*Char(0x3009)*"? [y/N]:  ",
                        color=96,
                        bold=true
            )
            (r=readuntil(stdin,"\n"); r=string(r[1]))
            if r in ("y", "Y", "yes", "Yes", "YES")
              rm(
                 Silk*bunchee*"/Tyyni/"*l*".yaml",
                 force=true
              )
              printstyled(
                          l,
                          " unTYYNIed: ",
                          Char(0x2714)*"\n",
                          color=10
              )
            else
              printstyled(
                          l,
                          " unTYYNIed: ",
                          Char(0x2717)*"\n",
                          color=9
              )
            end
          end
        else
          printstyled(
                      "\nNot Fixing unTYYNIed from $bunchee ...",
                      "Exiting...\n",
                      color=102,
                      bold=true
          )
        end
      end
      tyd, uud, tb = nothing, nothing, nothing
    end
  else
    nothing
  end
end
