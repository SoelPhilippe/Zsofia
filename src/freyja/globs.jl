# This script is part of Zsofia. Soel Philippe © 2025

begin
  let
    _init_zsofia_db("Hrafnhildur")
  end
  
  """
     HRAFNHILDUR
     
  A constant holding the singleton type `Hrafnhildur`, which is the
  built-in data base.
  """
  const HRAFNHILDUR = Hrafnhildur()
  
  """
      BRYNHILD
  
  A global constant holding the singleton instance of `Brynhild`.
  """
  const BRYNHILD = Brynhild()
  
  """
      HJORDIS
  
  A global constant holding the singleton instance of `Hjordis`.
  """
  const HJORDIS = Hjordis()
  
  """
      BJORN
      
  A global constant holding the singleton instance of `Bjorn`.
  """
  const BJORN = Bjorn()
  
  """
      VALTYR
      
  A global constant holding the singleton instance of `Valtyr`.
  """
  const VALTYR = Valtyr()
  
  
  """
      APIstocks::Dict{HolidayCalendar, AbstractString}
  
  # Internal Facility
  A global constant holding a map (as `Dict`object) from `HolidayCalendar`s to
  corresponding API-friendly silos.
  
  The current implementation is supposed to hold the following correspondences.
  This is for information purpose only, the key/value may be different. Refer
  to runtime values.
  
  |Calendar|SiloString|Description|
  |---:|:---|:---|
  |`AU`|".AX"|Australia|
  |`BE`|".BR"|Brussels|
  |`CA`|".TO"|Toronto|
  |`CN`|".HK"|Hong-Kong|
  |`DK`|".CO"|Copenhagen|
  |`EE`|".TL"|Tulsa|
  |`FI`|".HE"|Helsinki|
  |`FR`|".PA"|Paris|
  |`DE`|".DE"|Deutschland|
  |`HK`|".HK"|Hong-Kong|
  |`IS`|".IS"|Iceland|
  |`IN`|".IC"||
  |`IE`|".IR"|Ireland|
  |`IL`|".TA"|Tel-Aviv|
  |`IT`|".MI"|Milan|
  |`JP`|".T"|Tokyo|
  |`LV`|".RG"|Riga|
  |`LT`|".VS"||
  |`NL`|".AS"|Amsterdam|
  |`NO`|".OL"|Oslo|
  |`PT`|".LS"|Lisbon|
  |`SA`|".DB"|Dubai|
  |`ZA`|".JH"|Johannesburg|
  |`ES`|".MC"|Madrid|
  |`SE`|".ST"|Stockholm|
  |`CH`|".SW"|Switzerland|
  |`GB`|".L"|London|
  |`US`|""|NewYork|
  
  # Notes
  These are conventions we adopted, therefore not meant to be modified.
  """
  const APIstocks::Dict{HolidayCalendar,AbstractString} = begin
    Dict{HolidayCalendar,AbstractString}(
                                 AU => ".AX", BE => ".BR",
                                 CA => ".TO", CN => ".HK",
                                 DK => ".CO", EE => ".TL",
                                 FI => ".HE", FR => ".PA",
                                 DE => ".DE", HK => ".HK",
                                 IS => ".IS", IN => ".IC",
                                 IE => ".IR", IL => ".TA",
                                 IT => ".MI", JP => ".T",
                                 LV => ".RG", LT => ".VS",
                                 NL => ".AS", NO => ".OL",
                                 PT => ".LS", SA => ".DB",
                                 ZA => ".KR", ES => ".MC",
                                 SE => ".ST", CH => ".SW",
                                 GB => ".L",  US => ""
    )
  end
  
  
  """
      UNNUR::Dict{AbstractString, Any}
  
  # Internal facility
  A constant holding the current state of `Zsofia`.
  """
  const UNNUR::Dict{AbstractString,Any} = Dict{AbstractString,Any}()
  
  """
      VALKYRJA::SimpleLogger
  
  # Internal Facility
  A constant holding the errors (dumping) logger.
  """
  const VALKYRJA::SimpleLogger = let 
    p = joinpath(homedir(), ".Zsofia", ".Valkyrja")
    _init_valkyrja(p)
  end
  
  """
      FOTO::Dict{AbstractString,Any}
  
  # Internal Facility
  A constant intended to hold the `fotografi` of the latest state of zsofia.
  This is useful at the database level to track current levels of financial
  instruments prices.
  """
  const FOTO::Dict{AbstractString,Any} = Dict{AbstractString,Any}()
  
  function __init__()
    merge!(UNNUR, _init_unnur())
    merge!(FOTO, _init_fotografi())
    nothing
  end
  
  """
      TYYNITAG::Dict{AbstractString, Dict{AbstractString, AbstractString}}

  A map associating bunches (Artemis, ..., Nemesis...) to maps of
  user-friendly tags associated to server-friendly tags.
  """
  const TYYNITAGs = begin
    Dict(
         "Artemis" => Dict(
                           "Address" => "address1", "City" => "city",
                           "State" => "state", "Zip" => "zip",
                           "ISIN" => "isin", "Country" => "country",
			                     "Phone" => "phone", "Website" => "website",
			                     "Industry" => "industry", "Sector" => "sector",
                           "BusinessSummary" => "longBusinessSummary",
                           "AuditRisk" => "auditRisk","BoardRisk"=> "boardRisk",
                           "CompensationRisk" => "compensationRisk",
                           "ShareholderRisk" => "shareHolderRightsRisk",
                           "OverAllRisk" => "overallRisk",
                           "Beta" => "beta","Employees" => "fullTimeEmployees",
                           "DividendRate" => "dividendRate",
                           "DividendYield" => "dividendYield",
			                     "Cap" => "marketCap",
                           "FloatingShares" => "floatShares",
                           "OutstandingShares" => "sharesOutstanding",
                           "SharesShort" => "sharesShort",
                           "SharesShortPctOfFloat" => "shortPercentOfFloat",
                           "SharesShortMonthPrior" => "sharesShortMonthPrior",
                           "InsidersPctHeld" => "heldPercentInsiders",
                           "InstitutionsPctHeld" => "heldPercentInstitutions",
                           "ShortRatio" => "shortRatio",
                           "LastFiscalYearEnd" => "lastFiscalYearEnd",
                           "NextFiscalYearEnd" => "nextFiscalYearEnd",
                           "LastSplitFactor" => "lastSplitFactor",
                           "LastSplitDate" => "lastSplitDate",
                           "LastDividendValue" => "lastDividendValue",
                           "LastDividendDate" => "lastDividendDate",
                           "exDate" => "exDividendDate",
                           "DebtToEquity" => "debtToEquity",
			                     "Ebitda" => "ebitda",
                           "DebtTotal" => "totalDebt",
                           "QuickRatio" => "quickRatio",
                           "RoA" => "returnOnAssets",
                           "RoE" => "returnOnEquity",
			                     "BookValue" => "bookValue",
                           "PriceToBook" => "priceToBook",
                           "Cash" => "totalCash",
                           "CashPerShare" => "totalCashPerShare",
                           "QuickRatio" => "quickRatio",
                           "FirstTrade" => "firstTradeDateEpochUtc",
                           "Exchange" => "exchange",
			                     "Name" => "shortName",
                           "Symbol" => "symbol", "Isin" => "isin",
                           "Officers" => "companyOfficers",
                           "Currency" => "currency",
                           "MutualFundHolders" => "MutualFundHolders",
                           "NumberOfSharesHist" => "NumberOfSharesHist",
                           "InstitutionalHolders" => "InstitutionalHolders"
                      ),
         "Demeter" => Dict(
                           "Underlying" => "underlyingSymbol",
                           "Expiration" => "expireDate",
			                     "Name" => "shortName",
                           "OpenInterest" => "openInterest",
                           "FirstTrade" => "firstTradeDateEpochUtc",
                           "Symbol" => "symbol",
			                     "Exchange" => "exchange",
                           "Currency" => "currency"
                      ),
         "Euterpe" => Dict(
                           "Exchange" => "exchange",
                           "TimeZone" => "timeZoneFullName",
                           "Currency" => "currency",
			                     "QuoteType" => "quoteType",
                           "Name" => "shortName"
                      ),
         "Nemesis" => Dict(
                           "Underlying" => "underlyingSymbol",
                           "Expiration" => "expireDate",
			                     "Name" => "shortName",
                           "FirstTrade" => "firstTradeDateEpochUtc",
                           "OpenInterest" => "openInterest",
			                     "Symbol" => "symbol",
                           "Currency" => "currency",
			                     "Exchange" => "exchange"
                      ),
         "Ophelya" => Dict(
                           "LegalType" => "legalType",
                           "FundFamily" => "fundFamily",
                           "LongName" => "longName",
                           "Underlying" => "underlyingSymbol",
			                     "Yield" => "yield",
                           "TotalAssets" => "totalAssets",
			                     "Beta3Yr" => "beta3Year",
                           "TimeZone" => "timeZoneFullName",
                           "NavPrice" => "navPrice",
			                     "Name" => "shortName",
                           "FirstTrade" => "fundInceptionDate",
			                     "Symbol" => "symbol",
                           "Currency" => "currency",
			                     "Category" => "category",
                           "Exchange" => "exchange"
                      ),
         "Origami" => Dict(
                           "Address" => "address1",
			                     "City" => "city",
			                     "Zip" => "zip",
                           "Country" => "country",
			                     "Phone" => "phone",
                           "Website" => "website",
			                     "Industry" => "industry",
                           "Sector" => "sector",
                           "BusinessSummary" => "longBusinessSummary",
                           "AuditRisk" => "auditRisk",
			                     "BoardRisk"=> "boardRisk",
                           "CompensationRisk" => "compensationRisk",
                           "ShareholderRisk" => "shareHolderRightsRisk",
                           "OverAllRisk" => "overallRisk",
			                     "Beta" => "beta",
                           "Employees" => "fullTimeEmployees",
                           "DividendRate" => "dividendRate",
                           "DividendYield" => "dividendYield",
			                     "Cap" => "marketCap",
                           "FloatingShares" => "floatShares",
                           "OutstandingShares" => "sharesOutstanding",
                           "InsidersPctHeld" => "heldPercentInsiders",
                           "InstitutionsPctHeld" => "heldPercentInstitutions",
                           "LastFiscalYearEnd" => "lastFiscalYearEnd",
                           "NextFiscalYearEnd" => "nextFiscalYearEnd",
                           "LastSplitFactor" => "lastSplitFactor",
                           "LastSplitDate" => "lastSplitDate",
                           "LastDividendValue" => "lastDividendValue",
                           "LastDividendDate" => "lastDividendDate",
                           "exDate" => "exDividendDate",
                           "DebtToEquity" => "debtToEquity",
                           "Ebitda" => "ebitda",
                           "DebtTotal" => "totalDebt",
			                     "QuickRatio" => "quickRatio",
                           "RoA" => "returnOnAssets",
			                     "RoE" => "returnOnEquity",
                           "BookValue" => "bookValue",
			                     "PriceToBook" => "priceToBook",
                           "Cash" => "totalCash",
                           "CashPerShare" => "totalCashPerShare",
                           "QuickRatio" => "quickRatio",
                           "FirstTrade" => "firstTradeDateEpochUtc",
                           "Exchange" => "exchange",
			                     "Name" => "shortName",
                           "Symbol" => "symbol",
			                     "Isin" => "isin",
                           "Officers" => "companyOfficers",
			                     "Currency" => "currency",
                           "MutualFundHolders" => "MutualFundHolders",
                           "NumberOfSharesHist" => "NumberOfSharesHist",
                           "InstitutionalHolders" => "InstitutionalHolders"
                      ),
         "Phorcys" => Dict(
                           "Exchange" => "exchange",
			                     "QuoteType" => "quoteType",
                           "Symbol" => "symbol",
			                     "Name" => "shortName",
                           "Currency" => "currency"
                      ),
         "Satoshi" => Dict(
                           "CirculatingSupply" => "circulatingSupply",
                           "Exchange" => "exchange",
			                     "QuoteType" => "quoteType",
                           "Name" => "shortName",
			                     "Symbol" => "symbol",
                           "TimeZone" => "timeZoneFullName",
			                     "Exchange" => "exchange"
                      ),
         "Mercure" => Dict(
                           "Id" => "id",
			                     "Title" => "title",
                           "UnitsShort" => "units_short",
			                     "Units" => "units",
                           "SeasAdjShort" => "seas_adj_short",
                           "SeasAdj" => "seas_adj",
			                     "FreqShort" => "freq_short",
                           "Frequency" => "freq",
                           "RealTimeStart" => "realtime_start",
                           "RealTimeEnd" => "realtime_end",
                           "LastUpdated" => "last_updated",
			                     "Notes" => "notes",
                           "TransShort" => "trans_short"
                      )
    )
  end
  
  Zojlz::Dict{AbstractString, AbstractString} = begin
    Dict{AbstractString, AbstractString}(
                         "RoE" => "RoE",
                         "NextFiscalYearEnd" => "FY",
                         "City" => "City",
                         "State" => "State",
                         "Employees" => "EE",
                         "DebtTotal" => "DEBT",
                         "LastDividendDate" => "LastDividendDate",
                         "CompensationRisk" => "WagesRisk",
                         "FloatingShares" => "Floating",
                         "Cash" => "Cash",
                         "Beta" => "Beta",
                         "Sector" => "Sector",
                         "Exchange" => "Exchange",
                         "Currency" => "Currency",
                         "BusinessSummary" => "Descr.",
                         "InsidersPctHeld" => "Insiders",
                         "ShortRatio" => "ShortRatio",
                         "DividendYield" => "DivY",
                         "InstitutionsPctHeld" => "Institutions",
                         "LastSplitDate" => "LastSplitDate",
                         "DebtToEquity" => "DoE",
                         "exDate" => "ex-Date",
                         "LastDividendValue" => "DivV",
                         "Symbol" => "Ticker",
                         "LastFiscalYearEnd" => "LastFY",
                         "OverAllRisk" => "OverAllRisk",
                         "FirstTrade" => "IPO",
                         "LastSplitFactor" => "Split",
                         "OutstandingShares" => "Out.Sh",
                         "BoardRisk" => "BoardRisk",
                         "QuickRatio" => "QuickRatio",
                         "Cap" => "Mkt.Cap",
                         "Industry" => "Industry",
                         "RoA" => "RoA",
                         "Website" => "Website",
                         "Ebitda" => "EBITDA",
                         "SharesShort" => "SharesShort",
                         "SharesShortPctOfFloat" => "ShortoFloat",
                         "Country" => "Country",
                         "Address" => "Address",
                         "Phone" => "Phone",
                         "ShareholderRisk" => "ShHldrRisk",
                         "Name" => "Name",
                         "DividendRate" => "DivR",
                         "AuditRisk" => "AuditRisk",
                         "Zip" => "Zip",
                         "PriceToBook" => "PoB",
                         "BookValue" => "Book",
                         "CashPerShare" => "CoS",
                         "QuickRatio" => "QuickR",
                         "Yield" => "Yield",
                         "CirculatingSupply" => "Circ.Supply",
                         "SeasAdj" => "Seas.Adj",
                         "QuoteType" => "Type",
                         "TransShort" => "TransShort",
                         "FreqShort" => "Frequency",
                         "LegalType" => "Legal.Type",
                         "Expiration" => "Exp.",
                         "RealTimeEnd" => "Obs.",
                         "Title" => "Descr.",
                         "FundFamily" => "Family",
                         "SeasAdjShort" => "Seas.Adj",
                         "Category" => "Category",
                         "Officers" => "Officers",
                         "Units" => "Unit",
                         "TimeZone" => "TZ",
                         "LastUpdated" => "Updated",
                         "Id" => "Ticker",
                         "NavPrice" => "NAVP",
                         "RealTimeStart" => "Obs.",
                         "OpenInterest" => "OI",
                         "Isin" => "isin",
                         "Underlying" => "Und.",
                         "SharesShortMonthPrior" => "LastMonthShorted",
                         "LongName" => "Name",
                         "UnitsShort" => "Unit",
                         "Frequency" => "Freq.",
                         "Notes" => "More",
                         "TotalAssets" => "Assets",
                         "Beta3Yr" => "Beta3Y"
    )
  end
end