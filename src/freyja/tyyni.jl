þs = Dict{String, String}(
                           "RoE" => "RoE",
                           "NextFiscalYearEnd" => "FY",
                           "City" => "City",
                           "State" => "State",
                           "Employees" => "EE",
                           "DebtTotal" => "DEBT",
                           "LastDividendDate" => "LastDividendDate",
                           "CompensationRisk" => "WagesRisk",
                           "FloatingShares" => "Floating",
                           "Cash" => "Cash",
                           "Beta" => "Beta",
                           "Sector" => "Sector",
                           "Exchange" => "Exchange",
                           "Currency" => "Currency",
                           "BusinessSummary" => "Descr.",
                           "InsidersPctHeld" => "Insiders",
                           "ShortRatio" => "ShortRatio",
                           "DividendYield" => "DivY",
                           "InstitutionsPctHeld" => "Institutions",
                           "LastSplitDate" => "LastSplitDate",
                           "DebtToEquity" => "DoE",
                           "exDate" => "ex-Date",
                           "LastDividendValue" => "DivV",
                           "Symbol" => "Ticker",
                           "LastFiscalYearEnd" => "LastFY",
                           "OverAllRisk" => "OverAllRisk",
                           "FirstTrade" => "IPO",
                           "LastSplitFactor" => "Split",
                           "OutstandingShares" => "Out.Sh",
                           "BoardRisk" => "BoardRisk",
                           "QuickRatio" => "QuickRatio",
                           "Cap" => "Mkt.Cap",
                           "Industry" => "Industry",
                           "RoA" => "RoA",
                           "Website" => "Website",
                           "Ebitda" => "EBITDA",
                           "SharesShort" => "SharesShort",
                           "SharesShortPctOfFloat" => "ShortoFloat",
                           "Country" => "Country",
                           "Address" => "Address",
                           "Phone" => "Phone",
                           "ShareholderRisk" => "ShHldrRisk",
                           "Name" => "Name",
                           "DividendRate" => "DivR",
                           "AuditRisk" => "AuditRisk",
                           "Zip" => "Zip",
                           "PriceToBook" => "PoB",
                           "BookValue" => "Book",
                           "CashPerShare" => "CoS",
                           "QuickRatio" => "QuickR",
                           "Yield" => "Yield",
                           "CirculatingSupply" => "Circ.Supply",
                           "SeasAdj" => "Seas.Adj",
                           "QuoteType" => "Type",
                           "TransShort" => "TransShort",
                           "FreqShort" => "Frequency",
                           "LegalType" => "Legal.Type",
                           "Expiration" => "Exp.",
                           "RealTimeEnd" => "Obs.",
                           "Title" => "Descr.",
                           "FundFamily" => "Family",
                           "SeasAdjShort" => "Seas.Adj",
                           "Category" => "Category",
                           "Officers" => "Officers",
                           "Units" => "Unit",
                           "TimeZone" => "TZ",
                           "LastUpdated" => "Updated",
                           "Id" => "Ticker",
                           "NavPrice" => "NAVP",
                           "RealTimeStart" => "Obs.",
                           "OpenInterest" => "OI",
                           "Isin" => "isin",
                           "Underlying" => "Und.",
                           "SharesShortMonthPrior" => "LastMonthShorted",
                           "LongName" => "Name",
                           "UnitsShort" => "Unit",
                           "Frequency" => "Freq.",
                           "Notes" => "More",
                           "TotalAssets" => "Assets",
                           "Beta3Yr" => "Beta3Y"
  )
begin
  """
      tyyni(fi::FiZ,[rfyl])
  
  Fetch `fi`-related information (fields) according (to the glob) `Mξ`.
  
  ## Details
  
  `tyyni` fetches a set of tagged (according to) informations
  according to `Mξ` and stores the proceed into a TOML file within
  the relevant Bunch in Hrafnhildur (each Bunch holds a tyyni sub-dir).
  """
  function tyyni(fi::FiZ;rfyl::Union{String,IO}=Rfyl)::Union{FiZ,Nothing}
    #globs>
    begin
      t0   = Dates.now()
      d0   = Dict{String,Any}()           #<-- the container
      de   = get(Mξ,fi.land,Dict{String,String}())
      poml = joinpath(Silk,fi.land,"Tyyni",string(fi.tckr,".toml"))
      pr(msg,kol=102)=heil(msg,fi=fi,iam="tyyni",kol=kol,bud=true,rfyl=stdout)
    end
    
    #valids>
    if ~in(fi.land,Bunch)
      printstyled(fi.land," is not a valid land.\nExiting...\n",color=11)
      return nothing
    end
    
    if ~in(fi.tckr,Unnur[fi.land])
      pr("is not unnured... It'll eventually be added in Hrafnhildur!")
    end
    
    pr("is processing...")
    
    if in(fi.junk,("ECN",))
      pr("is being processed...",102)
      f = Fred(FredAPIK)
      a = ("","","","","","","","","","",now(),"","",DataFrame(),DataFrame())
      x = FredSeries(a...)
      try
        x = get_data(f,APIz[fi.junk](fi.tckr),limit=2)
      catch err
        pr("faced some troubles with "*k,102)
        pr(string("spitted:\n",string(err)),11)
      finally
        if isempty(x.id)
          pr("is being exited...",102)
          return fi
        else
          for k in keys(de)
            ck_e = hasproperty(x,Symbol(de[k]))
            ck_f = ck_e && ~isempty(getfield(x,Symbol(de[k])))
            ck_f ? get!(d0,k,getfield(x,Symbol(de[k]))) : nothing
            ck_f ? pr(k*" → "*Char(0x2714),102) : pr(k*" → "*Char(0x2718),102)
          end
        end
      end
    else
      tckr = yf.Ticker(APIz[fi.junk](fi.tckr))
      pr("has been tickerized in a csilla-friendly manner!",102)
      x = nothing
      
      inh = Dict{String,Any}() # institutional holders
      mfh = Dict{String,Any}() # mutual funds holers
      shh = Dict{String,Any}() # shares history
      
      try
        x = tckr.get_info()
        if in(fi.junk,("STK",))
          mfh = pyconvert(Dict{String,Any},tckr.get_mutualfund_holders())
          pr("mutual funds holders: fetched.")
          inh = pyconvert(Dict{String,Any},tckr.get_institutional_holders())
          pr("institutional holders: fetched.")
          shh = pyconvert(Dict{String,Any},tckr.get_shares_full())
          pr("number of shares (history): fetched!")
          isin = pyconvert(String,tckr.isin)
        end
      catch err
        pr("encountered some troubles! Caught ↓\n"*string(err),11)
        if isnothing(x)
          return fi
        end
      end
      ##
      ##
      ##
      for k in keys(de)
        if k in (
                 "Expiration","exDate",
                 "FirstTrade","LastDividendDate",
                 "LastFiscalYearEnd","LastSplitDate","NextFiscalYearEnd"
                )
          d0[k] = let p = x.get(de[k],0)
            a = pyconvert(Int64,p)
            b = unix2datetime(a)
            Date(b)
          end
        elseif k in (
                     "DividendYield","InsidersPctHeld",
                     "InstitutionsPctHeld","SharesShortPctOfFloat"
                    )
          d0[k] = Float64(100)*pyconvert(Float64,x.get(de[k],NaN))
        elseif in(k,("Officers",))
          d0[k] = pyconvert(Vector{Dict},x.get(de[k],Dict{String,Any}[]))
          n = length(d0[k]) ## <-- number of officers
          if iszero(n)
            continue
          else
            d0[k] = Dict(d["title"] => d for d in d0[k])
          end
          ## temp officers
          z = Dict{String,Any}()
          for j in keys(d0[k])
            ## removing '&' and multi '_' and non-printable characters.
            s = let charz = (Char(i) for i in 129:153)
              s1 = join(c for c in j if ~in(c,charz))
              s2 = replace(s1,r"\s+" => "_")
              s3 = replace(s2,"&" => "and")
              s4 = replace(s3,r"_+" => "_")
              titlecase(s4)
            end
            ## updating z
            get!(
                 z,
                 s,
                 Dict(titlecase(i) => d0[k][j][i] for i in keys(d0[k][j]))
            )
            ##
            haskey(z[s],j) ? delete!(z[s],j) : nothing
            for l in keys(z[s])
              if isa(z[s][l],String)
                z[s][l] = let charz = (Char(i) for i in 129:153)
                  s1 = join(c for c in z[s][l] if ~in(c,charz))
                  s2 = replace(s1,r"\s+" => "_")
                  s3 = replace(s2,"&" => "and")
                  replace(s3,r"_+" => "_")
                end
              end
            end
          end
          ##
          get!(d0,k,z)
          ##
        elseif in(k,("BusinessSummary",))
          d0[k] = let charz = (Char(i) for i in 129:153)
            s1 = x.get(de[k],"")
            s2 = join(c for c in s1 if ~in(c,charz))
            s3 = replace(s2,r"\s+" => "_")
            s4 = replace(s3,"&" => "and")
            s5 = replace(s4,r"_+" => "_")
          end
        elseif in(k,("MutualFundHolders",))
          if isempty(mfh)
            pr("Mutual Funds Holders --> Empty!",11)
          else
            uu = (
                  ["Value","Name","Shares","PctOut","ReportDate"],
                  ["Value","Holder","Shares","% Out","Date Reported"],
                  (x -> string("Holder_",1+x))
                 )
            kk = let df = mfh
              d1 = pyconvert(Dict,df.to_dict())
              d2=(
                (d1[i]=d1[j];i) for (i,j) in zip(uu[1],uu[2]) if haskey(d1,j)
              )
              Tuple(d2)
            end
            l_one = -(getfield(mfh[kk[begin]],:count),1)
            d0[k] = Dict(
                         uu[3](i) => Dict(
                                          a => mfh[a][i] for a in kk
                                      ) for i=0:l_one
                    )
          end
        elseif in(k,("InstitutionalHolders",))
          if isempty(inh)
            pr("Institutional Holders --> Empty!",11)
          else
            vv = (
                  ["Value","Name","Shares","PctOut","ReportDate"],
                  ["Value","Holder","Shares","% Out","Date Reported"],
                  (x -> string("Holder_",1+x))
                 )
            oo = let df = inh
              d1 = pyconvert(Dict,df.to_dict())
              d2 = (
                (d1[i]=d1[j];i) for (i,j) in zip(vv[1],vv[2]) if haskey(d1,j)
              )
              Tuple(d2)
            end
            l_two = -(getfield(mfh[kk[begin]],:count),1)
            d0[k] = Dict(
                         vv[3](i) => Dict(
                                          a => inh[a][i] for a in oo
                                     ) for i=0:l_two
                    )
          end
        elseif in(k,("NumberOfSharesHist",))
          if isempty(shh)
            pr("Shares History → Empty!",11)
          else
            d0[k] = let df = shh
              d1 = pyconvert(Dict,df.to_dict())
              Dict(pyconvert(Date,j.date()) => d1[j] for j in keys(d1))
            end
          end
        elseif in(k,("ISIN",))
          d0[k] = isin
        else
          d0[k] = pyconvert(Union{String,Number,Date,DateTime},x.get(de[k],""))
          if isa(d0[k],String)
            d0[k] = join(c for c=d0[k] if ~in((Char(i) for i in 129:153))(c))
          end
        end
        pr(string(k," has been processed!"))
      end
      #######################################################################
    end
    ## LateUpdate
    d0["LU"] = Dates.today()
    ##
    if isfile(poml) && isless(10,countlines(poml))
      ## Actual dictionary.
      d0x0 = TOML.parsefile(poml)
      ##
      if <(length(d0),0.9length(d0x0)) && >=(d0x0["LU"],Date(t0)-Month(1))
        pr("poorly fielded!\nSticking with the last update!",11)
        pr(string("Time elapsed: ",round(Dates.now()-t0,Second)))
        ## Tryna force the GC.
        d0x0 = d0 = nothing
        return nothing
      else
        pr("is being updated on Hrafnhildur...")
      end
    else
      touch(poml)
      pr("container touched!")
    end
    open(poml,"w") do io
      TOML.print(io,d0)
      pr(string("updated on Hrafnhildur → ",Char(0x2714)))
    end
    pr(string("Time elapsed: ",round(Dates.now()-t0,Second)))
    return nothing
  end
  """
      tyyni(fi::Vector{FiZ};[rfyl])
  
  Vectorized version of `fi`.
  `tyyni(i)` every `i` in `fi` and returns the vector of `i` which
  did not update or `nothing` if everything went successfully.
  
  ## Notes
  
  The function is parallelized.
  """
  function tyyni(
                 fi::Vector{FiZ};
                 rfyl::Union{IO,String}=Rfyl
           )::Union{Vector{FiZ},Nothing}
    if isempty(fi)
      printstyled("Empty FiZ...\nExiting...\n",color=102)
      return nothing
    end
    
    #globs>
    begin
      t0 = Dates.now()
      ph = (fi[1].land,string(Char(0x2022)))
      
      pr(msg,kol=102)=heil(msg,fi=ph,iam="tyyni",kol=kol,bud=true,rfyl=stdout)
    end
    
    #main>
    vfi = begin
      fetch.(collect((Threads.@spawn tyyni(i,rfyl=rfyl)) for i in fi))
    end
    
    if any(!isnothing,vfi)
      pr("some instruments did not processed successfully!")
      filter!(!isnothing,vfi)
      for i in vfi
        printstyled(i.tckr," ",color=13)
      end
      printstyled("\nPress [ENTER] to continue...",color=102)
      string(readline(stdin))
      pr("exiting...")
      msg = string("Whole time elapsed: ",round(Dates.now()-t0,Second))
      printsyled(msg,color=11)
      sleep(0.5)
      return vfi
    end
    msg1 = string("Whole time elapsed: ",round(Dates.now()-t0,Second))
    printstyled(msg1,"\n",color=102)
  end
  """
      tyyni(b::String,[rfyl])
      
  `tyyni` every element of the `Bunch` `b`.
  """
  function tyyni(
                 b::String;
                 rfyl::Union{IO,String}=Rfyl
           )::Union{Vector{FiZ},Nothing}
    #valids>
    if ~in(b,Bunch)
      printstyled(b, " is not a valid Bunch\nExiting...\n",color=11)
      return nothing
    end
    
    if isempty(get(Unnur,b,String[]))
      printstyled("The bunch ",b,"is empty!\n",color=102)
      return nothing
    end
    
    printstyled("Collecting ",b,"...",color=102,blink=true)
    vfi = collect(FiZ(b,i) for i in get(Unnur,b,String[]))
    printstyled("done!")
    sleep(0.25)
    printstyled("\033[2J","\033[H",color=102)
    ######################
    tyyni(vfi,rfyl=rfyl) #
    ######################
  end
  
end
##
## MGMT
##
begin
  """
      tyyni_mgmt(command,[rfyl])
  """
  function tyyni_mgmt(
                      command::NamedTuple{(:args,:bunch,:frmk,:scheme,:verb)};
                      rfyl::Union{String,IO}=Rfyl
  )
    nothing
  end
end