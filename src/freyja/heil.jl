begin
  """
      heil(msg,[fi,[context,[logger,[loglevel]]]])
  
  Flush a timestamped log into `logger` (defaulting to the
  global Zsofia::Valkyrja).
  
  `msg`     : the message to print.\\
  `fi`      : the `FiZ` context.\\
  `context` : the context (defaulting to "Zsofia").\\
  `logger`  : the logger to process/flush the log.\\
  `loglevel`: the log-level.
  """
  function heil(
                msg::Union{AbstractString, AbstractChar},
                fi::FiZ=FiZ("Artemis", string(Char(0x2022)), "US");
                context::String="Zsofia",
                logger::Logging.AbstractLogger=Valkyrja,
                loglevel::LogLevel=Info
               )::Nothing
    #header>
    begin
      hdr = " " * Char(0x2799) * " [:" * context * ":]\n" *
            "\t" * Char(0x276c) *
            getfield(fi, :land) * "|" * getfield(fi, :tckr) *
            Char(0x276d) * "\n" *
            msg * "\n"
      out = string(now(), hdr)
    end
    
    #body>
    with_logger(logger) do io
      @logmsg loglevel msg
    end
    flush(getfield(logger, :stream))
    return nothing
  end
end