# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _fotografi(path::AbstractString)
      _fotografi(db::ZsofiaDB, bunch::AbstractString, ::ZsofiaDBContext, ::Observables)
      _fotografi(db::ZsofiaDB, bunch::AbstractString, ::ZsofiaDBContext)
      _fotografi(db::ZsofiaDB, bunch::AbstractString)
      _fotografi(bunch::AbstractString)
      
  # Internal Facility
  Return the 'cliché' of the record found in `path` which should be a
  comma-separated-value file.
  
  Basically, the function return the last line of informations recorded in
  `path` as a dictionary having as keys the specific column tags.
  
  ### 2nd method
      _fotografi(db::ZsofiaDB, bunch, ::ZsofiaDBContext, ::Observables)
  
  Given an `Observables`, return `fotografi` of the element stored in the silo
  of the `ZsofiaDBContext` for the specified `bunch` in the specified
  `ZsofiaDB` having as name the ticker gotten from the `Observables`.
  
  ### 3rd method
      _fotografi(db::ZsofiaDB, bunch::AbstractString, ::ZsofiaDBContext)
  
  Return `_fotografi` of whatever available from the specified `ZsofiaDB`, in
  the specified `bunch` for the specified `ZsofiaDBContext` in the latest date.
  Note that this method only perform the `fotofrafi` for of the latest available
  records.
  
  ### 4th method
      _fotografi(db::ZsofiaDB, bunch::AbstractString)
  
  Return `_fotografi(db::ZsofiaDB, bunch::AbstractString, ::ZsofiaDBContext)`
  for any available context.
  
  # Notes
  When the 2nd method is called for `FXRate<:Observables`, the convention
  is to name the files as: `foreign_domestic`.

  When the method `_fotografi(db::ZsofiaDB, bunch::AbstractString)` is called,
  a dictionary `Dict{ZsofiaDBContext,...}` is returned.
  
  When the method `_fotografi(db::ZsofiaDB)` is called, a dictionary mapping
  each `bunch∈BUNCH` to `_fotografi(db::ZsofiaDB, bunch::AbstractString)`
  is returned.
  """
  function _fotografi(path::AbstractString)
    _features = string.(split(first(eachline(path)),","))
    _valuesof = string.(split(last(eachline(path)),","))
    dict = Dict{AbstractString,Any}()
    for (k, v) in zip(_features, _valuesof)
      dict[k] = if in(k,("Date","timestamp","date"))
        Date(v)
      elseif in(k,("Time",))
        Time(v)
      elseif in(k, ("Shares","Volume"))
        floor(Int, tryparse(Float64,v))
      elseif in(k, ("Tckr",))
        v
      else
        tryparse(Float64, v)
      end
    end
    dict["LastFoto"] = now()
    Dict{
         AbstractString,
         Dict{AbstractString,Any}
    }(dict["Tckr"] => dict)
  end
  
  function _fotografi(
                      db::ZsofiaDB,
                      bunch::AbstractString,
                      ::Brynhild,
                      obs::Observables
           )
    @assert in(bunch,BUNCH)
    _fotografi(
               joinpath(
                        UNNUR["paths"][string(db)], bunch, "Brynhild",
                        getparams(obs,"ticker") * ".csv"
               )
    )
  end

  function _fotografi(
                      db::ZsofiaDB,
                      bunch::AbstractString,
                      ::Brynhild,
                      obs::Stock
           )
    @assert in(bunch, BUNCH)
    @assert hasparams(obs, "calendar") "calendar not inferrable"
    _fotografi(
               joinpath(
                        UNNUR["paths"][string(db)], bunch, "Brynhild",
                        _stock_silo(obs), getparams(obs,"ticker") * ".csv"
               )
    )
  end
  
  function _fotografi(bunch::AbstractString, ::Brynhild, obs::Observables)
    _fotografi(HRAFNHILDUR, bunch, BRYNHILD, obs)
  end
  
  function _fotografi(db::ZsofiaDB, bunch::AbstractString, ::Brynhild)
    @assert in(bunch, BUNCH)
    if isequal(bunch,"Artemis")
      dict = Dict{
                  AbstractString,
                  Dict{AbstractString,Dict{AbstractString,Any}}
              }()
      _geosp = joinpath(UNNUR["paths"][string(db)],bunch,"Brynhild")
      if ~isdir(_geosp)
        return dict
      end
      _geos = readdir(_geosp,join=false)
      for s in _geos
        dict[s] = Dict{AbstractString,Dict{AbstractString,Any}}()
        _p = joinpath(UNNUR["paths"][string(db)],bunch,"Brynhild",s)
        _tasks = map(readdir(_p,join=true)) do f
          @task begin
            endswith(".csv")(f) ? _fotografi(f) : Dict{AbstractString,Any}()
          end
        end
        n = length(_tasks)
        for _task in _tasks
          setfield!(_task, :sticky, false)
        end
        (schedule.(_tasks); wait.(_tasks))
        _faid = filter(istaskfailed, _tasks)
        m = round(iszero(n) ? 0 : /(100length(_faid),n), digits=2)
        filter!(istaskdone, _tasks)
        merge!(dict[s], fetch.(_tasks)...)
        if isless(0,m)
          @warn "Foto $(bunch)[BRYNHILD]($(s)) $(m)% failures" failures=_faid
        else
          @info "$(bunch)[$(s)][Brynhild] " * Char(0x2714)
        end
      end
    else
      dict = Dict{AbstractString,Dict{AbstractString,Any}}()
      _p = joinpath(UNNUR["paths"][string(db)],bunch,"Brynhild")
      if ~isdir(_p)
        return dict
      end
      _paths = filter(endswith(".csv"), readdir(_p, join=true))
      _tasks = map(_paths) do f
        @task _fotografi(f)
      end
      for _task in _tasks
        setfield!(_task, :sticky, false)
      end
      n = length(_tasks)
      (schedule.(_tasks); wait.(_tasks))
      _failed_tasks = filter(istaskfailed, _tasks)
      m = round(iszero(n) ? 0 : /(100length(_failed_tasks),n), digits=2)
      filter!(istaskdone, _tasks)
      isempty(_tasks) ? nothing : merge!(dict, fetch.(_tasks)...)
      if isless(0,m)
        @warn "Foto $(bunch)[BRYNHILD] $(m)% failures!" failures=_failed_tasks
      else
        @info "$(bunch)[Brynhild] fotografi " * Char(0x2714)
      end
    end
    dict
  end
  
  function _fotografi(bunch::AbstractString,::Brynhild)
    _fotografi(HRAFNHILDUR,bunch,BRYNHILD)
  end
  
  function _fotografi(db::ZsofiaDB,bunch::AbstractString,::Bjorn)
    @assert in(bunch, BUNCH)
    if isequal(bunch,"Artemis")
      dict=Dict{AbstractString,Dict{AbstractString,Dict{AbstractString,Any}}}()
      _dir = joinpath(UNNUR["paths"][string(db)],bunch,"Bjorn")
      if ~isdir(_dir)
        return dict
      end
      _geos = readdir(_dir,join=false)
      filter!(f -> isdir(joinpath(_dir,f)), _geos)
      if isempty(_geos)
        return dict
      end
      _tasks = map(_geos) do geo
        dict[geo] = Dict{AbstractString,Dict{AbstractString,Any}}()
        @task begin
          _df = read_csv(
                         last(readdir(joinpath(_dir,geo),join=true)),
                         DataFrame,
                         stringtype=String,
                         truestrings=nothing,
                         falsestrings=nothing,
                         types=Dict(:Tckr => String)
                ) |> (df -> groupby(df,:Tckr)) |> (df -> combine(df,last))
          for r in eachrow(_df)
            dict[geo][r.Tckr] = begin
              Dict{String,Any}(
                               string(k) => getproperty(r,k)
                               for k in getfield(getfield(r,:colindex),:names)
              )
            end
          end
        end
      end
      n = length(_tasks)
      (schedule.(_tasks); wait.(_tasks))
      _failed_tasks = filter(istaskfailed, _tasks)
      filter!(istaskdone, _tasks)
      m = round(iszero(n) ? 0 : /(100length(_failed_tasks),n),digits=2)
      if isless(0,m)
        @warn "$(m)% failures, Foto $(bunch)[BRYNHILD]" failures=_failed_tasks
      else
        @info "Fotografi of $(bunch)[BRYNHILD] " * Char(0x2714)
      end
    else
      dict = Dict{AbstractString,Dict{AbstractString,Any}}()
      _dir = joinpath(UNNUR["paths"][string(db)],bunch,"Bjorn")
      if ~isdir(_dir)
        return dict
      end
      _p = last(readdir(_dir, join=true))
      _df = begin
        read_csv(
                 _p, DataFrame, stringtype=String, truestrings=nothing,
                 falsestrings=nothing, types=Dict(:Tckr => String)
        ) |> (df -> groupby(df,:Tckr)) |> (df -> combine(df,last))
      end
      for r in eachrow(_df)
        dict[r.Tckr] = begin
          Dict{String,Any}(
                           string(k) => getproperty(r,k)
                           for k in getfield(getfield(r,:colindex),:names)
          )
        end
      end
    end
    dict
  end
  
  function _fotografi(bunch::AbstractString, ::Bjorn)
    _fotografi(HRAFNHILDUR, bunch, BJORN)
  end
  
  function _fotografi(
                      db::ZsofiaDB,
                      bunch::AbstractString,
                      ::Hjordis,
                      obs::Stock
           )
    @assert in(bunch,BUNCH)
    _p0 = joinpath(UNNUR["paths"][string(db)],bunch,"Hjordis")
    tckr = getparams(obs,"ticker")
    if ~isdir(_p0)
      return Dict{AbstractString,Any}(tckr => Dict{AbstractString,Any}())
    end
    _s0 = _stock_silo(obs)
    _dr = readdir(_p0, join=true)
    filter!(isdir, _dr)
    while ~isempty(_dr)
      _p = joinpath(pop!(_dr), _s0, tckr * ".csv")
      if isless(25,stat(_p).size)
        return _fotografi(_p)
      end
    end
    Dict{AbstractString,Any}(tckr => Dict{AbstractString,Any}())
  end
  
  function _fotografi(
                      db::ZsofiaDB,
                      bunch::AbstractString,
                      ::Hjordis,
                      obs::Observables
           )
    @assert in(bunch,BUNCH)
    _p0 = joinpath(UNNUR["paths"][string(db)],bunch,"Hjordis")
    tckr = getparams(obs,"ticker")
    if ~isdir(_p0)
      return Dict{AbstractString,Any}(tckr => Dict{AbstractString,Any}())
    end
    _dr = readdir(_p0, join=true)
    filter!(isdir, _dr)
    while ~isempty(_dr)
      _p = joinpath(pop!(_dr), tckr * ".csv")
      if isless(25, stat(_p).size)
        return _fotografi(_p)
      end
    end
    Dict{AbstractString,Any}(tckr => Dict{AbstractString,Any}())
  end
  
  function _fotografi(bunch::AbstractString, ::Hjordis, obs::Observables)
    _fotografi(HRAFNHILDUR, bunch, HJORDIS, obs)
  end
  
  function _fotografi(db::ZsofiaDB, bunch::AbstractString, ::Hjordis)
    @assert in(bunch,BUNCH)
    if isequal(bunch,"Artemis")
      dict=Dict{AbstractString,Dict{AbstractString,Dict{AbstractString,Any}}}()
      _p0 = joinpath(UNNUR["paths"][string(db)],bunch,"Hjordis")
      if ~isdir(_p0)
        return dict
      end
      _d0 = readdir(_p0, join=true)
      filter!(isdir, _d0)
      if isempty(_d0)
        return dict
      end
      _d0 = last(_d0)
      _s0 = readdir(_d0, join=false)
      filter!(v -> isdir(joinpath(_d0,v)), _s0)
      for s in _s0
        _paths = readdir(joinpath(_d0,s), join=true)
        _tasks = map(_paths) do p
          @task _fotografi(p)
        end
        for _task in _tasks
          setfield!(_task, :sticky, false)
        end
        (schedule.(_tasks); wait.(_tasks))
        n = length(_tasks)
        _failed = filter(istaskfailed, _tasks)
        m = round(iszero(n) ? 0 : /(100length(_failed),n), digits=2)
        filter!(istaskdone, _tasks)
        dict[s] = merge(fetch.(_tasks)...)
        if isless(0,length(_failed))
          @warn "Foto of $(bunch)[HJORDIS] failures $(m)% " failures=_failed
        else
          @info "Foto of $(bunch)[HJORDIS] " * Char(0x2714)
        end
      end
    else
      dict = Dict{AbstractString,Dict{AbstractString,Any}}()
      _prd = joinpath(UNNUR["paths"][string(db)],bunch,"Hjordis")
      if ~isdir(_prd)
        return dict
      end
      _d0 = last(readdir(_prd,join=true))
      _paths = readdir(_d0, join=true)
      filter!(endswith(".csv"), _paths)
      if isempty(_paths)
        return dict
      end
      _tasks = map(_paths) do p
        @task _fotografi(p)
      end
      for _task in _tasks
        setfield!(_task, :sticky, false)
      end
      (schedule.(_tasks); wait.(_tasks))
      n = length(_tasks)
      _failed = filter(istaskfailed, _tasks)
      m = round(iszero(n) ? 0 : /(100length(_failed),n), digits=2)
      filter!(istaskdone, _tasks)
      dict = merge(fetch.(_tasks)...)
      if isless(0,length(_failed))
        @warn "Foto of $(bunch)[HJORDIS] failures $(m)% " failures=_failed
      else
        @info "Foto of $(bunch)[HJORDIS] " * Char(0x2714)
      end
    end
    dict
  end
  
  function _fotografi(bunch::AbstractString, ::Hjordis)
    _fotografi(HRAFNHILDUR, bunch, HJORDIS)
  end
  
  function _fotografi(db::ZsofiaDB, bunch::AbstractString)
    @assert in(bunch, BUNCH)
    _p = joinpath(UNNUR["paths"][string(db)], bunch)
    _silos = [Bjorn, Brynhild, Hjordis]
    filter!(x -> isdir(joinpath(_p,string(x))), _silos)
    dict = Dict{ZsofiaDBContext,Dict{AbstractString,Dict{AbstractString,Any}}}()
    for s in _silos
      dict[s()] = _fotografi(db, bunch, s())
    end
    dict
  end
  
  function _fotografi(db::ZsofiaDB)
    dict = Dict{
                AbstractString,
                Dict{
                     ZsofiaDBContext,
                     Dict{
                          AbstractString,
                          Dict{
                               AbstractString,
                               Any
                          }
                     }
                }
           }()
    _tasks = map(BUNCH) do bunch
      @task begin
        dict[bunch] = _fotografi(db, bunch)
      end
    end
    (schedule.(_tasks); wait.(_tasks))
    dict
  end
  
  _fotografi() = _fotografi(HRAFNHILDUR)

  
  function _write_fotografi()
    open(UNNUR["paths"]["Fotografi"],"w+") do io
      _d = Dict(k => Dict(string(l) => v[l] for l in keys(v)) for (k,v)=FOTO)
      printtoml(io, _d)
      @info "forogradi stamped up!" * Char(0x2714)
    end
  end
end


begin
  """
      fotografi(df::AbstractDataFrame)
      fotografi(db::ZsofiaDB, zc::ZsofiaDBContext, obs::Observables)
      fotografi(db::ZsofiaDB, zc::ZsofiaDBContext)
      fotografi(db::ZsofiaDB)
      fotografi()
  
  Return the *cliché* of records into `db`, as a dictionary.
  
  # Arguments
  - `db::ZsofiaDB`: data base instance.
  - `zc::ZsofiaDBContext`: either `Brynhild`, `Bjorn` or `Hjordis`.
  - `obs::Observables`: underlying observables.
  - `df::AbstractDataFrame`: dataframe we want fotografi of.
  
  # Notes
  The `fotografi()` method is intended to return a dictionary keyed by bunches
  (elements in `Zsofia.BUNCH`) having as values a dictionary, precizely
  `Dict{ZsofiaDBContext,Dict{AbstractString,Any}`. The used of this method
  is equivalent to the used of the method `fotografi(db::ZsofiaDB)` with
  `db=HRAFNHILDUR`.
  
  The `fotografi(obs::Observables)` method is intended to return a dictionary
  keyed by `getparams(obs, "ticker")` of the `Brynhild` context features. By
  default, when no `ZsofiaDBContext` is specified, `Brynhild` context is used.
  
  The `fotografi(db::ZsofiaDB)` method is intended to return a dictionary keyed
  by available bunches in `db` having a dictionary keyed by `ZsofiaDBContext`
  instances available in `db`. `fotografi()` is a particular case of this where
  `db=HRAFNHILDUR`.
  
  The `fotografi(db::ZsofiaDB,zc::ZsofiaDBContext)` method is intended to
  return a dictionary keyed by bunches (elements in `Zsofia.BUNCH`) having
  as values `Dict{AbstractString,Dict{AbstractString,Any}}` keyed by each
  either tickers or silos (if any).
  
  The `fotografi(db::Zsofia,zc::ZsofiaDBContext,obs::Observables)` method is
  intended to return a dictionary keyed by `getparams(obs,"ticker")` having
  as value a `Dict{AsbtractString,Any}` keyed by record's `features`. Those
  features can be one of the following:
  
  |**Feature**|**Description**|
  |---:|:---|
  |`Date`|trading session|
  |`Tckr`|ticker symbol|
  |`Time`|time (hh:mm:ss)|
  |`Price`|latest session's price|
  |`Open`|session's opening price|
  |`High`|session's highest price|
  |`Low`|lowest price of the session|
  |`Shares`|exchanged shares volume within the very time period|
  |`Co`|Open-Close relative variation (in %)|
  |`Hl`|Low-High relative variation (in %)|
  |`Ind`|indecision metric, kinda 100*(`Co`/`Hl` - 1)|
  |`Rvn`|`Price`-to-`Price` relative variation (session-to-session)|
  |`Gap`|ovenight gap (in %)|
  |`Gaug`|level of `Price` within the total variation range (in %)|
  |`rShares`|volume change session-to-session|
  |`cHigh`|running maximum|
  |`cLow`|running minimum|
  |`cCo`|running `Co`|
  |`cHl`|running `Hl`|
  |`cInd`|running `Ind`|
  |`cGaug`|running `Gaug`|
  |`cCoH`|running level of `High` w.r.t. first `Open`|
  |`cCoL`|running level of `Low` w.r.t. first `Open`|
  |`cShares`|running shares (in percentage) w.r.t. session's total|
  
  Each `fotografi` at the lowest level (`obs` layer) has an additional feature
  `LastFoto` holding the timepoint of the last `fotografi`.
  """
  function fotografi(df::AbstractDataFrame)
    _df = last(df,1,view=true)
    _gm = getfield(getfield(_df,:colindex),:names)
    Dict{AbstractString,Dict{AbstractString,Any}}(
      only(_df.Tckr) => Dict(string(k) => only(getproperty(_df,k)) for k=_gm)
    )
  end
  
  function fotografi(
                     db::ZsofiaDB,
                     zc::S,
                     obs::U
           ) where {U<:Fetchables,S<:Union{Bjorn,Brynhild,Hjordis}}
    _fotografi(db, _to_bunch(obs), zc, obs)
  end
  
  function fotografi(
                     zc::S,
                     obs::U
           ) where {S<:Union{Bjorn,Brynhild,Hjordis},U<:Fetchables}
    fotografi(HRAFNHILDUR, zc, obs)
  end
  
  function fotografi(db::ZsofiaDB,zc::S) where S<:Union{Bjorn,Brynhild,Hjordis}
    dict = Dict{AbstractString,Any}() 
    @threads for bunch in BUNCH
      try
        dict[bunch] = _fotografi(db, bunch, zc)
      catch er
        @warn er
        continue
      end
    end
    dict
  end
  
  function fotografi(zc::S) where S<:Union{Bjorn,Brynhild,Hjordis}
    fotografi(HRAFNHILDUR, zc)
  end
  
  function fotografi(db::ZsofiaDB,obs::U) where U<:Fetchables
    fotografi(db, BRYNHILD, obs)
  end
  
  fotografi(db::ZsofiaDB) = _fotografi(db)
  
  function fotografi(obs::U) where U<:Fetchables
    fotografi(HRAFNHILDUR, obs)
  end
  
  fotografi() = _fotografi()
  
  """
      fotografi!(db::ZsofiaDB, zc::ZsofiaDBContext, obs::Observables)
      fotografi!(db::ZsofiaDB, zc::ZsofiaDBContext)
      fotografi!(db::ZsofiaDB)
      fotografi!()
  
  `fotografi` w.r.t. the arguments and modify the global fotografi container
  in place. The `fotografi`ed elements are returned, not the whole `db`.
  
  See also [`fotografi`](@ref).
  """
  function fotografi!(
                      db::ZsofiaDB,
                      zc::S,
                      obs::U
           ) where {S<:Union{Bjorn,Brynhild,Hjordis},U<:Fetchables}
    _foto = fotografi(db, zc, obs)
    if ~isdefined(Zsofia, :FOTO)
      @warn "glob FOTO not detectable, strange! Open an issue."
      return _foto
    end
    let bunch = _to_bunch(obs)
      get!(FOTO,bunch,Dict{ZsofiaDBContext,Any}())
      get!(FOTO[bunch],zc,Dict{AbstractString,Any}())
      if isequal(bunch,"Artemis")
        _silo = _stock_silo(obs)
        get!(FOTO[bunch][zc],_silo,Dict{AbstractString,Any}())
        merge!(FOTO[bunch][zc][_silo],_foto)
      else
        get!(FOTO[bunch][zc],_foto,Dict{AbstractString,Any}())
      end
      _write_fotografi()
      @info "fotografi $(bunch)[$(string(zs))][$(getparams(obs,"ticker"))]"
      _foto
    end
  end
  
  function fotografi!(
                      db::ZsofiaDB,
                      zc::S
           ) where S<:Union{Bjorn,Brynhild,Hjordis}
    _foto = fotografi(db, zc)
    if ~isdefined(Zsofia, :FOTO)
      @warn "glob FOTO not detectable, strange! Open an issue."
      return _foto
    end
    merge!(FOTO, _foto)
    _write_fotografi()
    @info "fotografi of $(db)[$(string(zc))] stamped " * Char(0x2714)
    _foto
  end
  
  function fotografi!(obs::U) where U<:Fetchables
    fotografi!(HRAFNHILDUR,obs)
  end
  
  function fotografi!(db::ZsofiaDB)
    _foto = fotografi(db::ZsofiaDB)
    if ~isdefined(Zsofia, :FOTO)
      @warn "glob FOTO not detectable, strange! Open an issue."
      return _foto
    end
    merge!(FOTO, _foto)
    _write_fotografi()
    @info "fotografi stamped in $(db) " * Char(0x2714)
    UNNUR["LastFoto"] = now()
    save_unnur()
    _foto
  end
  
  fotografi!() = fotografi!(HRAFNHILDUR)
  
  """
      _string_to_obj(::AbstractString)
  
  # Internal Facility
  Transform some strings into some useful objects.
  """
  function _string_to_obj(zc::AbstractString)
    if isequal(zc,"Brynhild")
      BRYNHILD
    elseif isequal(zc,"Hjordis")
      HJORDIS
    elseif isequal(zc,"Bjorn")
      BJORN
    elseif isequal(zc,"Valtyr")
      VALTYR
    end
  end
  
  """
      _init_fotografi()
  
  # Internal Facility
  Try to load `Fotografi` from disk.
  """
  function _init_fotografi()
    dict=Dict{AbstractString,Dict{Union{AbstractString,ZsofiaDBContext},Any}}()
    try
      merge!(dict,parsefile(UNNUR["paths"]["Fotografi"]))
    catch er
      @warn er
      for bunch in BUNCH
        get!(dict,bunch,Dict{ZsofiaDBContext,Any}())
        for con in (Brynhild,Bjorn,Hjordis)
          get!(dict[bunch],con(),Dict{AbstractString,Any}())
        end
      end
    end
    for (k,v) in dict
      for k_ in keys(v)
        ~in(k_, ("Brynhild","Bjorn","Hjordis","Valtyr")) ? continue : nothing
        v[_string_to_obj(k_)] = v[k_]
        delete!(dict[k],k_)
      end
    end
    dict
  end
  
  """
      getfotografi(obs::Observables, cont::ZsofiaDBContext)
      getfotografi(::Type{Stock}, sym, cal::HolidayCalendar, cont)
      getfotografi(::Type{U}, sym::AbstractString, cont::ZsofiaDBContext)
  
  Return the `fotografi` of `obs` under context `cont` for the given. Return
  a `Dict{AbstractString,Any}`. When `Type`s methods are used, return the
  `fotografi` of provided ticker symbol `sym`.
  
  # Arguments
  - `obs::Observables`: observable to get `fotografi` of.
  - `cont::ZsofiaDBContext=BRYNHILD`: either `Brynhild`, `Bjorn`, `Hjordis`...
  
  # Notes
  This method is intended to read into glob `FOTO`.
  """
  function getfotografi(obs::U, cont::ZsofiaDBContext) where U<:Fetchables
    @assert isdefined(Zsofia,:FOTO) "FOTO not initialized!"
    bunch = _to_bunch(obs)
    @assert haskey(FOTO, bunch) "$(bunch) not Fotografi'ed."
    @assert haskey(FOTO[bunch], cont) "$(cont) context not Fotografi'ed."
    if isequal(bunch,"Artemis")
      _s = _stock_silo(obs)
      _t = getparams(obs,"ticker")
      @assert haskey(FOTO[bunch][cont], _s) "$(_s) not found, try `fotografi`"
      get(FOTO[bunch][cont][_s], _t, Dict{AbstractString,Any}())
    else
      get(FOTO[bunch][cont], _t, Dict{AbstractString,Any}())
    end
  end
  
  function getfotografi(
                        ::Type{Stock},
                        sym::AbstractString,
                        cal::HolidayCalendar,
                        cont::ZsofiaDBContext
           )
    @assert isdefined(Zsofia, :FOTO) "FOTO glob not initialized!"
    bunch = _to_bunch(Stock)
    @assert haskey(FOTO,bunch) "$(bunch) not Fotografi'ed."
    @assert haskey(FOTO[bunch],cont) "$(cont) context not Fotografi'ed."
    _s = _stock_silo(cal)
    @assert haskey(FOTO[bunch][cont], _s) "$(_s) zone not found for Stocks!"
    get(FOTO[bunch][cont][_s],sym,Dict{AbstractString,Any}())
  end
  
  function getfotografi(
                        ::Type{U},
                        sym::AbstractString,
                        cont::ZsofiaDBContext
           ) where U<:Observables
    @assert isdefined(Zsofia, :FOTO) "FOTO glob not initialized!"
    bunch = _to_bunch(U)
    @assert haskey(FOTO,bunch) "$(bunch) not Fotografi'ed."
    @assert haskey(FOTO[bunch],cont) "$(cont) context not Fotografi'ed."
    get(FOTO[bunch][cont],sym,Dict{AbstractString,Any}())
  end
end