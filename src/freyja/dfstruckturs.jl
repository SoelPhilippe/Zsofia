# This script is part of Zsofia. Soel Philippe © 2025

#> _is_featurizable [foo]
begin
  """
      _is_featurizable(::Observables, ::ZsofiaDBContext, ::DataFrame)
      _is_featurizable(::Observables, ::Valtyr, ::Dict{AbstractString,Any})
  
  # Internal Facility
  Return `true` if the input `DataFrame` or `Dict{AbstractString,Any}`
  (when `ZsofiaDBContext` is `Valtyr`) is processable given the
  `::ZsofiaDBContext` and throw a `MissingException` summarizing
  missing features, otherwise.
  
  ## Arguments
  - `obs::Observables`: underlying observable.
  - `::ZsofiaDBContext`: Zsofia's data base context. 
  - `df::DataFrame`: dataframe to check fetures of.
  - `dict::Dict{String,Any}`: when context if `Valtyr`, the `Dict` to check.
  """
  function _is_featurizable(
                           obs::U,
                           ::V,
                           df::AbstractDataFrame
           ) where {U<:YahooFetchables,V<:Union{Bjorn,Hjordis}}
    if isempty(df) || ~issubset(UNNUR["OuHjo"], getfield(df,:colindex).names)
      d = setdiff(UNNUR["OuHjo"], getfield(df,:colindex).names)
      throw(MissingException("missing features -> $(d)"))
    end
    true
  end
  
  function _is_featurizable(
                            ::Type{U},
                            ::V,
                            df::AbstractDataFrame
           ) where {U<:YahooFetchables,V<:Union{Bjorn,Hjordis}}
    if isempty(df) || ~issubset(UNNUR["OuHjo"],getfield(df,:colindex).names)
      d = setdiff(UNNUR["OuHjo"],getfield(df,:colindex).names)
      throw(MissingException("missing features -> $(d)"))
    end
    true
  end
  
  function _is_featurizable(
                            ::Union{EconoIndicator,Type{EconoIndicator}},
                            ::V,
                            ::AbstractDataFrame
           ) where V<:Union{Bjorn,Hjordis}
    throw(
          MethodError(
                      _is_featurizable,
                      Tuple{EconoIndicator,Bjorn,AbstractDataFrame}
          )
     )
  end
  
  function _is_featurizable(
                            ::Union{U,Type{U}},
                            ::Brynhild,
                            df::AbstractDataFrame
           ) where U<:YahooFetchables
    if isempty(df) || ~issubset(UNNUR["OuBry"],getfield(df,:colindex).names)
      d = setdiff(UNNUR["OuBry"], getfield(df,:colindex).names)
      throw(MissingException("missing features -> $(d)"))
    end
    true
  end
  
  function _is_featurizable(
                            ::Union{EconoIndicator,Type{EconoIndicator}},
                            ::Brynhild,
                            df::DataFrame
           )
    if isempty(df)
      throw(MissingException("df is empty"))
    elseif ~issubset([:date,:value,:Tckr],getfield(df,:colindex).names)
      d=setdiff([:date,:value,:Tckr],getfield(df,:colindex).names)
      throw(MissingException("missing features -> $(d)"))
    end
    true
  end
  
  function _is_featurizable(
                            ::Union{U,Type{U}},
                            ::Valtyr,
                            dict::Dict{AbstractString,Any}
           ) where U<:Fetchables
    throw(
          MethodError(
                      _is_featurizable,
                      Tuple{U,Valtyr,Dict{AbstractString,Any}}
          )
    )
  end
end

#> _adhoc_types [foo]
begin
  """
      _adhoc_types!(::Union{Bjorn,Hjordis,Brynhild}, df::AbstractDataFrame)
      _adhoc_types!(::Valtyr, dict::Dict{AbstractString,Any})
  
  # Internal Facility
  Check, process and transform `df`'s features' into their appropriate required
  types, given the underlying given `ZsofiaDBContext`.
  """
  function _adhoc_types!(
                         ::Union{U,Type{U}},
                         ::Union{Bjorn,Hjordis},
                         df::AbstractDataFrame
           ) where U<:Fetchables
    for c in (:Close, :Open, :High, :Low, :Price, :Adjclose, :Value, :value)
      if in(c,getfield(df,:colindex).names)
        transform!(df, c => (x -> convert(Vector{Real},x)) => c)
      end
    end
    for c in (:Timestamp,)
      if in(c,getfield(df,:colindex).names)
        transform!(df, c => ByRow(DateTime) => c)
      end
    end
    for c in (:Volume,:Vol)
      if in(c,getfield(df,:colindex).names)
        transform!(df,c => (x->convert(Vector{Integer},x)) => c)
      end
    end
    df
  end
  
  function _adhoc_types!(
                         ::Union{U,Type{U}},
                         ::Brynhild,
                         df::AbstractDataFrame
           ) where U<:Fetchables
    for c in (
              :Close,:Open,:High,:Low,:Price,
              :Adjclose,:Value,:value
             )
      if in(c,getfield(df,:colindex).names)
        transform!(df,c => (x->convert(Vector{Real},x)) => c)
      end
    end
    for c in (:Timestamp,:date,:Date)
      if in(c,getfield(df,:colindex).names)
        transform!(df,c => (x->convert(Vector{Date},x)) => c)
      end
    end
    for c in (:Volume,)
      if in(c, getfield(df,:colindex).names)
        transform!(df, :Volume => (x -> convert(Vector{Int},x)) => :Volume)
      end
    end
    df
  end
end

#> _drop_nothing(df::DataFrame)
begin
  """
      _drop_nothing!(df::AbstractDataFrame, [cols::Union{Symbol,Vector{Symbol}])
  
  # Internal Facility
  
  Drop rows containing `nothing` in-place, return modified `df`.
  """
  function _drop_nothing!(df::AbstractDataFrame)
    for c in getfield(df, :colindex).names
      filter!(c => !isnothing, df)
    end
    df
  end
  
  function _drop_nothing!(df::AbstractDataFrame, cols::Symbol)
    if in(cols, getfield(df, :colindex).names)
      filter!(cols => !isnothing, df)
    end
    df
  end
  
  function _drop_nothing!(df::AbstractDataFrame, cols::Vector{Symbol})
    for c in cols
      if in(c, getfield(df, :colindex).names)
        filter!(c => !isnothing, df)
      end
    end
    df
  end
end

#> buildfeatures! [foo]
begin
  """
      buildfeatures!(obs, [zc::ZsofiaDBContext], df::AbstractDataFrame)
      buildfeatures!(obs, zc::Valtyr, dict::Dict{AbstractString,Any})
  
  Build in-place relevant features of `df`, within the specified
  `ZsofiaDBContext`.
  
  Built columns consist of some (if not all) of the herebelow described
  features.
  
  # Arguments
  - `obs::Observables`: underlying instrument of which `df` is price action.
  - `zc::ZsofiaDBContext`: one of `BJORN`, `BRYNHILD`, `HJORDIS` or `VALTYR`
  - `df::AbstractDataFrame`: dataframe to build features' of.
  - `dict::Dict{AbstractString,Any}`: dictionary to process.
  
  # Notes
  When the `ZsofiaDBContext` is not provided, `BRYNHILD` is assumed. When
  `df` is not processable, an error is thrown.
  
  When `isa(obs, EconoIndicator)`, `Brynhild` is the sole context available.
  
  ---
  
  Whenever the context is not `Valtyr`, a dataframe is required and features
  that are built are amongst the following:
  
  |**Feature**|**Description**|
  |---:|:---|
  |`Date`|trading session|
  |`Tckr`|ticker symbol|
  |`Time`|time (hh:mm:ss)|
  |`Price`|latest session's price|
  |`Open`|session's opening price|
  |`High`|session's highest price|
  |`Low`|lowest price of the session|
  |`Shares`|exchanged shares volume within the very time period|
  |`Co`|Open-Close relative variation (in %)|
  |`Hl`|Low-High relative variation (in %)|
  |`Ind`|indecision metric, kinda 100*(`Co`/`Hl` - 1)|
  |`Rvn`|`Price`-to-`Price` relative variation (session-to-session)|
  |`Gap`|ovenight gap (in %)|
  |`Gaug`|level of `Price` within the total variation range (in %)|
  |`rShares`|volume change session-to-session|
  |`cHigh`|running maximum|
  |`cLow`|running minimum|
  |`cCo`|running `Co`|
  |`cHl`|running `Hl`|
  |`cInd`|running `Ind`|
  |`cGaug`|running `Gaug`|
  |`cCoH`|running level of `High` w.r.t. first `Open`|
  |`cCoL`|running level of `Low` w.r.t. first `Open`|
  |`cShares`|running shares (in percentage) w.r.t. session's total|
  
  For the `ZsofiaDBContext` `Brynhild`, the first 15 features are expected to
  be built; when it's `Bjorn` or `Hjordis`, the whole list can be expected.
  """
  function buildfeatures!(
                          obs::Union{U,Type{U}},
                          ::Brynhild,
                          df::AbstractDataFrame
           ) where U<:YahooFetchables
    _is_featurizable(obs,BRYNHILD,df)
    idies = completecases(df)
    if isempty(idies) || ~any(idies)
      throw("none of the record is complete, featurizing failure!")
    elseif ~all(idies)
      @warn "incomplete rows detected -> " incompleterows=view(df, .!idies,:)
      dropmissing!(df)
    end
    _drop_nothing!(df)
    _adhoc_types!(obs, BRYNHILD, df)
    if in(:Timestamp, getfield(df, :colindex).names)
      renamedf!(df, :Timestamp => :Date)
    end
    if in(:Adjclose, getfield(df,:colindex).names)
      renamedf!(df, :Adjclose => :Price)
    end
    _has_volume = in(:Volume, getfield(df, :colindex).names)
    _has_volume ? renamedf!(df, :Volume => :Shares) : nothing
    select!(df,view(UNNUR["RfBry"],ifelse(_has_volume,Colon(),Colon()(1,7))))
    unique!(df,:Date)
    sort!(df,:Date)
    filter!(:Date => <(today()), df)
    transform!(df, [:Open,:Price] => pnl => :Co)
    transform!(df, [:Low,:High] => pnl => :Hl)
    transform!(df, [:Co,:Hl] => indcz => :Ind)
    transform!(df, :Price => pnl => :Rvn)
    transform!(df, [:Price,:Open] => gap => :Gap)
    transform!(df, [:Price,:High,:Low] => gauge => :Gaug)
    if _has_volume
      transform!(df, :Shares => pnl => :rShares)
    end
  end
  
  function buildfeatures!(
                          obs::Union{U,Type{U}},
                          zc::V,
                          df::AbstractDataFrame
           ) where U<:YahooFetchables where V<:Union{Bjorn,Hjordis}
    _is_featurizable(obs, zc, df)
    idies = completecases(df)
    if isempty(idies) || ~any(idies)
      throw("dataframe wholly incomplete")
    elseif ~all(idies)
      @warn "Incomplete rows detected!" incompleterows=view(df, .!idies, :)
      dropmissing!(df)
    end
    _drop_nothing!(df)
    _adhoc_types!(obs, zc, df)
    if in(:Close, getfield(df, :colindex).names)
      renamedf!(df, :Close => :Price)
    end
    if in(:Timestamp, getfield(df, :colindex).names)
      if ~in(:Date,getfield(df,:colindex).names)
        transform!(df, :Timestamp => ByRow(Date) => :Date)
      end
      if ~in(:Time,getfield(df,:colindex).names)
        renamedf!(df, :Timestamp => :Time)
        transform!(df, :Time => ByRow(Time) => :Time)
      end
    end
    _has_volume = in(:Volume, getfield(df, :colindex).names)
    _has_volume ? renamedf!(df, :Volume => :Shares) : nothing
    select!(df, view(UNNUR["RfHjo"], _has_volume ? Colon() : 1:8))
    df = groupby(df, :Tckr)
    transform!(df, [:Open, :Price] => pnl => :Co)
    transform!(df, [:Low, :High] => pnl => :Hl)
    transform!(df, [:Co, :Hl] => indcz => :Ind)
    transform!(df, :Price => pnl => :Rvn)
    transform!(df, [:Price, :Open] => gap => :Gap)
    transform!(df, [:Price, :High, :Low] => gauge => :Gaug)
    if _has_volume
      transform!(df, :Shares => pnl => :rShares)
    end
    transform!(df, :High => cummax => :cHigh)
    transform!(df, :Low => cummin => :cLow)
    transform!(df, [:Open, :Price] => ((x,y) -> pnl(first(x), y)) => :cCo)
    transform!(df, [:cLow, :cHigh] => pnl => :cHl)
    transform!(df, [:cCo, :cHl] => indcz => :cInd)
    transform!(df, [:Price, :cHigh, :cLow] => gauge => :cGaug)
    transform!(df, [:Open, :cHigh] => ((x,y) -> pnl(first(x), y)) => :cCoH)
    transform!(df, [:Open, :cLow] => ((x,y) -> pnl(first(x), y)) => :cCoL)
    if _has_volume
      transform!(df, :Shares => (x -> /(100cumsum(x), sum(x))) => :cShares)
    end
    df = transform!(df, ungroup=true)
  end
  
  function buildfeatures!(
                          obs::Union{EconoIndicator,Type{EconoIndicator}},
                          ::Brynhild,
                          df::AbstractDataFrame
           )
    _is_featurizable(obs,BRYNHILD,df)
    idies = completecases(df)
    if isempty(idies) || ~any(idies)
      throw(ErrorException("dataframe wholly incomplete"))
    elseif ~all(idies)
      @warn "Incomplete rows detected!" incompleterows=view(df, .!idies, :)
      dropmissing!(df)
    end
    _drop_nothing!(df)
    _adhoc_types!(obs,BRYNHILD,df)
    if in(:date, getfield(df,:colindex).names)
      renamedf!(df,:date => :Date)
    end
    if in(:value, getfield(df,:colindex).names)
      renamedf!(df,:value => :Price)
    end
    if in(:Price,getfield(df,:colindex).names)
      transform!(df,:Price => pnl => :Rvn)
    end
    select!(df,[:Date,:Tckr,:Price,:Rvn])
  end

  function buildfeatures!(
                          obs::Stock,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end
  
  function buildfeatures!(
                          obs::Futures,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end

  function buildfeatures!(
                          obs::ETF,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end
  
  function buildfeatures!(
                          obs::FXRate,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end

  function buildfeatures!(
                          obs::CryptoCurrency,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end
  
  function buildfeatures!(
                          obs::Indices,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end

  function buildfeatures!(
                          obs::EconoIndicator,
                          ::Valtyr,
                          dict::Dict{AbstractString,Any}
           )
    throw("NOT YET IMPLEMENTED")
  end
  
  function buildfeatures!(obs::U,df::AbstractDataFrame) where U<:YahooFetchables
    buildfeatures!(obs, BRYNHILD, df) 
  end
  
  function buildfeatures!(
                          obs::U,
                          dict::Dict{AbstractString,Any}
           ) where U<:YahooFetchables
    buildfeatures!(obs,VALTYR,dict)
  end
  
  function buildfeatures!(obs::EconoIndicator, df::AbstractDataFrame)
    buildfeatures!(obs, BRYNHILD, df)
  end

  """
      rebuildfeatures!(obs, [zc::ZsofiaDBContext], df::AbstractDataFrame)
      rebuildfeatures!(obs, zc::Valtyr, dict::Dict{AbstractString,Any})
  
  Rebuild relevant features of `df` in-place, within the specified
  `ZsofiaDBContext`.
  
  Rebuilt columns consist of some (if not all) of the herebelow described
  features.
  
  # Arguments
  - `obs::Observables`: underlying instrument of which `df` is price action.
  - `zc::ZsofiaDBContext`: one of `BJORN`, `BRYNHILD`, `HJORDIS` or `VALTYR`
  - `df::AbstractDataFrame`: dataframe to build features' of.
  - `dict::Dict{AbstractString,Any}`: dictionary to process.
  
  # Notes
  When the `ZsofiaDBContext` is not provided, `BRYNHILD` is assumed. When
  `df` is not processable, an error is thrown.
  
  When `isa(obs, EconoIndicator)`, `Brynhild` is the sole context available.
  
  ---
  
  Whenever the context is not `Valtyr`, a dataframe is required and features
  that are built are amongst the following:
  
  |**Feature**|**Description**|
  |---:|:---|
  |`Date`|trading session|
  |`Tckr`|ticker symbol|
  |`Time`|time (hh:mm:ss)|
  |`Price`|latest session's price|
  |`Open`|session's opening price|
  |`High`|session's highest price|
  |`Low`|lowest price of the session|
  |`Shares`|exchanged shares volume within the very time period|
  |`Co`|Open-Close relative variation (in %)|
  |`Hl`|Low-High relative variation (in %)|
  |`Ind`|indecision metric, kinda 100*(`Co`/`Hl` - 1)|
  |`Rvn`|`Price`-to-`Price` relative variation (session-to-session)|
  |`Gap`|ovenight gap (in %)|
  |`Gaug`|level of `Price` within the total variation range (in %)|
  |`rShares`|volume change session-to-session|
  |`cHigh`|running maximum|
  |`cLow`|running minimum|
  |`cCo`|running `Co`|
  |`cHl`|running `Hl`|
  |`cInd`|running `Ind`|
  |`cGaug`|running `Gaug`|
  |`cCoH`|running level of `High` w.r.t. first `Open`|
  |`cCoL`|running level of `Low` w.r.t. first `Open`|
  |`cShares`|running shares (in percentage) w.r.t. session's total|
  
  For the `ZsofiaDBContext` `Brynhild`, the first 15 features are expected to
  be built; when it's `Bjorn` or `Hjordis`, the whole list can be expected.
  """
  function rebuildfeatures!(
                            obs::Union{U,Type{U}},
                            ::Brynhild,
                            df::AbstractDataFrame
           ) where U<:YahooFetchables
    idies = completecases(df)
    @assert ~isempty(idies) || any(idies) throw("$(U)'s  Brynhildable")
    if ~all(idies)
      @warn "some incomple rows detected!" incompleterows=view(df, .!idies, :)
      dropmissing!(df)
    end
    _drop_nothing!(df)
    _adhoc_types!(obs,BRYNHILD,df)
    for c in (:Timestamp => :Date, :Adjclose => :Price, :Volume => :Shares)
      _f, _t = c
      in(_f,getfield(df,:colindex).names) ? renamedf!(df, _f => _t) : nothing
    end
    _has_shrs = in(:Shares, getfield(df,:colindex).names)
    select!(df,view(UNNUR["RfBry"],ifelse(_has_shrs,Colon(),Colon()(1,7))))
    unique!(df, :Date)
    sort!(df, :Date)
    filter!(:Date => <(today()), df)
    transform!(df, [:Open,:Price] => pnl => :Co)
    transform!(df, [:Low,:High] => pnl => :Hl)
    transform!(df, [:Co,:Hl] => indcz => :Ind)
    transform!(df, :Price => pnl => :Rvn)
    transform!(df, [:Price,:Open] => gap => :Gap)
    transform!(df, [:Price,:High,:Low] => gauge => :Gaug)
    _has_shrs ? transform!(df, :Shares => pnl => :rShares) : nothing
  end
  
  function rebuildfeatures!(
                            obs::Union{U,Type{U}},
                            zc::V,
                            df::AbstractDataFrame,
           ) where {U<:YahooFetchables, V<:Union{Bjorn,Hjordis}}
    idies = completecases(df)
    @assert ~isempty(idies) || any(idies) throw("$(zc)[$(U)]'s not shaped!")
    if ~all(idies)
      @warn "some incomple rows detected!" incompleterows=view(df, .!idies, :)
      dropmissing!(df)
    end
    _drop_nothing!(df)
    _adhoc_types!(obs,zc,df)
    for c in (:Close => :Price,)
      _f, _t = c
      in(_f, getfield(df,:colindex).names) ? renamedf!(df, _f => _c) : nothing
    end
    _has_shrs = in(:Shares, getfield(df,:colindex).names)
    if in(:Timestamp, getfield(df,:colindex).names)
      transform!(df, :Timestamp => ByRow(Date) => :Date)
      transform!(df, :Timestamp => ByRow(Time) => :Timestamp)
      renamedf!(df, :Timestamp => :Time)
    end
    select!(df,view(UNNUR["RfHjo"],ifelse(_has_volume,Colon(),Colon()(1,8))))
    df = groupby(df,:Tckr)
    transform!(df,[:Open,:Price] => pnl => :Co)
    transform!(df,[:Low,:High] => pnl => :Hl)
    transform!(df,[:Co,:Hl] => indcz => :Ind)
    transform!(df,:Price => pnl => :Rvn)
    transform!(df,[:Price,:Open] => gap => :Gap)
    transform!(df,[:Price,:High,:Low] => gauge => :Gaug)
    _has_shrs ? transform!(df, :Shares => pnl => :rShares) : nothing
    transform!(df,:High => cummax => :cHigh)
    transform!(df,:Low => cummin => :cLow)
    transform!(df,[:Open,:Price] => ((x,y) -> pnl(first(x),y)) => :cCo)
    transform!(df,[:cLow,:cHigh] => pnl => :cHl)
    transform!(df,[:cCo,:cHl] => indcz => :cInd)
    transform!(df,[:Price,:cHigh,:cLow] => gauge => :cGaug)
    transform!(df,[:Open,:cHigh] => ((x,y) -> pnl(first(x),y)) => :cCoH)
    transform!(df,[:Open,:cLow] => ((x,y) -> pnl(first(x),y)) => :cCoL)
    _has_shrs ? transform!(df,:Shares => cumsum => :cShares) : 0
    _has_shrs ? transfomr!(df,:cShares => (x -> /(x,last(x))) => :cShares) : 0
    df = transform!(df,ungroup=true)
  end

  function rebuildfeatures!(
                            obs::Union{EconoIndicator,Type{EconoIndicator}},
                            ::Brynhild,
                            df::AbstractDataFrame
           )
    idies = completecases(df)
    zc    = BRYNHILD
    @assert ~isempty(idies) || any(idies) throw("$(zc)[EconoIndicator] nott")
    if ~all(idies)
      @warn "some incomple rows detected!" incompleterows=view(df, .!idies, :)
      dropmissing!(df)
    end
    _drop_nothing!(df)
    _adhoc_types!(obs,zc,df)
    if in(:Price,getfield(df,:colindex).names)
      transform!(df,:Price => pnl => :Rvn)
    end
    select!(df,[:Date,:Tckr,:Price,:Rvn])
  end
  
  function rebuildfeatures!(
                            obs::Union{U,Type{U}},
                            df::AbstractDataFrame
           ) where U<:YahooFetchables
    rebuildfeatures!(obs, BRYNHILD, df)
  end
  
  function rebuildfeatures!(
                            obs::Union{U,Type{U}},
                            dict::Dict{AbstractString,Any}
           ) where U<:YahooFetchables
    rebuildfeatures!(obs,VALTYR,dict)
  end
  
  function rebuildfeatures!(
                            obs::Union{EconoIndicator,Type{EconoIndicator}},
                            df::AbstractDataFrame
           )
    rebuildfeatures!(obs, BRYNHILD, df)
  end
end