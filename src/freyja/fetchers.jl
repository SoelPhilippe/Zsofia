# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      getbunch(obs::Observables)
      getbunch(::Type{U}) where U<:Observables
  
  Return ad-hoc `BUNCH` value associated to `obs` (given type).
  
  # Notes
  We have built a naming convention to *silo* `Observables`...
  We built a naming convention to *silo* `Observables`... this is useful for
  data base structure at lower level.
  
  See also [`HRAFNHILDUR`](@ref), [`ZsofiaDB`](@ref).
  
  # Examples
  ```julia
  julia> pltr = Stock("PLTR");
  
  julia> getbunch(pltr)
  "Artemis"
  """
  getbunch(obs::Observables) = _to_bunch(obs)
  getbunch(obs::Type{U}) where U<:Observables = _to_bunch(obs)
  
  """
      collect(db::ZsofiaDB, ::Bjorn, date::Date, obs::Observables)
      collect(db::ZsofiaDB, ::Bjorn, obs::Observables)
      collect(db::ZsofiaDB, ::Bjorn, date::Date, obs::Type{<:Observables})
      collect(db::ZsofiaDB, ::Bjorn, obs::Type{Observables})
      collect(db::ZsofiaDB, ::Bjorn)
      collect(db::ZsofiaDB, ::Brynhild, obs::Observables)
      collect(db::ZsofiaDB, ::Brynhild, obs::Type{Observables})
      collect(db::ZsofiaDB, ::Brynhild)
      collect(db::ZsofiaDB, ::Hjordis, date::Date, ::Observables)
      collect(db::ZsofiaDB, ::Hjordis, date::Date, ::Type{Observables})
      collect(db::ZsofiaDB, ::Hjordis, date::Date)
      collect(db::ZsofiaDB, ::Hjordis)
      collect(db::ZsofiaDB, ::Valtyr)
      collect(db::ZsofiaDB)
  
  Collect observables available into `db` within the `::ZsofiaDBContext`.
  
  # Arguments
  - `db::ZsofiaDB`: actual Zsofia data base to seek into.
  - `::ZsofiaDBContext`: either `BJORN`, `BRYNHILD`, `HJORDIS` or `VALTYR`.
  - `date::Date`: specific date.
  - `obs::Observables`: serves as context/silo hinting; especially for ̀ Stock`s.
  
  # Notes
  The idea of `collect`ing is to collect a bunch of `Observables` therefore
  when `obs` is precized in the `args`, it's for `bunch`-hinting only. For the
  facility to infer which `bunch` is required, not the very particular
  `Observables`.
  """
  function Base.collect(
                        db::ZsofiaDB,
                        ::Bjorn,
                        date::Date,
                        obs::U
                ) where U<:Fetchables
    _collect(db, _to_bunch(obs), BJORN, date)
  end
  
  function Base.collect(
                        db::ZsofiaDB, 
                        ::Bjorn,
                        date::Date,
                        ::Type{U}
                ) where U<:Fetchables
    _collect(db, _to_bunch(U), BJORN, date)
  end

  function Base.collect(db::ZsofiaDB, ::Bjorn, date::Date)
    _collect(db, BJORN, date)
  end
  
  function Base.collect(db::ZsofiaDB, ::Bjorn, obs::U) where U<:Fetchables
    _collect(db, _to_bunch(obs), BJORN)
  end
  
  function Base.collect(db::ZsofiaDB, ::Bjorn, ::Type{U}) where U<:Fetchables
    _collect(db, _to_bunch(U), BJORN)
  end
  
  function Base.collect(
                        db::ZsofiaDB,
                        ::Hjordis,
                        date::Date,
                        obs::U
                ) where U<:YahooFetchables
    _collect(db, _to_bunch(obs), HJORDIS, date)
  end
  
  function Base.collect(
                        db::ZsofiaDB,
                        ::Hjordis,
                        date::Date,
                        obs::Type{U}
                ) where U<:YahooFetchables
    _collect(db, _to_bunch(obs), HJORDIS, date)
  end
       
  
  function Base.collect(db::ZsofiaDB, ::Hjordis, date)
    _collect(db, HJORDIS, date)
  end
  
  function Base.collect(db::ZsofiaDB, ::Hjordis)
    _collect(db, HJORDIS)
  end
  
  function Base.collect(
                        db::ZsofiaDB,
                        ::Hjordis,
                        obs::U
                ) where U<:YahooFetchables
    _collect(db, _to_bunch(obs), HJORDIS)
  end
  
  function Base.collect(
                        db::ZsofiaDB,
                        ::Hjordis,
                        obs::Type{U}
                ) where U<:YahooFetchables
    _collect(db, _to_bunch(obs), HJORDIS)
  end
  
  function Base.collect(db::ZsofiaDB,::Brynhild,obs::U) where U<:Fetchables
    _collect(db, _to_bunch(obs), BRYNHILD)
  end
  
  function Base.collect(
                        db::ZsofiaDB,
                        ::Brynhild,
                        obs::Type{U}
                ) where U<:Fetchables
    _collect(db, _to_bunch(obs), BRYNHILD)
  end
  
  function Base.collect(db::ZsofiaDB, ::Brynhild)
    _collect(db, BRYNHILD)
  end
  
  function Base.collect(zc::ZsofiaDBContext)
    collect(HRAFNHILDUR, zc)
  end
end

#> readers (from db)
begin
  """
      read(::ZsofiaDB, ::Observables, ::Bjorn, [::Date])
      read(::ZsofiaDB, ::Observables, ::Brynhild, [::Date, ::Date])
      read(::ZsofiaDB, ::Observables, ::Hjordis, [::Date])
      read(::ZsofiaDB, ::Observables, ::Hjordis, [::Vector{Date}])
      read(::ZsofiaDB, ::Observables, ::Valtyr)
      read(::ZsofiaDB, ::Observables)
  
  Read/load `obs` price history from `::ZsofiaDB` under the `::ZsofiaDBContext`;
  the return value if a dataframe whenever `::Observables` and a dictionary
  mapping observables to `DataFrame`s whenever `::Vector{Observables}`.

  # Arguments
  - `db::ZsofiaDB`: Zsofia data base.
  - `obs::Observables`: underlying observables.
  - `::ZsofiaDBContext`: either `Bjorn`, `Brynhild`, `Hjordis` or `Valtyr`. 
  - `date::Union{Date,Vector{Date},NTuple{Date}}`: date(s), when `:Hjordis`.
  - `from::Date`: if `::Brynhild`, get data from `from`.
  - `to::Date`: if `::Brynhild`, get data to `to`.
  """
  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Bjorn,
                     date::Date
                ) where U<:Fetchables
    _p = _path_to(db, obs, BJORN, date)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw(ArgumentError("Can't find $(_to_bunch(obs))[Bjorn] for $(date)"))
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     cal::HolidayCalendar,
                     ::Bjorn,
                     date::Date
                )
    _p = _path_to(db,Stock,cal,BJORN,date)
    if ~isfile(_p) && iszero(stat(p).size)
      throw(ArgumentError("Can't find $(_to_bunch(Stock))[BJORN] for $(date)"))
    end
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     ::Bjorn,
                     date::Date
                ) where U<:Fetchables
    _p = _path_to(db,U,BJORN,date)
    if ~isfile(_p) && iszero(stat(p).size)
      throw(ArgumentError("Can't find $(U)[BJORN] for $(date)"))
    end
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end

  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Bjorn
                ) where U<:Fetchables
    _p = last(readdir(dirname(_path_to(db, obs, BJORN, today())), join=true))
    isfile(_p) || throw(ArgumentError("No record for this BJORN $(U)"))
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     cal::HolidayCalendar,
                     ::Bjorn
                )
    _p = last(filter(endswith(".csv"),readdir(_path_to(db,Stock,cal,BJORN))))
    isfile(_p) || throw(ArgumentError("Stock[$(_stock_silo(cal))] inexistent"))
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end

  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     ::Bjorn
                ) where U<:Fetchables
    _p = last(filter(endswith(".csv"),readdir(_path_to(db,U,BJORN))))
    isfile(_p) || throw(ArgumentError("$(U)[BJORN] inexistent"))
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end

  function Base.read(
                     obs::U,
                     ::Bjorn,
                     date::Date
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, obs, BJORN, date)
  end
  
  function Base.read(
                     ::Type{Stock},
                     cal::HolidayCalendar,
                     ::Bjorn,
                     date::Date
                )
    Base.read(HRAFNHILDUR, Stock, cal, BJORN, date)
  end
  
  function Base.read(
                     ::Type{U},
                     ::Bjorn,
                     date::Date
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, U, BJORN, date)
  end

  function Base.read(
                     obs::U,
                     ::Bjorn
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR,obs,BJORN)
  end
  
  function Base.read(::Type{Stock},cal::HolidayCalendar,::Bjorn)
    Base.read(HRAFNHILDUR,Stock,cal,BJORN)
  end
  
  function Base.read(::Type{U},::Bjorn) where U<:Fetchables
    Base.read(HRAFNHILDUR,U,BJORN)
  end

  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Bjorn,
                     date::Vector{Date},
                ) where U<:Fetchables
    _v = collect(d => _path_to(db, obs, BJORN, d) for d in date)
    _n = length(_v)
    filter!(x -> isfile(x.second) && !iszero(stat(x.second).size), _v)
    if isempty(_v)
      throw("Can't find $(U)[BJORN] for any of the provided dates")
    elseif isless(length(_v), _n)
      @warn "dropped $(length(_v)) non existent trading sessions (non-existent)"
    end
    odict = Dict{Date,AbstractDataFrame}()
    for v in _v
      odict[v.first] = read_csv(
                                v.second, DataFrame, stringtype=String,
                                truestrings=nothing, falsestrings=nothing,
                                types=Dict(:Tckr => String)
                       )
    end
    odict
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     cal::HolidayCalendar,
                     ::Bjorn,
                     dates::Vector{Date}
                )
    _v = collect(dt => _path_to(db,Stock,cal,BJORN,dt) for dt in dates)
    n = length(_v)
    filter!(v -> isfile(v.second) && !iszero(stat(v.second).size), _v)
    m = length(_v)
    if iszero(m)
      throw("Can't find Stock($(_stock_silo(cal)) of any of provided dates!")
    elseif isless(m, n)
      @warn "$(round(100m/n,digits=2))% only processing (due to inexistence)"
    end
    odict = Dict{Date,AbstractDataFrame}()
    for v in _v
      odict[v.first] = read_csv(
                                v.second, DataFrame, stringtype=String,
                                truestrings=nothing, falsestrings=nothing,
                                types=Dict(:Tckr => String)
                       )
    end
    odict
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     ::Bjorn,
                     dates::Vector{Date}
                ) where U<:Fetchables
    _v = collect(dt => _path_to(db,U,BJORN,dt) for dt in dates)
    n = length(_v)
    filter!(v -> isfile(v.second) && !iszero(stat(v.second).size), _v)
    m = length(_v)
    if iszero(m)
      throw("Can't find Stock($(_stock_silo(cal)) of any of provided dates!")
    elseif isless(m, n)
      @warn "$(round(100m/n,digits=2))% only processing (due to inexistence)"
    end
    odict = Dict{Date,AbstractDataFrame}()
    for v in _v
      odict[v.first] = read_csv(
                                v.second, DataFrame, stringtype=String,
                                truestrings=nothing, falsestrings=nothing,
                                types=Dict(:Tckr => String)
                       )
    end
    odict
  end

  function Base.read(
                     obs::U,
                     ::Bjorn,
                     date::Vector{Date}
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, obs, BJORN, date)
  end
  
  function Base.read(
                     ::Type{U},
                     ::Bjorn,
                     dates::Vector{Date}
                ) where U<:Fetchables
    read(HRAFNHILDUR, U, BJORN, dates)
  end
  
  function Base.read(
                     ::Type{Stock},
                     cal::HolidayCalendar,
                     ::Bjorn,
                     dates::Vector{Dates}
                )
    read(HRAFNHILDUR, Stock, cal, BJORN, dates)
  end

  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Brynhild,
                ) where U<:Fetchables
    _p = _path_to(db, obs, BRYNHILD)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw(ArgumentError("Can't find $(U)[Brynhild] in $(db)"))
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Brynhild
                )
    _p = _path_to(db, Stock, sym, cal, BRYNHILD)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find BRYNHILD of Stock[$(cal)][$(sym)] in $(db)")
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::AbstractString,
                     ::Brynhild
                ) where U<:Fetchables
    _p = _path_to(db, Stock, sym, BRYNHILD)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find BRYNHILD of $(U)[$(sym)] in $(db)")
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end

  function Base.read(
                     obs::U,
                     ::Brynhild,
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, obs, BRYNHILD)
  end
  
  function Base.read(
                     ::Type{U},
                     sym::AbstractString,
                     ::Brynhild
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, U, sym, BRYNHILD)
  end
  
  function Base.read(
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Brynhild
                )
    Base.read(HRAFNHILDUR, Stock, sym, cal, BRYNHILD)
  end

  function Base.read(
                     obs::U
                ) where U<:Fetchables
    Base.read(obs, BRYNHILD)
  end

  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Hjordis,
                     date::Date
               ) where U<:Fetchables
    _p = _path_to(db, obs, HJORDIS, date)
    tckr = getparams(obs,"ticker")
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find $(U)[HJORDIS] of $(tckr) in $(db) for $(date)")
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Hjordis,
                     date::Date
                )
    _p = _path_to(db,Stock,sym,cal,HJORDIS,date)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find $(sym) in (db)'s Stock[HJORDIS] for $(date)")
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::AbstractString,
                     ::Hjordis,
                     date::Date
                ) where U<:Fetchables
    _p = _path_to(db,U,sym,HJORDIS,date)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find $(sym) in $(db)'s $(U)[HJORDIS] for $(date)")
    end
    read_csv(
             _p, DataFrame, stringtype=String,
             truestrings=nothing, falsestrings=nothing,
             types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                    db::ZsofiaDB,
                    obs::U,
                    ::Hjordis,
                    date::Vector{Date}
                ) where U<:Fetchables
    odict = Dict{Date,DataFrame}()
    for d in date
      try
        odict[d] = read(db, obs, HJORDIS, d)
      catch er
        @info string(er)
        continue
      end
    end
    odict
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Hjordis,
                     dates::Vector{Date}
                )
    odict = Dict{Date,AbstractDataFrame}()
    for dt in dates
      try
        odict[dt] = read(db,Stock,sym,cal,HJORDIS,dt)
      catch er
        @warn string(er)
        continue
      end
    end
    odict
  end          
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::AbstractString,
                     ::Hjordis,
                     date::Vector{Date}
                ) where U<:Fetchables
    odict = Dict{Date,AbstractDataFrame}()
    for dt in dates
      try
        odict[dt] = read(db,U,sym,HJORDIS,dt)
      catch er
        @warn string(er)
        continue
      end
    end
    odict
  end
  
  function Base.read(
                     obs::U,
                     ::Hjordis,
                     date::Vector{Date}
                ) where U<:Fetchables
    return read(HRAFNHILDUR, obs, HJORDIS, date)
  end
  
  function Base.read(
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Hjordis,
                     dates::Vector{Date}
                )
    Base.read(HRAFNHILDUR, Stock, sym, cal, HJORDIS, dates)
  end
  
  function Base.read(
                     ::Type{U},
                     sym::AbstractString,
                     ::Hjordis,
                     dates::Vector{Date}
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, U, sym, HJORDIS, dates)
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Hjordis
                ) where U<:Fetchables
    _p = _path_to(db,obs,HJORDIS)
    if isempty(_p)
      throw("No Record in $(db) HJORDIS for $(U)'s $(getparams(obs,"ticker"))")
    end
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Hjordis
                )
    _p = _path_to(db,Stock,sym,cal,HJORDIS)
    if isempty(_p)
      throw("No Record in $(db) HJORDIS for Stock $(getparams(obs,"ticker"))")
    end
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::AbstractString,
                     ::Hjordis
                ) where U<:Fetchables
    _p = _path_to(db,U,sym,HJORDIS)
    if isempty(_p)
      throw("No Record in $(db) HJORDIS for $(U)'s $(sym)")
    end
    read_csv(
             _p, DataFrame, stringtype=String, truestrings=nothing,
             falsestrings=nothing, types=Dict(:Tckr => String)
    )
  end

  function Base.read(
                     obs::U,
                     ::Hjordis,
                     date::Date
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, obs, HJORDIS, date)
  end

  function Base.read(
                     obs::U,
                     ::Hjordis
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, obs, HJORDIS)
  end

  function Base.read(
                     ::Type{U},
                     sym::AbstractString,
                     ::Hjordis
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, U, sym, HJORDIS)           
  end
  
  function Base.read(
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Hjordis
                )
    Base.read(HRAFNHILDUR, Stock, sym, cal, HJORDIS)
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     obs::U,
                     ::Valtyr
                ) where U<:Fetchables
    _p = _path_to(db, obs, VALTYR)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find Valtyr of $(U)'s $(getparams(obs,"ticker")) in $(db)")
    end
    parsefile(_p)
  end


  function Base.read(
                     obs::U,
                     ::Valtyr,
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, obs, VALTYR)
  end

  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Valtyr
                )
    _p = _path_to(db, Stock, sym, cal, VALTYR)
    _s = _stock_silo(cal)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find Valtyr of $(cal) Stock $(sym) in $(db)")
    end
    parsefile(_p)
  end
  
  function Base.read(
                     ::Type{Stock},
                     sym::AbstractString,
                     cal::HolidayCalendar,
                     ::Valtyr
                )
    Base.read(HRAFNHILDUR, Stock, sym, cal, VALTYR)
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::AbstractString,
                     ::Valtyr
                ) where U<:Fetchables
    _p = _path_to(db, U, sym, VALTYR)
    if ~isfile(_p) || iszero(stat(_p).size)
      throw("Can't find Valtyr of $(U)'s $(sym) in $(db)")
    end
    parsefile(_p)
  end

  function Base.read(
                     ::Type{U},
                     sym::AbstractString,
                     ::Valtyr
                ) where U<:Fetchables
    Base.read(HRAFNHILDUR, U, sym, VALTYR)
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     obs::Vector{U},
                     silo::Hjordis,
                     date::Date
                ) where U<:Fetchables
    odict = Dict{Observables,AbstractDataFrame}()
    for o in obs
      odict[o] = read(db, o, silo, date)
    end
    odict
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::Vector{AbstractString},
                     cal::HolidayCalendar,
                     ::Hjordis,
                     date::Date
                )
    odict = Dict{AbstractString,AbstractDataFrame}()
    for s in sym
      try
        odict[s] = read(db,Stock,s,cal,HJORDIS,date)
      catch er
        @warn string(er)
        continue
      end
    end
    odict
  end

  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::Vector{AbstractString},
                     ::Hjordis,
                     date::Date
                ) where U<:Fetchables
    odict = Dict{AbstractString,AbstractDataFrame}()
    for s in sym
      try
        odict[s] = read(db,Stock,s,HJORDIS,date)
      catch er
        @warn string(er)
        continue
      end
    end
    odict
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     ::Type{Stock},
                     sym::Vector{AbstractString},
                     cal::HolidayCalendar,
                     ::T
                ) where T<:Union{Brynhild,Valtyr}
    odict = Dict{AbstractString, AbstractDataFrame}()
    for s in sym
      try
        odict[s] = read(db, Stock, s, cal, T)
      catch er
        @warn string(er)
        continue
      end
    end
    odict
  end

  function Base.read(
                     db::ZsofiaDB,
                     ::Type{U},
                     sym::Vector{AbstractString},
                     ::T
                ) where U<:Fetchables where T<:Union{Brynhild,Valtyr}
    odict = Dict{AbstractString, AbstractDataFrame}()
    for s in sym
      try
        odict[s] = read(db, U, s, T)
      catch er
        @warn string(er)
        continue
      end
    end
    odict
  end

  function Base.read(
                     ::Type{U},
                     sym::Vector{AbstractString},
                     ::T
                ) where U<:Fetchables where T<:Union{Brynhild,Valtyr}
    Base.read(HRAFNHILDUR,U,sym,T)
  end
  
  function Base.read(
                     db::ZsofiaDB,
                     obs::Vector{U},
                     silo::S
                ) where U<:Fetchables where S<:Union{Brynhild, Valtyr}
    odict = Dict{Observables, DataFrame}()
    for o in obs
      odict[o] = read(db, o, silo)
    end
    odict
  end
  
  function Base.read(
                     obs::Vector{U},
                     silo::S
                ) where U<:Fetchables where S<:Union{Brynhild, Valtyr}
    read(HRANHILDUR, obs, silo)
  end

  function Base.read(
                     obs::Vector{U},
                     silo::Hjordis,
                     date::Date
                ) where U<:Fetchables
    read(HRANHILDUR, obs, silo, date)
  end
end


# writers (into db)
begin
  """
      write([::ZsofiaDB], ::Observables, ::DataFrame, [::ZsofiaDBSilos])
  
  # Notes
  The `DataFrame` should be under appropriate skeleton; see `is_zsofiadb_able`.
  """
  function Base.write(
                      db::ZsofiaDB,
                      obs::U,
                      df::AbstractDataFrame,
                      silo::ZsofiaDBContext
                ) where U<:YahooFetchables
    _p = begin
      if isa(silo, Bjorn) || isa(silo, Hjordis)
        _path_to(db, obs, silo, df.Date[end])
      elseif isa(silo, Brynhild) || isa(silo, Valtyr)
        _path_to(db, obs, silo)
      else
        throw("incompleteness in implementation")
      end
    end
    mkpath(dirname(_p))
    write_csv(_p, df, delim=",", compress=false)
  end

  function Base.write(
                      db::ZsofiaDB,
                      ::Type{Stock},
                      sym::AbstractString,
                      cal::HolidayCalendar,
                      df::AbstractDataFrame,
                      silo::ZsofiaDBContext
                )
    _p = if isa(silo,Bjorn) || isa(silo,Hjordis)
      _path_to(db, Stock, sym, cal, silo, last(df.Date))
    elseif isa(silo, Brynhild) || isa(silo, Valtyr)
      _path_to(db, Stock, sym, cal, silo)
    end
    mkpath(dirname(_p))
    write_csv(_p, df, delim=",", compress=false)
  end
  
  function Base.write(
                      db::ZsofiaDB,
                      ::Type{U},
                      sym::AbstractString,
                      df::AbstractDataFrame,
                      silo::ZsofiaDBContext
                ) where U<:Fetchables
    _p = if isa(silo,Bjorn) || isa(silo,Hjordis)
      _path_to(db, U, sym, silo, last(df.Date))
    elseif isa(silo, Brynhild) || isa(silo, Valtyr)
      _path_to(db, U, sym, silo)
    end
    mkpath(dirname(_p))
    write_csv(_p, df, delim=",", compress=false)
  end                 

  function Base.write(
                      obs::U,
                      df::DataFrame,
                      silo::ZsofiaDBContext
                ) where U<:YahooFetchables
    Base.write(HRAFNHILDUR, obs, df, silo)
  end
  
  function Base.write(
                      db::ZsofiaDB,
                      obs::EconoIndicator,
                      df::DataFrame,
                      silo::S
                ) where S<:Union{Brynhild,Valtyr}
    _p = _path_to(db, obs, silo)
    mkpath(dirname(_p))
    write_csv(_p, df, delim=",", compress=false)
  end
  
  function Base.write(
                      db::ZsofiaDB,
                      ::Type{EconoIndicator},
                      sym::AbstractString,
                      df::DataFrame,
                      silo::S
                ) where S<:Union{Brynhild,Valtyr}
    _p = _path_to(db, EconoIndicator, sym, silo)
    mkpath(dirname(_p))
    write_csv(_p, df, delim=",", compress=false)
  end
  
  function Base.write(
                      obs::EconoIndicator,
                      df::DataFrame,
                      silo::V
                ) where V<:Union{Brynhild,Valtyr}
    Base.write(HRAFNHILDUR, obs, df, silo)
  end
  
  function Base.write(
                      ::Type{EconoIndicator},
                      sym::AbstractString,
                      df::DataFrame,
                      silo::V
                ) where V<:Union{Brynhild,Valtyr}
    Base.write(HRAFNHILDUR, EconoIndicator, sym, df, silo)
  end
end



#> fetchers (from remote servers)
begin
  """
      fetch(::Type{Observables}, ticker::AbstractString, silo, date; <kwargs>)
      fetch(obs::Observables, silo::ZsofiaContext, date::Date; <kwargs>)
      fetch(obs::Vector{Observables},::ZsofiaDBContext, date::Date, <kwargs>)
      fetch(obs::Vector{Observables},::ZsofiaDBContext, date::Vector{Date},...)
      
  Fetch `obs` data from Yahoo Finance or St-Louis Federal Reserve's FRED.
  
  When kwarg `return_data` is `true` (default):
  
  |**Args**|**Return Value**|
  |--:|:---|
  |`obs::Observables`, `date::Date`|`DataFrame`|
  |`obs::Vector{Observables}`, `date::Date`|`Dict{Observables,DataFrame}`|
  |`obs::Vector{Observables}`, `date::Vector{Date}`|`Dict{Date,Dict{Observables,DataFrame}}`|
  |`obs::Observables`, `date::Vector{Date}`|`Dict{Date,DataFrame}`|
  
  # Arguments
  - `obs::Union{Observables,Vector{Observables}}`: observable(s) to fetch.
  - `silo::ZsofiaDBContext`: either `BJORN`, `BRYNHILD`, `HJORDIS` or `VALTYR`.
  - `date::Union{Date,Vector{Date}}`: `fetch` from `date`.
  - `interval::String`: kwarg, "1m", "2m","5m","15m","30m","60m", or "90m".
  - `force::Bool`: kwarg, when `false` (default), no fetch if locally available
  - `localdb::ZsofiaDB`: kwarg, local data base defaulting to `HRAFNHILDUR`.
  - `return_data::Bool`: when `true` (default), return the proceed data.
  - `timeout::Int64`: when `silo` is `HJORDIS`, number of waiting seconds.
  - `run_fotografi::Bool=false`: whether or not to run `fotografi!()`.
  
  # Notes
  Note that some kwargs are not available for some contexts, what's following
  is a table of available kwargs per context (`ZsofiaDBContext`):
  
  |**Context**|**Args**|**Kwargs**|
  |:---:|:---:|:---:|
  |`BJORN`|obs, silo, date|to, force, localdb, return_data, timeout|
  |`BRYNHILD`|obs, silo|to, force, localdb, return_data|
  |`HJORDIS`|obs, silo, date|to, force, localdb, return_data, timeout|
  |`VALTYR`|obs, silo|force, localdb, return_data|
  
  Since `BJORN` is the facility/context of gathered/consolidated
  data from `HJORDIS`, `obs` is only used as a class representant
  of the global bunch (in `BUNCH`) representing such observables.
  When `fetch` is called under the `BJORN` context with an `obs::Observables`,
  the information about which observables to fetch is gotten from `UNNUR`.
  
  **WARNING**  
  80 percent of the time, `EconoIndicator`s are fetched from FRED while
  others are fetched from yahoo finance. Make sure to have correct
  parameters before proceeding.
  
  We decided not to separate `fetch`ing procedures from data
  processing and featuring (see `buildfeatures` function).
  Therefore, if one wants to benchmark the fetching time, the
  `buildfeatures` facility is available.
  
  ---
  
  The output `DataFrame` holds the following features:
  
  |**Feature Name**|**Description**|
  |---:|:---|
  |`Date`|trading session|
  |`Tckr`|ticker symbol|
  |`Time`|time (hh:mm:ss)|
  |`Price`|latest session's price|
  |`Open`|session's opening price|
  |`High`|session's highest price|
  |`Low`|lowest price of the session|
  |`Shares`|exchanged shares volume within the very time period|
  |`Co`|Open-Close relative variation (in %)|
  |`Hl`|Low-High relative variation (in %)|
  |`Ind`|indecision metric, kinda 100*(`Co`/`Hl` - 1)|
  |`Rvn`|`Price`-to-`Price` relative variation (session-to-session)|
  |`Gap`|ovenight gap (in %)|
  |`Gaug`|level of `Price` within the total variation range (in %)|
  |`rShares`|volume change session-to-session|
  |`cHigh`|running maximum|
  |`cLow`|running minimum|
  |`cCo`|running `Co`|
  |`cHl`|running `Hl`|
  |`cInd`|running `Ind`|
  |`cGaug`|running `Gaug`|
  |`cCoH`|running level of `High` w.r.t. first `Open`|
  |`cCoL`|running level of `Low` w.r.t. first `Open`|
  |`cShares`|running shares (in percentage) w.r.t. session's total|
  """
  function Base.fetch(
                      obs::U,
                      ::Hjordis,
                      date::Date;
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false
                ) where U<:YahooFetchables
    cal, tckr = getparams(obs,"calendar",US), getparams(obs,"ticker")
    if ~isbday(cal,date)
      @warn "HJORDIS[$(U)]> $(date) is not a business day for [$(tckr)]"
      return DataFrame()
    end
    if ~in(obs, localdb, silo=HJORDIS, date=date) || force
      df = _fetch_yahoo(
                        HJORDIS, _apitckr(obs), date, date+Day(1),
                        interval=interval, version=8, timeout=timeout
           )
    else
      return (return_data ? read(localdb,obs,HJORDIS) : nothing)
    end
    transform!(df, :Open => ByRow(x -> tckr) => :Tckr)
    buildfeatures!(obs, HJORDIS, df)
    filter!(:Date => <=(date), df)
    write(localdb, obs, df, HJORDIS)
    @info "HJORDIS[$(U)]> $(tckr), -got-> $(date) " * Char(0x2714)
    run_fotografi ? fotografi!() : nothing
    return_data ? df : nothing
  end
  
  function Base.fetch(
                      ::Type{Stock},
                      sym::AbstractString,
                      cal::HolidayCalendar,
                      ::Hjordis,
                      date::Date;
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRANFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false
                )
    if ~isbday(cal,date)
      @warn "HJORDIS[Stock($(cal))]> for $(sym), $(date) not a business day."
      return DataFrame()
    end
    if ~in(sym,Stock,cal,HJORDIS,date,db=localdb) || force
      df = _fetch_yahoo(
                        HJORDIS, _apitckr(sym,Stock,cal), date, date+Day(1),
                        interval=interval, version=8, timeout=timeout
           )
    else
      return (return_data ? read(localdb,Stock,sym,cal,HJORDIS,date) : nothing)
    end
    transform!(df, :Open => ByRow(x -> sym) => :Tckr)
    buildfeatures!(Stock,HJORDIS,df)
    filter!(:Date => <=(date), df)
    write(localdb,Stock,sym,cal,df,HJORDIS)
    @info "HJORDIS[Stock($(cal))]> $(sym), -got-> $(date) " * Char(0x2714)
    run_fotografi ? fotografi!() : nothing
    return_data ? df : nothing
  end

  function Base.fetch(
                      ::Type{U},
                      sym::AbstractString,
                      ::Hjordis,
                      date::Date;
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRANFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false,
                      cal::HolidayCalendar=US
                ) where U<:Fetchables
    if ~isbday(cal,date)
      @warn "HJORDIS[$(U)]> for $(sym), $(date) not a business day."
      return DataFrame()
    end
    if ~in(sym,U,HJORDIS,date,db=localdb) || force
      df = _fetch_yahoo(
                        HJORDIS, _apitckr(sym,U), date, date+Day(1),
                        interval=interval, version=8, timeout=timeout
           )
    else
      return (return_data ? read(localdb,U,sym,HJORDIS,date) : nothing)
    end
    transform!(df, :Open => ByRow(x -> sym) => :Tckr)
    buildfeatures!(U,HJORDIS,df)
    filter!(:Date => <=(date), df)
    write(localdb,U,sym,df,HJORDIS)
    @info "HJORDIS[$(U)]> $(sym), -got-> $(date) " * Char(0x2714)
    run_fotografi ? fotografi!() : nothing
    return_data ? df : nothing
  end
  
  function Base.fetch(
                      obs::AbstractVector{U},
                      ::Hjordis,
                      date::Date;
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false
                ) where U<:YahooFetchables
    odict = Dict{Observables,Union{AbstractDataFrame,Nothing}}()
    _taskies = map(obs) do _obs
      @task begin
          odict[_obs] = fetch(
                              _obs, HJORDIS, date, interval=interval,
                              force=force, localdb=localdb,
                              return_data=return_data, timeout=timeout
                        )
      end
    end
    for t in _taskies
      setfield!(t, :sticky, false)
    end
    (schedule.(_taskies); wait.(_taskies))
    _failed = filter(istaskfailed, _taskies)
    if isless(0,length(_failed))
      @warn "HJORDIS[$(U)]> experienced some failures " failures=_failed
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{Stock},
                      sym::AbstractVector{AbstractString},
                      cal::HolidayCalendar,
                      ::Hjordis,
                      date::Date;
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false
                )
    odict = Dict{AbstractString,Union{AbstractDataFrame,Nothing}}()
    _taskies = map(sym) do _sym
      @task begin
          odict[_sym] = fetch(
                              Stock, _sym, cal, HJORDIS, date,
                              interval=interval, force=force, localdb=localdb,
                              return_data=return_data, timeout=timeout
                        )
      end
    end
    for t in _taskies
      setfield!(t, :sticky, false)
    end
    (schedule.(_taskies); wait.(_taskies))
    _failed = filter(istaskfailed, _taskies)
    if isless(0,length(_failed))
      @warn "HJORDIS[Stock]> experienced some failures " failures=_failed
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{U},
                      sym::AbstractVector{AbstractString},
                      ::Hjordis,
                      date::Date;
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false,
                      cal::HolidayCalendar=US
                ) where U<:YahooFetchables
    odict = Dict{AbstractString,Union{AbstractDataFrame,Nothing}}()
    _taskies = map(sym) do _sym
      @task begin
          odict[_sym] = fetch(
                              U, _sym, HJORDIS, date,
                              interval=interval, force=force, localdb=localdb,
                              return_data=return_data, timeout=timeout,
                              cal=cal
                        )
      end
    end
    for t in _taskies
      setfield!(t, :sticky, false)
    end
    (schedule.(_taskies); wait.(_taskies))
    _failed = filter(istaskfailed, _taskies)
    if isless(0,length(_failed))
      @warn "HJORDIS[$(U)]> experienced some failures " failures=_failed
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end
  
  function Base.fetch(
                      obs::U,
                      ::Hjordis,
                      dates::AbstractVector{Date};
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=true
                ) where U<:YahooFetchables
    _cal, tckr = getparams(obs, "calendar", US), getparams(obs, "ticker")
    odict = Dict{Date,Union{AbstractDataFrame,Nothing}}()
    @threads for dt in dates
      if ~isbday(_cal, dt)
        @info "HJORDIS[$(U)]> for $(tckr), $(dt) is not a business day."
        continue
      end
      try
        odict[dt] = fetch(
                          obs, HJORDIS, dt, interval=interval, force=force,
                          localdb=localdb, return_data=return_data,
                          timeout=timeout
                    )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{Stock},
                      sym::AbstractString,
                      cal::HolidayCalendar,
                      ::Hjordis,
                      dates::AbstractVector{Date};
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=true
                )
    odict = Dict{Date,Union{AbstractDataFrame,Nothing}}()
    @threads for dt in dates
      if ~isbday(cal, dt)
        @info "HJORDIS[Stock($(cal))]> $(sym), $(dt) is not a business day."
        continue
      end
      try
        odict[dt] = fetch(
                          Stock, sym, cal, HJORDIS, dt, interval=interval,
                          force=force, localdb=localdb,
                          return_data=return_data, timeout=timeout
                    )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{U},
                      sym::AbstractString,
                      ::Hjordis,
                      dates::AbstractVector{Date};
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=true,
                      cal::HolidayCalendar=US
                ) where U<:YahooFetchables
    odict = Dict{Date,Union{AbstractDataFrame,Nothing}}()
    @threads for dt in dates
      if ~isbday(cal, dt)
        @info "HJORDIS[$(U)]> $(sym), $(dt) is not a business day."
        continue
      end
      try
        odict[dt] = fetch(
                          U, sym, HJORDIS, dt, interval=interval,
                          force=force, localdb=localdb,
                          return_data=return_data, timeout=timeout,
                          cal=cal
                    )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      obs::AbstractVector{U},
                      ::Hjordis,
                      dates::AbstractVector{Date};
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=true
                ) where U<:YahooFetchables
    odict = Dict{Date,Dict{Observables,Union{AbstractDataFrame,Nothing}}}()
    for dt in dates
      try
        odict[dt] = fetch(
                          obs, HJORDIS, dt, interval=interval, force=force,
                          localdb=localdb, return_data=return_data,
                          timeout=timeout
                    )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{Stock},
                      sym::Vector{AbstractString},
                      cal::HolidayCalendar,
                      ::Hjordis,
                      dates::AbstractVector{Date};
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=true
                )
    odict = Dict{Date,Dict{AbstractString,Union{AbstractDataFrame,Nothing}}}()
    for dt in dates
      try
        odict[dt] = fetch(
                          Stock, sym, cal, HJORDIS, dt, interval=interval,
                          force=force, localdb=localdb,
                          return_data=return_data, timeout=timeout
                    )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{U},
                      sym::Vector{AbstractString},
                      ::Hjordis,
                      dates::AbstractVector{Date};
                      interval::AbstractString="1m",
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=true,
                      cal::HolidayCalendar=US
                ) where U<:YahooFetchables
    odict = Dict{Date,Dict{AbstractString,Union{AbstractDataFrame,Nothing}}}()
    for dt in dates
      try
        odict[dt] = fetch(
                          U, sym, HJORDIS, dt, interval=interval,
                          force=force, localdb=localdb,
                          return_data=return_data, timeout=timeout,
                          cal=cal
                    )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end
  
  function Base.fetch(
                      obs::U,
                      ::Brynhild;
                      interval::Tuple{D,D}=(Date(1970,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false
                ) where {U<:Fetchables, D<:Date}
    from, to = minmax(interval[1],interval[2])
    tckr = getparams(obs, "ticker")
    _nxtbd = tobday(getparams(obs,"calendar",US),from)
    if isless(to,_nxtbd)
      throw(ArgumentError("No business days between $(from) and $(to)"))
    end
    if ~in(obs, localdb, silo=BRYNHILD) || force
      df = _fetch_yahoo(
                        BRYNHILD, _apitckr(obs), from, to, interval="1d",
                        version=8, timeout=timeout
           )
    else
      return (return_data ? read(localdb, obs, BRYNHILD) : nothing)
    end
    transform!(df, :Open => ByRow(x -> tckr) => :Tckr)
    buildfeatures!(obs,BRYNHILD,df)
    write(localdb,obs,df,BRYNHILD)
    @info "BRYNHILD[$(U)]> $(tckr) -got->> $(last(df.Date))"
    run_fotografi ? fotografi!() : nothing
    return_data ? df : nothing
  end

  function Base.fetch(
                      ::Type{Stock},
                      sym::AbstractString,
                      cal::HolidayCalendar,
                      ::Brynhild;
                      interval::Tuple{D,D}=(Date(1970,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int64=30,
                      run_fotografi::Bool=false
                ) where D<:Date
    from, to = minmax(interval[1],interval[2])
    _nxtbd = tobday(cal,from)
    if isless(to,_nxtbd)
      throw("$(cal) shows No Business Days Between $(from) and $(to)")
    end
    if ~in(sym, Stock, cal, BRYNHILD, db=localdb) || force
      df = _fetch_yahoo(
                        BRYNHILD, _apitckr(sym,Stock,cal), from,
                        to, interval="1d", version=8, timeout=timeout
           )
    else
      return (return_data ? read(localdb,Stock,sym,cal,BRYNHILD) : nothing)
    end
    transform!(df, :Open => ByRow(x -> sym) => :Tckr)
    buildfeatures!(Stock,BRYNHILD,df)
    write(localdb,Stock,sym,cal,df,BRYNHILD)
    @info "BRYNHILD[Stock($(cal))]> $(sym) -got->> $(last(df.Date))"
    run_fotografi ? fotografi!() : nothing
    return_data ? df : nothing
  end

  function Base.fetch(
                      ::Type{U},
                      sym::AbstractString,
                      ::Brynhild;
                      interval::Tuple{D,D}=(Date(1970,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=false,
                      cal::HolidayCalendar=US
                ) where {U<:Fetchables, D<:Date}
    from, to = minmax(interval[1],interval[2])
    _nxtbd = tobday(cal,from)
    if isless(to,_nxtbd)
      throw("$(cal) shows No Business Days Between $(from) and $(to)")
    end
    if ~in(sym, U, BRYNHILD, db=localdb) || force
      df = _fetch_yahoo(
                        BRYNHILD, _apitckr(sym,U), from,
                        to, interval="1d", version=8, timeout=timeout
           )
    else
      return (return_data ? read(localdb,U,sym,BRYNHILD) : nothing)
    end
    transform!(df, :Open => ByRow(x -> sym) => :Tckr)
    buildfeatures!(U,BRYNHILD,df)
    write(localdb,U,sym,df,BRYNHILD)
    @info "BRYNHILD[$(U)]> $(sym) -got->> $(last(df.Date))"
    run_fotografi ? fotografi!() : nothing
    return_data ? df : nothing
  end

  function Base.fetch(
                      obs::AbstractVector{U},
                      ::Brynhild;
                      interval::Tuple{D,D}=(Date(1970,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=ifelse(length(obs)>5,true,false)
                ) where {U<:YahooFetchables,D<:Date}
    odict = Dict{Observables,Union{Nothing,AbstractDataFrame}}()
    @threads for _obs in obs
      try
        odict[_obs] = fetch(
                            _obs, BRYNHILD, interval=interval, force=force,
                            localdb=localdb, return_data=return_data,
                            timeout=timeout
                      )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{Stock},
                      sym::AbstractVector{AbstractString},
                      cal::HolidayCalendar,
                      ::Brynhild;
                      interval::Tuple{D,D}=(Date(1970,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int=30,
                      run_fotografi::Bool=ifelse(length(obs)>5,true,false)
                ) where D<:Date
    odict = Dict{AbstractString,Union{Nothing,AbstractDataFrame}}()
    for _sym in sym
      try
        odict[_sym] = fetch(
                            Stock, _sym, cal, BRYNHILD, interval=interval,
                            force=force, localdb=localdb,
                            return_data=return_data, timeout=timeout
                      )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{U},
                      sym::AbstractVector{AbstractString},
                      ::Brynhild;
                      interval::Tuple{D,D}=(Date(1970,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      timeout::Int64=30,
                      run_fotografi::Bool=ifelse(length(obs)>7,true,false),
                      cal::HolidayCalendar=US
                ) where {U<:YahooFetchables,D<:Date}
    odict = Dict{AbstractString,Union{Nothing,AbstractDataFrame}}()
    for _sym in sym
      try
        odict[_sym] = fetch(
                            U, _sym, BRYNHILD, interval=interval,
                            force=force, localdb=localdb,
                            return_data=return_data, timeout=timeout,
                            cal=cal
                      )
      catch er
        @warn er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end
  
  function Base.fetch(
                      obs::EconoIndicator,
                      ::Brynhild;
                      interval::Tuple{Date,Date}=(Date(1950,1,1), today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      fred_api_key::AbstractString=UNNUR["FRED_API_KEY"],
                      run_fotografi::Bool=false
                )
    tckr = getparams(obs,"ticker")
    if ~in(obs,localdb,silo=BRYNHILD) || force
      df = get_data(
                    Fred(fred_api_key),
                    _apitckr(obs),
                    observation_start=string(interval[1]),
                    observation_end=string(interval[2])
           ) |> (x -> getfield(x,:data))
    else
      return (return_data ? read(localdb,obs,silo=BRYNHILD) : nothing)
    end
    transform!(df, :value => ByRow(x -> tckr) => :Tckr)
    buildfeatures!(obs,BRYNHILD,df)
    write(localdb,obs,df,BRYNHILD)
    run_fotografi ? fotografi!() : nothing
    @info "BRYNHILD[EconoIndicator]> $(tckr) -got->> $(last(df.Date))"
    return_data ? df : nothing
  end

  function Base.fetch(
                      ::Type{EconoIndicator},
                      sym::AbstractString,
                      ::Brynhild;
                      interval::Tuple{Date,Date}=(Date(1950,1,1), today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      fred_api_key::AbstractString=UNNUR["FRED_API_KEY"],
                      run_fotografi::Bool=false
                )
    if ~in(sym, EconoIndicator, BRYNHILD, db=localdb) || force
      df = get_data(
                    Fred(fred_api_key),
                    _apitckr(sym, EconoIndicator),
                    observation_start=string(minimum(interval)),
                    observation_end=string(maximum(interval))
           ) |> (x -> getfield(x,:data))
    else
      return (return_data ? read(localdb,EconoIndicator,sym,BRYNHILD) : nothing)
    end
    transform!(df, :value => ByRow(x -> sym) => :Tckr)
    buildfeatures!(EconoIndicator,BRYNHILD,df)
    write(localdb,EconoIndicator,sym,df,BRYNHILD)
    run_fotografi ? fotografi!() : nothing
    @info "BRYNHILD[EconoIndicator]> $(sym) -got->> $(last(df.Date))"
    return_data ? df : nothing
  end
  
  function Base.fetch(
                      obs::AbstractVector{EconoIndicator},
                      ::Brynhild;
                      interval::Tuple{Date,Date}=(Date(1950,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      fred_api_key::Fred=Fred(UNNUR["FRED_API_KEY"]),
                      run_fotografi::Bool=false
                )
    odict = Dict{EconoIndicator,Union{DataFrame,Nothing}}()
    for _obs in obs
      try
        odict[_obs] = fetch(
                            _obs, BRYNHILD, interval=interval, force=force,
                            localdb=localdb, return_data=return_data,
                            fred_api_key=fred_api_key
                      )
      catch er
        @info er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end

  function Base.fetch(
                      ::Type{EconoIndicator},
                      sym::AbstractVector{AbstractString},
                      ::Brynhild;
                      interval::Tuple{Date,Date}=(Date(1950,1,1),today()),
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      fred_api_key::Fred=Fred(UNNUR["FRED_API_KEY"]),
                      run_fotografi::Bool=false
                )
    odict = Dict{AbstractVector,Union{DataFrame,Nothing}}()
    for _sym in sym
      try
        odict[_sym] = fetch(
                            EconoIndicator, _sym, BRYNHILD, interval=interval,
                            force=force, localdb=localdb,
                            return_data=return_data, fred_api_key=fred_api_key
                      )
      catch er
        @info er
      end
    end
    run_fotografi ? fotografi!() : nothing
    return_data ? odict : nothing
  end
  
  function Base.fetch(
                      obs::U,
                      ::Valtyr;
                      force::Bool=false,
                      localdb::ZsofiaDB=HRAFNHILDUR,
                      return_data::Bool=true,
                      run_fotografi::Bool=false
                ) where U<:YahooFetchables
    nothing
  end
end