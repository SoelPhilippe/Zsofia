"""
*UPDATE of CSILLA*

**Name**: `csiupdate`.

**NArgs**: (1,1)

**Summary**: `csiupdate` is meant to perform
the "UPDATE" task of Zsofia's CSILLA.
It does take a bunchee as arguement, applies the
`csilla` function to every single
of its element and returns nothing special.
"""
function csiupdate(bunchee::String; rfyl=Rfyl)
  t0 = now()
  iam = "csiupdate"
  Ħ=(bunchee, Char(0x2022), Junkme[bunchee])
  pr(
     msg;
     fyle = stdout,
     ĸ = Ħ[2],
     kol = 102
  ) = heil(msg, rfyl=fyle, fi=FiZ(Ħ[1], ĸ, Ħ[3]), kol=kol, iam=iam)
  ape = YAML.load_file("Unnur.yaml")[Bmap[Ħ[1]]]
  t=nothing
  try
    t=filter(endswith(".csv"), readdir(Silk*bunchee*"/Dailies/"))
    t=(match(r"\d{4}-\d{2}-\d{2}", x).match for x in t)
    t=maximum(Date(x) for x in t)
    pr("Last record detected → "*string(t)*".", kol=94)
  catch err
    if isfile(rfyl) && occursin("/", rfyl)
      tr=+(last(findlast("/",rfyl)),1)
      tr=rfyl[tr:end]
    else
      tr=+(last(findlast("/",Rfyl)),1)
      tr=Rfyl[tr:end]
    end
    pr("something went wrong...\nSee "*tr*" for more details.")
    pr("something went wrong ↓\n"*string(err), fyle=rfyl)
    return nothing
  end
  t = advancebdays(holidayz.cal[bunchee], t, 1)
  pr("Next Business Day detected →"*string(t), kol=94)
  t=(x for x=t:Day(1):(today()-Day(1)) if isbday(holidayz.cal[bunchee],x))
  if isempty(t)
    pr("DATA NOT AVAILABLE!")
  else
    for t in t
      zoll, well = [], nothing
      mkpath(Silk*Ħ[1]*"/"*"Csilla"*Dtag[Ħ[1]]*"-"*string(t)*"/")
      pr("patheed!")
      pr("trying "*string(t)*"...")
      fi = [PyZsofia.PyIz(Ħ[1], ape[k], Ħ[3]) for k in ape]
      pr("container has been built!", kol=13)
      dunni = [@task csilla(i, string(t), rfyl=rfyl) for i in fi]
      (schedule.(dunni); wait.(dunni))
      dunni = map(x -> getfield(x, :result), dunni)
      !all(isnothing,dunni) ? dunni=filter(!isnothing,dunni) : nothing
      isempty(dunni) ? nothing : well=map(x -> getfield(x,2), dunni)
      isempty(dunni) ? nothing : dunni=map(x -> getfield(x,1), dunni)
      if isempty(dunni)
        pr("empty well!\nExiting...", kol=9, bold=true)
        pr("hasn't processed at all. Investigate it...", rfyl=rfyl)
        pr(Did*Dtag[Ħ[1]]*"-"*t*".csv"*" is generated though!")
        pr(Did*Dtag[Ħ[1]]*"-"*t*".csv"*" is generated though.", rfyl=rfyl)
        touch(Silk*bunchee*"/Dailies/"*Did*Dtag[Ħ[1]]*"-"*t*".csv")
        continue
      end
      (unique!(well); sort!(well))
      if isless(1, length(well))
        printstyled(length(well), " wells detected: ", well, "\n", color=13)
        printstyled("Choosing "*last(well)*"\n", color=13)
      elseif isone(length(well))
        well=only(well)
      else
        printstyled("Something weird did happen!\n", color=9)
        continue
      end
      dunni = map(x -> getfield(x, :tckr), dunni)
      zoll = setdiff(ape, dunni)
      if !isempty(zoll)
        pr("some unnured instruments have not processed:↓", fyle=rfyl)
        pr("some unnured instruments have not been processed:↓")
        msg=(ifelse(isless(0.72, rand()), p*"\n", p*"\t") for p in zoll)
        msg=join(msg)
        broadcast((x -> pr(msg, rfyl=x, kol=94)), [rfyl, stdout])
      end
      pr("is Hludanizing...")
      hludana(Ħ[1], dunni, well, rfyl=rfyl)
    end
  end
  pr("[>>END<<]", fyle=rfyl)
  t0=string(round(now()-t0,Second))
  pr("Time elapsed: "*t0, fyle=rfyl)
end
"""
**CSILLA ADD**.

**Name**: `csiadd`.

**NArgs**: (1,1)

**Summary**: This function purpose of this function
is to add an instrument into the Zsofia's CSILLA DB
and refresh it as much as possible.
"""
function csiadd(bunchee::String; kromi::Vector{String}=String[],rfyl=Rfyl)
  if isempty(kromi)
    msg="Nothing to be processed in Csilla of "*bunchee*"\nQuitting..."
    printstyled(msg, color=9)
    return nothing
  end
  dick = YAML.load_file("Unnur.yaml")
  them = kromi
  msg = "\tTrying to ADD some fi into "*bunchee*"...\n"
  printstyled(msg, color=rand(Coul), bold=true)
  damn = [x for x in them if x in dick[Bmap[bunchee]]]
  thorn = String[]
  if !isempty(damn)
    msg="The following instruments are already in "*bunchee*": \n"
    printstyled(msg, color=102)
    (msg=(i for i in damn); msg=join(msg))
    printstyled(msg, color=13)
    printstyled("\nThey'll be handled as better as possible!\n", color=102)
    printstyled("Starting to process them...\n", color=102)
    dr = (today()-Day(30), today() - Day(23))
    dr = [t for t in dr[1]:Day(1):dr[2] if isbday(holidayz.cal[bunchee], t)]
    df = readdir(Silk*bunchee*"/Dailies/")
    dfs= [p for p=df if occursin(t, p) for t in dr]
    dfs= [Silk*bunchee*"/Dailies/"*p for p in dfs]
    df = DataFrame()
    if !isempty(dfs)
      for f in dfs
        append!(
                df,
                CSV.read(
                         f,
                         DataFrame,
                         stringtype=String,
                         truestrings=nothing,
                         falsestrings=nothing
                ),
                promote=true
        )
      end
      g=(in(i, unique(df.Tckr)) ? push!(thorn, i) : nothing for i=damn)
      (collect(g); g=nothing)
      if !isempty(thorn)
        them = setdiff(them, thorn)
        printstyled("The following instruments won't be added:\n")
        g=join(j*" " for j in thorn)
        printstyled(g, "\n", color=11)
        g=nothing
      else
        msg="The following instruments won't be added:"
        println(msg)
        msg="The following instruments will be added from "*string(dr[end])
        println(msg)
        msg=join(j*" " for j in damn)
        printstyled(msg, "\n", color=102)
      end
    else
      msg="Dailies of "*bunchee*" do not exist between\n"
      msg*=string(dr[end])*" and "*string(dr[end])*"\nContinue..."
      printstyled(msg, color=102, bold=true)
      readuntil(stdin, "\n")
      println("")
      return nothing
    end
  else
    printstyled("Okay:"*string(Char(0x2714))*"\n", color=102)
  end
  msg="Adding some "*Char(0x3008)*" "*bunchee*"|"*Char(0x2022)
  msg*=Char(0x3009)*"\nContainerizing..."
  printstyled(msg, color=102, bold=true)
  fi=(PyZsofia.PyIz(bunchee, i, Junkme[bunchee]) for i=them)
  printstyled(Char(0x2714)*"\n", color=10)
  thorn=[]
  printstyled("Thorned: "*Char(0x2714)*"\n", color=102)
  for i in fi
    msg="Csilla'ing "*Char(0x3008)*i.land*"|"*i.tckr*Char(0x3009)*"...\n"
    printstyled(msg, color=102, bold=true)
    t=(today()-Day(29), today())
    hrzn=Tuple(l for l=t[1]:Day(1):t[2] if in(dayofweek(l), 1:5))
    hrzn=Tuple(l for l=hrzn if !in(l, holidayz.holl[bunchee]))
    for d in hrzn
      msg=
      printstyled("Processing $d ...\n", color=102)
      push!(thorn, csilla(i, string(d), rfyl=rfyl))
      printstyled(" processed!\n", color=102)
    end
  end
  filter!(x -> !isnothing(x), thorn)
  if !isempty(thorn)
    reg=r"([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))" # To get dates
    peee=readdir(Silk*bunchee*"/Dailies/")
    peee=map(x -> x[findfirst(reg, x)], peee)
    paths=getfield.(thorn, 2)
    for p in paths
      dee=p[findfirst(reg, p)]
      if !in(dee, peee)
        printstyled("Not in Dailies...\nSkipping...", color=11, bold=true)
        continue
      end
      printstyled("$dee fixed for $p \n", color=102, bold=true)
      tickers=map(
                  x -> x[begin:(end-4)],
                  filter(
                         x -> occursin(".csv", x),
                         readdir(p)
                  )
              )
      printstyled("Firstly tickerized!\n", color=102)
      hlu=CSV.read(
                   Silk*bunchee*"/Dailies/"*
                   "Dailies"*Dtag[bunchee]*"-"*
                   string(dee)*".csv",
                   DataFrame,
                   stringtype=String,
                   truestrings=nothing,
                   falsestrings=nothing
          )
      printstyled(
                  "Loading $( bunchee )'s Dailies of $dee: "*Char(0x2714)*"\n",
                  color=102
      )
      dtickers=Tuple(t for t in unique(hlu.Tckr) if !in(t, tickers))
      printstyled("Secondly tickerized!\n", color=102)
      for i in dtickers
        oj=filter(x -> x.Tckr in (i,), hlu)
        DataFrames.rename!(
                           oj,
                           :Time => :Datetime,
                           :Vol => :Volume,
                           :Price => :Close
        )
        oj=transform(oj, :Close => (x -> x) => :adjclose)
        DataFrames.rename!(oj, "adjclose" => "Adj Close")
        z=["Datetime", "Open", "High", "Low", "Close", "Adj Close", "Volume"]
        DataFrames.select!(oj, z)
        oj=transform(
                     oj,
                     :DateTime =>
                           ByRow(x -> replace(string(x), "T" => " ")) =>
                     :DateTime
           )
        CSV.write(p*i*".csv", oj)
        printstyled(
                    Char(0x3008)*bunchee*"|"*i*Char(0x3009)*
                    " popped from Dailies of $dee: "*Char(0x2714)*"\n",
                    color=11
        )
        oj, z = nothing, nothing
      end
      dtickers=nothing
      tickers=map(
                  x -> x[begin:(end-4)],
                  filter(
                         x -> occursin(".csv", x),
                         readdir(p)
                  )
              )
      printstyled("Tickerized: "*Char(0x2714)*"\n")
      hludana(
              bunchee,
              tickers,
              p,
              rfyl=rfyl
      )
    end
    printstyled("Updating Unnur...", color = 102)
    lou=union(
              dick[Bmap[bunchee]],
              getfield.(getfield.(thorn, 1), :tckr)
    )
    (sort!(lou); unique!(lou))
    dick[Bmap[bunchee]]=lou
    lou=nothing
    printstyled("... done!\n", color = 102)
    YAML.write_file("Unnur.yaml", dick, "---\n")
    printstyled("Instruments unnured: "*Char(0x2714)*"\n", color=10)
    return getfield.(getfield.(thorn, 1), :tckr)
  else
    printstyled(
                "None of the following has been processed successfully:\n",
                color=96
    )
    for i in getfield.(fi, 2)
      printstyled(i, " ", color=9, bold=true)
    end
    println("")
    printstyled(
                "There are some wormholes out there, try to find'em out!\n",
                color=9,
                bold=true
    )
    return nothing
  end
end
"""
**Name**: `csiverify`.

**NArgs**: (1,1)

**Summary**: This function has been made up
to perform the "VERIFY" task of CSILLA.
"""
function csiverify(
                   bunchee::String;
                   rfyl=Rfyl
         )
  csiv=("SOME", "TUTTI", "DEEP")
  printstyled(
              "\t\t[MESSAGE]\n"*
              "\tThis panel verifies if an instrument is     \n"*
              "\tpresent into Zsofia's CSILLA DB.               \n"*
              "\t [x] [[SOME]] checks if input are Unnured.     \n"*
              "\t [x] [[TUTTI]] Unnured and their parameters.   \n"*
              "\t [x] [[DEEP]] generates a whole report from DB.\n",
              color=102,
              bold=true
  )
  sleep(2)
  ccsiv=chooseme(csiv, "CSILLA::VERIFY:::$bunchee")
  if iszero(ccsiv)
    printstyled("Quitting...", color=13, bold=true)
  elseif isequal(csiv[ccsiv], "SOME")
    print("\t[:CSILLA::VERIFY:::$bunchee ]\nEnter some tickers:  ")
    them=string.(split(readuntil(stdin, "\n")))
    d=YAML.load_file("Unnur.yaml")[Bmap[bunchee]]
    length(them) > 0 ? println("\n\t [RESULTS]") : nothing
    for t in range(1, length(them))
      if iszero(mod(t,4))
        if in(them[t], d)
          printstyled(them[t]*" ["*string(Char(0x2713))*"]\n",
                      color=10, bold=true)
        else
          printstyled(them[t]*" ["*string(Char(0x2717))*"]\n",
                      color=9, bold=true)
        end
      else
        if in(them[t], d)
          printstyled(them[t]*" ["*string(Char(0x2713))*"]\t",
                      color=10, bold=true)
        else
          printstyled(them[t]*" ["*string(Char(0x2717))*"]\n",
                      color=9, bold=true)
        end
      end
    end
    readuntil(stdin, "\n")
  elseif isequal(csiv[ccsiv], "TUTTI")
    d=YAML.load_file("Unnur.yaml")[Bmap[bunchee]]
    for t in range(1,length(d))
      iszero(mod(t,4)) ? print("\t"*d[t]*"\n") : print("\t"*d[t])
    end
    readuntil(stdin, "\n")
    run(`clear`)
    printstyled(
                " Now Check Outdated from $bunchee for the last 30 days:\n"*
                " Detailing:\n"*
                "\t+ Last Hludanas: Date & Size.\n"*
                "\t+ Outdated Instruments\n\n",
                color=13,
                bold=true
    )
    printstyled("Continue...")
    readuntil(stdin, "\n")
    println("")
    hlu=readdir(Silk*bunchee*"/Dailies/", sort=true)
    hlu=filter!(x -> occursin("Dailies", x), hlu)
    hlu=last(hlu, 30)
    phlu=joinpath.(Silk*bunchee*"/Dailies/", hlu)
    odin=Vector{Tuple{Int64, Int64}}(undef, length(phlu))
    for p in range(1, length(hlu))
      printstyled(
         hlu[p]*" → "*string(
                             size(
                                   CSV.read(
                                            phlu[p],
                                            DataFrame,
                                            stringtype=String
                                       )
                             )
         )*"\n",
         color=13,
         bold=true
      )
    end
    readuntil(stdin, "\n")
  elseif isequal(csiv[ccsiv], "DEEP")
    dumpname=homedir()*"/"*"Zsofia/"*
             "CsillaZsofiaReport-"*
             bunchee*"-"*string(today())*
             ".yaml"
    if isfile(dumpname)
      printstyled(
        """Wanted to generate into "$dumpname" but file exists!"""*
        "\nEnter a valid name (empty erases!).\n",
        color=102,
        bold=true
      )
      printstyled("Filename:  ")
      crt=string(readuntil(stdin, "\n"))
      if !isempty(crt)
        dumpname=crt
      end
    end
    pee=readdir(Silk*bunchee*"/Dailies/", join=true)
    filter!(x -> occursin(".csv", x), pee)
    pee = last(pee, 21)
    tb=DataFrame()
    đ = Dict{String, Any}()
    for p in pee
      tb=CSV.read(
                  p,
                  DataFrame,
                  stringtype=String,
                  truestrings=nothing,
                  falsestrings=nothing
      )
      if :Tckr in propertynames(tb)
        um = unique(string.(Date.(tb.Time)))[1]
        printstyled(
                    "Extracting $um of $bunchee ...\n",
                    color=13,
                    bold=true
        )
        đ[um]=unique(tb.Tckr)
      end
    end
    msg = "Dailies of $bunchee for $( sort(Date.(keys(đ)))[1] ) "*Char(0x2197)*
          " $( sort(Date.(keys(đ)))[end] )\n"
    YAML.write_file(dumpname, đ, msg*"---\n")
    println("The report $dumpname has been generated!")
  end
end

csillamap = Dict(
                 "ADDSOME" => csiadd,
                 "UPDATE" => csiupdate,
                 "VERIFY" => csiverify
            )
"""
**Name**: `maincsilla`  

**NArgs**: (0, 0)

**Summary**: `maincsilla` is a zero-argument function
purposely built to to perform the "CSILLA" task from 
Zsofia MainMenu.
"""
function maincsilla(; rfyl=Rfyl)
  csib=(
        "Artemis","Demeter",
        "Euterpe","Nemesis",
        "Ophelya","Origami",
        "Phorcys","Satoshi"
       )
  csi=("ADDSOME", "UPDATE", "VERIFY")
  while true
    run(`clear`)
    ccsi=chooseme(csi, "CSILLA")
    if isnothing(ccsi)
      break
    elseif !iszero(ccsi)
      cbunch=chooseme(csib, "CSILLA::"*csi[ccsi])
      if isnothing(cbunch)
        nothing
      elseif !iszero(cbunch)
        csillamap[csi[ccsi]](csib[cbunch])
      else
        nothing
      end
    else
      nothing
    end
    readuntil(stdin, "\n")
  end
end
