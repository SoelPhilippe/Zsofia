# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      save_unnur()
  
  # Internal Facility
  Save the current state of glob `UNNUR`.
  """
  function save_unnur()
    @assert isdefined(Zsofia, :UNNUR) "badly used, this is an internal facility"
    un = Dict{AbstractString,Any}()
    merge!(un,UNNUR)
    for (f,g) in zip(
                     ("RfBjo", "RfHjo", "RfBry"),
                     ("OuBjo", "OuHjo", "OuBry")
                 )
      un[f] = map(string,UNNUR[f])
      un[g] = map(string,UNNUR[g])
    end
    open(un["paths"]["Self"],"w+") do io
      printtoml(io,un)
    end
    @info "Unnured " * Char(0x2714)
    un = nothing
  end
  
  """
      config_unnur(dbname::AbstractString="Hrafnhildur")
  
  Interactive facility to configure the global constant `UNNUR`.
  """
  function config_unnur!(dbname::AbstractString="Hrafnhildur")
    un = Dict{String,Any}()
    _configurable = Dict{String,DataType}(
          "field" => "typeoffield",
    )
    _checker = collect(keys(_configurable))
    typpyst("\nFill the following fields with appropriate values...\n\n")
    _typed::String = ""
    f::String = ""
    while ~isempty(_checker)
      try
        f = pop!(_checker)
        printstyled(f, ":  ", color=11)
       _typed = readuntil(stdin, "\n")
        un[f] = convert(_configurable[f], _typed)
      catch er
        @warn string(er)
        @info "failure at $(f)"
        continue
      end
    end
    for (k, v) in _configurable
      print(k, " -> ", v, "\n")
    end
    typpyst("\nCan't be reversed, validate? [Y/n]:  ")
    _typed = readuntil(stdin, "\n")
    if isequal("y", lowercase(strip(_typed)))
      if ~isdefined(Main,:UNNUR)
        UNNUR = _init_unnur(dbname)
      end
      merge!(UNNUR, un)
      open(UNNUR["Self"], "w") do io
        printtoml(io, UNNUR)
      end
    end
    UNNUR
  end
end