# This script is part of Zsofia. Soel Philippe © 2025

begin
  """
      _db_er(x::AbstractString...; name::AbstractString="Hrafnildur")
  
  # Internal Facility
  Return the path to `x...` going from data base entry point `name`.
  
  # Examples
  ```julia
  julia> p1 = _db_er("a","b","c",name="db");
  
  julia> p2 = joinpath(homedir(), ".Zsofia", "db", "a", "b", "c");
  
  julia> isequal(p1, p2)
  true
  ```
  """
  function _db_er(x::AbstractString...; name::AbstractString="Hrafnhildur")
    normpath(joinpath(homedir(), ".Zsofia", "." * name, x...))
  end
  
  """
      _init_zsofia_db([dbname::AbstractString])
  
  # Internal Facility
  Insure the existence of `dbname` on system path tree.
  
  See also [`_init_unnur`](@ref).
  
  # Notes
  When arg `dbname` not specified, defaults to `"Hrafnhildur"`.
  This function is intended to check and generate viceral system tools,
  **THIS IS NOT INTENDED TO BE USED BY THE USER, IRREVERSIBLE SYSTEM FAILURES
  MAY OCCUR**.
  """
  function _init_zsofia_db(dbname::AbstractString="Hrafnhildur")
    @assert ~any(dir -> ~isdir(joinpath(RADIX,dir)),("docs","src","test"))
    @assert ~any(fil -> ~isfile(joinpath(RADIX,fil)),("LICENSE","Project.toml"))
    _u = isdefined(Zsofia, :UNNUR)
    _pu = joinpath(homedir(), ".Zsofia", ".unnur.toml")
    isfile(_pu) ? nothing : (mkpath(dirname(_pu));touch(_pu))
    un = isdefined(Zsofia,:UNNUR) ? UNNUR : parsefile(_pu)
    get!(un,"paths",Dict{AbstractString,Any}())
    get!(un["paths"],"Self",touch(_pu))
    get!(un["paths"],dbname,mkpath(_db_er(name=dbname)))
    get!(un,"local_dbs",Dict{AbstractString,Any}())
    get!(un["local_dbs"],dbname,Dict{AbstractString,Any}())
    for bunch in BUNCH
      get!(un["local_dbs"][dbname],bunch,Dict{AbstractString,Any}())
      mkpath(_db_er(bunch,name=dbname))
    end
    open(un["paths"]["Self"],"w+") do io
      printtoml(io,un)
    end
    nothing
  end
  
  """
      _init_unnur([dbname::AbstractString="Hrafnhildur"])
  
  # Internal Facility
  Initialize and configure the state (of the world) for the global constant
  `UNNUR` according to inferred current session parameters.
  
  # Notes
  This function is at the core of Zsofia. It is intended to consolidate and
  retrieve persistent information at each runtime.
  """
  function _init_unnur(dbname::AbstractString="Hrafnhildur")
    _p = joinpath(homedir(), ".Zsofia", ".unnur.toml")
    un = try
      if isfile(_p)
        parsefile(_p)
      else 
        Dict{AbstractString,Any}()
      end
    catch er
      @warn er
      Dict{AbstractString,Any}()
    end
    isempty(un) ? merge!(un, _init_zsofia_db(dbname)) : nothing
    for bunch in BUNCH
      if isequal(bunch,"Artemis")
        get!(
             un["local_dbs"][dbname],
             bunch,
             Dict{AbstractString,Dict{AbstractString,Vector{AbstractString}}}()
        )
        # Brynhild part, of Artemis
        get!(
             un["local_dbs"][dbname][bunch],
             "Brynhild",
             Dict{AbstractString,Any}()
        )
        ispath(_db_er(bunch,"Brynhild",name=dbname)) ? nothing : continue
        for s in readdir(_db_er(bunch,"Brynhild",name=dbname),join=false)
          un["local_dbs"][dbname][bunch]["Brynhild"][s] = begin
            map(
                x -> replace(x,".csv" => ""),
                filter(
                       endswith(".csv"),
                       readdir(
                               _db_er(
                                      bunch,
                                      "Brynhild",
                                      s,
                                      name=dbname
                               ),
                               join=false
                       )
                )
            )
          end
        end
        # Valtyr part, of Artemis
        get!(
             un["local_dbs"][dbname][bunch],
             "Valtyr",
             Dict{AbstractString,Any}()
        )
        ispath(_db_er(bunch,"Valtyr",name=dbname)) ? nothing : continue
        for s in readdir(_db_er(bunch,"Valtyr",name=dbname), join=false)
          un["local_dbs"][dbname][bunch]["Valtyr"][s] = begin
            map(
                x -> replace(x,".toml" => ""),
                filter(
                       endswith(".toml"),
                       readdir(_db_er(bunch,"Valtyr",s,name=dbname),join=false)
                )
            )
          end
        end
      else
        # Brynhild part, of non-Artemis
        ispath(_db_er(bunch,"Brynhild",name=dbname)) ? nothing : continue
        un["local_dbs"][dbname][bunch]["Brynhild"] = begin
          map(
              x -> replace(x,".csv" => ""),
              filter(
                     endswith(".csv"),
                     readdir(_db_er(bunch,"Brynhild",name=dbname),join=false)
              )
          )
        end
        # Valtyr part, of non-Artemis
        ispath(_db_er(bunch,"Valtyr",name=dbname)) ? nothing : continue
        un["local_dbs"][bunch]["Valtyr"] = begin
          map(
              x -> replace(x,".toml" => ""),
              filter(
                     endswith(".toml"),
                     readdir(_db_er(bunch,"Valtyr",name=dbname),join=false))
          )
        end
      end
    end
    for zc in ("Brynhild","Valtyr","Bjorn")
      un["local_dbs"][dbname]["PitsIn" * zc * "Artemis"] = begin
        isdir(_db_er("Artemis",zc,name=dbname)) ? nothing : continue
        map(readdir(_db_er("Artemis",zc,name=dbname),join=true)) do d
         isdir(d) ? d : nothing 
        end |> (x -> filter(!isnothing,x))
      end
    end
    un["local_dbs"][dbname]["PitsInHjordisArtemis"] = begin
      if isdir(_db_er("Artemis","Hjordis",name=dbname))
        readdir(
                _db_er(
                       "Artemis",
                       "Hjordis",
                       last(
                            readdir(
                                    _db_er(
                                           "Artemis",
                                           "Hjordis",
                                           name=dbname
                                    ),
                                    join=false
                            )
                       ),
                       name=dbname
                ),
                join=true
        ) |> (x -> filter(isdir,x))
      else
        AbstractString[]
      end
    end
    for _sil in ("Brynhild","Bjorn","Hjordis","Valtyr")
      k = "PitsIn" * _sil * "Artemis"
      haskey(un["local_dbs"][dbname],k) ? nothing : continue
      map!(un["local_dbs"][dbname][k],un["local_dbs"][dbname][k]) do str
        string(getindex(str,Colon()(last(findlast("/",str))+1,length(str))))
      end
    end
    get!(un,"Author","Soel Philippe")
    get!(un,"Maintainer", "Soel Philippe")
    get!(un, "FRED_API_KEY", "")
    un["RfBry"] = [
                   "Date","Tckr","Price","Open",
                   "High","Low","Close","Shares"
                  ]
    un["RfHjo"] = [
                   "Date","Tckr","Time","Price",
                   "Open","High","Low","Shares"
                  ]
    un["RfBjo"] = un["RfHjo"]
    un["OuBry"] = [
                   "Timestamp","Close","High","Low",
                   "Open","Adjclose"
                  ]
    un["OuHjo"] = [
                   "Timestamp","Close","High","Low",
                   "Open"
                  ]
    un["OuBjo"] = un["OuHjo"]
    # To be updated whatever ###########################################
    begin
      un["LastLogOn"] = now()
      un["paths"]["Self"] = joinpath(homedir(), ".Zsofia", ".unnur.toml")
      un["paths"]["Valkyrja"] = mkpath(_db_er(".Valkyrja", name=""))
      touch(get!(un["paths"], "Fotografi", _db_er(".fotografi.toml", name="")))
    end
    ###################################################################
    open(un["paths"]["Self"], "w+") do io
      printtoml(io, un)
    end
    for (f,g) in zip(
                     ("RfBjo", "RfHjo", "RfBry"),
                     ("OuBjo", "OuHjo", "OuBry")
                 )
      un[f] = map(Symbol, un[f])
      un[g] = map(Symbol, un[g])
    end
    un
  end
  
  """
      _init_valkyrja(path::AbstractString)
  
  # Internal facility
  Return a `SimpleLogger` pointing to the internal, daily-specific errors
  dumping facility.
  """
  function _init_valkyrja(path::AbstractString)
    open(joinpath(path, "zsolog_" * string(today()) * ".txt"), "a+") |>
    SimpleLogger
  end
  
  """
      _scrawl_db(db::ZsofiaDB)
      
  # Internal Facility
  Update critical fields when some events do occur!
  """
  function _scrawl_db!(db::ZsofiaDB)
    nothing
  end
end