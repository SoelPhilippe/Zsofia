begin
  Fundamentals::Dict{String, Tuple{Vararg{String}}} = begin
    Dict(
    "Artemis" => (
                  "Name", "Sector", "Industry", "Country", "City",
                  "State", "Cap", "OutstandingShares", "FloatingShares",
                  "FirstTrade", "InsidersPctHeld", "InstitutionsPctHeld",
                  "LastDividendDate", "LastDividendRate",
                  "LastDividendValue", "exDate", "Cash", "CashPerShare",
                  "BookValue", "PriceToBook", "RoA", "RoE", "QuickRatio",
                  "DividendYield", "DividendDate", "DividendValue",
                  "ShortRatio", "SharesShort", "SharesShortPctOfFloat",
                  "LastSplitDate", "LastSplitFactor", "Beta", "DebtTotal",
                  "DebtToEquity", "Ebitda", "Employees", "OverAllRisk",
                  "ShareholderRisk", "CompensationRisk", "BoardRisk",
                  "AuditRisk", "LastFiscalYearEnd", "NextFiscalYearEnd"
                ),
    "Demeter" => (
                  "Symbol", "Name", "Underlying", "Exhange", "Expiration",
                  "OpenInterest", "FirstTrade"
                 ),
    "Euterpe" => (
                  "Name", "TimeZone", "Exhange", "QuoteType", "Currency"
                 ),
    "Mercure" => (
                  "Id", "LastUpdated", "SeasAdj", "Frequency", "LastUpdated"
                 ),
    "Nemesis" => (
                  "Symbol", "Name", "Underlying", "Exchange", "Expiration",
                  "OpenInterest", "FirstTrade"
                 )
    "Ophelya" => (
                  "Name", "Symbol", "Category", "FundFamily", "TotalAssets",
                  "Underlying", "Yield", "Exhange", "Beta3Yr", "LegalType",
                  "FirstTrade", "TimeZone"
                 )
    "Origami" => (
                  "Name", "Sector", "Industry", "Country", "City", "Cap",
                  "OutstandingShares", "FloatingShares", "InsidersPctHeld",
                  "InstitutionsPctHeld", "LastDividendDate",
                  "LastDividendRate", "LastDividendValue", "exDate", "Cash",
                  "CashPerShare", "BookValue", "PriceToBook", "RoA", "RoE",
                  "QuickRatio", "DividendYield", "DividendRate",
                  "DividendValue", "LastSplitDate", "LastSplitFactor", "Beta",
                  "DebtTotal", "DebtToEquity", "Ebitda", "Employees",
                  "OverAllRisk", "ShareholderRisk", "CompensationRisk",
                  "BoardRisk", "AuditRisk", "LastFiscalYearEnd",
                  "NextFiscalYearEnd"
                 )
    "Phorcys" => (
                  "Name", "Symbol", "Currency", "Exchange", "QuoteType",
                 )
    "Satoshi" => (
                  "Name", "Symbol", "CirculatingSupply", "Exchange",
                  "TimeZone"
                 )
    )
  end
  """
      show_fundamentals(fi::FiZ)
   
  Display *Fundamentals* of `fi`.
  """
    function show_fundamentals(fi::FiZ)::Nothing
    if ~isfile(joinpath(Silk, fi.land, "Tyyni", fi.tckr * ".toml"))
      @warn "$( fi ) not found in Hrafnhildur."
      return nothing    
    end
    #header>
    begin
      path_toml = joinpath(Silk, fi.land, "Tyyni", fi.tckr * ".toml")
      container = parsefile(path_toml)
      trojanize = intersect(Fundamentals[fi.land], keys(container))
      msg_vectr = Vector{String63}(undef, length(trojanize))
      somfieldz = (
                   "EBITDA", "DEBT", "Out.Sh",
                   "Mkt.Cap", "Floating", "SharesShort",
                   "Cash", "OpenInterest"
                  )
    end
    #main>
    for i in length(trojanize)
      vee = trojanize[i]
      voo = container[vee]
      if isa(voo, Number)
        if occursin("Pct", vee)
          msg_vectr[i] = Zojlz[vee] * ": " * string(round(voo, digits=2))
        elseif in(Zojlz[vee], somfieldz) && ~isempty(voo)
          msg_vectr[i] = Zojlz[vee] * ": " * string(convert(Int64, voo))
        elseif ~isempty(voo)
          msg_vectr[i] = Zojlz[vee] * ": " * string(round(voo, digits=2))
        end
      else
        msg_vectr[i] = Zojlz[vee] * ": " * string(voo)
      end
    end
    printstyled("\n", join(msg_vectr, "\t"), "\n")
  end
end