begin
  """
      Metrics
  
  Required metrics for *bunch*es in **Hrafnhildur**.
  """
  Metrics::Dict{String, Tuple{Vararg{String}}} = begin
    nothing
  end
  
  """
      show_metrics(fi::FiZ)
  
  Show **Metrics** according to *Metrics* fields'.
  """
  function show_metrics(fi::FiZ)::Nothing
    nothing
  end
  
  """
      annikki(fi::FiZ;[ci::Float64],[hist_hz::Union{Int64,String}],[rfyl])
  
  + `fi` the FiZ we want to `annikki`.
  + `ci` the confidence interval to use within any model spitted inside.
  + `hist_hz` history horizon. Either a string or an integer helping to
    determine at which date the history should begin. If it's a string
    object, should be in a format: '7d', '1y', '3mo' for (resp.) 7 days,
    1 year or 3 months.
  + `scenarii` the number of scenarios to be generated.
  + `rfyl` the casual IO dump for errors reporting.

  ## Description
  
  Compute some metrics given into `Metrics` using *Mercedesz*' silo.
  """
  function annikki(
                   fi::FiZ;
                   ci::Float64=0.95,
                   hist_hz::Union{Int64,String}="2y",
                   scenarii::Int64=1000
           )::Union{FiZ, Nothing}
    t0 = now()
    #assert>
    if ~in(fi.tckr, Unnur[fi.land])
      heil("Is not unnured!", fi, context="annikki", loglevel=Warn)
      heil(
           "Is not unnured!",
           fi,
           context="annikki",
           logger=ConsoleLogger,
           loglevel=Warn
      )
      return nothing
    end
    if <=(
          firstbdayofmonth(
                           Vala[fi.vala],
                           tobday(Vala[fi.vala], Foto[fi.land][fi.tckr]["Date"])
                          ),
          get!(Foto[fi.land][fi.tckr], "Annikki", Date(t0) - Month(1))
         )
      heil("Metrics: up-to-date!", fi, context="annikki", logger=ConsoleLogger)
      return nothing
    end
    #header>
    begin
      path_toml = joinpath(Silk, fi.land, "Tyyni", fi.tckr * ".toml")
      path_csv  = joinpath(Silk, fi.land, fi.tckr * ".csv")
      kalenteri = Vala[fi.vala]
      date_foto = Foto[fi.land][fi.tckr]["Date"]
      last_annk = get!(Foto[fi.land][fi.tckr], "Annikki", Date(t0) - Month(1))
      ##------ strat parameters ------->
      inception = firstbdayofmonth(kalenteri, tobday(kalenteri, date_foto))
      maturityd = lastbdayofmonth(kalenteri, inception)
    end
    #body>
    ##------------------------------->
    begin
      days_of_concern = listbdays(kalenteri, inception, maturityd)
      n = bdayscount(
                     kalenteri,
                     firstdayofyear(inception),
                     lastdayofyear(maturityd)
                    )
      d   = Dict{String,Any}()  ##<- annikki container
      pr(msg,kol=102)=heil(
                           msg,
                           fi=fi,
                           iam="annikki",
                           kol=kol,
                           bud=true, 
                           rfyl=stdout
                      )
    end
    
    #valids>
    ##---------- MSG_FUN
    
    
    ##---- Fixing History Init.
    hz::Date = let
      if isa(hist_hz,String)
        a = uppercase(hist_hz)
        n = parse(Int64,match(r"\d+",a).match)
        m = match(r"[A-Z]+",a).match
        if ~in(m,("D","W","Y","MO")) || ~isless(0,n)
          printstyled("[hist_hz] → Incorrect Syntax!\nExiting...\n",color=102)
          return nothing
        elseif in(m,("D",))
          tobday(kt,firstdayofyear(-(d_0,Day(n))))
        elseif in(m,("W",))
          tobday(kt,firstdayofyear(-(d_0,Week(n))))
        elseif in(m,("Y",))
          tobday(kt,firstdayofyear(-(d_0,Year(n))))
        elseif in(m,("MO",))
          tobday(kt,firstdayofyear(-(d_0,Month(n))))
        end
      else
        if ~isless(0,hist_hz)
          let me0 = string(Char(0x276c),fi.land,"|",fi.tckr,Char(0x276d))
            me1 = string("Annikki → ",me0)
            printstyled(me," Invalid history size...!\nExiting...\n",color=102)
            return nothing
          end
        else
          tobday(kt,firstdayofyear(-(d_0,Day(hist_hz))))
        end
      end
    end
    ##
    ##
    pr(string("historized from ",hz,"..."))
    ##
    ## Determining the `skipto` value.
    ##
    begin
      l0 = bdayscount(kt,hz,d_0)
      n0 = -(countlines(f0),l0)+1
    end
    ##
    ## data, the asset.
    ##
    begin
      data::DataFrame = let
        if >=(n0,2)
          CSV.read(
                   f0,
                   DataFrame,
                   stringtype=String,
                   truestrings=nothing,
                   falsestrings=nothing,
                   skipto=n0
              )
        elseif >=(n0,24-l0)
          CSV.read(
                   f0,
                   DataFrame,
                   stringtype=String,
                   truestrings=nothing,
                   falsestrings=nothing
              )
        else
          pr("INSUFFICIENT HISTORY!",11)
          printstyled("Quitting...\n",color=11)
          return fi
        end
      end
    end
    ##
    ## Getting ticker size (ticker_size) and initial price (price0).
    ##
    begin
      ticker_size::Float64 = let
        a = tickersize(last(data.Price,30))
        iszero(a) ? one(Float64) : a
      end
      price0 = last(data.Price)
    end
    
    #main>
    pr("fitting a T-GARCH the overnight returns...")
    # TGARCH of Close-to-Close ##############################################
    kali_rvn = fit(TGARCH{1,1,1},0.01tb.Rvn,meanspec=ARMA{1,1},dist=StdGED) #
    #########################################################################
    pr("TGARCH fitted!")
    ##--> Simulation & Co.
    begin
      pr(string("simulating ",scenarii," scenarios..."))
      pr("Building containers...")

      Ys_ = Matrix{Rational}(undef,size(t_s,1),scenarii)
      pr(string("\nYields → built: ",Char(0x2714)))
      Vs_ = Matrix{Float64}(undef,size(t_s,1),scenarii)
      pr(string("\nVolas → built: ",Char(0x2714)))
      Rs_ = Matrix{Float64}(undef,size(t_s,1),scenarii)
      pr(string("\nmeanReturns → built: ",Char(0x2714)))

      pr("Containers built!")

      for j in range(1,scenarii)
        pr("processing scenario", j," ...\n")
        ##
        szimula = simulate(kali_rvn,size(t_s,1))
        ## x -> simulations, v -> volas, r -> VaR(ci)
        x = map(x -> rationalize(round(x,digits=4)),getfield(szimula,:data))
        v = map(x -> rationalize(round(x,digits=4)),volatilities(szimula))
        r = map(x -> rationalize(round(x,digits=4)),VaRs(szimula,ci))
        ##
        setindex!(Ys_,x,:,j)
        pr(string("Building blocks ",j, " of Ys: OK!"))
        setindex!(Vs_,v,:,j)
        pr(string("Building blocks ",j, " of Vs OK!"))
        setindex!(Rs_,r,:,j)
        pr(string("Building blocks ",j, " of Rs OK!"))
      end
      ##
      pr(string(scenarii," scenarios simulated → ",Char(0x2714)))
      ##--> ditching unlikely simulations
      pr(string("filtering the ",round(100ci,digits=4),"%-level frontier..."))
      loys = let
        a = predict(kali_rvn,:VaR,1,level=+(0.5,0.5ci))
        round(a,digits=4)
      end
      upys = let
        a = predict(kali_rvn,:VaR,1,level=-(0.5,0.5ci))
        round(a,digits=4)
      end
      pr(string("\nLower bound: ",loys,"\n","Upper bound: ",upys))
      idx_lik::Vector{Int64} = begin
        findall(x -> (loys <= x <= upys),view(Ys_,1,:))
      end
      if iszero(size(idx_lik,1))
        pr("trouble!",11)
        pr("likelihood paths space seems empty!\nQuitting...")
        return fi
      end
      pr("extracting inbounds paths...")
      ##-->
      Ys::Matrix{Float64} = let
        map(x -> round(x,digits=4),view(Ys_,:,idx_lik))
      end
      Vs::Matrix{Float64} = let
        map(x -> round(sqrt(b_y)*x,digits=4),view(Vs_,:,idx_lik))
      end
      Rs::Matrix{Float64} = let
        map(x -> round(x,digits=4),view(Rs_,:,idx_lik))
      end
      ##-->
      pr("extracted!\nZsimula consolidated!")
      pr("constructing paths...")
      map!(exp,Ys,Ys)
      cumprod!(Ys,Ys,dims=1)
      pr("paths constructed!")
      ##  Ks -> Prices
      Ks::Matrix{Float64} = begin
        *(ticker_size,round.(/(price0,ticker_size)*Ys,digits=0))
      end
    end
    ##
    ##--> Historical Metrics & Co.
    return nothing
  end
end