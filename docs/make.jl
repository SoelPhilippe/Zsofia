# This script is part of Zsofia. Soel Philippe © 2025

begin
  using Documenter
  using Zsofia
end

begin
  makedocs(
           sitename = "Zsofia",
           modules = [Zsofia],
           pages = [
                    "Home" => "index.md",
           #         "API Reference" => "api.md",
                    "Testing guidance" => "testing.md"
                   ],
           checkdocs = :none,
           #repo = "https://codeberg.org/SoelPhilippe/Zsofia.jl",
           remotes=nothing
  )

  deploydocs(
             repo = "https://codeberg.org/SoelPhilippe/Zsofia.jl.git",
             make = "make.jl"
  )
end
