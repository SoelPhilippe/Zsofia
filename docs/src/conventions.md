# Conventions

In this section, we consign conventions we adopt throughout
the package.

## Observables

### InterestRate

Prefer continous compounding. Whenever possible, and for
the purpose of computations, interest rates levels
are transformed into their continous-compounding counterpart
levels before any computation.

# Methods, Algos, Implementation

## Bootstrapping

The bootstrapping process starts with market data for
overnight indexed swaps. These rates are used to derive
discount factors for various maturities, which are then
used to discount the cash flows of financial instruments.

To build the OIS curve, we first feed the *method* with
market data for overnight indexed swaps across different
maturities. This data is then used to calculate discount
factors for each maturity point. The discount factors are
derived by solving for the rate that equates the present
value of the swap's cash flows to its market price.

In the context of dual curve bootstrapping, an OIS curve
is built separately from a forward curve, such as a LIBOR
curve. The OIS curve is used for discounting cash flows,
while the forward curve is used to determine the floating
leg of interest rate swaps. This dual approach ensures that
the valuation of swaps is more accurate and reflects the true
cost of funding and the risk-free rate.

$$
P^{\text{OIS}}(t,T_n)=\frac{P^{\text{OIS}}(t,T_n)\cdot P^{\text{IBOR}}(t,T_{n-1})}{\text{Quote}_n\sum_{i=1}^{m}{P^{\text{OIS}}(t,T_i)} + P^{\text{OIS}}(t,T_n)-\sum_{\ell=1}^{n-1}\left(\frac{P^{\text{IBOR}}(t,T_{\ell-1})}{P^{\text{IBOR}}(t,T_{\ell})}-1\right)P^{\text{OIS}}(t,T_\ell)}
$$