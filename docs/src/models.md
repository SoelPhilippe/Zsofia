# Equity Models

## Black-Scholes

The Black-Scholes model assumes the underlying stochastic
process $(x_t)_{t\geq 0}$ to follow a geometric brownian
motion, i.e. there exist $\mu\in\mathbb{R}$ and $\sigma>0$
such that $(x_t)_{t\geq 0}$ is subordinated to the following
dynamic, where we denote $(W_t)_{t\geq 0}$ to be the wiener
process:

$$dx_t = \mu x_t dt + \sigma x_t dW_t$$

Over the fixed interval $(t_{i-1},t_i]$ if we denote
$\alpha = \mu - \frac{\sigma^2}{2}$ and $\tau_i = t_{i} - t_{i-1}$, knowing
$x_{t_{i-1}}$ the process solves at $t_i$ to the following:
$$x_{t_i} = x_{t_{i-1}}\exp\left(\alpha\tau_i + \sigma W_{\tau_i}\right)$$

### Parameters Estimation
Having observed $(x_{t_i})_{1\leq i \leq n}$ and assuming the Black-Scholes
(Geometric Brownian Motion) dynamic, one can estimate the parameters using
some statistical techniques. Amongst the most popular are method of moments
and maximum likelihood estimation.

Note that $r_{t_i}\coloneqq \log\left(\frac{x_{t_i}}{x_{t_{i-1}}}\right)$
is normally distributed since
$r_{t_i}=\alpha\tau_i+\sigma W_{\tau_i}$. Which is distributed
the same as $\alpha\tau_i + \sigma\sqrt{\tau_i} \varepsilon_i$
where we denote $\varepsilon_i\sim\mathcal{N}(0,1)$.

#### Method of Moments
In presence of large $n$ (as a rule of thumb, $n\geq 30$),
one can use the method of moments under the influence of
Law of Large Numbers typically replacing the mean and
variance by their empirical estimators and then propose
as parameters estimators the corresponding solved parameters.

$r_{t_i} = \alpha\tau_i + \sigma\sqrt{\tau_i} \varepsilon_i$,
then $\mathbb{E}[r] = \alpha \tau$ and
$\mathbb{V}[r]=\sigma^2 \tau$. Remark that constructing
the variable $r$ leads to data set size recduction from $n$
to $m = n-1$ and then:
$$\varepsilon_i = \frac{r_{t_i}-\alpha\tau_i}{\sigma\sqrt{\tau_i}}=\frac{1}{\sigma}\frac{r_{t_i}}{\sqrt{\tau_i}}-\frac{\alpha}{\sigma}\sqrt{\tau_i}$$
should be following the standard normal distribution.
Define $y_i=\frac{r_{t_i}}{\sqrt{\tau_i}}$
