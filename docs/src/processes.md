# Processes

I herein consign anything relevant about processes implemented in
**Zsofia**.

## Bernoulli process

### Glance

The **Bernoulli process** is defined as the sequence $(x_n)_{n\in\mathbb{N}}$
of independent random variables such that there is $p\in[0,1]$ such that:
- for each $i\in\mathbb{N}$, $x_i\in\{0,1\}$;
- for all $i\in\mathbb{N}$, the probability that $x_i=1$ is $p$.

### Parameters estimation

The **Bernoulli process** depends only on one parameter $p$ which is the
probability of observing $x_n=1$ for any $n$. If one has in hand the
sequence $(x_i)_{1\leq i\leq n}$ then one might estimate $p$ by denoting
$f$ the probability distribution (mass) function of the underlying generic
random variable $X$ having the identical distribution as of $x_i$ for any
$i\in\mathbb{N}$. Therefore:

$$f(x_i) = p^{x_i}(1-p)^{1-x_i}$$

The likelihood of observing the sequence $(x_1, ... , x_n)$ is therefore:

$$\mathcal{L}(p\mid \underline{x}_n)=\prod_{i=1}^{n}{p^{x_i}(1-p)^{1-x_i}}=p^{s_n}(1-p)^{n - s_n}$$

where we denoted $s_n = \sum_{i}{x_i}$. One might estimate $p$ using the
maximum likelihood astimator by arguing that:

$$\hat{p}_n = \text{arg}\max_{p\in[0,1]}{\mathcal{L}(p\mid \underline{x}_n)}=\frac{s_n}{n}=\bar{x}_n$$


## Ornstein-Uhlenbeck process

### Glance

The **Ornstein-Uhlenbeck process** is a continuous-time process
$(x_t)_{t\geq 0}$ which dynamic is, given the parameters
$(\theta, \sigma)\in (0,\infty)\times (0,\infty)$, by:

$$dx_t = -\theta x_t dt + \sigma dW_t$$

The process solves, given the starting point $t_0$, for $t > t_0$ in:

$$x_t = x_{t_0}e^{-\theta (t-t_0)}+\sigma\int_{t_0}^{t}{e^{-\theta (t-u)}dW_u}$$

Which shows that the process is a gaussian process with the following
mean and variance:

$$\begin{aligned}\mathbb{E}\left[x_t\mid\mathcal{F}_{t_0}\right] &= x_{t_0}e^{-\theta (t-t_0)}\\ \mathbb{V}\left[x_t\mid\mathcal{F}_{t_0}\right] &=\frac{\sigma^2}{2\theta}\left[1-e^{-2\theta (t-t_0)}\right]\end{aligned}$$

For simulation purposes, we subdivide
$(t_0,t]=(t_0,t_1]\cup ... \cup (t_{n-1},t]$ and write the increment on 
any given subinterval as:

$$x_{t_i} = x_{t_{i-1}}e^{-\theta (t_i-t_{i-1})}+\sigma\int_{t_{i-1}}^{t_i}{e^{-\theta(t_i-u)}dW_u}$$

We then write:

$$x_{t_i} = x_{t_{i-1}}e^{-\theta (t_i-t_{i-1})}+\sqrt{\frac{\sigma^2}{2\theta}\left[1-e^{-2\theta (t_i-t_{i-1})}\right]}\mathcal{N}(0,1)$$
