# Tests Standards

I herein consign the Zsofia's tests building standard.

The tests are enclosed into nested `testset` facility from
**Julia**'s unit testing facility.

The global title of tests has the following canva:
"*NameOfUnit*-space-unit testing (Zsofia)".

When building tests, privilege building set-ups, headers first then
test the facility afterward. If more relevant, gradually modify objects
according to usual use cases schemes.

## Instruments

When building tests for instruments, the following tests (sets)
are mandatory:

- **Integrity and Consistency**  
  This section gathers the following tests:
  - Types hierarchy tests.
  - Fields/properties checking tests
  - Constructors methods checking.
  - Interaction with other objects at type level.
  - Any other relevant tests.
- **Operability**  
  This section gathers the following tests:
  - Operations with any method modifying the struct state.
  - Operations with any available model/object interacting meaned to interact.
  - Any other relevant operation.
- **Erroring**  
  This section should test failures/error throwers.

## Models

When building test for models, the following tests (sets) are mandatory:

- **Integrity and Consistency**  
  This section gathers the following tests:
  - Types hierarchy tests.
  - Fields/properties checking tests.
  - Constructors methods checking.
  - Interaction with other objects/instruments at type level.
- **Operability and Fitting**  
  This section gathers the following tests:
  - Interact with some of the objects (be general as much as possible) intended.
  - Test building and fitting some objects.
  - Tests special cases if any.
- **Erroring**
  This section should tests failures/errors, exhibit pitfalls and threat
  special cases. 
