using Zsofia

# simulate a geometric brownian motion over 1month
let
  ccy = Currency("USD")
  rf  = InterestRate(0.050, currency=ccy)
  bsm = BlackScholes(rf)
  aal = Stock("AAL")
  par = (0.08570, 0.69850)
  
  simuli = begin
    simulate(
             bsm, par, Date(2024,12,10), Date(2025,1,10),
             Day(1), n=10, nsteps=360, x0=72.50, rn=false,
             dcc=Act360()
    )
  end
end

# simulate a geometric brownian motion over [0, 1]
let
  ccy = Currency("EUR")
  rf  = InterestRate(0.050, currency=ccy)
  bsm = BlackScholes(rf)
  aal = Stock("AAL")
  par = (0.08570, 0.69850) 
  
  simuli = begin
    simulate(
              bsm, aal, par, 0.0, 1.0, 1.0/252, n=10, nsteps=360,
              x0=72.50, rn=false, dcc=Act360()
    )
  end
end


# Fetch some intraday data within the HJORDIS context
let
  futs = begin
    (
      Futures(5825.50,today()+Month(3),Stock("S&P500")),
      Futures(19125.75,today()+Month(3),Stock("NASDAQ100")),
      Futures(2055.7,today()+Month(3),Stock("RUSSELL2000"))
    )
  end
  dfs = Dict{String,DataFrame}()
  for (f, tckr) in zip(futs, ("ES", "NQ", "MES"))
    setparams!(f, "ticker", tckr, force=true)
    setparams!(f, "calendar", UnitedStatesEx(), force=true)
  end
  for f in futs
    dfs[getparams(f,"ticker")] = begin
      fetch(
            f, HJORDIS,
            tobday(getparams(f,"calendar"), today()-Day(1), forward=false),
            force=true
      )
    end
  end
end

let
  obs = Futures(5825.50,today()+Month(3),Stock("S&P500"))
  setparams!(obs, "ticker", "ES", force=true)
  setparams!(obs, "calendar", UnitedStatesEx(), force=true)
  df=fetch(obs, HJORDIS, today()-Day(1), force=true)
end

let
  pltr = Stock("PLTR")
  cal = setparams!(pltr, "calendar", UnitedStatesEx()).second
  df = fetch(
             pltr, HJORDIS, tobday(cal,today()-Day(1), forward=false),
             interval="1m", localdb=HRAFNHILDUR, return_data=true
       )
end


let
  v = ["TELIA","ALFA","ABB","EVO","SINCH","SAND","AZN","BOL"]
  stocks = Stock[]
  for tckr in v
    push!(stocks, Stock(tckr))
    setparams!(stocks[end], "calendar", SwedenEx())
  end
  for stock in stocks
    fetch(stock, BRYNHILD, force=true)
  end #----> tested, works fine.
end

begin
  # instantiate a Stock (eg, PALANTIR)
  pltr = Stock("PLTR")
  
  # set the calendar to United States ...
  setparams!(pltr, "calendar", UnitedStatesEx())
  
  # instantiate an american put on PALANTIR,
  # struck at 90 and maturing on Valentines Day
  opt = begin
    AmericanOption(
                   90, Date(2025,2,14), option=Put,
                   underlying=pltr, inception=Date(2025,1,15)
    )
  end
  
  # don't forget to set the calendar for the option...
  setparams!(opt, "calendar", UnitedStatesEx())
  
  # set a nice and ergonomic ticker name for your option...
  setparams!(opt, "ticker", "90-Feb14-PLTR", force=true)
  
  # That's all, now fetch daily prices!
  df = fetch(opt, BRYNHILD)
end


begin
  bunchie = open(joinpath(homedir(),"Downloads","stoo.txt"),"r+") do io
    readlines(io)
  end
  stocks::Vector{Stock} = collect(Stock(j) for j in bunchie)
  cal = UnitedStatesEx()
  dates = listbdays(cal,toprev(today(),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for stock in stocks
    try
      setparams!(stock, "calendar", cal)
      dfs[getparams(stock,"ticker")] = begin
        fetch(
              stock,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(stock,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(stock,"ticker") * " " * Char(0x2718)
    end
  end
end

# T stocks
begin
  bunchie=collect(keys(fotografi(HRAFNHILDUR,BRYNHILD)["Artemis"]["T"]))
  stocks::Vector{Stock} = collect(Stock(j) for j in bunchie)
  cal = JapanEx()
  dates = listbdays(cal,toprev(today()-Week(1),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for stock in stocks
    try
      setparams!(stock, "calendar", cal)
      dfs[getparams(stock,"ticker")] = begin
        fetch(
              stock,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(stock,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(stock,"ticker") * " " * Char(0x2718)
    end
  end
end

# Futures NEM
begin
  bunchie = collect(keys(Zsofia.FOTO["Nemesis"][BJORN]))
  futs::Vector{Futures} = map(bunchie) do tckr
    Futures(rand(1000:2500),lastdayofquarter(today()),Stock(tckr))
  end
  for f in futs
    setparams!(f,"ticker",getparams(f.underlying,"ticker"),force=true)
  end
  cal = UnitedStatesEx()
  dates = listbdays(cal,toprev(today()-Week(1),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for fut in futs
    try
      setparams!(fut,"calendar",cal)
      dfs[getparams(fut,"ticker")] = begin
        fetch(
              fut,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(fut,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(fut,"ticker") * " " * Char(0x2718)
    end
  end
end

# Futures DEM
begin
  bunchie = collect(keys(Zsofia.FOTO["Demeter"][BJORN]))
  futs::Vector{Futures} = map(bunchie) do tckr
    Futures(rand(1000:2500),lastdayofquarter(today()),Commodity(tckr))
  end
  for f in futs
    setparams!(f,"ticker",getparams(f.underlying,"ticker"),force=true)
  end
  cal = UnitedStatesEx()
  dates = listbdays(cal,toprev(today()-Week(1),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for fut in futs
    try
      setparams!(fut,"calendar",cal)
      dfs[getparams(fut,"ticker")] = begin
        fetch(
              fut,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(fut,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(fut,"ticker") * " " * Char(0x2718)
    end
  end
end


# ETFs
begin
  bunchie = collect(keys(fotografi(HRAFNHILDUR,BJORN)["Ophelya"]))
  etfs::Vector{ETF} = map(bunchie) do tckr
    ETF(tckr)
  end
  cal = UnitedStatesEx()
  dates = listbdays(cal,toprev(today()-Week(1),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for etf in etfs
    try
      setparams!(etf,"calendar",cal)
      dfs[getparams(etf,"ticker")] = begin
        fetch(
              etf,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(etf,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(etf,"ticker") * " " * Char(0x2718)
    end
  end
end

# FX
begin
  bunchie = collect(keys(fotografi(HRAFNHILDUR,BJORN)["Phorcys"]))
  dccy = Currency("USD")
  ccys::Vector{FXRate} = map(bunchie) do tckr
    FXRate(Currency(tckr),dccy)
  end
  for ccy in ccys
    setparams!(ccy,"ticker",ccy.foreign.ticker *"_"*ccy.domestic.ticker,force=true)
  end
  cal = UnitedStatesEx()
  dates = listbdays(cal,toprev(today()-Week(1),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for fx in ccys
    try
      setparams!(fx,"calendar",cal)
      dfs[fx.foreign.ticker] = begin
        fetch(
              fx,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(fx,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(fx,"ticker") * " " * Char(0x2718)
    end
  end
end

# Crypto
begin
  bunchie = collect(keys(fotografi(HRAFNHILDUR,BJORN)["Satoshi"]))
  dccy = Currency("USD")
  cryptos::Vector{CryptoCurrency} = map(bunchie) do tckr
    CryptoCurrency(tckr,dccy)
  end
  cal = UnitedStatesEx()
  dates = listbdays(cal,toprev(today()-Week(1),Mon),today()-Day(1))
  dfs = Dict{AbstractString,Any}()
  @threads for crypto in cryptos
    try
      setparams!(crypto,"calendar",cal)
      dfs[getparams(crypto,"ticker")] = begin
        fetch(
              crypto,HJORDIS,dates,interval="1m",
              force=true,localdb=HRAFNHILDUR
        )
      end
      printstyled(getparams(crypto,"ticker")," -> ", Char(0x2714),"\n")
    catch er
      @warn getparams(crypto,"ticker") * " " * Char(0x2718)
    end
  end
end


begin
  futs = collect(
    (
     f=Futures(rand(1:10),Date(2025,3,21),Stock(tckr));
     setparams!(f,"ticker",tckr,force=true);
     setparams!(f,"calendar",Zsofia.US);
     f
    ) for tckr in keys(Zsofia.FOTO["Nemesis"][HJORDIS])
  )
end


let
  pltr = Stock("PLTR")
  strk, expy = 60.0, Date(2025,1,31)
  ccy, dcc = Currency("USD"), Thirty360()
  bs = BlackScholes(0.041037, currency=ccy)
  inc = Date(2025,1,3)
  op=EuropeanOption(strk,expy,option=Put,underlying=pltr,inception=inc)
  insertprices!(pltr, 79.89, inc)
  setparams!(pltr,"sigma",0.6327,force=true)

  mc = MonteCarlo(bs)
  
  v = Vector{Float64}(undef,100);
  
  for i in range(1,length(v))
    v[i] = evaluate(mc, op, n=250i, dcc=dcc)
  end
end

let  
  r = InterestRate(Tenor("3m"), currency=Currency("USD"))
  inc, xp = Date(2024,1,3), Date(2027,1,3)
  swap1 = Swap(0.05, xp, r,inception=inc)
  swap2 = Swap(0.05, Tenor("3y"), r, inc)
  isequal(swap1.fixingsfix, swap2.fixingsfix)
end

let
  # Environment
  ccy = Currency("USD")
  rfr = InterestRate(0.04360, currency=ccy)
  bs  = BlackScholes(rfr)
  mc2 = MonteCarlo(bs,bs)
  dcc = Thirty360()
  inc = Date(2025,3,1)
  _cr = 0.80
  mat = Date(2025,3,21)
  strike = 80.0
  
  # Citigroup vs Wells Fargo
  citi, wells = Stock("C"), Stock("WFC")
  
  setparams!(ciri, "calendar", UnitedStatesEx(), force=true)
  setparams!(citi, "sigma", 0.2390, force=true)
  setparams!(wells, "carryrate", 0.028, force=true)
  insertprices!(citi, 79.95, inc)
  setparams!(wells, "calendar", UnitedStatesEx(), force=true)
  setparams!(wells, "sigma", 0.2983, force=true)
  setparams!(wells, "carryrate", 0.0204, force=true)
  insertprices!(wells, 78.32, inc)
  
  citi80 = (
            EuropeanOption(
                           strike, mat, option=Call,
                           underlying=citi, inception=inc
            ),
            EuropeanOption(
                           strike, mat, option=Put,
                           underlying=citi, inception=inc
            )
          )
  wells80 = (
             EuropeanOption(
                            strike, mat, option=Call,
                            underlying=wells, inception=inc
             ),
             EuropeanOption(
                            strike, mat, option=Put,
                            underlying=wells, inception=inc
             )
            )
  spredo = SpreadOption(mat, wells, citi, option=Call, inception=inc)
end

let
  s0    = 79.89
  pltr  = Stock("PLTR")
  ccy   = Currency("USD")
  rfr   = InterestRate(0.041037, currency=ccy)
  bs    = BlackScholes(rfr)
  mcbs  = MonteCarlo(bs)
  dcc   = Thirty360()
  start = Date(2025,1,3)
  expiry= Date(2025,1,31)
  strike= 60.0
  σ     = 0.6327
  
  setparams!(pltr, "calendar", UnitedStatesEx(), force=true)
  setparams!(pltr, "sigma", σ, force=true)
  insertprices!(pltr, s0, start)

  bp = BinaryOption(strike, expiry, option=Put, underlying=pltr, inception=start)
  ep = EuropeanOption(strike, expiry, option=Put, underlying=pltr, inception=start)
  
  evaluate(bs, ep, dcc=dcc)
  
  goog = Stock("GOOG")
  setparams!(goog, "sigma", 0.47, force=true)
  insertprices!(goog, 80.25, start)
  
  mc2 = MonteCarlo(bs,bs)
  corr = 0.67
  
  
  so = SpreadOption(expiry, goog, pltr, inception=start)
  
  ns = collect(1000k for k in 1:250)
  vs = collect(evaluate(mc2,so,n=k,dcc=dcc, corr=corr) for k in ns)
  
  marg = Margrabe()
  
  marg_price_so = evaluate(marg, so, corr=corr, dcc=dcc)
end

let
  t0 = Date(2025,3,3)
  m = Vasicek()
  zb = ZeroBond(
               Tenor("6m"),
               t0,
               currency=Currency("USD")
       )
  setparams!(zb, "ticker", "ZCB6M", force=true)
  par = (0.023,0.045,0.3987)
  for (k,v) in zip(("θ","μ","σ",),par)
    setparams!(zb, k, v, force=true)
  end
  sim = simulate(
                 Vasicek(), par, t0, getfield(zb,:maturity),
                 Day(1), x0=0.0435, n=3, dcc=Thirty360()
        )
  op = EuropeanOption(
                      0.95,Date(2025,3,31),option=Call,
                      underlying=zb,inception=t0
        )
end

let
  tickers = getoutdated(Stock, HJORDIS, Zsofia.FR)
  stocks = Vector{Stock}(undef, length(tickers))
  for i in range(1,length(tickers))
    stocks[i] = Stock(tickers[i])
    setparams!(stocks[i],"calendar",Zsofia.FR)
  end
  fr = listbdays(Zsofia.FR, Date(2025,2,3),Date(2025,3,2))
  update(stocks,BRYNHILD)
end


let
  ccy = Currency("USD")
  irs = InterestRate[]
  for str in ("1m","2m","3m")
    push!(irs, InterestRate(Tenor(str), currency=ccy))
  end
  yc1 = YieldCurve(irs, title="YC-one")
  yc2 = YieldCurve(irs..., title="YC-two")
  
  @assert isequal(getfield(yc1,:grip),getfield(yc2,:grip))
  
  t0 = today()
  
  for ir in yc1
    insertprices!(ir, 0.045 + 0.01*rand(), t0)
  end
end
