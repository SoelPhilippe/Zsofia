# Artemis
let bunch="Origami"
  bry_pit = joinpath(homedir(),"Zsofia",".Hrafnhildur",bunch)
  hjo_pit = joinpath(homedir(),"Zsofia",".Hrafnhildur",bunch,"Dailies")
  tickers = readdir(bry_pit,join=false)
  filter!(endswith(".csv"),tickers)
  map!(s -> replace(s,".csv" => ""),tickers,tickers)
  printstyled("Summary\nTickers:$(length(tickers))\nContinue?",color=11)
  readuntil(stdin,"\n")
  stocks = Vector{Stock}(undef,length(tickers))
  for i in 1:length(tickers)
    try
      stocks[i] = Stock(replace(tickers[i],".T" => ""))
      setparams!(stocks[i],"calendar",JapanEx())
    catch er
      printstyled(er,color=9)
      printstyled(tickers[i],Char(0x2718))
    end
  end
  printstyled("Summary[Observables]\nTickers:$(length(stocks))\nContinue?",color=11)
  readuntil(stdin,"\n")
  # brynhild part
  printstyled("\nBuilding Brynhild...\n",color=4)
  @threads for i in 1:length(stocks)
    df = read_csv(
                  joinpath(bry_pit,tickers[i] * ".csv"),
                  DataFrame,
                  stringtype=String,
                  truestrings=nothing,
                  falsestrings=nothing
         )
    map!(s -> replace(s,".T" => ""),df.Tckr,df.Tckr)
    renamedf!(df, :Date => :Timestamp)
    renamedf!(df, :Price => :Close)
    transform!(df, :Close => ByRow(identity) => :Adjclose)
    renamedf!(df, :Vol => :Volume)
    printstyled(tickers[i]," load from db: ", Char(0x2714),"\n",color=4)
    buildfeatures!(stocks[i],BRYNHILD,df)
    printstyled(getfield(df,:colindex).names,"\n",color=10)
    printstyled(tickers[i]," features: ",Char(0x2714),"\n",color=4)
    write(HRAFNHILDUR, stocks[i], df, BRYNHILD)
    printstyled(tickers[i]," written out ",Char(0x2714),color=4)
  end
  dbs_hjo = readdir(hjo_pit, join=true)
  filter!(endswith(".csv"), dbs_hjo)
  printstyled("BJORN: processing Hjordis...\n",color=4)
  @threads for p in dbs_hjo
    try
      printstyled("processing ", p, "...", color=4)
      df = read_csv(
                    p,DataFrame,stringtype=String,
                    truestrings=nothing,falsestrings=nothing
           )
      map!(s -> replace(s,".T" => ""),df.Tckr,df.Tckr)
      if in(:Time,getfield(df,:colindex).names) && isa(df.Time,Vector{String})
        df.Time .= map(x -> replace(x," " => "T"), df.Time)
      end
      df.Tckr .= map(x -> replace(x,"=F" => ""), df.Tckr)
      renamedf!(df, :Time => :Timestamp)
      renamedf!(df, :Price => :Close)
      renamedf!(df, :Vol => :Volume)
      printstyled(getfield(df,:colindex).names,color=4)
      buildfeatures!(stocks[end],BJORN,df)
      printstyled(getfield(df,:colindex).names,color=10)
      write(HRAFNHILDUR,stocks[end],df,BJORN)
    catch er
      printstyled(string(er),color=9)
    end
  end
end
