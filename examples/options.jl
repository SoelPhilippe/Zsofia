# This script is part of Zsofia. Soel Philippe © 2025

#> european options
let
  pltr = Stock("PLTR")
  setparams!(pltr, "calendar", UnitedStatesEx())
  ccy   = Currency("USD")
  model = BlackScholes(0.041037, currency=ccy)
  dcc   = Thirty360()
  start = Date(2025,1,3)
  expiry= Date(2025,1,27)
  strike= 60.0
  sigma = 0.6327
  
  setparams!(pltr, "sigma", sigma, force=true)
  
  op = EuropeanOption(
                      strike, expiry, option=Put,
                      underlying=pltr, inception=start
       )
  
  evaluate(model, op, sigma=getparams(pltr, "sigma"), dcc=dcc)
end